package com.john.bluetoothprinter.helper;

import com.intlpos.sirclepos.R;

public class PrinterCommands {
public static final byte[] INIT = {27, 64};
public static byte[] FEED_LINE = {10};

public static byte[] SELECT_FONT_A = {27, 33, 0};

public static byte[] SET_BAR_CODE_HEIGHT = {29, 104, 100};
public static byte[] PRINT_BAR_CODE_1 = {29, 107, 2};
public static byte[] PRINT_IMAGE = {0x1B, 0x4A, 0x18, 
	0x1D, 0x76, 0x30, 0x00, 0x10, 0x00, (byte) 0x80, 0x00,};
public static byte[] SEND_NULL_BYTE = {0x00};

public static byte[] SELECT_PRINT_SHEET = {0x1B, 0x63, 0x30, 0x02};
public static byte[] STARFEED_PAPER_AND_CUT = {0x1B, 0x64, 50};
public static byte[] FEED_PAPER_AND_CUT = {0x1D, 0x56, 66, 0x00};

public static byte[] SELECT_CYRILLIC_CHARACTER_CODE_TABLE = {0x1B, 0x74, 0x11};

public static byte[] SELECT_BIT_IMAGE_MODE = {0x1B, 0x2A, 33, -128, 0};
public static byte[] SET_LINE_SPACING_24 = {0x1B, 0x33, 24};
public static byte[] SET_LINE_SPACING_30 = {0x1B, 0x33, 30};

public static byte[] TRANSMIT_DLE_PRINTER_STATUS = {0x10, 0x04, 0x01};
public static byte[] TRANSMIT_DLE_OFFLINE_PRINTER_STATUS = {0x10, 0x04, 0x02};
public static byte[] TRANSMIT_DLE_ERROR_STATUS = {0x10, 0x04, 0x03};
public static byte[] TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS = {0x10, 0x04, 0x04};

//public static byte[] OPEN_CASHDROWER_5PIN = {0x1b,0x70,0x01,0x10,0x10};
public static byte[] OPEN_CASHDROWER_2PIN = {0x1b,0x70,0x00,0x10,0x10};
public static byte[] START = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1B,
	0x40, 0x1B, 0x33, 0x00 };
//public static byte[] START = {0x1B, 0x40, 0x1B, 0x33, 0x00 };
public static byte[] END = { 0x1d, 0x4c, 0x1f, 0x00 }; 
}

