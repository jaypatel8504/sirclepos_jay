package com.john.bluetoothprinter.helper;

import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ReceiptDetail;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.storemaintenance.SalesTotal;

public class ZoutReport {
	protected final String[] SPLIT = {"================================","==========================================\n"}; 
	public final String[] SPACE = {"                                ","                                          \n"}; 
	private final String[] ITEMFORMAT_WITH_TAXNAME_FLOAT = {"%-6.2f%-15s%7s%4s","%-6.2f%-25s%7s%4s\n"};
//	private static String[] ITEMFORMAT_INT = {"%-4d%-21s%-7s","%-6.2f%-35s%7s"};
	private final String[] ITEMFORMAT_FLOAT_BLANK = {"%-6s%-19s%7s","%-6s%-29s%7s\n"};
//	private static String[] ITEMFORMAT_INT_BLANK = {"%-4s%-21s%-7s","%-6s%-35s%7s"};
	private final String[] SUBFORMAT = {"%-25s%7s","%-35s%7s\n"};
	private final String[] TITELFORMAT = {"%-32s","%-42s\n"};
	private final int[] DESC_LENGTH = {17,26};
	private final int[] DESC_LENGTH_WITH_TAXRATE = {13,22};
	private String storeName;
	protected String datetime;
	protected String store_no;
	protected String totalUndistSales;
	protected String WholeInvoiceDis;
	protected String netSales;
	protected String netTax1, netTax2, netTax3;
	protected String GrandTotal;
	protected String netTaxedSales;
	protected String netNonTaxedSales;
	protected String Tax1Ex,Tax2Ex,Tax3Ex, Tax12Ex, Tax13Ex, Tax23,Tax132Ex;
	protected String cash;
	protected String credit;
	protected String debit;
	protected String giftCard;
	protected String foodStamp;
	protected String toAccount;	
	protected String detailedDaily;
	protected String reportTotal;
	protected String deptBreak; 
	protected String startTime;
	protected String endTime;
	public ArrayList<ArrayList<String>> printout;
	public final int NORMAL = 1;
	public final int LARGE = 2;
	private Context context;
	private CornerStorePOS app;
	
	
	
public	ZoutReport(Context context, ArrayList<SalesTotal> detail, ArrayList<SalesTotal> depart, String[] payment, boolean breakdown, boolean log, String title,String start, String end){
		this.context = context;
		this.app = (CornerStorePOS) context.getApplicationContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int Sheettype;
		if(prefs.getBoolean("paperType", false)){
			Sheettype = 1;
		}else{
			Sheettype = 0;
		}
		 
		this.datetime = getString(R.string.datetime);
		this.store_no = getString(R.string.store_no);
		this.totalUndistSales = "Ttl Undisc";
		this.WholeInvoiceDis = "Invoice Disc";
		this.netSales = getString(R.string.Net_Sales);
		this.netTax1 = getString(R.string.Net_Tax1);
		this.netTax2 = getString(R.string.Net_Tax2);
		this.netTax3 = getString(R.string.Net_Tax3);
		this.GrandTotal = "Grand Ttl";
		this.netTaxedSales = "TaxedSales";
		this.netNonTaxedSales = "NetNon Taxed";
		this.Tax1Ex = "Tax1 Exempt";
		this.Tax2Ex = "Tax2 Exempt";
		this.Tax3Ex = "Tax3 Exempt";
		this.Tax12Ex = "Tax1,2 Exempt";
		this.Tax13Ex = "Tax1,3 Exempt"; 
		this.Tax23 = "Tax2,3 Exempt";
		this.Tax132Ex = "Tax1,2,3 Exempt";
		this.cash = getString(R.string.Cash);
		this.debit = getString(R.string.Debit);
		this.credit = getString(R.string.Credit);
		this.giftCard = getString(R.string.Gift);
		this.foodStamp = getString(R.string.FoodStamp);
		this.toAccount = getString(R.string.ToAccount);
		this.detailedDaily = getString(R.string.DetailedDailyReport);
		this.reportTotal = getString(R.string.ZoutReportTotals);
		this.deptBreak = getString(R.string.DetailedDepartmentReport);
		this.startTime = "Start: ";
		this.endTime = "End: ";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		String currentDateandTime = sdf.format(new Date());
		StartTime sTime = CSSharedPreferences.loadStartTime();
		EndTime eTime = CSSharedPreferences.loadEndTime();
		
		DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d1 = null;
		Date d2 = null;
		try {
			if(log == true)
				d1 = f1.parse(start);
			
				d2 = f1.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DateFormat f2 = new SimpleDateFormat("MM/dd/yyyy h:mma");
		 // "12:18am"
		if(breakdown == true){
			printout = new ArrayList<ArrayList<String>>();
			printout.add(buildArray("",0));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], title),LARGE));
			this.storeName = String.format(TITELFORMAT[Sheettype], ReceiptDetail.store_name);
			printout.add(buildArray(this.storeName,LARGE));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], store_no+ReceiptDetail.store_no),NORMAL));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], startTime + f2.format(d1)),NORMAL));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], endTime + f2.format(d2)),NORMAL));
			printout.add(buildArray(SPLIT[Sheettype],NORMAL));
			
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.totalUndistSales, detail.get(0).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.WholeInvoiceDis, detail.get(1).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netSales, detail.get(2).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax1, detail.get(3).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax2, detail.get(4).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax3, detail.get(5).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.GrandTotal, detail.get(6).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTaxedSales, detail.get(7).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax1Ex, detail.get(8).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax2Ex, detail.get(9).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax3Ex, detail.get(10).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax12Ex, detail.get(11).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax13Ex, detail.get(12).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax23, detail.get(13).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax132Ex, detail.get(14).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netNonTaxedSales, detail.get(15).getTotal()),NORMAL));
			printout.add(buildArray(SPLIT[Sheettype],NORMAL));
			
			for(int i = 0; i < depart.size(); i++){
				 		
				if(depart.get(i).getSales().equals("Dept Id") || depart.get(i).getSales().equals("Dept Name") || depart.get(i).getSales().equals("Sales Amount") || depart.get(i).getSales().equals("Total Qty")){
					if(depart.get(i).getTotal().length()>7)
					{
						  depart.get(i).setTotal(depart.get(i).getTotal().substring(0, 7)); 
					}
					printout.add(buildArray(String.format(SUBFORMAT[Sheettype], depart.get(i).getSales(), depart.get(i).getTotal()), NORMAL));
				}else{
					if(depart.get(i).getSales().length() > 64){
						depart.get(i).setSales(depart.get(i).getSales().substring(0, 63));
					}
					printout.add(buildArray(String.format("%-42s\n", depart.get(i).getSales()), NORMAL));
					String spaces = "";
					int size = 32 - depart.get(i).getTotal().length(); 
					for(int j = 0; j < size; j++)
					{
						spaces+= " ";
					}
					printout.add(buildArray(String.format("%-32s\n", spaces+depart.get(i).getTotal()), NORMAL));
				}
			}
					 
				
			
			printout.add(buildArray(SPLIT[Sheettype],NORMAL));

			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.cash, payment[0]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.credit, payment[1]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.debit, payment[2]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.giftCard, payment[3]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.foodStamp, payment[4]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.toAccount, payment[5]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "Check", payment[6]),NORMAL));
			printout.add(buildArray(SPACE[Sheettype],NORMAL));
		}
		else{
			printout = new ArrayList<ArrayList<String>>();
			printout.add(buildArray("",0));
	
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], title),LARGE));
			this.storeName = String.format(TITELFORMAT[Sheettype], ReceiptDetail.store_name);
			printout.add(buildArray(this.storeName,LARGE));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], store_no+ReceiptDetail.store_no),NORMAL));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], startTime + f2.format(d1)),NORMAL));
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], endTime + f2.format(d2)),NORMAL));
			printout.add(buildArray(SPLIT[Sheettype],NORMAL));
			
			
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.totalUndistSales, detail.get(0).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.WholeInvoiceDis, detail.get(1).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netSales, detail.get(2).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax1, detail.get(3).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax2, detail.get(4).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTax3, detail.get(5).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.GrandTotal, detail.get(6).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netTaxedSales, detail.get(7).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax1Ex, detail.get(8).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax2Ex, detail.get(9).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax3Ex, detail.get(10).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax12Ex, detail.get(11).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax13Ex, detail.get(12).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax23, detail.get(13).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.Tax132Ex, detail.get(14).getTotal()),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.netNonTaxedSales, detail.get(15).getTotal()),NORMAL));
			printout.add(buildArray(SPLIT[Sheettype],NORMAL));

			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.cash, payment[0]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.credit, payment[1]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.debit, payment[2]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.giftCard, payment[3]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.foodStamp, payment[4]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], this.toAccount, payment[5]),NORMAL));
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "Check", payment[6]),NORMAL));
			printout.add(buildArray(SPACE[Sheettype],NORMAL));
		}
		
	}
	
	private String getString(int strID) {
		return context.getString(strID);
		
	}
	
	private ArrayList<String> buildArray(String str, int which){
		ArrayList<String> list = new ArrayList<String>();
		list.add(str);
		list.add(which+"");		
		return list;		
	}
	
}