package com.john.bluetoothprinter.helper;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Invoice;
import com.intlpos.database.Order;
import com.intlpos.database.ReceiptDetail;
import com.intlpos.database.SplitInvoice;
import com.intlpos.database.Tax;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.PaymentFragmentListener;

@SuppressLint("NewApi")
public class ReceiptBuilder {
	protected final String[] SPLIT = {"================================","==========================================\n"}; 
	public final String[] SPACE = {"                                ","                                          \n"}; 
	public final String[] SIGNATURE = {"Signature_______________________","Signature_________________________________\n"}; 
	private final String[] ITEMFORMAT_FLOAT = {"%-6.2f%-19s%7s","%-6.2f%-29s%7s\n"};
	private final String[] ITEMFORMAT_WITH_TAXNAME_FLOAT = {"%-6.2f%-15s%7s%4s","%-6.2f%-25s%7s%4s\n"};
//	private static String[] ITEMFORMAT_INT = {"%-4d%-21s%-7s","%-6.2f%-35s%7s"};
	private final String[] ITEMFORMAT_FLOAT_BLANK = {"%-6s%-19s%7s","%-6s%-29s%7s\n"};
//	private static String[] ITEMFORMAT_INT_BLANK = {"%-4s%-21s%-7s","%-6s%-35s%7s"};
	private final String[] SUBFORMAT = {"%-25s%7s","%-35s%7s\n"};
	private final String[] TITELFORMAT = {"%-32s","%-42s\n"};
	public final String DUP_COPY = "      ***DUPLICATE COPY***      "; 
	private final int[] DESC_LENGTH = {17,26};
	private final int[] DESC_LENGTH_WITH_TAXRATE = {13,22};
	private String storeName;
	public String signature;
	protected String invoiceNum;
	protected String datetime;
	protected String cashier;
	protected String station;
	protected String paymentType;
	protected String itemCount;
	protected String undiscountedTotal;
	protected String subtotal;
	protected String amtTendered;
	protected String change;
	protected String[] tax_name;
	protected String grandTotal;
//	protected String store_no;
	public ArrayList<ArrayList<String>> printout;
	public boolean printReceiptWithSign;
	public String barcode;
	public int NORMAL = 4;
	public int LARGE = 3;
	private BigDecimal wholeInvoiceQty;
	private Context context;
	private CornerStorePOS app;
	public boolean duplicateCopy;
	private boolean hasChange;
	private String CustomerName;
	private String token;
	
	public ReceiptBuilder(ArrayList<Order> productList, Invoice invoice, 
			String invoiceNum, boolean dup, Context context,String CustomerName, int token){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String print= prefs.getString("printerType", "");
    	if( print.equals("Star Micronics")){
    		NORMAL = 2;
    		LARGE = 1;
    	}
		this.context = context;
		this.token = String.format(TITELFORMAT[1], Integer.toString(token));
		this.duplicateCopy = dup;
		this.app = (CornerStorePOS) context.getApplicationContext();
		this.invoiceNum = getString(R.string.invoice_number);
		this.datetime = getString(R.string.datetime);
		this.cashier = getString(R.string.cashier);
		this.station = getString(R.string.station);
		this.paymentType = getString(R.string.payment_type);
		this.itemCount = getString(R.string.item_count);
		this.undiscountedTotal = getString(R.string.undiscountedtotal);
		this.subtotal = "Subtotal";
		this.amtTendered = getString(R.string.amt_tendered);
		this.change = getString(R.string.change);
		this.grandTotal = getString(R.string.grand_total);
//		this.store_no = getString(R.string.store_no);
		this.printReceiptWithSign = false;
		this.CustomerName = CustomerName;
		wholeInvoiceQty = BigDecimal.ZERO;
		printout = new ArrayList<ArrayList<String>>();
		printout.add(buildArray("",0));
		int Sheettype;
		if(prefs.getBoolean("paperType", false)){
			Sheettype = 1;
		}else{
			Sheettype = 0;
		}		
		
		if(duplicateCopy){
			printout.add(buildArray(DUP_COPY,NORMAL));
		}
		
		if(token != 0){
			if(print.equals("Generic") || print.equals("Partner Tech")){
				printout.add(buildArray("Order #: "+this.token,14));
			}else{
				printout.add(buildArray("Order #: "+this.token,16));
			}
		}
		
//		Log.d("Receipt Info", ReceiptDetail.store_name + " " + ReceiptDetail.address1 + " " + ReceiptDetail.address2 + " " + ReceiptDetail.contact_no + " " +ReceiptDetail.email_id);
		this.storeName = String.format(TITELFORMAT[Sheettype], ReceiptDetail.store_name);
		printout.add(buildArray(this.storeName,LARGE));
		if (ReceiptDetail.address1 != null && !ReceiptDetail.address1.isEmpty()){
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], ReceiptDetail.address1), NORMAL));
		if (!ReceiptDetail.address2.isEmpty())
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], ReceiptDetail.address2), NORMAL));
		}
		if (ReceiptDetail.contact_no != null && !ReceiptDetail.contact_no.isEmpty())
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], ReceiptDetail.contact_no), NORMAL));
		if (ReceiptDetail.email_id != null && !ReceiptDetail.email_id.isEmpty())
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], ReceiptDetail.email_id), NORMAL));
		printout.add(buildArray(SPACE[Sheettype],NORMAL));
//		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], store_no+ReceiptDetail.store_no),NORMAL));
		this.invoiceNum += invoiceNum;
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], this.invoiceNum),NORMAL));
		this.datetime += invoice.getInvoice_date();
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], this.datetime),NORMAL));
		this.cashier += app.employee.first_name;
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], this.cashier),NORMAL));
		this.station += invoice.getStation_id();
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], this.station),NORMAL));
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], paymentType+" : "+Invoice.PAYMENT_TYPE[invoice.getPayment_type()-1]),NORMAL));
		if(CustomerName.length() > 15){
			CustomerName = CustomerName.substring(0, 15);
		}
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "Customer Name : "+ CustomerName),NORMAL));
		int index = printout.size();
		printout.add(buildArray(SPLIT[Sheettype],NORMAL));
		
		int len = 0;
		if(prefs.getBoolean("switch_inventory_with_taxrate", false)){
			len = DESC_LENGTH_WITH_TAXRATE[Sheettype];
		}else{
			len = DESC_LENGTH[Sheettype];
		}	
		
		if(Sheettype == 1)
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "Qty" + "   Item" + "                            Cost"),NORMAL));
		else
			printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "Qty" + "   Item" + "                  Cost"),NORMAL));
		int fullDesc = 0;
		for(Order order: productList){
			String desc = order.getItem_desc().split("\n")[1];
			String[] descArray = order.getItem_desc().split("\n");
			ArrayList<String> noes = new ArrayList<String>();
			for(int i = 0; i < descArray.length; i++){
				if(descArray[i].contains("No ")){
					noes.add(descArray[i]);
				}
			}
			
			String[] desc_arr = new String[fullDesc+1];
			desc_arr[0] = desc.length() > len ? desc.substring(0,len-1):desc;
			if(order.getRow_id() != 0){
				desc_arr[0] = ">"+desc_arr[0];
			}
			if(desc.length() > len && fullDesc == 1){
				desc_arr[1] = desc.length() > len*2 ? desc.substring(len,len*2):desc.substring(len,desc.length());
			}
			wholeInvoiceQty = wholeInvoiceQty.add(order.getQty_ordered());
			String item;
			if(prefs.getBoolean("switch_inventory_with_taxrate", false)){
				String tax_desc = "";
//				Log.d("tax", "tax1"+order.getTax1_after_whole_invoice_discount()+" tax2"+order.getTax2_after_whole_invoice_discount()+" tax3"+order.getTax3_after_whole_invoice_discount());
				if(order.getTax1_after_whole_invoice_discount().compareTo(BigDecimal.ZERO) == 1){
					tax_desc += Tax.tax_desc[0].substring(0, 1);
				}
				if(order.getTax2_after_whole_invoice_discount().compareTo(BigDecimal.ZERO) == 1){
					tax_desc += Tax.tax_desc[1].substring(0, 1);
				}
				if(order.getTax3_after_whole_invoice_discount().compareTo(BigDecimal.ZERO) == 1){
					tax_desc += Tax.tax_desc[2].substring(0, 1);
				}
				
					item = String.format(ITEMFORMAT_WITH_TAXNAME_FLOAT[Sheettype], order.getQty_ordered().doubleValue(),desc_arr[0],order.getQty_ordered().multiply(order.getCurrent_price()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),tax_desc);
			}
			else{
				item = String.format(ITEMFORMAT_FLOAT[Sheettype], order.getQty_ordered().doubleValue(),desc_arr[0],order.getQty_ordered().multiply(order.getCurrent_price()).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
				
			}
			printout.add(buildArray(item,NORMAL));
			if(order.getNote() != null &&!order.getNote().isEmpty()){
				String note = order.getNote().replace("\n", "\n>"); 
				printout.add(buildArray(note+"\n",NORMAL));
			}
			if(!noes.isEmpty()){
				for(String no:noes){
					if(no.length() > 32){
						no = no.substring(0,no.length()-3);
					}
					printout.add(buildArray(">"+no+"\n",NORMAL));
				}
			}
			if (fullDesc == 1 && desc_arr[1] != null && !desc_arr[1].isEmpty()){
				printout.add(buildArray(String.format(ITEMFORMAT_FLOAT_BLANK[Sheettype], "",desc_arr[1],""),NORMAL));
			}
			if (order.getLine_discount_percent() != 0)
				printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "-Discounted "+order.getLine_discount_percent()+"%"),NORMAL));			
		}
		this.itemCount += wholeInvoiceQty.setScale(2,BigDecimal.ROUND_HALF_EVEN).toString();
		printout.add(index,buildArray(String.format(TITELFORMAT[Sheettype], this.itemCount),NORMAL));
		printout.add(buildArray(SPLIT[Sheettype],NORMAL));
		//end of item list
		if (invoice.getWhole_invoice_discount_percent() > 0){
			undiscountedTotal = String.format(SUBFORMAT[Sheettype], this.undiscountedTotal, 
					invoice.getUndiscounted_sub_total().setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
			printout.add(buildArray(undiscountedTotal,NORMAL));
			String discountedTotal = String.format(SUBFORMAT[Sheettype], "- "+invoice.getWhole_invoice_discount_percent() + "% Discount", 
					CornerStorePOS.dec_format.format(invoice.getWhole_invoice_discount_amount()));
			printout.add(buildArray(discountedTotal,NORMAL));
		}
		this.subtotal = String.format(SUBFORMAT[Sheettype], this.subtotal, invoice.getSub_total().toString().trim());
		printout.add(buildArray(this.subtotal,NORMAL));
		this.tax_name = new String[3];
		if(Tax.tax_desc[0].length() > 3 )
			Tax.tax_desc[0] = Tax.tax_desc[0] .substring(0, 3);
		if( Tax.tax_desc[1].length()> 3)
			Tax.tax_desc[1] = Tax.tax_desc[1] .substring(0, 3);
		if(Tax.tax_desc[2].length()> 3)
			Tax.tax_desc[2] = Tax.tax_desc[2] .substring(0, 3);
		tax_name[0] = Tax.tax_desc[0].concat("("+Tax.tax_rate1.setScale(4,BigDecimal.ROUND_HALF_EVEN).toString().trim()+"%)");
		tax_name[1] = Tax.tax_desc[1].concat("("+Tax.tax_rate2.setScale(4,BigDecimal.ROUND_HALF_EVEN).toString().trim()+"%)");
		tax_name[2] = Tax.tax_desc[2].concat("("+Tax.tax_rate3.setScale(4,BigDecimal.ROUND_HALF_EVEN).toString().trim()+"%)");
//		BigDecimal[] tax_value = invoice.getTotal_tax();
//		for(int i = 0; i < 3; i++){
////			Log.d("Tax", "("+tax_name[i]+")");
//			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], tax_name[i], tax_value[i].setScale(2,BigDecimal.ROUND_HALF_EVEN).toString().trim()),NORMAL));
//		}
		printout.add(buildArray(String.format(SUBFORMAT[Sheettype], tax_name[0], invoice.getTotal_tax1().setScale(2,BigDecimal.ROUND_HALF_EVEN).toString().trim()),NORMAL));
		printout.add(buildArray(String.format(SUBFORMAT[Sheettype], tax_name[1], invoice.getTotal_tax2().setScale(2,BigDecimal.ROUND_HALF_EVEN).toString().trim()),NORMAL));
		printout.add(buildArray(String.format(SUBFORMAT[Sheettype], tax_name[2], invoice.getTotal_tax3().setScale(2,BigDecimal.ROUND_HALF_EVEN).toString().trim()),NORMAL));
//		if(PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).getString("nickel", "").equals("1")){
//			PaymentFragmentListener pay = new PaymentFragmentListener(invoice.getGrand_total());
//			String roundTotal = String.format(SUBFORMAT[Sheettype], "Rounded Total", pay.nearestnickel(invoice.getGrand_total()).toString());
//			printout.add(buildArray(roundTotal,LARGE));
//		}
		this.grandTotal = String.format(SUBFORMAT[Sheettype], this.grandTotal, invoice.getGrand_total().toString().trim());
		printout.add(buildArray(this.grandTotal,LARGE));
		printout.add(buildArray(SPACE[Sheettype],NORMAL));
		for(SplitInvoice si:invoice.getSplit_invoice_list()){
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], Invoice.PAYMENT_TYPE[si.payment_type-1], si.total.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString().trim()), NORMAL));
			if(si.payment_type == 2){
				hasChange = true;
//				printout.add(buildArray(String.format(SUBFORMAT[Sheettype], amtTendered, si.total.add(invoice.getChange_amt()).toString().trim()), NORMAL));
			}
			if(!CSSharedPreferences.getExpressCheckout().getExpress()){
				if(si.payment_type == 3){
					printReceiptWithSign = true;
					if(invoice.getAuth_code() == null){
						printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "APPR Code", si.auth_code), NORMAL));
						printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "************"+si.last_four), NORMAL));
					}else{
						printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "APPR Code", invoice.getAuth_code().toString().trim()), NORMAL));
						printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "************"+invoice.getLast_four().toString().trim()), NORMAL));
					}
				}
			}else{
				if(si.payment_type == 3){
					printReceiptWithSign = true;
				}
			}
			if(si.payment_type == 4){
				if(invoice.getAuth_code() == null){
					printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "APPR Code", si.auth_code), NORMAL));
					printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "************"+si.last_four), NORMAL));
				}else{
					printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "APPR Code", invoice.getAuth_code().toString().trim()), NORMAL));
					printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "************"+invoice.getLast_four().toString().trim()), NORMAL));
				}
			}
			if(si.payment_type == 8){
				if(invoice.getAuth_code() != null){
					printout.add(buildArray(String.format(SUBFORMAT[Sheettype], "Check Number", si.auth_code), NORMAL));
					
				}
			}
		}
		if(hasChange){
			printout.add(buildArray(String.format(SUBFORMAT[Sheettype], change, invoice.getChange_amt().toString().trim()), NORMAL));
		}
		printout.add(buildArray(SPLIT[Sheettype],NORMAL));
		String[] footer = ReceiptDetail.footer;
		for (String str: footer){
			if (str != null && str.trim().length() > 0){
				printout.add(buildArray(String.format(TITELFORMAT[Sheettype],str),NORMAL));
			}
		}
		barcode = "*I"+ReceiptDetail.store_no+"-"+invoiceNum+"*";
		int spaces = 16 - barcode.length()/2;
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i< spaces; i++){
			sb.append(" ");
		}
		sb.append(barcode);
		sb.append("\n");
		printout.add(buildArray(sb.toString(),NORMAL));
//		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], barcode),NORMAL));
//		printout.add(buildArray(SPACE[Sheettype],NORMAL));
	}
	
	public ReceiptBuilder(ArrayList<Order> productList, String note, String customerName, Context context, int token){
		int fullDesc = 0, len;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		this.context = context;
		this.app = (CornerStorePOS) context.getApplicationContext();
		this.datetime = getString(R.string.datetime);
		int Sheettype = 1;
		
//		Log.d("Receipt Info", ReceiptDetail.store_name + " " + ReceiptDetail.address1 + " " + ReceiptDetail.address2 + " " + ReceiptDetail.contact_no + " " +ReceiptDetail.email_id)'
		printout = new ArrayList<ArrayList<String>>();
		printout.add(buildArray("\n",NORMAL));
		printout.add(buildArray("Customer Name: "+customerName+"\n",0));
		printout.add(buildArray(SPLIT[Sheettype],NORMAL));
		len = DESC_LENGTH[Sheettype];
		for(Order order: productList){
			String desc = order.getItem_desc().split("\n")[1];
			String[] descArray = order.getItem_desc().split("\n");
			ArrayList<String> noes = new ArrayList<String>();
			for(int i = 0; i < descArray.length; i++){
				if(descArray[i].contains("No ")){
					noes.add(descArray[i]);
				}
			}
			
			String[] desc_arr = new String[fullDesc+1];
			desc_arr[0] = desc.length() > len ? desc.substring(0,len):desc;
			if(order.getRow_id() != 0){
				desc_arr[0] = "-->"+desc_arr[0];
			}
			if(desc.length() > len && fullDesc == 1){
				desc_arr[1] = desc.length() > len*2 ? desc.substring(len,len*2):desc.substring(len,desc.length());
			}
			String item = String.format(SUBFORMAT[Sheettype], desc_arr[0],order.getQty_ordered().doubleValue());
			printout.add(buildArray(item,NORMAL));
			if(!noes.isEmpty()){
				for(String no:noes){
					printout.add(buildArray("-->"+no+"\n",NORMAL));
				}
			}
		}
		printout.add(buildArray(SPLIT[Sheettype],NORMAL));
		printout.add(buildArray(note,NORMAL));
	}

	public String getInvoiceNum() {
		return invoiceNum;
	}

	private String getString(int strID) {
		return context.getString(strID);
	}

	private ArrayList<String> buildArray(String str, int which){
		ArrayList<String> list = new ArrayList<String>();
		list.add(str);
		list.add(which+"");		
		return list;		
	}
	
	public ReceiptBuilder(Context context){
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		printout = new ArrayList<ArrayList<String>>();
		this.context = context;
		int Sheettype = 0;
		if(prefs.getBoolean("paperType", false)){
			Sheettype = 1;
		}else{
			Sheettype = 0;
		}	
		this.storeName = String.format(TITELFORMAT[Sheettype], "TEST STORE");
		printout.add(buildArray(this.storeName,LARGE));
		
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "TEST PRINT"), NORMAL));
		
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "123456789"), NORMAL));
		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], "test@print.com"), NORMAL));
		printout.add(buildArray(SPACE[Sheettype],NORMAL));
//		printout.add(buildArray(String.format(TITELFORMAT[Sheettype], store_no+ReceiptDetail.store_no),NORMAL));
		
	}
}
