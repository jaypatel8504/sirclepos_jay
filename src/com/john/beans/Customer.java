package com.john.beans;

import java.util.ArrayList;

import org.json.JSONObject;

public class Customer {
	double account_balance;
	int customer_id;
	String address1,address2,city,email,first_name,last_name,phone_1,state,zipcode;
	public Customer(double balance, int id, String address1, String address2, String city, String email, String first, String last, String phone
			,String state, String zipcode){
		this.account_balance = balance;
		this.customer_id = id;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.email = email;
		this.first_name = first;
		this.last_name = last;
		this.phone_1 = phone;
		this.state = state;
		this.zipcode = zipcode;
		
	}
	
	public Customer(int balance, int id,  String first, String last){
		this.account_balance = balance;
		this.customer_id = id;
		this.first_name = first;
		this.last_name = last;
	}
	
	public Customer(){
		
	}
	
	public Customer(String email){
		this.email = email;
		this.first_name = "Guest";
		this.last_name = "Customer";
	}
	public double getAccount_balance() {
		return account_balance;
	}
	public void setAccount_balance(int account_balance) {
		this.account_balance = account_balance;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPhone_1() {
		return phone_1;
	}
	public void setPhone_1(String phone_1) {
		this.phone_1 = phone_1;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
}
