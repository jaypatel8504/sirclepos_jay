package com.john.beans;

import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;
import com.intlpos.initsetup.PostRequestTask;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.GetEmailRequest;
import com.john.email.SendMailTLSTask;

public class SaveCustomer {
	StoreDetail storedetail;
	Customer customer;
	
	public SaveCustomer(StoreDetail storedetail, Customer customer){
		this.storedetail = storedetail;
		this.customer = customer;
	}
	
	public StoreDetail getStoredetail() {
		return storedetail;
	}
	public void setStoredetail(StoreDetail storedetail) {
		this.storedetail = storedetail;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public static String saveEmail(String email){
		final String SAVE_EMAIL_URL = "CustomerMaintenence/CustomerService.svc/savecustomer";
		Gson gson = new Gson();
		Customer customer = new Customer(email);
		CornerStorePOS app = new CornerStorePOS();
		StoreDetail storedetail = new StoreDetail(CornerStorePOS.email_id,CornerStorePOS.password,CornerStorePOS.StoreId,CornerStorePOS.StationId);
		SaveCustomer save = new SaveCustomer(storedetail,customer);
		String request = gson.toJson(save,SaveCustomer.class);
		int flag = 0;
		while(flag != 2){
			PostRequestTask postTask = new PostRequestTask(); 
			String uri = flag == 0 ? CornerStorePOS.Url:CornerStorePOS.BackupUrl;
			postTask.execute(uri+SAVE_EMAIL_URL,request);
			String response = null;
			try {
				response = postTask.get();
				Log.d("email", response);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
				flag++;
			}
			JSONObject json = null;
			try {
				 json = new JSONObject(response);
				 return json.getString("resultString");
			} catch (JSONException e) {			
				e.printStackTrace();
				flag++;
			} 
		}
		return "Failed send request.";
	}
}
