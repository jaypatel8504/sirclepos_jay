package com.john.beans;

import org.json.JSONObject;

import com.google.gson.Gson;

import android.util.Log;

public class Product {
	private int product_id;
	private float price,cost,item_stock,re_order_level;
	private boolean tax_rate1,tax_rate2,tax_rate3,prompt_price,prompt_qty,foodstampable,product_tsdisplay;
	private String item_no,item_name,prompt_items_id,prompt_message,department_id,department_name;
	private String date_created;
	private boolean dept_tsdisplay;
	private String picture;
	private boolean tax_inclusive;
	private boolean hasmodifier;
	private boolean ismodifier;
	
	public Product(String item_no, String item_name, String price, String cost, String item_stock, boolean tax1, boolean tax2, boolean tax3, String department_id, String department_name, String picture, 
			boolean taxInclusive, boolean hasMods, boolean isMods, boolean food){
		this.item_no = item_no;
		this.item_name = item_name;
		this.price = Float.parseFloat(price);
		this.cost = Float.parseFloat(cost);
		this.item_stock = Float.parseFloat(item_stock);
		this.tax_rate1 = tax1;
		this.tax_rate2 = tax2;
		this.tax_rate3 = tax3;
		this.department_id = department_id;
		this.department_name = department_name;
		this.product_id = 0;
		this.re_order_level = 0;
		this.product_tsdisplay = true;
//		StringBuilder sb = new StringBuilder();
//		sb.append("\\");
//		sb.deleteCharAt(0);
//		sb.append("/Date(");
//		sb.append(System.currentTimeMillis());
//		sb.append(")\\/");
//		sb.deleteCharAt(15);
		this.date_created = "/Date(" + System.currentTimeMillis() + ")/";
		Log.d("Print", this.date_created.length()+"");
		this.picture = picture;
		this.tax_inclusive = taxInclusive;
		this.hasmodifier = hasMods;
		this.ismodifier = isMods;
		this.foodstampable = food;
		
		
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public float getItem_stock() {
		return item_stock;
	}
	public void setItem_stock(float item_stock) {
		this.item_stock = item_stock;
	}
	public float getRe_order_level() {
		return re_order_level;
	}
	public void setRe_order_level(float re_order_level) {
		this.re_order_level = re_order_level;
	}
	public boolean isTax_rate1() {
		return tax_rate1;
	}
	public void setTax_rate1(boolean tax_rate1) {
		this.tax_rate1 = tax_rate1;
	}
	public boolean isTax_rate2() {
		return tax_rate2;
	}
	public void setTax_rate2(boolean tax_rate2) {
		this.tax_rate2 = tax_rate2;
	}
	public boolean isTax_rate3() {
		return tax_rate3;
	}
	public void setTax_rate3(boolean tax_rate3) {
		this.tax_rate3 = tax_rate3;
	}
	public boolean isPrompt_price() {
		return prompt_price;
	}
	public void setPrompt_price(boolean prompt_price) {
		this.prompt_price = prompt_price;
	}
	public boolean isPrompt_qty() {
		return prompt_qty;
	}
	public void setPrompt_qty(boolean prompt_qty) {
		this.prompt_qty = prompt_qty;
	}
	public boolean isFoodstampable() {
		return foodstampable;
	}
	public void setFoodstampable(boolean foodstampable) {
		this.foodstampable = foodstampable;
	}
	public boolean isProduct_tsdisplay() {
		return product_tsdisplay;
	}
	public void setProduct_tsdisplay(boolean product_tsdisplay) {
		this.product_tsdisplay = product_tsdisplay;
	}
	public String getItem_no() {
		return item_no;
	}
	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getPrompt_items_id() {
		return prompt_items_id;
	}
	public void setPrompt_items_id(String prompt_items_id) {
		this.prompt_items_id = prompt_items_id;
	}
	public String getPrompt_message() {
		return prompt_message;
	}
	public void setPrompt_message(String prompt_message) {
		this.prompt_message = prompt_message;
	}
	public String getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public String getDate_created() {
		return date_created;
	}
	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
}
