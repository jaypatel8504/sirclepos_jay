package com.john.beans;

public class Department {
	private String department_id,department_name,description;
	private boolean tsdisplay;
	public Department(){
		//empty constructor
	}
	public String getDepartment_id() {
		return department_id;
	}
	public void setDepartment_id(String department_id) {
		this.department_id = department_id;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isTsdisplay() {
		return tsdisplay;
	}
	public void setTsdisplay(boolean tsdisplay) {
		this.tsdisplay = tsdisplay;
	}
}
