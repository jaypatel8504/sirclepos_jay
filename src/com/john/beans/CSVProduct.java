package com.john.beans;

import java.util.ArrayList;

public class CSVProduct 
 {
	String item_no,item_name,date_created,department_name;
	double cost,price,re_order_level;
	int item_stock;
	boolean tax_rate1,tax_rate2,tax_rate3;
	
	public CSVProduct(ArrayList<String> list){
		this.item_no = list.get(0);
		this.item_name = list.get(1);
		this.cost = Double.parseDouble(list.get(2));
		this.price = Double.parseDouble(list.get(3));
		this.item_stock = Integer.parseInt(list.get(4));
		this.tax_rate1 = Boolean.parseBoolean(list.get(5).toLowerCase());
		this.tax_rate2 = Boolean.parseBoolean(list.get(6).toLowerCase());
		this.tax_rate3 = Boolean.parseBoolean(list.get(7).toLowerCase());
		this.department_name = list.get(8);
		this.re_order_level = Double.parseDouble(list.get(9));
		this.date_created = list.get(10);
	}

	public String getItem_no() {
		return item_no;
	}

	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getDate_created() {
		return date_created;
	}

	public void setDate_created(String date_created) {
		this.date_created = date_created;
	}
}
