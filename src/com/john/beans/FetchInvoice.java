package com.john.beans;

import java.lang.reflect.Type;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.intlpos.database.Invoice;
import com.intlpos.database.Order;
import com.intlpos.database.SplitInvoice;

public class FetchInvoice  {
	private Invoice Key;
	private ArrayList<Order> Value;
	
	public FetchInvoice(Invoice Key, ArrayList<Order> Value){
		this.Key = Key;
		this.Value = Value;
	}
	
	public Invoice getKey() {
		return Key;
	}
	public void setKey(Invoice key) {
		Key = key;
	}
	public ArrayList<Order> getValue() {
		return Value;
	}
	public void setValue(ArrayList<Order> value) {
		Value = value;
	}

	 @Override
	public String toString() {
		 StringBuilder sb = new StringBuilder();
		 sb.append("Invoice Id =").append(Key.getInvoice_id());
		 sb.append(" Product size = ").append(Value.size());
	return sb.toString();
	}
	 
	@SuppressLint("NewApi")
	public FetchInvoice(JSONObject jsonObject) throws JSONException{
		Gson gson = new Gson();
//		ArrayList<FetchInvoice> list = gson.fromJson(response, type);
		this.Key = gson.fromJson(jsonObject.get("Key").toString(), Invoice.class);
		Key.setSplit_invoice_list(new ArrayList<SplitInvoice>());
		Type type = new TypeToken<Order>(){}.getType();
		this.Value = new ArrayList<Order>();
		JSONArray itemArr = new JSONArray(jsonObject.get("Value").toString());
		for(int i = 0;i < itemArr.length();i++){
			this.Value.add(gson.fromJson(itemArr.getString(i),Order.class));
		}
//		Log.d("response", "Key="+Key.getPayment_type()+" Value="+Value.size());
	}

}
