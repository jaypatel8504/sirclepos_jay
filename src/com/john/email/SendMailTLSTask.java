package com.john.email;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.RegisterTablet;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

public class SendMailTLSTask extends AsyncTask<String, Void, String> {
	protected static final int USERNAME = 0, PASSWORD = 1, SENDTO = 2,SUBJECT = 3, HOST = 4, PORT = 5, CONTENT = 6;
	private Context context;
	private Activity activity;
	private String email, name, phone;
	
	public SendMailTLSTask(){
		
	}
	
	public SendMailTLSTask(Context context, Activity activity, String email, String name, String phone){
		this.context = context;
		this.activity = activity;
		this.email = email;
		this.name = name;
		this.phone = phone;
	}
	
    @Override
    protected void onPreExecute() {
        //disableUI();
    }

    @Override
    protected String doInBackground(String... formFieldValues) {
    	
    	final String username = formFieldValues[USERNAME];
    	Log.d("formFieldValues",formFieldValues[USERNAME]);
		final String password = formFieldValues[PASSWORD];
		Log.d("formFieldValues",formFieldValues[PASSWORD]);
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", formFieldValues[HOST]);
		props.put("mail.smtp.port", "587");// 587 for gmail
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
//		Session session = Session.getDefaultInstance( props, null );
 
		try {
 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			if(formFieldValues.length == 7){
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(formFieldValues[SENDTO]));
			}else{
				String emails = formFieldValues[SENDTO]+","+formFieldValues[7]+","+formFieldValues[8];
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(emails));
			}
//			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse("long@internationalpointofsale.com"));
			message.setReplyTo(InternetAddress.parse(username));
			message.setSubject(formFieldValues[SUBJECT]);
			//message.setText(formFieldValues[4]); // for plain-text email
			message.setContent(formFieldValues[CONTENT], "text/html");  
 
            Transport transport = session.getTransport("smtp");
            transport.connect(formFieldValues[HOST], username, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
			return formFieldValues[SUBJECT];
		} catch (MessagingException e) {
			Log.e("Exception", e.toString());
		}
		return "Failed send mail";
    }

    @Override
    protected void onPostExecute(String result) {
    	if(result.equals("Thank you for trying out Sircle POS")){
	    	StoreDetail storedetail = new StoreDetail();		
			storedetail.setnotfirsttime(true);
			storedetail.setemail_id("demo@sirclepos.com");
			storedetail.setpassword("CcJMRbrd4");
			storedetail.setstoreid("10000");
			storedetail.setstationid("100");
			storedetail.setPaymentModule(false);
			CSSharedPreferences.savePreferences(storedetail);
			
			storedetail.setExpress(true);
			CSSharedPreferences.setExpressCheckout(storedetail);
			
			try {
				storedetail.setVersion_code(activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionCode);
				storedetail.setVersion_name(activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SendMailTLSTask sendMailTask = new SendMailTLSTask(); 
			String emailsend = "NAME: "+name+ "<br>" +"EMAIL: " + email+ "<br>" + "PHONE: "+phone;
		    sendMailTask.execute("sirclepos@gmail.com","yyydluzndsbkrinb", "ed@internationalpointofsale.com","New Demo User From Play Store","smtp.gmail.com","587",emailsend,"tech@internationalpointofsale.com","jake@internationalpointofsale.com");
			
		    CSSharedPreferences.setDemoPref(true);
			CSSharedPreferences.saveUpdatePreferences(storedetail);
			RegisterTablet.setPreferenceStartTime();
			CornerStorePOS app = (CornerStorePOS) activity.getApplication(); 	
			app.ReadVariables();
	     	Intent i = new Intent(activity, EmployeeLogin.class);
			i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
			i.putExtra("has_update", false);
			activity.startActivity(i);
    	}
    }
}