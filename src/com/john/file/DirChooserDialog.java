/**
 * 
 */
package com.john.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.john.beans.CSVProduct;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author HKP
 * 2011-6-17
 *
 */
public class DirChooserDialog extends Dialog implements android.view.View.OnClickListener{
	protected static final String[] HEADERS = {"item_no","item_name","cost","price","item_stock","tax_rate1","tax_rate2","tax_rate3","department_name","re_order_level"};
	private ListView list;
	private ListView filelist;
	ArrayAdapter<String> Adapter;
	ArrayList<String> arr=new ArrayList<String>();
	
	Context context;
	private String path;
	
	private TextView title;
	private EditText et;
	private Button home,back,ok;
	private LinearLayout titleView;
	
	private int type = 1;
	private String[] fileType = null;
	private FileAdapter m_adapter ;
	public final static int TypeOpen = 1;
	public final static int TypeSave = 2;
	
	/**
	 * @param context
	 * @param type 閸婇棿璐�1鐞涖劎銇氶崚娑樼紦閹垫挸绱戦惄顔肩秿缁鐎烽惃鍕嚠鐠囨繃顢嬮敍锟芥稉鍝勫灡瀵よ桨绻氱�涙ɑ鏋冩禒璺哄煂閻╊喖缍嶇猾璇茬�烽惃鍕嚠鐠囨繃顢�
	 * @param fileType 鐟曚浇绻冨銈囨畱閺傚洣娆㈢猾璇茬��,null鐞涖劎銇氶崣顏堬拷閹封晝娲拌ぐ锟�
	 * @param resultPath 閻愮瓌K閹稿鎸虫潻鏂挎礀閻ㄥ嫮绮ㄩ弸婊愮礉閻╊喖缍嶉幋鏍拷閻╊喖缍�+閺傚洣娆㈤崥锟�
	 */
	public DirChooserDialog(Context context,int type,String[]fileType,String resultPath,ListView filelist) {
		super(context);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.type = type;
		this.fileType = fileType;
		this.path = resultPath;
		this.filelist = filelist;
	}
	/* (non-Javadoc)
	 * @see android.app.Dialog#dismiss()
	 */
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismiss();
	}
	/* (non-Javadoc)
	 * @see android.app.Dialog#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chooserdialog);
		
		path = getRootDir();
		arr = (ArrayList<String>) getDirs(path);
		Adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1, arr);
		
		list = (ListView)findViewById(R.id.list_dir);
		list.setAdapter(Adapter);
		
		list.setOnItemClickListener(lvLis);

		home = (Button) findViewById(R.id.btn_home);
		home.setOnClickListener(this);
		
		back = (Button) findViewById(R.id.btn_back);
		back.setOnClickListener(this);
		
		ok = (Button) findViewById(R.id.btn_ok);
		ok.setOnClickListener(this);
		
		titleView = (LinearLayout) findViewById(R.id.dir_layout);
		
		if(type == TypeOpen){
			title = new TextView(context);
			titleView.addView(title);
			title.setText(path);
		}else if(type == TypeSave){
			et = new EditText(context);
			et.setWidth(240);
			et.setHeight(70);
			et.setGravity(Gravity.CENTER);
			et.setPadding(0, 2, 0, 0);
			titleView.addView(et);
			et.setText("wfFileName");
		}
//		title = (TextView) findViewById(R.id.dir_str);
//		title.setText(path);
		
	}
	//閸斻劍锟介弴瀛樻煀ListView
	Runnable add=new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			arr.clear();
//System.out.println("Runnable path:"+path);

			//韫囧懘銆忓妤冩暏鏉╂瑧顫掗弬瑙勭《娑撶rr鐠у锟介幍宥堝厴閺囧瓨鏌�
			List<String> temp = getDirs(path);
			for(int i = 0;i < temp.size();i++)
				arr.add(temp.get(i));
			Adapter.notifyDataSetChanged();
		}   	
    };
	private ArrayList<CSVProduct> csvlist;
	public ArrayList<CSVProduct> getCsvlist() {
		return csvlist;
	}
	public void setCsvlist(ArrayList<CSVProduct> csvlist) {
		this.csvlist = csvlist;
	}
	private OnItemClickListener lvLis=new OnItemClickListener(){
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			String temp = (String) arg0.getItemAtPosition(arg2);
//System.out.println("OnItemClick path1:"+path);			
			if(temp.equals(".."))
				path = getSubDir(path);
			else if(path.equals("/"))
				path = path+temp;
			else
				path = path+"/"+temp;
			
//System.out.println("OnItemClick path2"+path);	
			if(type == TypeOpen){
				int len = path.length();
				List<String> typeList = Arrays.asList(fileType);
				title.setText(path);
				if(path.charAt(len - 4) == '.'){
					if(typeList.contains(path.substring(len - 3))){
						dismiss();
						ArrayList<ArrayList<String>> dataAL = new ArrayList<ArrayList<String>>();
						dataAL = ReadCSVtoArrayList.readCSVToArrayList(path);
						ArrayList<String> headers = dataAL.get(0);
						if(headers.size() != HEADERS.length){
							Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
							return;
						}
						for(int i = 0, length = headers.size(); i < length; i++){
							if (!headers.get(i).equals(HEADERS[i])){
								Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
								return;
							}
						}
						dataAL.remove(0);
						for(ArrayList<String> list:dataAL){
							if(list.get(0).isEmpty() || list.get(1).isEmpty()){
								showAlertDialog("Item_Name and Item_No cannot be Null");
							}
							if(list.get(2).isEmpty())
								list.set(2, "0.00");
							if(list.get(3).isEmpty())
								list.set(3, "0.00");
							if(list.get(4).isEmpty())
								list.set(4, "0.00");
//							if(!isNumeric(list.get(2)) || !isNumeric(list.get(3)) || !isNumeric(list.get(4))){
//								showAlertDialog("Price, Cost and Stock should be a Number");
//							}
							if(list.get(5).isEmpty())
								list.set(5, "true");
							if(list.get(6).isEmpty())
								list.set(6, "false");
							if(list.get(7).isEmpty())
								list.set(7, "false");
							if(list.get(8).isEmpty())
								list.set(8, "none");
							if(list.size() == 9)
								list.add("10");
							list.add("/Date(" + System.currentTimeMillis() + ")/");
						}
//						byte[] data = ReadCSVtoArrayList.readCSVToByteArray(path);
						csvlist = new ArrayList<CSVProduct>();
						for(ArrayList<String> list: dataAL){
							try{
							CSVProduct product = new CSVProduct(list);
							csvlist.add(product);
							}catch(Exception e){
								Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
								return;
							}
						}
						m_adapter = new FileAdapter(context, dataAL);
						filelist.setAdapter(m_adapter);
					}else{
						Toast.makeText(context, "This is not a valid file type", Toast.LENGTH_SHORT).show();
					}
				}
			}
			
			Handler handler=new Handler();
	    	handler.post(add);
		}
    };
	 public static boolean isNumeric(String str)  
	 {  
	   try  
	   {  
	     double d = Double.parseDouble(str);  
	   }  
	   catch(NumberFormatException nfe)  
	   {  
	     return false;  
	   }  
	   return true;  
	 }
    private void showAlertDialog(String msg) {
    	// TODO Auto-generated method stub
    	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
    			context);

    		// set title
    		alertDialogBuilder.setTitle("Invalid Data");

    		// set dialog message
    		alertDialogBuilder
    			.setMessage(msg)
    			.setCancelable(false)
    			.setPositiveButton("OK",new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog,int id) {
    					// if this button is clicked, close
    					dialog.dismiss();
    				}
    			  });

    			// create alert dialog
    			final AlertDialog alertDialog = alertDialogBuilder.create();
    			alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
    			    @Override
    			    public void onShow(DialogInterface dialog) {
    			        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
    			        btnPositive.setHeight(100);
    			        btnPositive.setTextSize(20);
    			    }
    			});
    			// show it
    			alertDialog.show();
    }
	private List<String> getDirs(String ipath){
		List<String> file = new ArrayList<String>();
//System.out.println("GetDirs path:"+ipath);		
		File[] myFile = new File(ipath).listFiles();
		if(myFile == null){
			file.add("..");
			
		}else
			for(File f: myFile){
				//鏉╁洦鎶ら惄顔肩秿
				if(f.isDirectory()){
					String tempf = f.toString();
					int pos = tempf.lastIndexOf("/");
					String subTemp = tempf.substring(pos+1, tempf.length());
//					String subTemp = tempf.substring(path.length(),tempf.length());
					file.add(subTemp);	
//System.out.println("files in dir:"+subTemp);
				}
				//鏉╁洦鎶ら惌銉╀壕缁鐎烽惃鍕瀮娴狅拷
				if(f.isFile() && fileType != null){
					for(int i = 0;i< fileType.length;i++){
						int typeStrLen = fileType[i].length();
						
						String fileName = f.getPath().substring(f.getPath().length()- typeStrLen);
						if (fileName.toLowerCase().equals(fileType[i])) {
							file.add(f.toString().substring(path.length()+1,f.toString().length()));
						}
					}
				}
			}
		
		if(file.size()==0)
			file.add("..");
		
//		System.out.println("file[0]:"+file.get(0)+" File size:"+file.size());
		return file;
	}
	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == home.getId()){
			path = getRootDir();
			if(type == TypeOpen)
				title.setText(path);			
			Handler handler=new Handler();
	    	handler.post(add);
		}else if(v.getId() == back.getId()){
			path = getSubDir(path);
			if(type == TypeOpen)
				title.setText(path);			
			Handler handler=new Handler();
	    	handler.post(add);
		}else if(v.getId() == ok.getId()){
			dismiss();
			if(type == TypeSave)
				path = path+"/"+et.getEditableText().toString()+".wf";
			Toast.makeText(context, path, Toast.LENGTH_SHORT).show();
		}
			
		
	}
	
	private String getSDPath(){ 
	       File sdDir = null; 
	       boolean sdCardExist = Environment.getExternalStorageState()   
	                           .equals(android.os.Environment.MEDIA_MOUNTED);   //閸掋倖鏌噑d閸椻剝妲搁崥锕�鐡ㄩ崷锟�
	       if(sdCardExist)   
	       {                               
	         sdDir = Environment.getExternalStorageDirectory();//閼惧嘲褰囬弽鍦窗瑜帮拷
	      }   
	       if(sdDir == null){
//Toast.makeText(context, "No SDCard inside!",Toast.LENGTH_SHORT).show();
	    	   return null;
	       }
	       return sdDir.toString(); 
	       
	} 
	
	private String getRootDir(){
		String root = "/";
		
		path = getSDPath();
		if (path == null)
			path="/";
		
		return root;
	}
	
	private String getSubDir(String path){
		String subpath = null;
		
		int pos = path.lastIndexOf("/");
		
		if(pos == path.length()){
			path = path.substring(0,path.length()-1);
			pos = path.lastIndexOf("/");
		}
		
		subpath = path.substring(0,pos);
		
		if(pos == 0)
			subpath = path;
		
		return subpath;
	}
}
