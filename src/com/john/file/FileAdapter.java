package com.john.file;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.intlpos.sirclepos.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;


public class FileAdapter extends BaseAdapter {
//    private FileAdapter m_adapter = this;
	private List<ArrayList<String>> data;
	private LayoutInflater layoutInflater;
//	private Context context;
//	private PopupWindow mPop;
	CsvHolder holder = null;
	protected final int ITEM_NO = 0, ITEM_NAME = 1, ITEM_PRICE = 2, ITEM_COST = 3, ITEM_STOCK = 4, TAX1 = 5
						, TAX2 = 6, TAX3 = 7, DEPT_NAME = 8 , REORDER_LEVEL = 9;
	
	/* here we must override the constructor for ArrayAdapter
	* the only variable we care about now is ArrayList<Item> objects,
	* because it is the list of objects we want to display.
	*/
	public FileAdapter(Context context, List<ArrayList<String>> data) {
//		this.context=context;
		this.data=data;
		this.layoutInflater=LayoutInflater.from(context);
	}

	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list item will look.
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		 
//		Log.d("position", position+"");		
		//first check to see if the view is null. if so we have to inflate it.
		//to inflate it basically means to render , to show , the view.
		
		if(convertView == null)
		{			
			convertView = layoutInflater.inflate(R.layout.csvfilegrid, null);
			holder = new CsvHolder();
			holder.Item_No = (TextView) convertView.findViewById(R.id.item_number);
			holder.Item_Name = (TextView) convertView.findViewById(R.id.item_name);
			holder.Item_Price = (TextView) convertView.findViewById(R.id.item_price);
			holder.Item_Cost = (TextView) convertView.findViewById(R.id.item_cost);
			holder.Item_Stock = (TextView) convertView.findViewById(R.id.item_stock);
			holder.Tax1 = (TextView) convertView.findViewById(R.id.tax1);
			holder.Tax2 = (TextView) convertView.findViewById(R.id.tax2);
			holder.Tax3 = (TextView) convertView.findViewById(R.id.tax3);
			holder.Dept_Name = (TextView) convertView.findViewById(R.id.dept_name);
			holder.Reorder_Level = (TextView) convertView.findViewById(R.id.reorder_level);
			convertView.setTag(holder);
		}
		else
		{
			holder=(CsvHolder)convertView.getTag();
		}
		int color = convertView.getResources().getColor(R.color.Red);
		String item_no,item_name,tax1,tax2,tax3,item_price,item_cost,item_stock;
		
		item_no = data.get(position).get(ITEM_NO);
		if(item_no.isEmpty())
			holder.Item_Price.setBackgroundColor(color);
		else
			holder.Item_No.setText(item_no);

		item_name = data.get(position).get(ITEM_NAME);
		if(item_name.isEmpty())
			holder.Item_Price.setBackgroundColor(color);
		else
			holder.Item_Name.setText(item_name);
		
		item_price = data.get(position).get(ITEM_PRICE);
//		if(isNumeric(item_price))
//			holder.Item_Price.setBackgroundColor(color);
//		else
//			holder.Item_Price.setText(item_price);
//		
//		item_cost = data.get(position).get(ITEM_COST);
//		if(isNumeric(item_cost))
//			holder.Item_Price.setBackgroundColor(color);
//		else
//			holder.Item_Cost.setText(item_cost);
//		
//		item_stock = data.get(position).get(ITEM_STOCK);
//		if(isNumeric(item_stock))
//			holder.Item_Price.setBackgroundColor(color);
//		else
//			holder.Item_Stock.setText(item_stock);

		holder.Item_Price.setText(data.get(position).get(ITEM_PRICE));
		holder.Item_Cost.setText(data.get(position).get(ITEM_COST));
		holder.Item_Stock.setText(data.get(position).get(ITEM_STOCK));
		holder.Tax1.setText(data.get(position).get(TAX1));
		holder.Tax2.setText(data.get(position).get(TAX2));
		holder.Tax3.setText(data.get(position).get(TAX3));
		holder.Dept_Name.setText(data.get(position).get(DEPT_NAME));
		holder.Reorder_Level.setText(data.get(position).get(REORDER_LEVEL));
		
		return convertView;
	}
	
	 public final class CsvHolder 
		{
		 	TextView Item_No, Item_Stock, Dept_No, Item_Name,Item_Price,Item_Cost,Tax1, Tax2, Tax3, Dept_Name, Reorder_Level;
		}
	 public static boolean isNumeric(String str)  
	 {  
	   try  
	   {  
	     double d = Double.parseDouble(str);  
	   }  
	   catch(NumberFormatException nfe)  
	   {  
	     return false;  
	   }  
	   return true;  
	 }
	@Override
	public int getCount() {
		return data.size();
	}
	@Override
	public Object getItem(int position) {
		return data.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
}

