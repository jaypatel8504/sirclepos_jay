package com.john.file;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;
import android.widget.Toast;

public class ReadCSVtoArrayList {
    
    /**
     * Read csv file, return ArrayList
     * @param csvpath csv path
     * @return ArrayList, Generic can be another ArrayList of each column or just a String of each line.
     */
//	public static void main(String[] args){
//		String path = "C:/Users/Admin/Desktop/Intl POS missing details.csv";
//		ArrayList<ArrayList> arr = new ArrayList<ArrayList>();
//		arr = readCSVToArrayList(path);
//	}
	
    public static ArrayList<ArrayList<String>> readCSVToArrayList(String csvpath) {
        
        //Store all the data
        ArrayList<ArrayList<String>> dataAL = new ArrayList<ArrayList<String>>();
        
        //Read file
        BufferedReader reader;
        File file = new File(csvpath);
        if (!file.exists()){
        	Log.d("Tag", "File is not exists!");
        }
        try {
            
            reader = new BufferedReader(new FileReader(file));
//            reader.readLine();// Do we read first line (Normally store name of each colomn)
            String line = null;// temp store
            
            // Read data
            while ((line = reader.readLine()) != null) {
                
                //Store each line
                ArrayList<String> ticketStr = new ArrayList<String>();
                
                String item[] = line.split(",");//split by comma
                
                //clear last line
                ticketStr.clear();
                
                //read each column(optional)
                for(int i=0; i<item.length; i++){
                    
                    ticketStr.add(i, item[i]);
                    
                    //System.out.println(ticketStr.get(i));
                }
                
                dataAL.add(ticketStr);
                
                
            }
            
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return dataAL;
    }
    
public static ArrayList<String> readCSVToStringList(String csvpath) {
        
        //Store all the data
        ArrayList<String> dataAL = new ArrayList<String>();
        
        //Read file
        BufferedReader reader;
        File file = new File(csvpath);
        if (!file.exists()){
        	Log.d("Tag", "File is not exists!");
        }
        try {
            
            reader = new BufferedReader(new FileReader(file));
//            reader.readLine();// Do we read first line (Normally store name of each colomn)
            String line = null;// temp store
            
            // Read data
            if ((line = reader.readLine()) != null) {                
                dataAL.add(line);         
                
            }
            
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return dataAL;
    }

public static ArrayList<ArrayList<String>> readCSVToFixSizeStringList(String csvpath, int size) {
    
    //Store all the data
	ArrayList<ArrayList<String>> dataAL = new ArrayList<ArrayList<String>>();
    
    //Read file
    BufferedReader reader;
    File file = new File(csvpath);
    if (!file.exists()){
    	Log.d("Tag", "File is not exists!");
    }
    try {
        
        reader = new BufferedReader(new FileReader(file));
//        reader.readLine();// Do we read first line (Normally store name of each colomn)
        String line = null;// temp store
        ArrayList<String> tempArray = new ArrayList<String>();
        // Read data
        if ((line = reader.readLine()) != null) {                
        	tempArray.add(line);         
            if (tempArray.size() == size){
            	dataAL.add(tempArray);
            	//Log.d("String Array", line);
            	tempArray.clear();
            }
        }
        if (!tempArray.isEmpty())
        	dataAL.add(tempArray);
        
    } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    
    return dataAL;
}
public static byte[] readCSVToByteArray(String csvpath) {
    
    //Store all the data
    byte[] dataAL = null;
    
    //Read file
    File file = new File(csvpath);
    if (!file.exists()){
    	Log.d("Tag", "File is not exists!");
    }
    try {
        
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	copy(new FileInputStream(file), baos, 50);
    	dataAL = baos.toByteArray();            
        
    } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    
    return dataAL;
}
public static void copy(InputStream input,
	      OutputStream output,
	      int bufferSize)
	      throws IOException {
	    byte[] buf = new byte[bufferSize];
	    int bytesRead = input.read(buf);
	    while (bytesRead != -1) {
	      output.write(buf, 0, bytesRead);
	      bytesRead = input.read(buf);
	    }
	    output.flush();
	  }
}
