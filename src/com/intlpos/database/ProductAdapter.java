package com.intlpos.database;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import android.widget.Toast;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.dialogs.ManagerPromptDialog;
import com.intlpos.dialogs.TaxPopUpDialog;
import com.intlpos.global.KeyPad2;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.DiscountDialog;
import com.intlpos.sirclepos.InvoiceHome;
import com.intlpos.sirclepos.PromptDialogFragment;


public class ProductAdapter extends BaseAdapter{
    private static final int ANIMATION_DURATION = 200;
	private ProductAdapter m_adapter = this;
	private ArrayList<Map<String, Object>> data;
	private LayoutInflater layoutInflater;
	private Context context;
	private PopupWindow mPop;
	ProductHolder holder = null;
	private TaxExempt isTaxExempt;
	private int customerId = 1;
	private boolean grant;
	public Boolean getHaskeypad() {
		return haskeypad;
	}

	public ArrayList<Map<String, Object>> getData() {
		return data;
	}

	public void setData(ArrayList<Map<String, Object>> data) {
		this.data = data;
	}

	public void setHaskeypad(Boolean haskeypad) {
		this.haskeypad = haskeypad;
	}
	private KeyPad2 keypad;
	private BigDecimal[] tax_value = new BigDecimal[3];
	private Boolean haskeypad = false;
	private BigDecimal totalBalance,totalTax,totalGrandBalance,totalBalanceAfterWholeInvoiceDiscount,totalGrandBalanceAfterWholeInvoiceDiscount;
	BigDecimal[] totalTaxAfterWholeInvoiceDiscount;
	private BigDecimal[] subTax;
	
	private TextView total_subtotal_text,total_tax_text,total_balance_text;
	private Map<Long,Integer> product_ids;
	private Double wholeInvoiceDiscountAmt = 0.00,wholeInvoiceDiscountPer = 0.00;
	private DiscountDialog dialog;
	private CornerStorePOS app;
	private FragmentManager fm;
	private ListView lv;
	private Activity activity;
	/* here we must override the constructor for ArrayAdapter
	* the only variable we care about now is ArrayList<Item> objects,
	* because it is the list of objects we want to display.
	*/
	
	public ProductAdapter(Context context, List<Map<String,Object>> data, TextView total_subtotal_text, TextView total_tax_text, TextView total_balance_text,FragmentManager fm, ListView lv, Activity activity) {
		this.context=context;
		this.data=(ArrayList<Map<String, Object>>) data;
		this.layoutInflater=LayoutInflater.from(context);
		this.totalBalance = BigDecimal.ZERO;
		this.totalTax = BigDecimal.ZERO;
		this.app = (CornerStorePOS) context.getApplicationContext();
		this.subTax = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		totalTaxAfterWholeInvoiceDiscount = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		this.total_subtotal_text = total_subtotal_text;
		this.total_tax_text = total_tax_text;
		this.total_balance_text = total_balance_text;
		product_ids = new HashMap<Long, Integer>();
		this.fm = fm;
		this.lv = lv;
		this.activity= activity;
	}

	/*
	 * we are overriding the getView method here - this is what defines how each
	 * list item will look.
	 */
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		 final View tempView = convertView;
//		Log.d("position", position+"");		
		//first check to see if the view is null. if so we have to inflate it.
		//to inflate it basically means to render , to show , the view.
		
		if(convertView == null || ((ProductHolder) convertView.getTag()).needInflate)
		{			
			convertView = layoutInflater.inflate(R.layout.customgrid, null);
			holder = new ProductHolder();
			holder.Row_No = (TextView)convertView.findViewById(R.id.txtRow_Id);
			holder.ItemQty = (EditText)convertView.findViewById(R.id.txtItem_Qty);
			holder.Item_Name = (TextView)convertView.findViewById(R.id.txtItem_Name);
			holder.Item_Price = (EditText)convertView.findViewById(R.id.txtItem_Price);
			holder.discountButton = (Button)convertView.findViewById(R.id.btnItemDiscount);
			holder.taxButton = (Button)convertView.findViewById(R.id.btnItem_Tax);
			holder.deleteButton = (Button)convertView.findViewById(R.id.btnItem_delete);
			holder.needInflate = false;
			convertView.setTag(holder);
		}
		else
		{
			holder=(ProductHolder)convertView.getTag();
		}

		

		 WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 final int displaywidth = metrics.widthPixels;
		 final int displayheight = metrics.heightPixels;	
				
				
		if(data != null)
		{
			holder.Row_No.setText(""+(position+1));
					
			holder.Item_Price.setText(CornerStorePOS.dec_format.format(data.get(position).get("item_subtotal")));
			holder.Item_Price.setTag(position);
			holder.Item_Price.setInputType(InputType.TYPE_NULL);
			holder.Item_Price.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(final View view, MotionEvent event) {
					// TODO Auto-generated method stub
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						final EditText edittext = (EditText)view;
					if(app.permission.price_change == 1)//If the user has permission then they can change the price
						changePriceListener(position,false,view,edittext);
					else if(app.permission.price_change == 0){//If the user does not have permission then they cannot change the price
						Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
					}
					else{//User needs to be granted permission by entering manager access id
						final ManagerPromptDialog dialog = new ManagerPromptDialog(context);
						dialog.show();
						dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
						if(displaywidth < 1920 && displayheight < 1200)
							dialog.getWindow().setLayout(410, 450);
						else
							dialog.getWindow().setLayout(810, 850);
						dialog.getLogin().setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								String access = dialog.getAcess();
								getManagerPermission(access,"price",dialog,view,position,false,edittext);//Checks if they have access
							}
						});
					}
					return false;
					}
					return true;
				}
				
				
			});
			
			holder.discountButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View v) {
					if(app.permission.discount == 1)//User has access to discount an item
						discount(position, v);
					else if(app.permission.discount == 0)//User cannot discount an item
						Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
					else{//User needs to be granted permission by entering manager access id
						final ManagerPromptDialog dialog = new ManagerPromptDialog(context);
						dialog.show();
						dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
						if(displaywidth < 1920 && displayheight < 1200)
							dialog.getWindow().setLayout(410, 450);
						else
							dialog.getWindow().setLayout(810, 850);
						dialog.getLogin().setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								getManagerPermission(dialog.getAcess(),"discount",dialog,v,position,false,null);
							}
						});
					}
				}
	 
			});
			setItemQtyEditText(holder.ItemQty,position);
			holder.ItemQty.setInputType(0);
			holder.ItemQty.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(final View views, MotionEvent event) {
					if(event.getAction() == MotionEvent.ACTION_DOWN){
						if(app.permission.qty_change == 1){//User can change qty of items
							changeQtyListener(position,views,false);
						}else if(app.permission.qty_change == 0){//Cannot change qty of items
							Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
						}else{//User needs to be granted permission by entering manager access id
							final ManagerPromptDialog dialog = new ManagerPromptDialog(context);
							dialog.show();
							dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
							if(displaywidth < 1920 && displayheight < 1200)
								dialog.getWindow().setLayout(410, 450);
							else
								dialog.getWindow().setLayout(810, 850);
							dialog.getLogin().setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									getManagerPermission(dialog.getAcess(),"qty",dialog,views,position,false,null);
								}
							});
						}
					return false;}
					return true;
				}
				
				
			});
			String color = (String) data.get(position).get("color");
			holder.Item_Name.setTextColor(Color.parseColor(color));
			holder.Item_Name.setText((String)data.get(position).get("item_desc") + (String)data.get(position).get("note"));
			holder.Item_Name.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boolean mods = (boolean) data.get(position).get("hasmodifier");
					if(mods){
						showMods(position);
					}
				}
			});
			final String[] tax_desc = Tax.tax_desc;
			final View view2 = convertView;	
			//Log.d("Total_tax", (BigDecimal)data.get(position).get("total_taxrate")+"");
			if (!((BigDecimal)data.get(position).get("total_taxrate")).equals(BigDecimal.ZERO))
			{		
				LayoutParams btnPara = holder.taxButton.getLayoutParams();			
				final int btn_width = btnPara.width;
				final int btn_height = btnPara.height;
				holder.taxButton.setText(context.getString(R.string.tax));
				holder.taxButton.setClickable(true);		
				

				holder.taxButton.setOnClickListener(new OnClickListener() {
			
				@Override
				public void onClick(View v) {
					taxPopup(position,view2);

				}
				});
				holder.taxButton.setTag(position);
		}
		
		final View view = convertView;		
		holder.deleteButton.setOnClickListener(new OnClickListener(){  
		       
            public void onClick(View v) {  
                // TODO Auto-generated method stub  
            	final int position = Integer.parseInt(v.getTag().toString());  
                m_adapter.setTotalBalance(m_adapter.getTotalBalance().subtract((BigDecimal) data.get(position).get("price")));
            	if(app.permission.delete_button ==1){//user can delete item from grid
            		if(data.get(position).containsKey("hasmodifier")){
            			ArrayList<Map<String,Object>> deleteRows = new ArrayList<Map<String,Object>>();
            			int row = (int) data.get(position).get("size");
            			for(Map<String,Object>map:data){
            				if(map.containsKey("row")){
            					int mapRow = (int) map.get("row");
            					if(row == mapRow){
            						deleteRows.add(map);
            					}
            				}
            			}
            			for(Map<String,Object>map:deleteRows){
            				data.remove(map);
//            				notifyDataSetChanged();
            			}
            			deleteCell(view,position);
            		}else{
            			deleteCell(view,position);
            		}
            		
            	}else if(app.permission.delete_button == 0){//user cannot delete item from grid
            		Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
            	}
				else{//User needs to be granted permission by entering manager access id
					final ManagerPromptDialog dialog = new ManagerPromptDialog(context);
					dialog.show();
					dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
					if(displaywidth < 1920 && displayheight < 1200)
						dialog.getWindow().setLayout(410, 450);
					else
						dialog.getWindow().setLayout(810, 850);
					dialog.getLogin().setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							getManagerPermission(dialog.getAcess(),"delete",dialog,view,position,false,null);
						}
					});
				}
            		
            }});  
		holder.deleteButton.setTag(position); 
		}			
		
		product_ids.put((Long) data.get(position).get("product_id"), position);
		return convertView;
	}
	private void setItemQtyEditText(EditText itemQty,int position) {
		// TODO Auto-generated method stub
		if(!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_qty_keypad", false)){
			BigDecimal decimal = (BigDecimal) data.get(position).get("qty");
			itemQty.setText(decimal.setScale(2).toString());
		}else{
			itemQty.setText(data.get(position).get("qty").toString());
		}
	}

	private void UpdateAfterDiscount(int position,Double discount,Double amount)
	{
		Map<String, Object> itemMap = new HashMap(data.get(position));					
		BigDecimal[] tax = new BigDecimal[3];
		BigDecimal[] tax_current = new BigDecimal[3];
		BigDecimal[] taxrate = (BigDecimal[]) itemMap.get("taxrate");
		BigDecimal item_qty = (BigDecimal)itemMap.get("qty");
		BigDecimal percent = new BigDecimal((100 - discount)/100);		
		//BigDecimal item_price = ((BigDecimal) itemMap.get("item_subtotal")).multiply(percent);
		BigDecimal item_price = ((BigDecimal) itemMap.get("before_discount_current_price")).multiply(percent).setScale(2, BigDecimal.ROUND_HALF_EVEN);		
//		Log.d("item_subtotal", item_price.setScale(2, BigDecimal.ROUND_UP).toString());
		//itemMap.put("before_discount_current_price",);
		itemMap.put("current_price",item_price);		
		itemMap.put("current_price_after_wholeinvoicediscount", item_price);
		boolean isTaxExempt[] = new boolean[3];
		if(itemMap.containsKey("tax_exempt")){
			isTaxExempt = (boolean[]) itemMap.get("tax_exempt");
		}
		
		for (int i = 0; i < 3; i++){
			if(!isTaxExempt[i]){
				tax[i] = item_price.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				tax_current[i] = item_price.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			}else{
				tax[i] = item_price.multiply(item_qty).multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				tax_current[i] = item_price.multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			}
		}
		
		itemMap.put("item_subtotal", item_price.multiply(item_qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
		itemMap.put("tax", tax);
		itemMap.put("tax_exempt",isTaxExempt);
		itemMap.put("item_tax_after_wholeinvoicediscount",tax);
		itemMap.put("tax_current", tax_current);
		itemMap.put("total_tax", tax[0].add(tax[1]).add(tax[2]));
		String item_desc = itemMap.get("item_no") +"@"+itemMap.get("current_price")+"- Discounted "+discount+"%"+"\n"+ itemMap.get("item_name");
		itemMap.put("item_desc", item_desc);
		itemMap.put("percentage_discount",discount);
		
			
		
		// I had to do this seperatly because dollar_discount needs to be updated whenever change happens to the row
		
		BigDecimal discount_amount = ((BigDecimal) itemMap.get("before_discount_current_price")).multiply(new BigDecimal(discount/100));
		itemMap.put("dollar_discount",discount_amount.setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue());
		
		String item_name = (String)itemMap.get("item_name");
		
		
		data.set(position, itemMap);
		updateSubtotal();
		getProducts gp = (getProducts) activity;
		gp.sendDataToCFD();
		notifyDataSetChanged();
		displayMange(item_name,discount.toString(),item_price.multiply(item_qty).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),false);
	}
	
	public void UpdateAfterWholeInvoiceDiscount(Double discount)
	{
			totalBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO; 
			totalGrandBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO;
			totalTaxAfterWholeInvoiceDiscount = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
			BigDecimal percent = new BigDecimal((100 - discount)/100);
			
			for (Map<String, Object> map:data){
				
				BigDecimal current_price = (BigDecimal) map.get("current_price");
				BigDecimal qty = (BigDecimal) map.get("qty");
				BigDecimal current_price_afterwholeinvoicediscount = current_price.multiply(percent).setScale(2,BigDecimal.ROUND_HALF_EVEN);
				map.put("current_price_after_wholeinvoicediscount", current_price_afterwholeinvoicediscount);
				//map.put("whole_invoice_discount_amount", discount);
				totalBalanceAfterWholeInvoiceDiscount = totalBalanceAfterWholeInvoiceDiscount.add(current_price_afterwholeinvoicediscount.multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				
				 
				BigDecimal[] tax = new BigDecimal[3];
				BigDecimal[] taxrate = (BigDecimal[]) map.get("taxrate");
				
				boolean isTaxExempt[] = new boolean[3];
				if(map.containsKey("tax_exempt")){
					isTaxExempt = (boolean[]) map.get("tax_exempt");
				}
				
				for (int i = 0; i < 3; i++){
					if(!isTaxExempt[i]){
						tax[i] = current_price_afterwholeinvoicediscount.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					}else{
						tax[i] = current_price_afterwholeinvoicediscount.multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					}
					totalTaxAfterWholeInvoiceDiscount[i] = totalTaxAfterWholeInvoiceDiscount[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				}
				
				map.put("item_tax_after_wholeinvoicediscount",tax);
				map.put("tax", tax);

			}
			
			notifyDataSetChanged();
			
			// I had to do this seperatly because I need to change wholeInvoiceDiscount whenever changes happens to invoice
			wholeInvoiceDiscountAmt =  m_adapter.getTotalBalance().multiply(new BigDecimal(discount/100)).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
			setWholeInvoiceDiscountAmt(wholeInvoiceDiscountAmt);
			
			setTotalBalanceAfterWholeInvoiceDiscount(totalBalanceAfterWholeInvoiceDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
			setTotalTaxAfterWholeInvoiceDiscount(totalTaxAfterWholeInvoiceDiscount);
			setTotalGrandBalanceAfterWholeInvoiceDiscount(totalBalanceAfterWholeInvoiceDiscount.add(totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN));		
			BigDecimal subtotal,grandtotal;
			subtotal = totalBalanceAfterWholeInvoiceDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			grandtotal = totalGrandBalanceAfterWholeInvoiceDiscount;
			total_subtotal_text.setText(subtotal.toString());
			total_balance_text.setText(grandtotal.toString());
			Log.d("grandtotal", grandtotal.toString());
	        total_tax_text.setText((totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());        	
	        setTempTax tempTax = (setTempTax) context;
	       
//	        tempTax.setOldTaxes(totalTaxAfterWholeInvoiceDiscount);
	        subTax = totalTaxAfterWholeInvoiceDiscount;
	        
	        for(int i =0; i< 3; i++){
	        	Log.d("total",totalTaxAfterWholeInvoiceDiscount[i].toString());
	        }
		
	}
	
	public void updateTaxAllAfterWholeInvoiceDiscount(Double discount){//Recomputes the grand total after taxall button adds or removes a tax
		Log.d("it comes here", "here");
		totalBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO; 
		totalGrandBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO;
		totalTaxAfterWholeInvoiceDiscount = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		BigDecimal[] oldTotalTaxAfterWholeInvoiceDisocunt = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		BigDecimal percent = new BigDecimal((100 - discount)/100);
		
		for (Map<String, Object> map:data){
			BigDecimal current_price = (BigDecimal) map.get("current_price");
			BigDecimal qty = (BigDecimal) map.get("qty");
			BigDecimal current_price_afterwholeinvoicediscount = current_price.multiply(percent).setScale(2,BigDecimal.ROUND_HALF_EVEN);
			map.put("current_price_after_wholeinvoicediscount", current_price_afterwholeinvoicediscount);
			//map.put("whole_invoice_discount_amount", discount);
			totalBalanceAfterWholeInvoiceDiscount = totalBalanceAfterWholeInvoiceDiscount.add(current_price_afterwholeinvoicediscount.multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
			
			TaxExempt isTaxed = (TaxExempt) map.get("tax_exempt");
			Boolean taxes[] = null;
			if(isTaxed == null){
				if(isTaxExempt == null){
					
				}else{
					taxes = isTaxExempt.getIsTaxExempt();
				}
			}
			else{
				taxes = isTaxed.getIsTaxExempt();
			}
			BigDecimal[] oldTax = (BigDecimal[]) map.get("tax");
			BigDecimal[] tax = new BigDecimal[3];
			BigDecimal[] taxrate = (BigDecimal[]) map.get("taxrate");
					
			for (int i = 0; i < 3; i++){
				if(taxes == null){
					Log.d("ITS HERE", "GRRRR");
					tax[i] = current_price_afterwholeinvoicediscount.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					totalTaxAfterWholeInvoiceDiscount[i] = totalTaxAfterWholeInvoiceDiscount[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
					oldTotalTaxAfterWholeInvoiceDisocunt[i] = oldTotalTaxAfterWholeInvoiceDisocunt[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
					Log.d("ITS HERE", oldTotalTaxAfterWholeInvoiceDisocunt[i].toString());
				}
				else{
					if(taxes[i] == null){
						
					}
					else if(taxes[i] == false){
						
						tax[i] = current_price_afterwholeinvoicediscount.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
						totalTaxAfterWholeInvoiceDiscount[i] = totalTaxAfterWholeInvoiceDiscount[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
						oldTotalTaxAfterWholeInvoiceDisocunt[i] = oldTotalTaxAfterWholeInvoiceDisocunt[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
						Log.d("TAXEXISFALSE", oldTotalTaxAfterWholeInvoiceDisocunt[i].toString());
					}else if(taxes[i] == true){
						tax[i] = current_price_afterwholeinvoicediscount.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
						oldTotalTaxAfterWholeInvoiceDisocunt[i] = oldTotalTaxAfterWholeInvoiceDisocunt[i].add(tax[i].multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
						Log.d("TAXEX", oldTotalTaxAfterWholeInvoiceDisocunt[i].toString());
					}
				}
			}
			BigDecimal theTaxes[]=new BigDecimal[3];
			for(int i =0; i <3; i++){
				if(tax[i] == null){
					Log.d("TAXDISCOUNT","ZERO");
					tax[i] = BigDecimal.ZERO;
				}else{
					theTaxes[i] = tax[i];
					Log.d("TAXDISCOUNT",theTaxes[i].toString());
				}
			}
			map.put("item_tax_after_wholeinvoicediscount",theTaxes);
			map.put("tax", theTaxes);
			if(map.containsKey("prev_tax")){
				BigDecimal[] prev = (BigDecimal[]) map.get("prev_tax");
				for(int i = 0; i < 3; i++){
					if(prev[i] == null){
						Log.d("DISCOUNT", "nulprev");
					}
					else if(prev[i].compareTo(BigDecimal.ZERO) != 0){
						if(theTaxes[i] == null){//Need to calculate previous tax because the tax is zero
							Log.d("DISCOUNTNULL", prev[i].toString());
							prev[i] = prev[i];
						}else{
							Log.d("DISCOUNT", theTaxes[i].toString());
							prev[i] = theTaxes[i];
						}
					}
				}
				map.put("prev_tax", prev);
			}
			
			
		}
		
		notifyDataSetChanged();
		
		wholeInvoiceDiscountAmt =  m_adapter.getTotalBalance().multiply(new BigDecimal(discount/100)).setScale(2, BigDecimal.ROUND_HALF_EVEN).doubleValue();
		setWholeInvoiceDiscountAmt(wholeInvoiceDiscountAmt);
        BigDecimal totalSubTax = subTax[0].add(subTax[1]).add(subTax[2]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal TotalTax = totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        setTempTax tempTax = (setTempTax) context;
       
			setTotalTaxAfterWholeInvoiceDiscount(totalTaxAfterWholeInvoiceDiscount);
			setTotalGrandBalanceAfterWholeInvoiceDiscount(totalBalanceAfterWholeInvoiceDiscount.add(totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN));		
			BigDecimal subtotal,grandtotal;
			subtotal = totalBalanceAfterWholeInvoiceDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			grandtotal = totalGrandBalanceAfterWholeInvoiceDiscount;
			total_subtotal_text.setText(subtotal.toString());
			total_balance_text.setText(grandtotal.toString());
			Log.d("grandtotal", grandtotal.toString());
	        total_tax_text.setText((totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());       
	        
			
			for(int i = 0; i < 3; i++){
				Log.d("OLDTAxXa",oldTotalTaxAfterWholeInvoiceDisocunt[i].toString());
			}
			
			 subTax = totalTaxAfterWholeInvoiceDiscount;
	}
	
	public BigDecimal[] getTotalTaxAfterWholeInvoiceDiscount() {
		return totalTaxAfterWholeInvoiceDiscount;
	}
	
	public String getTotalTaxAdded(){
		return (totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString();
	}

	public void setTotalTaxAfterWholeInvoiceDiscount(BigDecimal[] totalTax_after_wholeInvoiceDiscount) {
		this.totalTaxAfterWholeInvoiceDiscount = totalTax_after_wholeInvoiceDiscount;
	}

	public BigDecimal getTotalBalanceAfterWholeInvoiceDiscount() {
		return totalBalanceAfterWholeInvoiceDiscount;
	}

	public void setTotalBalanceAfterWholeInvoiceDiscount(
			BigDecimal totalBalanceAfterWholeInvoiceDiscount) {
		this.totalBalanceAfterWholeInvoiceDiscount = totalBalanceAfterWholeInvoiceDiscount;
	}

	
	public BigDecimal getTotalGrandBalanceAfterWholeInvoiceDiscount() {
		return totalGrandBalanceAfterWholeInvoiceDiscount;
	}

	public void setTotalGrandBalanceAfterWholeInvoiceDiscount(
			BigDecimal totalGrandBalanceAfterWholeInvoiceDiscount) {
		this.totalGrandBalanceAfterWholeInvoiceDiscount = totalGrandBalanceAfterWholeInvoiceDiscount;
	}
	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}
	public BigDecimal getTotalGrandBalance() {
		return totalGrandBalance;
	}

	public void setTotalGrandBalance(BigDecimal totalGrandBalance) {
		this.totalGrandBalance = totalGrandBalance;
	}
	
	public Double getWholeInvoiceDiscountAmt() {
		return wholeInvoiceDiscountAmt;
	}

	public void setWholeInvoiceDiscountAmt(Double amount) {
		this.wholeInvoiceDiscountAmt = amount;
	}

	public Double getWholeInvoiceDiscountPer() {
		return wholeInvoiceDiscountPer;
	}

	public void setWholeInvoiceDiscountPer(Double discount) {
		this.wholeInvoiceDiscountPer = discount;
	}

	public void resetTheView() {
		this.product_ids.clear();
	}
	public BigDecimal getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(BigDecimal totalTax) {
		this.totalTax = totalTax;
	}

	public BigDecimal[] getSubTax() {
		return subTax;
	}

	public void setSubTax(BigDecimal[] subTax) {
		this.subTax = subTax;
	}

	public int convertToPx(int input) {
        // Get the screen's density scale
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
//        Log.d("DP to PX", "Px = "+(int) (input * scale + 0.5f));
        return (int) (input * scale + 0.5f);
	    }
	
	 public final class ProductHolder 
		{
		 	public boolean needInflate;
			Product product;			
			EditText ItemQty;
			TextView Row_No;
			//String Product_Id;
			TextView Item_Name;
			TextView Item_Price;
			TextView Item_Tax;
			Button deleteButton;
			Button discountButton;
			Button taxButton;
		}

	@Override
	public int getCount() {
		return data.size();
	}
	@Override
	public Object getItem(int position) {
		return data.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void ChangePrice(int position,BigDecimal NewPrice)
	{
		Map<String, Object> itemMap = new HashMap<String, Object>(data.get(position));
		
		BigDecimal qty = (BigDecimal) itemMap.get("qty");
		itemMap.put("current_price", NewPrice);
		itemMap.put("before_discount_current_price",NewPrice);	
		itemMap.put("current_price_after_wholeinvoicediscount", NewPrice);
	
		data.set(position, itemMap);
		notifyDataSetChanged();
		Double discount = (Double) itemMap.get("percentage_discount");
		Double amount = (Double) itemMap.get("dollar_discount");
		if(discount != 0.00)
		{
			UpdateAfterDiscount(position, discount, amount);
		}
		else
		{
		BigDecimal item_subtotal = NewPrice.multiply(qty).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		itemMap.put("item_subtotal",item_subtotal);
		BigDecimal[] tax = new BigDecimal[3];
		BigDecimal[] tax_current = new BigDecimal[3];
		BigDecimal[] taxrate = (BigDecimal[]) itemMap.get("taxrate");
		boolean isTaxExempt[] = new boolean[3];
		if(itemMap.containsKey("tax_exempt")){
			isTaxExempt = (boolean[]) itemMap.get("tax_exempt");
		}
		//BigDecimal qty = (BigDecimal) itemMap.get("qty");
		for (int i = 0; i < 3; i++){
			if(!isTaxExempt[i]){
				tax[i] = item_subtotal.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				tax_current[i] = NewPrice.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			}else{
				tax[i] = item_subtotal.multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				tax_current[i] = NewPrice.multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			}
		}
		//itemMap.put("current_price", item_subtotal.divide(qty, 2, RoundingMode.HALF_UP));
		String item_desc = itemMap.get("item_no") +"@"+itemMap.get("current_price")+"\n"+ itemMap.get("item_name");
		
		itemMap.put("item_desc", item_desc);
		itemMap.put("tax", tax);
		itemMap.put("item_tax_after_wholeinvoicediscount",tax);
		itemMap.put("tax_current",tax_current);
		itemMap.put("total_tax", tax[0].add(tax[1]).add(tax[2]));
		
		String item_name = (String)itemMap.get("item_name");
		
		
		data.set(position, itemMap);
		notifyDataSetChanged();
		updateSubtotal();
		getProducts gp = (getProducts) activity;
		gp.sendDataToCFD();
		displayMange(item_name,item_subtotal.toString());
		}
	}
	
	private void changeQtyListener(final int position, final View view, boolean hasFocus){
		final EditText edittext = (EditText)view;
		boolean keypad_type = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_qty_keypad", false); 
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 final int displaywidth = metrics.widthPixels;
		 final int displayheight = metrics.heightPixels;	
		 if(displaywidth < 1920 && displayheight < 1200)
			 keypad = new KeyPad2(edittext, LayoutInflater.from(context).inflate(R.layout.keypad2, null), 200, 300, true);
		 else
			 keypad = new KeyPad2(edittext, LayoutInflater.from(context).inflate(R.layout.keypad2, null), 400, 500, true);
		keypad.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				//data.get(position).put("price",new BigDecimal(edittext.getText().toString()));
				if (keypad.isFinished()){
												
					BigDecimal qty = new BigDecimal(edittext.getText().toString());
					ChangeQty(position,qty);
					if(data.get(position).containsKey("hasmodifier")){
						int row = (int) data.get(position).get("size");
						int i = 0;
            			for(Map<String,Object>map:data){
            				if(map.containsKey("row")){
            					int mapRow = (int) map.get("row");
            					if(row == mapRow){
            						ChangeQty(i,qty);
            					}
            				}
            				i++;
            			}
					}
					
				}else{
					setItemQtyEditText(edittext,position);
					
				}
		 		//Log.d("Current Price", data.get(position).get("current_price").toString());
				
				view.clearFocus();
				haskeypad = false;
			}
		});
		
			//When focus on
			if(!haskeypad)
			{
				if(keypad.isDecimalPad())
				{
					edittext.setText("0.00"); // for right to left keypad
					//edittext.setText("");
					keypad.setEdittext(edittext);							
				}else{
					edittext.setText("");
					keypad.setEdittext(edittext);	
				}
				keypad.showAsDropDown(view);
				haskeypad = true;
			}
		
	}
	
	public void ChangeQty(int position,BigDecimal NewQty)
	{
		Map<String, Object> itemMap = new HashMap(data.get(position));			
		itemMap.put("qty",NewQty);
		data.set(position, itemMap);
		notifyDataSetChanged();
		Double discount = (Double) itemMap.get("percentage_discount");
		Double amount = (Double) itemMap.get("dollar_discount");
		if(discount != 0.00)
		{
			UpdateAfterDiscount(position, discount, amount);
		}
		else
		{
			BigDecimal[] tax = new BigDecimal[3];
			BigDecimal[] tax_current = new BigDecimal[3];
			BigDecimal[] taxrate = (BigDecimal[]) itemMap.get("taxrate");
			BigDecimal current_price = (BigDecimal) itemMap.get("current_price");
			boolean isTaxExempt[] = new boolean[3];
			if(itemMap.containsKey("tax_exempt")){
				isTaxExempt = (boolean[]) itemMap.get("tax_exempt");
			}
			
			for (int i = 0; i < 3; i++){
				if(!isTaxExempt[i]){
					tax[i] = current_price.multiply(NewQty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					tax_current[i] = current_price.multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				}else{
					tax[i] = current_price.multiply(NewQty).multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					tax_current[i] = current_price.multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				}
			}
			itemMap.put("tax_exempt",isTaxExempt);
			itemMap.put("item_subtotal", current_price.multiply(NewQty).setScale(2, BigDecimal.ROUND_HALF_EVEN));
			itemMap.put("tax", tax);
			itemMap.put("item_tax_after_wholeinvoicediscount",tax);
			itemMap.put("tax_current",tax_current);
			itemMap.put("total_tax", tax[0].add(tax[1]).add(tax[2]));
			data.set(position, itemMap);
			
			String item_name = (String)itemMap.get("item_name");
			
			
			notifyDataSetChanged();
			updateSubtotal();
			getProducts gp = (getProducts) activity;
			gp.sendDataToCFD();
			displayMange(item_name,NewQty.toString(),current_price.multiply(NewQty).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString(),true);
		}
	}
	
	
	private void discount(int position, final View v){
		dialog = new DiscountDialog((BigDecimal) data.get(position).get("item_subtotal"),position,context);
		dialog.setTitle("Discount Item");
		TextView title = (TextView)dialog.findViewById( android.R.id.title );
		title.setTextColor( Color.parseColor("#000000") );
		int titleDividerId = context.getResources().getIdentifier("titleDivider", "id", "android");
		View titleDivider = dialog.findViewById(titleDividerId);
		if (titleDivider != null)
		    titleDivider.setBackgroundColor(Color.parseColor("#2f6699"));
        dialog.show();	       
		dialog.getSubmitBtn().setOnClickListener(new View.OnClickListener() {
	        /** After click OK, update the price TextView         *  
	         */
	            @Override
	            public void onClick(View v) {
					int position = ((DiscountDialog) dialog).getPosition();
					Double discount = Double.parseDouble(((DiscountDialog) dialog).getPercentage().toString());
					Double amount = Double.parseDouble(((DiscountDialog) dialog).getAmount().toString());
					UpdateAfterDiscount(position, discount,amount);
	            	dialog.dismiss();
	            }
	        });
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				// TODO Auto-generated method stub
				v.clearFocus();
			}
		});
	}
	
	public void addRow(Map<String,Object> item) {
		   // items represents List<RowData> in your Adapter class
		long product_id = (Long)item.get("product_id");
//		if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_combine_lines", false) == true)
//		{
//			boolean mod = (boolean) item.get("hasmodifier");
//			if(product_ids.containsKey(product_id) && !item.containsKey("row") && !mod)
//			{
//				int position = (Integer)product_ids.get(product_id);
//				BigDecimal item_qty = (BigDecimal)data.get(position).get("qty");
//				BigDecimal NewQty = item_qty.add(BigDecimal.ONE);
//				ChangeQty(position,NewQty);		
//			}else if(item.containsKey("row")){
//				mods(item,product_id);
//			}
//			else
//			{
//				addRowAfterCheck(item,false);
//			}
//		   			
//		}
//		else
//		{
			if(item.containsKey("No")){
				boolean same = true;
				int row = (int) item.get("row");
				long id = (long) item.get("product_id");
				int rid = (int)id;
				if(data.get(row-1).containsKey("noMods")){
					ArrayList<Integer>no =(ArrayList<Integer>) data.get(row-1).get("noMods");
					for(int x:no){
						if(rid == x){
							same = false;
						}
					}
					
				}
				int i = 0;
				ArrayList<Map<String,Object>>removeItem = new ArrayList<Map<String,Object>>();
				for(Map<String,Object>map:data){
					if(map.containsKey("row")){
						int rowid = (int) map.get("row");
						long pid = (long) map.get("product_id");
						if(rowid == row && pid == id){
							Log.d("Remove", Long.toString(pid));
							removeItem.add(map);
						}
					}
					i++;
				}
				for(Map<String,Object>map:removeItem){
					data.remove(map);
					notifyDataSetChanged();
				}
				if(same)//maybe change to else
					addRowAfterCheck(item,false);
			}else if(item.containsKey("row")){
				int row = (int) item.get("row");
				boolean add = true;
				for(Map<String,Object>map:data){
					long id = (long) map.get("product_id");
					if(map.containsKey("row")){
						int rowid = (int) map.get("row");
						if(id == product_id && row == rowid){
							add = false;
						}
					}
				}
				if(m_adapter.getCount() < 0){
					if(data.get(row-1).containsKey("noMods")){
						ArrayList<Integer>no =(ArrayList<Integer>) data.get(row-1).get("noMods");
						for(int i = 0; i < no.size(); i++){
							if(no.get(i) == (int)product_id){
								String item_name = "\nNo " + (String)item.get("item_name");
								String desc = (String) data.get(row-1).get("item_desc");
								desc = desc.replace(item_name, "");
								data.get(row-1).put("item_desc", desc);
								no.remove(i);
								notifyDataSetChanged();
								break;
							}
						}
					}
				}
				if(add){
					addRowAfterCheck(item,false);
				}
			}else
				addRowAfterCheck(item,false);
//		}
		
		}
	public Map<Long, Integer> getProduct_ids() {
		return product_ids;
	}

	public void setProduct_ids(Map<Long, Integer> product_ids) {
		this.product_ids = product_ids;
	}

	public void addRowAfterCheck(Map<String,Object> item,Boolean VariablePrice)
	{
		Map<String,Object> newRow = new HashMap(item);
		if(item.containsKey("No")){
			long pid = (long) item.get("product_id");
			int row = (int) item.get("row");
			String item_name = "\nNo " + (String)newRow.get("item_name");
			String desc = (String) data.get(row-1).get("item_desc");
			data.get(row-1).put("item_desc", desc+item_name);
			notifyDataSetChanged();
			if(data.get(row-1).containsKey("noMods")){
				ArrayList<Integer>no =(ArrayList<Integer>) data.get(row-1).get("noMods");
				no.add((int)pid);
				data.get(row-1).put("noMods", no);
			}else{
				ArrayList<Integer>no = new ArrayList<Integer>();
				no.add((int)pid);
				data.get(row-1).put("noMods", no);
			}
			return;
		}else{
			BigDecimal item_qty = BigDecimal.ONE;
			//String product_id = (String)newRow.get("product_id");
			BigDecimal[] tax_value = new BigDecimal[3];
			Boolean[] tax_rate_bool = new Boolean[3];
			BigDecimal[] tax_rate = new BigDecimal[3];
			tax_rate[0] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);	
			tax_rate[1] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			tax_rate[2] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			tax_rate_bool[0] = (Boolean) newRow.get("tax1");
			tax_rate_bool[1] = (Boolean) newRow.get("tax2");
			tax_rate_bool[2] = (Boolean) newRow.get("tax3");
			BigDecimal hundred = new BigDecimal(100);
			BigDecimal item_price;
			if(VariablePrice == true)
			{
				item_price = (BigDecimal)newRow.get("current_price");
			}
			else
			{
				item_price = (BigDecimal)newRow.get("price");
			}
			boolean ti = (boolean) newRow.get("tax_inclusive");
			if(ti){
				BigDecimal total_taxpercentage = BigDecimal.ONE;
				for(int i = 0; i < 3; i++){
					if(tax_rate_bool[i]){
						if(i == 0)
							total_taxpercentage= total_taxpercentage.add(Tax.tax_rate1.divide(hundred));
						else if(i == 1)
							total_taxpercentage= total_taxpercentage.add(Tax.tax_rate2.divide(hundred));
						else if(i == 2){
							total_taxpercentage= total_taxpercentage.add(Tax.tax_rate3.divide(hundred));
						}
					}
				}
				item_price = item_price.divide(total_taxpercentage,2, RoundingMode.HALF_UP);
				newRow.put("current_price", item_price);
				newRow.put("before_discount_current_price", item_price);
			}
			
			String item_no = (String)newRow.get("item_no");
			String item_name = (String)newRow.get("item_name");
			String item_desc = item_no +"@"+item_price+"\n"+ item_name;
			
			newRow.put("current_price_after_wholeinvoicediscount", item_price);
		
			//this.setTotalBalance(this.getTotalBalance().add(item_price));
			newRow.put("item_desc", item_desc);
			//newRow.put("current_price", item_price); // new updated price
			newRow.put("item_subtotal", item_price); // current_priceXQty
			newRow.put("qty", item_qty);
			newRow.put("note","");
			// end collect data at position
			
			//final BigDecimal[] tax_value = new BigDecimal[3];
			tax_value[0] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);	
			tax_value[1] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			tax_value[2] = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal totalTaxRate = BigDecimal.ZERO;
			if(tax_rate_bool[0] == true)
			{
				
				tax_rate[0] = Tax.tax_rate1.divide(hundred);
				Log.d("RATE", tax_rate[0].toString());
				tax_value[0] = item_price.multiply(Tax.tax_rate1.divide(hundred)); 
	//			subTax[0].add(tax_value[0]);
				//totalTax.add(tax_value[0]);
			}
			
			if(tax_rate_bool[1] == true)
			{
				Log.d("RATE", Tax.tax_rate2.toString());
				tax_rate[1] = Tax.tax_rate2.divide(hundred);
				tax_value[1] = item_price.multiply(Tax.tax_rate2.divide(hundred));
	//			subTax[1].add(tax_value[1]);
				//totalTax.add(tax_value[1]);
			}
		
			if(tax_rate_bool[2] == true)
			{
				tax_rate[2] = Tax.tax_rate3.divide(hundred);
				tax_value[2] = item_price.multiply(Tax.tax_rate3.divide(hundred));
	//			subTax[2].add(tax_value[2]);
				//totalTax.add(tax_value[2]);
			}
			totalTaxRate = tax_rate[0].add(tax_rate[1]).add(tax_rate[2]);
			newRow.put("taxrate", tax_rate);
			newRow.put("total_taxrate", totalTaxRate);
			newRow.put("tax", tax_value);
			newRow.put("item_tax_after_wholeinvoicediscount",tax_value);
			newRow.put("tax_current", tax_value); //Qty is one so same as tax_value
			newRow.put("total_tax", totalTaxRate.multiply(item_price));
			newRow.put("size",getCount()+1);
			if(newRow.containsKey("row")){
				newRow.put("color", "#ff0000");
				int row = (int) newRow.get("row");
				this.data.add(row,newRow);
			}else{
				newRow.put("color","#000000");
			    this.data.add(newRow);
			}
			
			updateSubtotal();
			notifyDataSetChanged();
			displayMange(item_name, item_price.toString());
		}
	}
	
	public void addHeldInvoice(Map<String,Object> item){
		Map<String,Object> newRow = new HashMap(item);
		this.data.add(newRow);
		updateSubtotal();
		notifyDataSetChanged();
		
	}
	private void updateSubtotal(){
		totalBalance = BigDecimal.ZERO;
		totalBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO;
		totalBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO; 
		totalGrandBalanceAfterWholeInvoiceDiscount = BigDecimal.ZERO;
		totalTax = BigDecimal.ZERO;
		subTax = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		totalTaxAfterWholeInvoiceDiscount = new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		for (Map<String, Object> map:data){
			BigDecimal[] tax = (BigDecimal[]) map.get("tax");
			totalBalance = totalBalance.add((BigDecimal) map.get("item_subtotal"));
			totalTax = totalTax.add((BigDecimal) map.get("total_tax"));
			subTax[0] = subTax[0].add(tax[0]);
			subTax[1] = subTax[1].add(tax[1]);
			subTax[2] = subTax[2].add(tax[2]);
		}
		subTax[0].setScale(2,BigDecimal.ROUND_HALF_EVEN);
		subTax[1].setScale(2,BigDecimal.ROUND_HALF_EVEN);
		subTax[2].setScale(2,BigDecimal.ROUND_HALF_EVEN);
		this.setSubTax(subTax);
		this.setTotalTaxAfterWholeInvoiceDiscount(subTax);
        setTotalBalanceAfterWholeInvoiceDiscount(totalBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        total_tax_text.setText(totalTax.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
        setTotalGrandBalance(totalBalance.add(totalTax).setScale(2, BigDecimal.ROUND_HALF_EVEN));
        setTotalGrandBalanceAfterWholeInvoiceDiscount(totalBalance.add(totalTax).setScale(2, BigDecimal.ROUND_HALF_EVEN));
        
		BigDecimal subtotal,grandtotal;
		subtotal = totalBalanceAfterWholeInvoiceDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		grandtotal = totalGrandBalanceAfterWholeInvoiceDiscount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		total_subtotal_text.setText(subtotal.toString());
		total_balance_text.setText(grandtotal.toString());
		Log.d("grandtotal", total_balance_text.getText().toString());
//		if(subtotal.compareTo(BigDecimal.ZERO) != -1)
//			total_subtotal_text.setText(subtotal.toString());
//		else
//			total_subtotal_text.setText("("+subtotal.toString()+")");
        total_tax_text.setText((totalTaxAfterWholeInvoiceDiscount[0].add(totalTaxAfterWholeInvoiceDiscount[1]).add(totalTaxAfterWholeInvoiceDiscount[2])).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());        	
//        if(grandtotal.compareTo(BigDecimal.ZERO) != -1)
//        	total_balance_text.setText(grandtotal.toString());
//        else
//        	total_balance_text.setText("("+grandtotal.toString()+")");//Haven't put it into Strings.xml
        if(getWholeInvoiceDiscountPer()!= 0.00)
        {
        	UpdateAfterWholeInvoiceDiscount(getWholeInvoiceDiscountPer());
        }        
	}
	
	
	private void deleteCell(final View v, final int index) {
		AnimationListener al = new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				String name = (String)data.get(index).get("item_name");
				BigDecimal price = (BigDecimal)data.get(index).get("item_subtotal");
				
                product_ids.remove(data.get(index).get("product_id"));
                data.remove(index);
                updateSubtotal();
                getProducts gp = (getProducts) activity;
        		gp.sendDataToCFD();
                displayManageDelete(name);
                ProductAdapter.this.notifyDataSetChanged();  

				ProductHolder vh = (ProductHolder) v.getTag();
				vh.needInflate = true;

				ProductAdapter.this.notifyDataSetChanged();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}
		};

		animHideShowView(v, al, 0, false, ANIMATION_DURATION);
	}

	
	public static void animHideShowView(final View v, AnimationListener al, int measureHeight, final boolean show, int ainmTime) {

		if (measureHeight == 0) {
			measureHeight = v.getMeasuredHeight();
		}
		final int heightMeasure = measureHeight;
		Animation anim = new Animation() {

			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {

				if (interpolatedTime == 1) {

					v.setVisibility(show ? View.VISIBLE : View.GONE);
				} else {
					int height;
					if (show) {
						height = (int) (heightMeasure * interpolatedTime);
					} else {
						height = heightMeasure - (int) (heightMeasure * interpolatedTime);
					}
					v.getLayoutParams().height = height;
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		if (al != null) {
			anim.setAnimationListener(al);
		}
		anim.setDuration(ainmTime);
		v.startAnimation(anim);
	}
	
	

	public interface setTempTax{
		void setOldTax(BigDecimal old[]);
		void setTaxExempt(boolean tax[]);
		void displayMange(String text, String Price);
	}

	
	private void changePriceListener(final int position,boolean hasFocus, final View v, final EditText edittext){
		 WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 final int displaywidth = metrics.widthPixels;
		 final int displayheight = metrics.heightPixels;	
		 if(displaywidth < 1920 && displayheight < 1200)
			 keypad = new KeyPad2(edittext, LayoutInflater.from(context).inflate(R.layout.keypad2, null), 200, 300, true);
		 else
			 keypad = new KeyPad2(edittext, LayoutInflater.from(context).inflate(R.layout.keypad2, null), 400, 500, true);
		keypad.setOnDismissListener(new OnDismissListener() {
			
			@Override
			public void onDismiss() {
				//data.get(position).put("price",new BigDecimal(edittext.getText().toString()));
				if (keypad.isFinished()){
					BigDecimal NewPrice = new BigDecimal(edittext.getText().toString());
					ChangePrice(position,NewPrice);
//					
				}else{
					edittext.setText(data.get(position).get("item_subtotal").toString()); // now it is subtotal
				}
				//Log.d("Current Price", data.get(position).get("current_price").toString());
				v.clearFocus();
				haskeypad = false;
			}
		});
		
			if(!haskeypad)
			{
				if(keypad.isDecimalPad())
				{
					edittext.setText("0.00"); // for right to left keypad
					//edittext.setText("");
					keypad.setEdittext(edittext);							
				}
				keypad.showAsDropDown(v);
				haskeypad = true;
			}
		
	}
	
	

	
	
	public void alterTaxExempt(){
		int pos = 0;
		for (Map<String, Object> map:data){
			if(map.containsKey("tax_exempt")){
				TaxExempt getTaxEx = (TaxExempt) map.get("tax_exempt");
				Boolean[] isTax = getTaxEx.getIsTaxExempt();
				Boolean getTaxall[] = isTaxExempt.getIsTaxExempt();


				
				Boolean[] tax_rate_bool = new Boolean[3];
				tax_rate_bool[0] = (Boolean) map.get("tax1");
				tax_rate_bool[1] = (Boolean) map.get("tax2");
				tax_rate_bool[2] = (Boolean) map.get("tax3");
				int s =0;
				for(Boolean taxRate:tax_rate_bool){
					if(taxRate){
						isTax[s] = getTaxall[s];
					}
					s++;
				}
				
				
//				for(int i = 0; i < 3; i++){
//					if(getTaxall[i] == null){}else{
//						isTax[i] = getTaxall[i];
//					}
//				}
				getTaxEx.setIsTaxExempt(isTax);
				map.put("tax_exempt",getTaxEx);
				data.set(pos, map);
				notifyDataSetChanged();
			}
			pos++;
		}
	}
	
	public void setCustId(int id){
		this.customerId = id;
	}
	
	public int getCustId(){
		return this.customerId;
	}
	
	private void getManagerPermission(String Acess, final String option, final ManagerPromptDialog window, View view, final int position,final boolean hasFocus,final EditText edittext){//Checks for permission for the feature the user wants
		final ProgressDialog ringProgressDialog;
		ringProgressDialog = ProgressDialog.show(context,context.getResources().getString(R.string.pleasewait), context.getResources().getString(R.string.loading));
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/getemployeebyid";
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);

		
		params.put("storedetail", storedetail);
		params.put("access_id",Acess);
		final View v = view;
		 JSONParser jParser = new JSONParser(new MyListener() {
			 
			 @Override
			 public void onResult(JSONObject json) {
				
				 try {
					if(json.getJSONObject("employee").getString("employees_id").isEmpty()){
						 window.dismiss();
						 ringProgressDialog.dismiss();
						 Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
						 v.clearFocus();
					}
					else{
						window.dismiss();
						ringProgressDialog.dismiss();
						
						 Permission permission = Permission.convertjsontopermission(json);
						 
						 if(option.equals("delete")){
							 if(permission.delete_button  == 1){
								 if(data.get(position).containsKey("hasmodifier")){
				            			ArrayList<Map<String,Object>> deleteRows = new ArrayList<Map<String,Object>>();
				            			int row = (int) data.get(position).get("size");
				            			for(Map<String,Object>map:data){
				            				if(map.containsKey("row")){
				            					int mapRow = (int) map.get("row");
				            					if(row == mapRow){
				            						deleteRows.add(map);
				            					}
				            				}
				            			}
				            			for(Map<String,Object>map:deleteRows){
				            				data.remove(map);
//				            				notifyDataSetChanged();
				            			}
								 }
								 deleteCell(v,position);
							 }
							 else{
								 Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
								 v.clearFocus();
							 }
								 
						 }else if(option.equals("discount")){
							 if(permission.discount == 1)
								 discount(position, v); 
							 else{
								Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
								v.clearFocus();
							 }
						 }else if(option.equals("price")){
							 if(permission.price_change  == 1)
								 changePriceListener(position,hasFocus,v,edittext);
							 else{
									Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
									v.clearFocus();
								 }
						 }else if(option.equals("qty")){
							 if(permission.qty_change  == 1)
								 changeQtyListener(position,v,hasFocus);
							 else{
									Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
									v.clearFocus();
							}
						 }
					}
					
				 } catch (JSONException e) {
					 window.dismiss();
					 ringProgressDialog.dismiss();
					 Toast.makeText(context, "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
					 v.clearFocus();
					 e.printStackTrace();
				 }
			 }
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);

	}
	private void showMods(int position){
		int rowID = (int) data.get(position).get("size");
		long Id = (long) data.get(position).get("product_id");
		int productId = (int) Id;
		ArrayList<ProductModifierValues> groupIds = null;
		for(int i = 0; i < app.ProductModifier.size();i++){
			if(app.ProductModifier.get(i).getProductKey() == productId){
				groupIds = new ArrayList<ProductModifierValues>(app.ProductModifier.get(i).getProductValues());
				break;
			}
		}
		if(!groupIds.isEmpty()){
			String[] item_name = null;
			int[] item_id = null;
			String dialogTitle = "";
			String pic[] = null;
			String prices[] = null;
			
			for(int i = 0; i < app.GroupProducts.size(); i++){
				int index = 0;
				Log.d("index and i", Integer.toString(app.GroupProducts.get(i).getKey()) + "\n index: "+ groupIds.get(index).getModifierGroupId());
				if(app.GroupProducts.get(i).getKey() == groupIds.get(index).getModifierGroupId()){
					if(!app.GroupProducts.get(i).getModProducts().isEmpty()){
						item_name = new String[app.GroupProducts.get(i).getModProducts().size()];
						item_id = new int[app.GroupProducts.get(i).getModProducts().size()];
						pic = new String[app.GroupProducts.get(i).getModProducts().size()];
						prices = new String[app.GroupProducts.get(i).getModProducts().size()];
						for(int a = 0; a < app.GroupProducts.get(i).getModProducts().size(); a++){
							item_name[a] = app.GroupProducts.get(i).getModProducts().get(a).getItemName();
							prices[a] = app.GroupProducts.get(i).getModProducts().get(a).getPrice();
							item_id[a] = app.GroupProducts.get(i).getModProducts().get(a).getProductId();
							pic[a] = app.GroupProducts.get(i).getModProducts().get(a).getPic();
						}
						break;
					}
				}
			}
			ArrayList<Integer>selected = new ArrayList<Integer>();
			for(Map<String, Object>map:data){
				if(map.containsKey("row")){
					int row = (int) map.get("row");
					if(row == rowID){
						long pid = (long) map.get("product_id");
						selected.add((int) pid);
					}
				}
			}
			ArrayList<Integer>no = null;
			if(data.get(position).containsKey("noMods")){
				no =(ArrayList<Integer>) data.get(position).get("noMods");
			}
			
			for(ModifierGroups mg: app.ModifierGroup){
				if(mg.getGroupId() == groupIds.get(0).getModifierGroupId()){
					dialogTitle = mg.getPrompt();
					break;
				}
			}
			if(dialogTitle.isEmpty())
				dialogTitle = "Mods";
			
			groupIds.remove(0);
			GroupIds listener =(GroupIds) activity;
			listener.setGroupIds(groupIds);
			PromptDialogFragment alert = PromptDialogFragment.newInstance(item_name,item_id,dialogTitle,pic,true,rowID,InvoiceHome.convertIntegers(selected),InvoiceHome.convertIntegers(no),prices, true);
			alert.show(fm, "Alert_Dialog");
		}
	}
	
	public View getViewByPosition(int pos, ListView listView) {
	    final int firstListItemPosition = listView.getFirstVisiblePosition();
	    final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

	    if (pos < firstListItemPosition || pos > lastListItemPosition ) {
	        return listView.getAdapter().getView(pos, null, listView);
	    } else {
	        final int childIndex = pos - firstListItemPosition;
	        return listView.getChildAt(childIndex);
	    }
	}
	
	public  void removeItems(int row, ArrayList<Integer>remove){
		ArrayList<Map<String,Object>>deleted = new ArrayList<Map<String,Object>>();
		for(Map<String,Object>map:data){
			if(map.containsKey("row")){
				int mapRow = (int) map.get("row");
				long mapId = (long) map.get("product_id");
				if(mapRow == row){
					for(int delete:remove){
						if(delete == (int)mapId){
							deleted.add(map);
						}
					}
				}
			}
		}
		for(Map<String,Object>map:deleted){
			data.remove(map);
			notifyDataSetChanged();
		}
		
		if(data.get(row-1).containsKey("noMods")){
			ArrayList<Integer>removeNo = new ArrayList<Integer>();
			ArrayList<Integer>no =(ArrayList<Integer>) data.get(row-1).get("noMods");
			Log.d("No", "mods");
			
				for(int i =0; i <remove.size(); i++){
					if(no.contains(remove.get(i))){
						Log.d("No/",Integer.toString(remove.get(i))+ " \n no: ");
						int item= remove.get(i);
						for (Map.Entry<String, ArrayList<HashMap<String, Object>>> entry : CornerStorePOS.PMap.entrySet())
						{				   
						   ArrayList<HashMap<String, Object>> sListValue = entry.getValue();
						   for (int a =0; a<sListValue.size();a++)
					        {
							    HashMap<String, Object> tmpData = (HashMap<String, Object>) sListValue.get(a);
							    long pid = (long)tmpData.get("product_id");
							    if((int)pid == item){
							    	String item_name = "\nNo " + (String)tmpData.get("item_name");
									Log.d("item_name", item_name);
									String desc = (String) data.get(row-1).get("item_desc");
									desc = desc.replace(item_name, "");
									data.get(row-1).put("item_desc", desc);
									removeNo.add(remove.get(i));
									notifyDataSetChanged();
							    }
					        }       				   					   
						}
						
					}
				}
			
			
			
			for(int i = 0; i < removeNo.size();i++){
				no.remove(removeNo.get(i));
			}
			if(no.isEmpty())
				data.get(row-1).remove("noMods");
			else
				data.get(row-1).put("noMods",no);
		}
	}
	
	public interface GroupIds{
		public void setGroupIds(ArrayList<ProductModifierValues> groups);
	}
	
	public interface getProducts{
		public void sendDataToCFD();
	}
	
	public void alterAllTaxes(boolean isTaxExempt[],TaxPopUpDialog taxdialog){
		for(Map<String,Object>map:data){
			BigDecimal[] tax_value = new BigDecimal[3];//New tax values after alteration
			Boolean[] tax_rate_bool = new Boolean[3];//check which taxes apply to an item
			
			BigDecimal current_price = (BigDecimal) map.get("current_price");//Get Price after price change and/or discount
			BigDecimal[] taxrate = (BigDecimal[]) map.get("taxrate");//Get taxrate for those who have taxes
			BigDecimal item_qty = (BigDecimal)map.get("qty");
			tax_rate_bool[0] = (Boolean) map.get("tax1");
			tax_rate_bool[1] = (Boolean) map.get("tax2");
			tax_rate_bool[2] = (Boolean) map.get("tax3");
			
			int index = 0;
			boolean[] tax_exempt = new boolean[3];
			for(Boolean taxRate:tax_rate_bool){
				
				if(taxRate){
					tax_exempt[index] = isTaxExempt[index];
					if(isTaxExempt[index])
						tax_value[index] = current_price.multiply(BigDecimal.ZERO); 
					else{
						tax_value[index] = current_price.multiply(item_qty).multiply(taxrate[index]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
						Log.d("taxValues", tax_value[index].toString());
					}
				}else{
					tax_value[index] = BigDecimal.ZERO; 
				}
				index++;
			}
			
			map.put("tax_exempt", tax_exempt);
			map.put("tax", tax_value);
			map.put("item_tax_after_wholeinvoicediscount",tax_value);
			map.put("total_tax", tax_value[0].add(tax_value[1]).add(tax_value[2]));
		}
		notifyDataSetChanged();
		updateSubtotal();
		getProducts gp = (getProducts) activity;
		gp.sendDataToCFD();
	}
	
	
	private void taxPopup(final int position, View view2){
		BigDecimal []gettaxdisocunt = (BigDecimal[]) data.get(position).get("item_tax_after_wholeinvoicediscount");
		Boolean[] tax_rate_bool = new Boolean[3];//check which taxes apply to an item
		BigDecimal current_price = null;
		if(data.get(position).containsKey("current_price_after_wholeinvoicediscount")){
			 current_price = (BigDecimal) data.get(position).get("current_price_after_wholeinvoicediscount");
		}else{
			current_price = (BigDecimal) data.get(position).get("current_price");//Get Price after price change and/or discount
		}
		final BigDecimal[] taxrate = (BigDecimal[]) data.get(position).get("taxrate");//Get taxrate for those who have taxes
		final BigDecimal item_qty = (BigDecimal)data.get(position).get("qty");
		tax_rate_bool[0] = (Boolean) data.get(position).get("tax1");
		tax_rate_bool[1] = (Boolean) data.get(position).get("tax2");
		tax_rate_bool[2] = (Boolean) data.get(position).get("tax3");
		
		boolean isTaxExempt[] = new boolean[3];
		if(data.get(position).containsKey("tax_exempt")){
			isTaxExempt = (boolean[]) data.get(position).get("tax_exempt");
		}
		
		BigDecimal oldValues[] = {BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		for(int i = 0; i < 3; i++){
			if(isTaxExempt[i]){
				if(tax_rate_bool[i]){
					oldValues[i] = current_price.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				}
			}else{//?
				if(tax_rate_bool[i]){
					gettaxdisocunt[i] = current_price.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
				}
			}
		}
		
		final TaxPopUpDialog taxdialog = new TaxPopUpDialog(context,new TaxExemption(false, isTaxExempt, gettaxdisocunt, oldValues));
		taxdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = taxdialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();
		
		wlp.gravity = Gravity.TOP | Gravity.LEFT;
		wlp.x = (int) holder.taxButton.getX() - 130;   //x position
		wlp.y = (int) view2.getY() - 5;   //y position
		final BigDecimal current_pricedialog =  current_price;
		taxdialog.show();
		taxdialog.setCancelable(false);
		taxdialog.getOK().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				boolean isTaxExempt[] = taxdialog.getTaxExempt();
				BigDecimal[] tax = new BigDecimal[3];
				for (int i = 0; i < 3; i++){
					if(!isTaxExempt[i]){
						tax[i] = current_pricedialog.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
					}else{
						tax[i] = current_pricedialog.multiply(item_qty).multiply(BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_EVEN);
						setTempTax invoice = (setTempTax) context;
						invoice.setTaxExempt(isTaxExempt);
					}
				}
				data.get(position).put("tax_exempt", isTaxExempt);
				data.get(position).put("tax", tax);
				data.get(position).put("item_tax_after_wholeinvoicediscount",tax);
				data.get(position).put("total_tax", tax[0].add(tax[1]).add(tax[2]));
				taxdialog.dismiss();
				notifyDataSetChanged();
				updateSubtotal();
				getProducts gp = (getProducts) activity;
				gp.sendDataToCFD();
//				checkIfAltered();
			}
		});	
	}
	
	public void setOldTax(boolean isTaxExempt[]){
		BigDecimal oldTaxes[] = {BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		for(Map<String,Object>map:data){
			BigDecimal current_price = null;
			if(map.containsKey("current_price_after_wholeinvoicediscount")){
				 current_price = (BigDecimal) map.get("current_price_after_wholeinvoicediscount");
			}else{
				current_price = (BigDecimal) map.get("current_price");//Get Price after price change and/or discount
			}
			final BigDecimal[] taxrate = (BigDecimal[]) map.get("taxrate");//Get taxrate for those who have taxes
			final BigDecimal item_qty = (BigDecimal) map.get("qty");
			Boolean[] tax_rate_bool = new Boolean[3];//check which taxes apply to an item
			tax_rate_bool[0] = (Boolean) map.get("tax1");
			tax_rate_bool[1] = (Boolean) map.get("tax2");
			tax_rate_bool[2] = (Boolean) map.get("tax3");
			
			BigDecimal []gettaxdisocunt = (BigDecimal[]) map.get("item_tax_after_wholeinvoicediscount");
			for(int i =0;i <3;i++){
				if(tax_rate_bool[i]){
					if(isTaxExempt[i]){
						oldTaxes[i]= oldTaxes[i].add(current_price.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN));
					}
				}
			}
		}
		
		setTempTax invoice = (setTempTax) context;
		invoice.setOldTax(oldTaxes);
	}
	
	public void checkIfAltered(){
		BigDecimal oldTaxes[] = {BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		for(Map<String,Object>map:data){
			Boolean[] tax_rate_bool = new Boolean[3];//check which taxes apply to an item
			tax_rate_bool[0] = (Boolean) map.get("tax1");
			tax_rate_bool[1] = (Boolean) map.get("tax2");
			tax_rate_bool[2] = (Boolean) map.get("tax3");
			
			final BigDecimal current_price = (BigDecimal) map.get("current_price");//Get Price after price change and/or discount
			final BigDecimal[] taxrate = (BigDecimal[]) map.get("taxrate");//Get taxrate for those who have taxes
			final BigDecimal item_qty = (BigDecimal) map.get("qty");
			
			if(map.containsKey("tax_exempt")){
				boolean []isTaxExempt = (boolean[]) map.get("tax_exempt");
				for(int i = 0; i<3;i++){
					if(isTaxExempt[i]){
						if(tax_rate_bool[i]){
							BigDecimal temp = current_price.multiply(item_qty).multiply(taxrate[i]).setScale(2, BigDecimal.ROUND_HALF_EVEN);
							oldTaxes[i] = oldTaxes[i].add(temp);
						}
					}
				}
			}
		}
		setTempTax invoice = (setTempTax) context;
		invoice.setOldTax(oldTaxes);
	}
	public void displayMange(String text, String Price){
		if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true){
	    	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
	    	String print = preferences.getString("printerType", "");
	    	if(print.equals("Partner Tech")){
	    	   String dis = text;
			   
	    	   if(Price.length() == 4){
	    		   if(dis.length() < 16){
	    			   dis = spaces(dis, 16);
	    			}else if(dis.length() > 16){
	    			   dis = dis.substring(0, 15) + " ";
	    			}
	    	   }else if(Price.length() == 5){
	    		    if(dis.length() < 15){
	    			   dis =  spaces(dis, 15);
	    		    }else if(dis.length() > 15){
	    			   dis = dis.substring(0, 14) + " ";
	    			}
	    	   }else if(Price.length() == 6){
	    			if(dis.length() < 14){
	    			   dis =spaces(dis, 14);
	    			}else if(dis.length() > 14){
	    			   dis = dis.substring(0, 13) + " ";
	    			}
	    	   }else if(Price.length() == 7){
	    		   	   if(dis.length() < 13){
	    				   dis = spaces(dis, 13);
	    			   }else if(dis.length() > 13){
	    				   dis = dis.substring(0, 12) + " ";
	    			   }
	    	   }
	    	   
	    	   
			   setTempTax display = (setTempTax) context;
			   display.displayMange(dis+Price,totalGrandBalanceAfterWholeInvoiceDiscount.toString());
	    	}
		}
	}
	
	public void displayMange(String text, String qty, String Price, boolean qtyChange){
		if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true){
	    	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
	    	String print = preferences.getString("printerType", "");
	    	if(print.equals("Partner Tech")){
	    	   
	    	   int text_length  = 0;
	    	   String dis = text;
	    	   if(Price.length() == 4){
	    		   if(qty.length() == 3){
	    			   if(dis.length() < 9){
	    				   dis = spaces(dis, 9);
	    			   }else if(dis.length() > 9){
	    				   dis = dis.substring(0, 9);
	    			   }
	    		   }else if(qty.length() == 4){
	    			   if(dis.length() < 8){
	    				   dis = spaces(dis, 8);
	    			   }else if(dis.length() > 8){
	    				   dis = dis.substring(0, 8);
	    			   }
	    		   }else if(qty.length() == 5){
	    			   if(dis.length() < 7){
	    				   dis = spaces(dis, 7);
	    			   }else if(dis.length() > 7){
	    				   dis = dis.substring(0, 7);
	    			   }
	    		   }
	    	   }else if(Price.length() == 5){
	    		   if(qty.length() == 3){
	    			   if(dis.length() < 8){
	    				   dis = spaces(dis, 8);
	    			   }else if(dis.length() > 8){
	    				   dis = dis.substring(0, 8);
	    			   }
	    		   }else if(qty.length() == 4){
	    			   if(dis.length() < 7){
	    				   dis = spaces(dis, 7);
	    			   }else if(dis.length() > 7){
	    				   dis = dis.substring(0, 7);
	    			   }
	    		   }else if(qty.length() == 5){
	    			   if(dis.length() < 6){
	    				   dis =  spaces(dis, 6);
	    			   }else if(dis.length() > 6){
	    				   dis = dis.substring(0, 6);
	    			   }
	    		   }
	    	   }else if(Price.length() == 6){
	    		   if(qty.length() == 3){
	    			   if(dis.length() < 7){
	    				   dis = spaces(dis, 7);
	    			   }else if(dis.length() > 7){
	    				   dis = dis.substring(0, 7);
	    			   }
	    		   }else if(qty.length() == 4){
	    			   if(dis.length() < 6){
	    				   dis =spaces(dis, 6);
	    			   }else if(dis.length() > 6){
	    				   dis = dis.substring(0, 6);
	    			   }
	    		   }else if(qty.length() == 5){
	    			   if(dis.length() < 5){
	    				   dis =spaces(dis, 5);
	    			   }else if(dis.length() > 5){
	    				   dis = dis.substring(0, 5);
	    			   }
	    		   }
	    	   }else if(Price.length() == 7){
	    		   if(qty.length() == 3){
	    			   if(dis.length() < 6){
	    				   dis = spaces(dis, 6);
	    			   }else if(dis.length() > 6){
	    				   dis = dis.substring(0, 6);
	    			   }
	    		   }else if(qty.length() == 4){
	    			   if(dis.length() < 5){
	    				   dis = spaces(dis, 5);
	    			   }else if(dis.length() > 5){
	    				   dis = dis.substring(0, 5);
	    			   }
	    		   }else if(qty.length() == 5){
	    			   if(dis.length() < 4){
	    				   dis = spaces(dis, 4);
	    			   }else if(dis.length() > 4){
	    				   dis = dis.substring(0, 4);
	    			   }
	    		   }
	    	   }
	    	   
	    	   if(qtyChange)
	    		   dis = dis + " x " + qty + " " + Price;
	    	   else
	    		   dis = dis + " % " + qty + " " + Price;
	    	   setTempTax display = (setTempTax) context;
			   display.displayMange(dis,totalGrandBalanceAfterWholeInvoiceDiscount.toString());
	    	}
		}
	}
	
	void displayManageDelete(String item){
		if(item.length() < 13){
			item = spaces(item, 13);
		}else if(item.length() > 13){
			item = item.substring(0,13);
		}
		item += " Voided";
		setTempTax display = (setTempTax) context;
		display.displayMange(item,totalGrandBalanceAfterWholeInvoiceDiscount.toString());
	}
	
	public static String spaces(String text, int max){
		int spaces = max - text.length();
		 for(int i = 0; i < spaces; i++){
			 text += " ";
		   }
		 return text;
	}
}

