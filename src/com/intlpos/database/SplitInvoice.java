package com.intlpos.database;

import java.math.BigDecimal;
import java.util.ArrayList;

import android.util.Log;
import com.intlpos.sirclepos.R;

public class SplitInvoice {

	 public long split_invoice_id;
     public int payment_type;
     public String ref_no;
     public String auth_code;
     public BigDecimal total;
     public String transaction_id;
     public String last_four;
     public String card_holder_name;
     public long invoice_id;
     
     
//     1- Split Invoice
//     2 - cash
//     3 - credit
//     4 - debit
//     5- gift card
//     6 - food stamp
//     7 - To Account
     
     public ArrayList<SplitInvoice> convertToSplitInvoice(ArrayList<PaymentType> splitpayments)
     {
    	 int count = 0 ;
    	 ArrayList<SplitInvoice> split_invoice_list = new ArrayList<SplitInvoice>();
    	
    	 for(PaymentType payment :splitpayments)
    	 {
    		SplitInvoice splitinvoice = new SplitInvoice();
    		switch (payment.type)
    		{
    			case "Cash":
    				if(payment.amount != BigDecimal.ZERO )
    				{
    					splitinvoice.payment_type = 2;
    					splitinvoice.total = payment.amount;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "Credit":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 3;
    					splitinvoice.total = payment.amount;
    					splitinvoice.auth_code = payment.auth_code;
    					splitinvoice.ref_no = payment.ref_no;
    					splitinvoice.last_four = payment.last4Digit;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "Debit":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 4;
    					splitinvoice.total = payment.amount;
    					splitinvoice.auth_code = payment.auth_code;
    					splitinvoice.ref_no = payment.ref_no;
    					splitinvoice.last_four = payment.last4Digit;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "Gift Card":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 5;
    					splitinvoice.total = payment.amount;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "EBT":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 6;
    					splitinvoice.total = payment.amount;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "To Account":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 7;
    					splitinvoice.total = payment.amount;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    			case "Check":
    				if(payment.amount != BigDecimal.ZERO)
    				{
    					splitinvoice.payment_type = 8;
    					splitinvoice.total = payment.amount;
    					splitinvoice.auth_code = payment.auth_code;
    					split_invoice_list.add(splitinvoice);
    				}
    				break;
    		}
    	 }
    	 return split_invoice_list;
     }


	
}
