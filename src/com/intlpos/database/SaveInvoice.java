package com.intlpos.database;

import java.util.ArrayList;

public class SaveInvoice {
	
	private String OrderNameId;
	private String date;
	private ArrayList<Order> orders;
	private Double discount;
	
	
	public String getOrderNameId() {
		return OrderNameId;
	}
	public void setOrderNameId(String orderNameId) {
		OrderNameId = orderNameId;
	}
	public ArrayList<Order> getOrders() {
		return orders;
	}
	public void setOrders(ArrayList<Order> orders) {
		this.orders = orders;
	}
	
	public SaveInvoice(String orderNameId, String date, ArrayList<Order> orders, Double discount) {
		this.OrderNameId = orderNameId;
		this.orders = orders;
		this.date = date;
		this.discount = discount;
	}
	
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
