package com.intlpos.database;

public class ProductModifierValues {
	private boolean autoRingUp;
	private int modifierGroupId;
	private int modifierGroupOrder;
	
	public boolean isAutoRingUp() {
		return autoRingUp;
	}
	public void setAutoRingUp(boolean autoRingUp) {
		this.autoRingUp = autoRingUp;
	}
	public int getModifierGroupId() {
		return modifierGroupId;
	}
	public void setModifierGroupId(int modifierGroupId) {
		this.modifierGroupId = modifierGroupId;
	}
	public int getModifierGroupOrder() {
		return modifierGroupOrder;
	}
	public void setModifierGroupOrder(int modifierGroupOrder) {
		this.modifierGroupOrder = modifierGroupOrder;
	}
}
