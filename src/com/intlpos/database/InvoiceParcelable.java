package com.intlpos.database;

import java.util.HashMap;   

import com.john.beans.FetchInvoice;

import android.os.Parcel;   
import android.os.Parcelable;   
  
public class InvoiceParcelable implements Parcelable {   
    
    private FetchInvoice invoice;  
    
    public InvoiceParcelable(){
    }
    
    public InvoiceParcelable(FetchInvoice invoice){
    	this.invoice = invoice;
    }
	public FetchInvoice getInvoice() {
		return invoice;
	}

	public void setInvoice(FetchInvoice invoice) {
		this.invoice = invoice;
	}

	@Override  
    public int describeContents() {   
        return 0;   
    }   
  
    @Override  
    public void writeToParcel(Parcel dest, int flags) {   
  
        dest.writeValue(invoice);   
    }   
    public static final Parcelable.Creator<InvoiceParcelable> CREATOR = new Parcelable.Creator<InvoiceParcelable>() {   
    //Rewrite Creator
  
        @Override  
        public InvoiceParcelable createFromParcel(Parcel source) {   
            InvoiceParcelable p = new InvoiceParcelable();   
            p.invoice=(FetchInvoice) source.readValue(FetchInvoice.class.getClassLoader());   
            return p;   
        }   
  
        @Override  
        public InvoiceParcelable[] newArray(int size) {   
            return new InvoiceParcelable[size];   
        }   
    };   
  
}   
