package com.intlpos.database;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.intlpos.sirclepos.R;

public class Product implements Serializable{
	private long product_Id;
	private String item_no;
	private String item_name;
	private BigDecimal price;
	private BigDecimal cost;
	private BigDecimal item_stock;
	private BigDecimal re_order_level;
	private static final long serialVersionUID = 46543445; 
	public Product(String item_no, String item_name) {
		super();
		this.item_no = item_no;
		this.item_name = item_name;
	}

	private boolean tax_rate1;
	private boolean tax_rate2;
	private boolean tax_rate3;
	private boolean product_tsdisplay;
	private String prompt_items_id;
	private String prompt_message;
	private String dept_id;
	private String department_name;
	private boolean dept_tsdisplay;
	private String picture;
	private boolean taxInclusive;
	private boolean hasMods;
	private boolean isMods;
	private boolean foodstampable;
	
	public boolean isHasMods() {
		return hasMods;
	}

	public void setHasMods(boolean hasMods) {
		this.hasMods = hasMods;
	}

	public boolean isMods() {
		return isMods;
	}

	public void setMods(boolean isMods) {
		this.isMods = isMods;
	}

	public Product() {

	}

	public ArrayList<Product> convertjsontoproduct(JSONObject json) {
		ArrayList<Product> list = null;
		try {
			JSONArray productlist = json.getJSONArray("product_list");
			list = new ArrayList<Product>();
			for (int i = 0; i < productlist.length(); i++) {
				JSONObject item = productlist.getJSONObject(i);
				Product product = new Product();
				product.product_Id = item.getLong("product_id");
				product.item_no = item.getString("item_no");
				product.item_name = item.getString("item_name");
				Double x = item.getDouble("price");
				BigDecimal y = BigDecimal.valueOf(x);
				product.price =  BigDecimal.valueOf(item.getDouble("price")).setScale(2);
				product.cost =  BigDecimal.valueOf(item.getDouble("cost")).setScale(2);
				product.item_stock =  BigDecimal.valueOf(item.getDouble("item_stock")).setScale(2);
				product.re_order_level =  BigDecimal.valueOf(item.getDouble("re_order_level")).setScale(2);				
				product.tax_rate1 = item.getBoolean("tax_rate1");
				product.tax_rate2 = item.getBoolean("tax_rate2");
				product.tax_rate3 = item.getBoolean("tax_rate3");
				product.setProduct_tsdisplay(item.getBoolean("product_tsdisplay"));
				product.setPrompt_items_id(item.getString("prompt_items_id"));
				product.setPrompt_message(item.getString("prompt_message"));				
				product.dept_id = item.getString("department_id");
				product.department_name = item.getString("department_name");
				product.dept_tsdisplay = item.getBoolean("dept_tsdisplay");
				product.picture = item.getString("picture");
				product.taxInclusive = item.getBoolean("tax_inclusive");
				product.hasMods = item.getBoolean("hasmodifier");
				product.isMods = item.getBoolean("ismodifier");
				product.foodstampable = item.getBoolean("foodstampable");
				list.add(product);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;

	}

	public boolean isFoodstampable() {
		return foodstampable;
	}

	public void setFoodstampable(boolean foodstampable) {
		this.foodstampable = foodstampable;
	}

	public boolean isTaxInclusive() {
		return taxInclusive;
	}

	public void setTaxInclusive(boolean taxInclusive) {
		this.taxInclusive = taxInclusive;
	}

	public Product convertjsontoproductobject(JSONObject json) {
		Product product = null;
		
		try {
			JSONArray productlist = json.getJSONArray("product_list");
			
			for (int i = 0; i < productlist.length(); i++) {
				JSONObject item = productlist.getJSONObject(i);
				product = new Product();
				product.product_Id = item.getLong("product_id");
				product.item_no = item.getString("item_no");
				product.item_name = item.getString("item_name");
				product.price =  BigDecimal.valueOf(item.getDouble("price")).setScale(2);
				product.cost =  BigDecimal.valueOf(item.getDouble("cost")).setScale(2);
				product.tax_rate1 = item.getBoolean("tax_rate1");
				product.tax_rate2 = item.getBoolean("tax_rate2");
				product.tax_rate3 = item.getBoolean("tax_rate3");
				product.setProduct_tsdisplay(item.getBoolean("product_tsdisplay"));
				
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return product;

	}

	
	public String getPicture(){
		return picture;
	}
	
	public void setPicture(String pic){
		picture = pic;
	}
	
	public long getProduct_Id() {
		return product_Id;
	}

	public void setProduct_Id(long product_Id) {
		this.product_Id = product_Id;
	}

	public String getItem_no() {
		return item_no;
	}

	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isTax_rate1() {
		return tax_rate1;
	}

	public void setTax_rate1(boolean tax_rate1) {
		this.tax_rate1 = tax_rate1;
	}

	public boolean isTax_rate2() {
		return tax_rate2;
	}

	public void setTax_rate2(boolean tax_rate2) {
		this.tax_rate2 = tax_rate2;
	}

	public boolean isTax_rate3() {
		return tax_rate3;
	}

	public void setTax_rate3(boolean tax_rate3) {
		this.tax_rate3 = tax_rate3;
	}

	

	public String getDept_id() {
		return dept_id;
	}

	public void setDept_id(String dept_id) {
		this.dept_id = dept_id;
	}

	public String getPrompt_items_id() {
		return prompt_items_id;
	}

	public void setPrompt_items_id(String prompt_items_id) {
		this.prompt_items_id = prompt_items_id;
	}

	public boolean isProduct_tsdisplay() {
		return product_tsdisplay;
	}

	public void setProduct_tsdisplay(boolean product_tsdisplay) {
		this.product_tsdisplay = product_tsdisplay;
	}

	public String getPrompt_message() {
		return prompt_message;
	}

	public void setPrompt_message(String prompt_message) {
		this.prompt_message = prompt_message;
	}

	public String getDepartment_name() {
		return department_name;
	}

	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}

	public boolean isDept_tsdisplay() {
		return dept_tsdisplay;
	}

	public void setDept_tsdisplay(boolean dept_tsdisplay) {
		this.dept_tsdisplay = dept_tsdisplay;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public BigDecimal getItem_stock() {
		return item_stock;
	}

	public void setItem_stock(BigDecimal item_stock) {
		this.item_stock = item_stock;
	}

	public BigDecimal getRe_order_level() {
		return re_order_level;
	}

	public void setRe_order_level(BigDecimal re_order_level) {
		this.re_order_level = re_order_level;
	}

}
