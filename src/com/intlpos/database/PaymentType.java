package com.intlpos.database;

import com.intlpos.sirclepos.R;
import java.math.BigDecimal;

public class PaymentType {
	public String type;
	public BigDecimal amount;
	public String auth_code;
	public String last4Digit;
	public String ref_no;
	public PaymentType(String type, BigDecimal amount, String auth_code, String last_four, String ref_no){
		this.type = type;
		this.amount = amount;
		this.auth_code = auth_code;
		this.last4Digit = last_four;
		this.ref_no = ref_no;		
	}
}
