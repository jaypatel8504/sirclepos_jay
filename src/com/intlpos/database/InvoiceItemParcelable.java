package com.intlpos.database;

import java.util.HashMap;   

import com.john.beans.FetchInvoice;
import android.os.Parcel;   
import android.os.Parcelable;   
  
public class InvoiceItemParcelable implements Parcelable {   
    
    private Order invoiceitem;  
    
    public InvoiceItemParcelable(){
    }
    
    public InvoiceItemParcelable(Order invoice){
    	this.invoiceitem = invoice;
    }
	public Order getInvoice() {
		return invoiceitem;
	}

	public void setInvoice(Order invoice) {
		this.invoiceitem = invoice;
	}

	@Override  
    public int describeContents() {   
        return 0;   
    }   
  
    @Override  
    public void writeToParcel(Parcel dest, int flags) {   
  
        dest.writeValue(invoiceitem);   
    }   
    public static final Parcelable.Creator<InvoiceItemParcelable> CREATOR = new Parcelable.Creator<InvoiceItemParcelable>() {   
    //Rewrite Creator
  
        @Override  
        public InvoiceItemParcelable createFromParcel(Parcel source) {   
            InvoiceItemParcelable p = new InvoiceItemParcelable();   
            p.invoiceitem=(Order) source.readValue(Order.class.getClassLoader());   
            return p;   
        }   
  
        @Override  
        public InvoiceItemParcelable[] newArray(int size) {   
            return new InvoiceItemParcelable[size];   
        }   
    };   
  
}   
