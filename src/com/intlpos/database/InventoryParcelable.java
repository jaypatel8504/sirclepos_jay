package com.intlpos.database;

import java.util.HashMap;   
import android.os.Parcel;   
import android.os.Parcelable;   
  
public class InventoryParcelable implements Parcelable {   
  
    public HashMap<String,Object> map = new HashMap<String,Object> ();  

	@Override  
    public int describeContents() {   
        return 0;   
    }   
  
    @Override  
    public void writeToParcel(Parcel dest, int flags) {   
  
        dest.writeMap(map);   
    }   
    public static final Parcelable.Creator<InventoryParcelable> CREATOR = new Parcelable.Creator<InventoryParcelable>() {   
    //Rewrite Creator
  
        @Override  
        public InventoryParcelable createFromParcel(Parcel source) {   
            InventoryParcelable p = new InventoryParcelable();   
            p.map=source.readHashMap(HashMap.class.getClassLoader());   
            return p;   
        }   
  
        @Override  
        public InventoryParcelable[] newArray(int size) {   
            // TODO Auto-generated method stub   
            return null;   
        }   
    };   
  
}   
