package com.intlpos.database;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import com.intlpos.sirclepos.R;

public class Department {
	private String Dept_Id;
	private String Dept_Name;
	private String Url;
	//private String Desc;

	 private boolean TSDisplay;

	public Department()
	{}
	public Department(JSONObject json) 
	{
		try {
			this.Dept_Id = json.getString("department_id");
			this.Dept_Name = json.getString("department_name");
		//	this.Desc = json.getString("description");
			this.TSDisplay = json.getBoolean("tsdisplay");
			if(json.has("picture")){
				this.Url = json.getString("picture");
			}else{
				this.Url = "";
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setUrl(String url) {
		Url = url;
	}
	public Department(String deptid, String deptname, String desc, boolean tsdisplay) {
		this.Dept_Id = deptid;
		this.Dept_Name = deptname;
	//	this.Desc = desc;
		 this.TSDisplay = tsdisplay;
	}
	
	public String getUrl(){
		return Url;
	}

	public String getDept_Id() {
		return Dept_Id;
	}

	public void setDept_Id(String Dept_Id) {
		this.Dept_Id = Dept_Id;
	}

	public String getDept_Name() {
		return Dept_Name;
	}

	public void setDept_Name(String Dept_Name) {
		this.Dept_Name = Dept_Name;
	}
	public boolean getTSDisplay() {
		return TSDisplay;
	}
	public void setTSDisplay(boolean tSDisplay) {
		TSDisplay = tSDisplay;
	}
	/*public String getDesc() {
		return Desc;
	}

	public void setDesc(String Desc) {
		this.Desc = Desc;
	}
*/
	/*
	 * public int getTSDisplay() { return TSDisplay ; }
	 * 
	 * public void setTSDisplay(int TSDisplay) { this.TSDisplay = TSDisplay; }
	 */

	public static ArrayList<Department> convertJSONtodepartment(JSONObject json)
	{
		Log.d("DEPT", json.toString());
		ArrayList<Department> List_depts = null;
		try
		{
			JSONArray jDeptArray = json.getJSONArray("department_list");
			List_depts = new ArrayList<Department>();
			
			for (int i = 0; i < jDeptArray.length(); i++) 
			{
				JSONObject jsonObj = jDeptArray.getJSONObject(i);				
				Department department = new Department(jsonObj);
			/*	department.Dept_Id = jsonObj.getString("department_id");
				department.Dept_Name = jsonObj.getString("department_name");
				department.Desc = jsonObj.getString("description");*/
				Log.d("DEPT", department.Dept_Id+ " " + department.Dept_Name);
				if(department.TSDisplay)
					List_depts.add(department);
			}

		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		
		return List_depts;
	}
	
	public Department convertJSONtodepartmentobject(JSONObject json)
	{
		Department department = null;
		
		try
		{
			JSONObject jDeptObject = json.getJSONObject("dept");
	        department = new Department(jDeptObject);
		}
		catch (JSONException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		
		return department;
	}
	
		

}
