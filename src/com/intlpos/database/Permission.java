package com.intlpos.database;

import android.util.Log;

import com.intlpos.sirclepos.R;
import org.json.JSONException;
import org.json.JSONObject;

public final class Permission {
	public  int permissions_id;
	public  String permissions_name;
	public  int price_change;
	public  int qty_change;
	public  int delete_button;
	public  int shiftreport_atshiftclose;
	public  int reqclockin;
	public int discount;
	public int sell_non_inventory;
	public int void_invoice;
	public int open_cashdrawer;
	public int negate_invoice;
	
	
	public static Permission manager(){
		Permission permit = new Permission();
		permit.permissions_id = 1;
		permit.permissions_name = "";
		permit.price_change = 1;
		permit.qty_change = 1;
		permit.delete_button = 1;
		permit.shiftreport_atshiftclose = 1;
		permit.reqclockin = 1;	
		permit.discount = 1;
		permit.sell_non_inventory = 1;
		permit.void_invoice = 1;
		permit.open_cashdrawer = 1;
		permit.negate_invoice = 1;
		return permit;
	}
	
	public static Permission  convertjsontopermission(JSONObject json)
	{
		JSONObject jsonpermission;
		Permission permit = new Permission();
		try {
			jsonpermission = json.getJSONObject("permission");
			permit.permissions_id = jsonpermission.getInt("permissions_id");
			permit.permissions_name = jsonpermission.getString("permissions_name");
			permit.price_change = jsonpermission.getInt("price_change");
			permit.qty_change = jsonpermission.getInt("qty_change");
			permit.delete_button = jsonpermission.getInt("delete_button");
			permit.shiftreport_atshiftclose = jsonpermission.getInt("shiftreport_at_shiftend");
			permit.reqclockin = jsonpermission.getInt("reqclockin");	
			permit.discount = jsonpermission.getInt("discount_button");
			permit.sell_non_inventory =  jsonpermission.getInt("sell_non_inventory");
			permit.void_invoice =  jsonpermission.getInt("void_invoice");	
			permit.open_cashdrawer =  jsonpermission.getInt("open_cashdrawer");	
			permit.negate_invoice =  jsonpermission.getInt("negate_invoice");	
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return permit;
	}
	
}
