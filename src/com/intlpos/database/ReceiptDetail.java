package com.intlpos.database;

import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.util.Log;
import android.view.animation.Animation;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.sirclepos.CornerStorePOS;

public final class ReceiptDetail {

	public static int receiptdetail_id;
	public static String store_no;
	public static String store_name;
	public static String address1;
	public static String address2;
	public static String contact_no;
	public static String email_id;
	public static String picture;
	public static String[] footer;

	public static void setreceiptdetail() {
		final String methodName = "CompanyMaintenence/ReceiptService.svc/getreceiptdetail";
		
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		params.put("storedetail", storedetail);

		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {

					if (json.isNull("store_no")) {

					} else {
						convertjsontoreceiptdetail(json);
					}
				} catch (Exception e) {
					JSONParser jParser = new JSONParser(new MyListener() {

						@Override
						public void onResult(JSONObject json) {

							try {

								if (json.isNull("store_no")) {

								} else {
									convertjsontoreceiptdetail(json);
								}
							} catch (Exception e) {

							}
						}
					});

					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
				}
			}
		});

		jParser.execute(CornerStorePOS.Url, methodName, params);

	}

	public static void convertjsontoreceiptdetail(JSONObject json) {
		try {
			store_no = json.getString("store_no");
			store_name = json.getString("store_name");
			address1 = json.getString("address1");
			address2 = json.getString("address2");
			contact_no = json.getString("contact_no");
			email_id = json.getString("email_id");
			picture = json.getString("picture");
			Log.d("PC", picture);
			CSSharedPreferences.setPic(picture);
			footer = new String[10];
			JSONArray JSONfooter = json.getJSONArray("footer");
			if (JSONfooter != null) { 
				   int len = JSONfooter.length();
				   for (int i=0;i<len;i++){ 
				    footer[i] = JSONfooter.get(i).toString();
				   } 
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
