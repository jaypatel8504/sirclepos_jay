package com.intlpos.database;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import com.intlpos.sirclepos.R;

public class DeptProduct 
{
	private Department dept;
	private ArrayList<Product> productlist;
	
	public Department getDept() {
		return dept;
	}
	public void setDept(Department dept) {
		this.dept = dept;
	}
	

	public ArrayList<Product> getProductlist() {
		return productlist;
	}
	public void setProductlist(ArrayList<Product> productlist) {
		this.productlist = productlist;
	}

	public ArrayList<DeptProduct> convertjsontodeptproduct(JSONObject json) {
		
		ArrayList<DeptProduct> list = new ArrayList<DeptProduct>();
		
		try {
			
			JSONArray JSONdeptproduct = json.getJSONArray("dept_itemsList");
			for (int i = 0; i < JSONdeptproduct.length(); i++) 
			{
				DeptProduct deptproduct = new DeptProduct();
				JSONObject productsdeptlisting = JSONdeptproduct.getJSONObject(i);
				Department department = new Department();					
				deptproduct.setDept(department.convertJSONtodepartmentobject(productsdeptlisting));				
				Product product = new Product();
				deptproduct.setProductlist(product.convertjsontoproduct(productsdeptlisting));
				list.add(deptproduct);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return list;
	}
	
	
		

}
