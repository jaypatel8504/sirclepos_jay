package com.intlpos.database;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.bool;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

public final class Employee {

	// Member
	public  String employees_id;
	public  String email_id;
	public  String access_id;
	public  String password;
	public  String card_swipeid;
	public  boolean disabled;
	public  String first_name;
	public  String middle_name;
	public  String last_name;
	public  String contact_no;

	public  Date birthday;
	public  String picture;
	public  int job_code;
	public  String last_passchange;
	public  boolean resetpasswordflag;
	public  boolean admin_access;
	public  boolean access_to_pos;
	public  boolean access_to_backoffice;
	

	public Employee() {
	}

	public static Employee convertjsontoemp(JSONObject json, CornerStorePOS app) {
		Employee emp = new Employee();
		try {
			JSONObject jsonemployee = json.getJSONObject("employee");			
			emp.employees_id = jsonemployee.getString("employees_id");
			emp.email_id = jsonemployee.getString("email_id");
			emp.first_name = jsonemployee.getString("first_name");
			emp.last_name = jsonemployee.getString("last_name");
			emp.disabled = jsonemployee.getBoolean("disabled");
			emp.access_to_pos = jsonemployee.getBoolean("access_to_pos");
			emp.admin_access = jsonemployee.getBoolean("admin_access");
			if (emp.admin_access ==false )
			{
				emp.job_code = jsonemployee.getInt("job_code");
				app.permission = Permission.convertjsontopermission(json);
			}else{
				emp.job_code = jsonemployee.getInt("job_code");
				app.permission = Permission.manager();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return emp;

	}
	
	public static Employee convertjsontoemp(JSONObject json) {
		Employee emp = new Employee();
		try {
			JSONObject jsonemployee = json.getJSONObject("employee");			
			emp.employees_id = jsonemployee.getString("employees_id");
			emp.email_id = jsonemployee.getString("email_id");
			emp.first_name = jsonemployee.getString("first_name");
			emp.last_name = jsonemployee.getString("last_name");
			emp.disabled = jsonemployee.getBoolean("disabled");
			emp.access_to_pos = jsonemployee.getBoolean("access_to_pos");
			emp.admin_access = jsonemployee.getBoolean("admin_access");
			emp.job_code = jsonemployee.getInt("job_code");
//			if ( emp.admin_access == false )
//			{
//				emp.job_code = jsonemployee.getInt("job_code");
//			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return emp;

	}

}

