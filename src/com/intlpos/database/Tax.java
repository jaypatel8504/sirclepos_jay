package com.intlpos.database;

import java.math.BigDecimal;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

public class Tax {
	  public static int tax_rate_id;
      public static BigDecimal tax_rate1 =BigDecimal.ZERO;
      public static BigDecimal tax_rate2 = BigDecimal.ZERO;
      public static BigDecimal tax_rate3 = BigDecimal.ZERO;
      public static String[] tax_desc = new String[3];


public static void settaxrates()
{
	final String methodName = "CompanyMaintenence/TaxService.svc/gettaxdetail";
	
	final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
	storedetail.put("email_id", CornerStorePOS.email_id);
	storedetail.put("password", CornerStorePOS.password);
	storedetail.put("store_id",CornerStorePOS.StoreId);
	storedetail.put("station_id", CornerStorePOS.StationId);
	params.put("storedetail", storedetail);

	JSONParser jParser = new JSONParser(new MyListener() {

		@Override
		public void onResult(JSONObject json) {

			try {

				if (json.isNull("tax_rate_id")) {

				} else {
					convertjsontotax(json);
				}
			} catch (Exception e) {
				JSONParser jParser = new JSONParser(new MyListener() {

					@Override
					public void onResult(JSONObject json) {

						try {

							if (json.isNull("tax_rate_id")) {

							} else {
								convertjsontotax(json);
							}
						} catch (Exception e) {

						}
					}
				});

				jParser.execute(CornerStorePOS.BackupUrl, methodName, params);	
			}
		}
	});

	jParser.execute(CornerStorePOS.Url, methodName, params);

}

public static void convertjsontotax(JSONObject json) {
	try {
		tax_rate_id = json.getInt("tax_rate_id");
		tax_rate1 = BigDecimal.valueOf(json.getDouble("tax_rate1"));
		tax_rate2 =  BigDecimal.valueOf(json.getDouble("tax_rate2"));
		tax_rate3 =  BigDecimal.valueOf(json.getDouble("tax_rate3"));
		tax_desc[0] = json.getString("tax_desc1");
		tax_desc[1] = json.getString("tax_desc2");
		tax_desc[2] = json.getString("tax_desc3");
		//Log.d("Tax",""+tax_rate1+" "+tax_rate2+" "+tax_rate3+" "+tax_desc1+" "+tax_desc2+" "+tax_desc3);
		
	} 
	catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}