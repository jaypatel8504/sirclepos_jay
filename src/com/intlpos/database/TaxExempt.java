package com.intlpos.database;

import java.math.BigDecimal;

public class TaxExempt {
	private Boolean isTaxExempt[];
	private boolean changed;
	private BigDecimal tax[], previous_tax[];
	private int position; 
	
	public TaxExempt(Boolean taxEx[], boolean changed, BigDecimal tax[],BigDecimal previous_tax[], int pos){
		this.isTaxExempt = taxEx;
		this.changed = changed;
		this.tax = tax;
		this.previous_tax = previous_tax;
		this.position = pos;
	}

	public Boolean[] getIsTaxExempt() {
		return isTaxExempt;
	}

	public void setIsTaxExempt(Boolean[] isTaxExempt) {
		this.isTaxExempt = isTaxExempt;
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public BigDecimal[] getTax() {
		return tax;
	}

	public void setTax(BigDecimal[] tax) {
		this.tax = tax;
	}

	public BigDecimal[] getPrevious_tax() {
		return previous_tax;
	}

	public void setPrevious_tax(BigDecimal[] previous_tax) {
		this.previous_tax = previous_tax;
	}
	
	public void setposition(int pos){
		position = pos;
	}
	
	public int getPosition(){
		return position;
	}
	
	public BigDecimal getTotal(){
		BigDecimal pt = previous_tax[0].add(previous_tax[1]).add(previous_tax[2]);
		BigDecimal total = tax[0].add(tax[1]).add(tax[2]);
		
		if(pt.compareTo(total) ==1){
			return pt;
		}else{
			return total;
		}
	}
}
