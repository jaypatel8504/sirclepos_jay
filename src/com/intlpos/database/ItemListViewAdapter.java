package com.intlpos.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import com.intlpos.sirclepos.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
// try to extends ArrayAdapter and then getFilter should work.
public class ItemListViewAdapter extends BaseAdapter implements Filterable {
	private final ArrayList<InventoryParcelable> final_data;
	private ArrayList<InventoryParcelable> data;
	private Context context;
	private ListItemHolder holder;
	private LayoutInflater inflater;
	private Filter filter;
	
	
	public ItemListViewAdapter(Context context, ArrayList<InventoryParcelable> data){
		this.final_data = data;
		this.data = (ArrayList<InventoryParcelable>) data.clone();
		this.inflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	 public final class ListItemHolder 
		{
		  TextView item_id,item_name,item_price,item_stock;
		}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
//		if(convertView == null)
//		{			
//			convertView = inflater.inflate(R.layout.listitem_invoicehome, null);
//			holder = new ListItemHolder();
//			holder.item_id = (TextView) convertView.findViewById(R.id.item_id);
//			holder.item_name = (TextView) convertView.findViewById(R.id.item_name);
//			holder.item_price = (TextView) convertView.findViewById(R.id.item_price);
//			holder.item_cost = (TextView) convertView.findViewById(R.id.item_cost);
//			convertView.setTag(holder);
//		}else{
//			convertView.findViewWithTag(holder);
//		}
//		if(data != null)
//		{
//		}
		convertView = inflater.inflate(R.layout.listitem_invoicehome, null);
		holder = new ListItemHolder();
		holder.item_id = (TextView) convertView.findViewById(R.id.item_id);
		holder.item_name = (TextView) convertView.findViewById(R.id.item_name);
		holder.item_price = (TextView) convertView.findViewById(R.id.item_price);
		holder.item_stock = (TextView) convertView.findViewById(R.id.item_stock);
//		Log.d("keyword", "name = "+data.get(position).map.get("item_name"));
		holder.item_id.setText(data.get(position).map.get("item_no").toString());
		holder.item_name.setText(data.get(position).map.get("item_name").toString());
		BigDecimal price = (BigDecimal) data.get(position).map.get("price");
		holder.item_price.setText(price.setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
		holder.item_stock.setText(data.get(position).map.get("item_stock").toString()); 
		return convertView;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null){
			filter = new ProductFilter();
		}
		return filter;
	}
	

	public class ProductFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub
        	// determine which filter should use by prefix
        	String prefix = constraint.subSequence(0, 2).toString();
        	String keyword = constraint.subSequence(2, constraint.length()).toString().toLowerCase(Locale.getDefault());
//        	Log.d("keyword", prefix+" "+keyword);
            FilterResults result = new FilterResults();
            // if constraint is empty return the original names
//            if(keyword != null && keyword.length() > 0 ){
            
	            ArrayList<InventoryParcelable> Filtered_data = new ArrayList<InventoryParcelable>();
	            String filterableString = null;
	            
	        	for (int i = 0, len = final_data.size(); i < len; i++){
	        		final InventoryParcelable item = new InventoryParcelable(); 
	        		item.map = final_data.get(i).map;
	                if (prefix.equals("id")){
	                	filterableString = item.map.get("item_no").toString();
	                }else if(prefix.equals("nm")){    
	                	filterableString = item.map.get("item_name").toString();
	                }     
	        		if (filterableString.toLowerCase(Locale.getDefault()).startsWith(keyword)){
	        			Filtered_data.add(item);
//	        			Log.d("keyword", filterableString+" "+item.map.get("item_name").toString());
	        		}
	        	}
//	            for(InventoryParcelable ip: Filtered_data){
//            	Log.d("keywords", ip.map.get("item_id").toString()+ip.map.get("item_name"));
//            }
	          result.values = Filtered_data;
	          result.count = Filtered_data.size();
//            }
//            else{
//            	synchronized(this){
////            		Log.d("keyword",final_data.size()+"");
//                	result.values = final_data;
//                	result.count = final_data.size();
//            	}
//            }
            
//            for(int i = 0; i<Original_Names.size(); i++){
//                filterableString = Original_Names.get(i);
//                if(filterableString.toLowerCase().contains(filterString)){
//                    Filtered_Names.add(filterableString);
//                }
//            }
//            result.values = Filtered_Names;
//            result.count = Filtered_Names.size();
//            ArrayList<InventoryParcelable> tests = (ArrayList<InventoryParcelable>) result.values;
//            for(InventoryParcelable ip: tests){
//            	Log.d("keywords", ip.map.get("item_id").toString()+ip.map.get("item_name"));
//            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // TODO Auto-generated method stub
        	data.clear();
            notifyDataSetChanged();
          ArrayList<InventoryParcelable> newData = (ArrayList<InventoryParcelable>) results.values;
          for(InventoryParcelable ip: newData){                
        	  data.add(ip);
//             Log.d("keyword", data.get(i).map.get("item_name").toString());
            }
            notifyDataSetChanged();
        }

    }

}
