package com.intlpos.database;

import com.intlpos.sirclepos.R;
import java.util.HashMap;



public class ButtonParam {
	private String btn_text;
	private int btn_drawable;
	private int Textcolor;
	private int bgColor;
	private float price;
	private int item_id;
	private String link;
	private String pic;
public int getItem_id() {
		return item_id;
	}


	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}


	// Rinkal - While Department Lookup
	private String btn_deptId;
	private String btn_deptName;
	
	//Rinkal - While Items Lookup
	private HashMap<String,Object> btn_product;
	
	public int getBgColor() {
		return bgColor;
	}
	
	
	public void setBgColor(int bgColor) {
		this.bgColor = bgColor;
	}
	
	public String getText() {
		return btn_text;
	}
	public void setText(String btn_text) {
		this.btn_text = btn_text;
	}
	public int getDrawable() {
		return btn_drawable;
	}
	public void setDrawable(int btn_drawable) {
		this.btn_drawable = btn_drawable;
	}
	// Rinkal - Creating this for Department Lookup
	
	public ButtonParam(String btn_deptId,String btn_deptName,/* int btn_drawable,*/ int bg_color, String pic) {
		this.btn_deptId = btn_deptId;
		this.btn_deptName = btn_deptName;
	//	this.btn_drawable = btn_drawable;
		this.bgColor = bg_color;
		this.pic = pic;
	}
	
	public String getPic() {
		return pic;
	}


	public void setPic(String pic) {
		this.pic = pic;
	}


	//Rinkal - Creating this for Items Lookup
	public ButtonParam(HashMap<String,Object> btn_product, int bg_color) {
		this.btn_product = btn_product;

		this.bgColor = bg_color;
	}
	public ButtonParam(String btn_text, int btn_drawable, int bg_color) {
		this.btn_text = btn_text;
		this.btn_drawable = btn_drawable;
		this.bgColor = bg_color;
	}
	public ButtonParam(String btn_text,float price,int id,int btn_drawable,int bg_color)
	{
		this.btn_text = btn_text;
		this.btn_drawable = btn_drawable;
		this.bgColor = bg_color;
		this.setPrice(price);
		this.item_id = id;
	}
	
	public ButtonParam(String btn_text,float price,int id,String link,int bg_color)
	{
		this.btn_text = btn_text;
		this.link = link;
		this.bgColor = bg_color;
		this.setPrice(price);
		this.item_id = id;
	}

	public float getPrice() {
		return price;
	}

	public String getLink(){
		return link;
	}
	public void setPrice(float price) {
		this.price = price;
	}


	public String getBtn_deptId() {
		return btn_deptId;
	}


	public void setBtn_deptId(String btn_deptId) {
		this.btn_deptId = btn_deptId;
	}


	public String getBtn_deptName() {
		return btn_deptName;
	}


	public void setBtn_deptName(String btn_deptName) {
		this.btn_deptName = btn_deptName;
	}


	

	public HashMap<String,Object> getBtn_product() {
		
		return btn_product;
	}


	public void setBtn_product(HashMap<String,Object> btn_product) {
		this.btn_product = btn_product;
	}

}
