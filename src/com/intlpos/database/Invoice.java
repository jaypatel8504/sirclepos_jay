/*
 *  we had better to fix this class when we have time.
 */
package com.intlpos.database;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.intlpos.sirclepos.R;

public class Invoice {
	 public static String[] PAYMENT_TYPE = {"Split Invoice","Cash","Credit","Debit","Gift Card","EBT","To Account", "Check"};
	 private long invoice_id;
	 private BigDecimal undiscounted_sub_total;
     private BigDecimal sub_total;
     private BigDecimal[] total_tax;
     private BigDecimal total_tax1;
     private BigDecimal total_tax2;
     private BigDecimal total_tax3;
      private Double whole_invoice_discount_percent;
      private Double whole_invoice_discount_amount ;
      private BigDecimal grand_total;
      private String invoice_date;
      private int payment_type;
     // private BigDecimal amt_tendered;
      private BigDecimal change_amt;
      private String s_cashier_id;
      private int station_id;
      private String auth_code;
      private String ref_no;
      private String transaction_id;
      private String last_four;
      private String card_holder_name;
      private ArrayList<SplitInvoice>  split_invoice_list;
      private int customer_id;
      private String status;
      private String v_cashier_id;
      
     public int getCustomer_id() {
		return customer_id;
	}


	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}


	public String getV_cashier_id() {
		return v_cashier_id;
	}


	public void setV_cashier_id(String v_cashier_id) {
		this.v_cashier_id = v_cashier_id;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public long getInvoice_id() {
		return invoice_id;
	}


	public void setInvoice_id(long invoice_id) {
		this.invoice_id = invoice_id;
	}


	public BigDecimal getUndiscounted_sub_total() {
		return undiscounted_sub_total;
	}


	public void setUndiscounted_sub_total(BigDecimal undiscounted_sub_total) {
		this.undiscounted_sub_total = undiscounted_sub_total;
	}


	public BigDecimal getSub_total() {
		return sub_total;
	}


	public void setSub_total(BigDecimal sub_total) {
		this.sub_total = sub_total;
	}

	public Double getWhole_invoice_discount_percent() {
		return whole_invoice_discount_percent;
	}


	public void setWhole_invoice_discount_percent(
			Double whole_invoice_discount_percent) {
		this.whole_invoice_discount_percent = whole_invoice_discount_percent;
	}


	public Double getWhole_invoice_discount_amount() {
		return whole_invoice_discount_amount;
	}


	public void setWhole_invoice_discount_amount(
			Double whole_invoice_discount_amount) {
		this.whole_invoice_discount_amount = whole_invoice_discount_amount;
	}


	public BigDecimal getGrand_total() {
		return grand_total;
	}


	public void setGrand_total(BigDecimal grand_total) {
		this.grand_total = grand_total;
	}


	public String getInvoice_date() {
		return invoice_date;
	}


	public void setInvoice_date(String invoice_date) {
		this.invoice_date = invoice_date;
	}


	public int getPayment_type() {
		return payment_type;
	}


	public void setPayment_type(int payment_type) {
		this.payment_type = payment_type;
	}


	public BigDecimal getChange_amt() {
		return change_amt;
	}


	public void setChange_amt(BigDecimal change_amt) {
		this.change_amt = change_amt;
	}


	public String getS_cashier_id() {
		return s_cashier_id;
	}


	public void setS_cashier_id(String s_cashier_id) {
		this.s_cashier_id = s_cashier_id;
	}


	public int getStation_id() {
		return station_id;
	}


	public void setStation_id(int station_id) {
		this.station_id = station_id;
	}


	public ArrayList<SplitInvoice> getSplit_invoice_list() {
		return split_invoice_list;
	}


	public void setSplit_invoice_list(ArrayList<SplitInvoice> split_invoice_list) {
		this.split_invoice_list = split_invoice_list;
	}
     
     
    public Invoice converttoInvoice(BigDecimal undiscounted_sub_total,
			BigDecimal sub_total, BigDecimal[] subTax,
			Double whole_invoice_discount_amount, Double whole_invoice_discount_percent,
			BigDecimal totalGrandBalanceAfterWholeInvoiceDiscount,
			String today_datetime, ArrayList<PaymentType> splitinvoices,
			BigDecimal change_amt,String s_cashier_id,String station_id, int customerid) {
		
    	Invoice invoice = new Invoice();
    	invoice.undiscounted_sub_total = undiscounted_sub_total;
    	invoice.sub_total = sub_total;
    	invoice.setTotal_tax(subTax); // for Receipt Printing only (not used in webservice)
    	invoice.total_tax1 = subTax[0];
    	invoice.total_tax2 = subTax[1];
    	invoice.total_tax3 = subTax[2];
    	invoice.whole_invoice_discount_percent = whole_invoice_discount_percent;
    	invoice.whole_invoice_discount_amount= whole_invoice_discount_amount;
    	invoice.grand_total = totalGrandBalanceAfterWholeInvoiceDiscount;
    	invoice.invoice_date = today_datetime;    	
    	SplitInvoice splitinvoice = new SplitInvoice();
    	split_invoice_list = splitinvoice.convertToSplitInvoice(splitinvoices);
    	if(split_invoice_list.size() == 1)
    	{    		
    		invoice.payment_type = split_invoice_list.get(0).payment_type; 
    		invoice.auth_code = split_invoice_list.get(0).auth_code;
    		invoice.ref_no = split_invoice_list.get(0).ref_no;
    		invoice.last_four = split_invoice_list.get(0).last_four;
    	}
    	else
    	{
    		invoice.payment_type = 1;    		
    	}
    	invoice.split_invoice_list = split_invoice_list;
    	
    	invoice.change_amt = change_amt;
    	invoice.s_cashier_id = s_cashier_id;
    	invoice.station_id = Integer.valueOf(station_id);  // I will have to fix this
    	invoice.customer_id = customerid;
//    	Gson gson = new Gson();
//    	String InvoiceObj = gson.toJson(invoice);
    	return invoice;
	}


	public BigDecimal[] getTotal_tax() {
		return total_tax;
	}


	public void setTotal_tax(BigDecimal[] total_tax) {
		this.total_tax = total_tax;
	}


	public BigDecimal getTotal_tax1() {
		return total_tax1;
	}


	public void setTotal_tax1(BigDecimal total_tax1) {
		this.total_tax1 = total_tax1;
	}
	
	public void setTotal_tax1(double total_tax1) {
		this.total_tax1 = new BigDecimal(total_tax1);
	}


	public BigDecimal getTotal_tax2() {
		return total_tax2;
	}


	public void setTotal_tax2(BigDecimal total_tax2) {
		this.total_tax2 = total_tax2;
	}

	public void setTotal_tax2(double total_tax1) {
		this.total_tax2 = new BigDecimal(total_tax1);
	}

	public BigDecimal getTotal_tax3() {
		return total_tax3;
	}


	public void setTotal_tax3(BigDecimal total_tax3) {
		this.total_tax3 = total_tax3;
	}

	public void setTotal_tax3(double total_tax1) {
		this.total_tax3 = new BigDecimal(total_tax1);
	}

	public String getTransaction_id() {
		return transaction_id;
	}


	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}


	public String getLast_four() {
		return last_four;
	}


	public void setLast_four(String last_four) {
		this.last_four = last_four;
	}


	public String getCard_holder_name() {
		return card_holder_name;
	}


	public void setCard_holder_name(String card_holder_name) {
		this.card_holder_name = card_holder_name;
	}


	public String getAuth_code() {
		return auth_code;
	}


	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}


	public String getRef_no() {
		return ref_no;
	}


	public void setRef_no(String ref_no) {
		this.ref_no = ref_no;
	}
     
}
