package com.intlpos.database;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ProductModifierGroups {
	private int productKey;
	private ArrayList<ProductModifierValues> productValues;
	
	public static ArrayList<ProductModifierGroups> products(JSONObject json){
		ArrayList<ProductModifierGroups> products = new ArrayList<ProductModifierGroups>();
		try {
			JSONArray productArray = json.getJSONArray("product_modifiergroupsmap");
			for(int i = 0; i < productArray.length(); i++){
				JSONObject productObj = productArray.getJSONObject(i);
				ProductModifierGroups mods = new ProductModifierGroups();
				mods.productKey = productObj.getInt("Key");
				mods.productValues = new ArrayList<ProductModifierValues>();
				JSONArray values = productObj.getJSONArray("Value");
				for(int a = 0; a < values.length(); a++){
					JSONObject valuesObj = values.getJSONObject(a);
					ProductModifierValues modValues = new ProductModifierValues();
					modValues.setAutoRingUp(valuesObj.getBoolean("modifier_auto_ringup"));
					modValues.setModifierGroupId(valuesObj.getInt("modifier_group_id"));
					modValues.setModifierGroupOrder(valuesObj.getInt("modifier_group_order"));
					mods.productValues.add(modValues);
				}
				products.add(mods);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return products;
	}
	
	public int getProductKey() {
		return productKey;
	}
	public void setProductKey(int productKey) {
		this.productKey = productKey;
	}
	public ArrayList<ProductModifierValues> getProductValues() {
		return productValues;
	}
	public void setProductValues(ArrayList<ProductModifierValues> productValues) {
		this.productValues = productValues;
	}
}
