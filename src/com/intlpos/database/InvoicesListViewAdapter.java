package com.intlpos.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;

import com.intlpos.sirclepos.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
// try to extends ArrayAdapter and then getFilter should work.
public class InvoicesListViewAdapter extends BaseAdapter implements Filterable{
	private static String[] PAYMENT_TYPE = {"Split Invoice","Cash","Credit","Debit","Gift Card","EBT","To Account","Check"};
	private final ArrayList<InvoiceParcelable> final_data;
	private ArrayList<InvoiceParcelable> data;
	public ArrayList<InvoiceParcelable> getData() {
		return data;
	}

	public void setData(ArrayList<InvoiceParcelable> data) {
		this.data = data;
	}

	private Context context;
	private ListItemHolder holder;
	private LayoutInflater inflater;
	private Filter filter;
	
	
	public InvoicesListViewAdapter(Context context, ArrayList<InvoiceParcelable> data){
		this.final_data = data;
		this.data = (ArrayList<InvoiceParcelable>) data.clone();
		this.inflater=LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	 public final class ListItemHolder 
		{
		  TextView invoice_id,grand_total,payment_type,whole_invoice_discount, status;
		}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		if(convertView == null)
//		{			
//			convertView = inflater.inflate(R.layout.listitem_invoicehome, null);
//			holder = new ListItemHolder();
//			holder.item_id = (TextView) convertView.findViewById(R.id.item_id);
//			holder.item_name = (TextView) convertView.findViewById(R.id.item_name);
//			holder.item_price = (TextView) convertView.findViewById(R.id.item_price);
//			holder.item_cost = (TextView) convertView.findViewById(R.id.item_cost);
//			convertView.setTag(holder);
//		}else{
//			convertView.findViewWithTag(holder);
//		}
//		if(data != null)
//		{
//		}
		convertView = inflater.inflate(R.layout.invoicelist_item, null);
		holder = new ListItemHolder();
		holder.invoice_id = (TextView) convertView.findViewById(R.id.invoice_id);
		holder.grand_total = (TextView) convertView.findViewById(R.id.grand_total);
		holder.payment_type = (TextView) convertView.findViewById(R.id.payment_type);
		holder.whole_invoice_discount = (TextView) convertView.findViewById(R.id.whole_invoice_discount);
		holder.status = (TextView) convertView.findViewById(R.id.status);
//		Log.d("keyword", "name = "+data.get(position).map.get("item_name"));
		holder.invoice_id.setText(data.get(position).getInvoice().getKey().getInvoice_id()+"");
		holder.grand_total.setText(data.get(position).getInvoice().getKey().getGrand_total().setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
		holder.payment_type.setText(PAYMENT_TYPE[data.get(position).getInvoice().getKey().getPayment_type()-1]);
		holder.whole_invoice_discount.setText(data.get(position).getInvoice().getKey().getInvoice_date()+""); 
		holder.status.setText(data.get(position).getInvoice().getKey().getStatus());
		return convertView;
	}

	@Override
	public Filter getFilter() {
		// TODO Auto-generated method stub
		if (filter == null){
			filter = new ProductFilter();
		}
		return filter;
	}
	

	public class ProductFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub
        	// determine which filter should use by prefix
//        	String prefix = constraint.subSequence(0, 2).toString();
//        	String keyword = constraint.subSequence(2, constraint.length()).toString().toLowerCase(Locale.getDefault());
//        	Log.d("keyword", prefix+" "+keyword);
        	String keyword = constraint.toString();
            FilterResults result = new FilterResults();
            // if constraint is empty return the original names
            if(keyword != null && keyword.length() > 0 ){
            
	            ArrayList<InvoiceParcelable> Filtered_data = new ArrayList<InvoiceParcelable>();
	            String filterableString = null;
	            
	        	for (int i = 0, len = data.size(); i < len; i++){
	        		final InvoiceParcelable item = data.get(i); 
                	filterableString = item.getInvoice().getKey().getInvoice_id()+"";
	        		if (filterableString.toLowerCase(Locale.getDefault()).startsWith(keyword)){
	        			Filtered_data.add(item);
//	        			Log.d("keyword", filterableString+" "+item.map.get("item_name").toString());
	        		}
	        	}
//	            for(InventoryParcelable ip: Filtered_data){
//            	Log.d("keywords", ip.map.get("item_id").toString()+ip.map.get("item_name"));
//            }
	          result.values = Filtered_data;
	          result.count = Filtered_data.size();
            }else{
            	synchronized(this){
//            		Log.d("keyword",final_data.size()+"");
                	result.values = final_data;
                	result.count = final_data.size();
            	}
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            // TODO Auto-generated method stub
        	data.clear();
            notifyDataSetChanged();
          ArrayList<InvoiceParcelable> newData = (ArrayList<InvoiceParcelable>) results.values;
          for(InvoiceParcelable ip: newData){                
        	  data.add(ip);
//             Log.d("keyword", data.get(i).map.get("item_name").toString());
            }
            notifyDataSetChanged();
        }

    }

}
