package com.intlpos.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ItemListViewAdapter.ListItemHolder;
import com.intlpos.database.ProductAdapter.ProductHolder;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PaymentAdapter extends BaseAdapter {
	private int resourceId;
	private Context context;
	private ArrayList<PaymentType> data;
	private LayoutInflater inflater;
	private PaymentTypeItemHolder holder;
	private ListView listview;
	public PaymentAdapter(Context context, ListView listview,	ArrayList<PaymentType> objects) {
//		super();
		this.context = context;
		this.listview = listview;
		this.data = objects;
		this.inflater = LayoutInflater.from(context);
	}
	
	public ListView getListview() {
		return listview;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public PaymentType getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	 public final class PaymentTypeItemHolder 
		{
		  int position;
		  TextView paymentType,paymentAmount;
		}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent){
		
		if(convertView == null)
		{			
			convertView = inflater.inflate(R.layout.payment_listviewitem, null);
			holder = new PaymentTypeItemHolder();
			holder.paymentType = (TextView) convertView.findViewById(R.id.payment_type);
			holder.paymentAmount = (TextView) convertView.findViewById(R.id.payment_amount);
			convertView.setTag(holder);
		}else{
			holder=(PaymentTypeItemHolder)convertView.getTag();
		}
		if(data != null){
			PaymentType payment = data.get(position);
			holder.paymentType.setText(payment.type);
			holder.paymentAmount.setText(payment.amount.setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
		}
		return convertView;
	}

	public ArrayList<PaymentType> getData() {
		return data;
	}

	public void setData(ArrayList<PaymentType> data) {
		this.data = data;
	}

	public void addToRow(int i, BigDecimal num, boolean flag_change) {
		// TODO Auto-generated method stub
		PaymentType pt = data.get(i);
		pt.amount = flag_change ? pt.amount.subtract(num):pt.amount.add(num);
		data.set(i, pt);
		listview.setAdapter(new PaymentAdapter(context,listview,data));
	}
	
	public void scrollListViewToBottom() {
		listview.post(new Runnable() {
	        @Override
	        public void run() {
	            // Select the last row so it will scroll into view...
	        	listview.setSelection(getCount() - 1);
	        }
	    });
	}
}