package com.intlpos.database;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ModifierGroupProducts {
	private int key;
	private ArrayList<ModifierProducts> modProducts;
	
	public static ArrayList<ModifierGroupProducts> getModandProducts(JSONObject json){
		ArrayList<ModifierGroupProducts> getModandProducts = new ArrayList<ModifierGroupProducts>();
		try {
			JSONArray getModArray = json.getJSONArray("modifiergroup_modifiersmap");
			for(int i = 0; i < getModArray.length(); i++){
				JSONObject modObj = getModArray.getJSONObject(i);
				ModifierGroupProducts mods = new ModifierGroupProducts();
				mods.key = modObj.getInt("Key");
				mods.modProducts = new ArrayList<ModifierProducts>();
				JSONArray values = modObj.getJSONArray("Value");
				for(int a = 0; a < values.length(); a++){
					JSONObject valuesObj = values.getJSONObject(a);
					ModifierProducts products = new ModifierProducts();
					products.setItemName(valuesObj.getString("item_name"));
					products.setItemNo(valuesObj.getString("item_no"));
					products.setPrice(valuesObj.getString("price"));
					products.setProductId(valuesObj.getInt("product_id"));
					products.setPic(valuesObj.getString("picture"));
					mods.modProducts.add(products);
				}
				getModandProducts.add(mods);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return getModandProducts;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public ArrayList<ModifierProducts> getModProducts() {
		return modProducts;
	}

	public void setModProducts(ArrayList<ModifierProducts> modProducts) {
		this.modProducts = modProducts;
	}
}
