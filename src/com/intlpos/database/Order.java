/*
 *  we had better to fix this class when we have time.
 */
package com.intlpos.database;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import android.util.Log;

import com.google.gson.Gson;
import com.intlpos.sirclepos.R;

public class Order {
	
	        private String order_id;
	        private long product_id;
	        private BigDecimal current_price;
	        private BigDecimal current_price_after_wholeinvoicediscount;
	        private BigDecimal tax1_after_whole_invoice_discount;
	        private BigDecimal tax2_after_whole_invoice_discount;
	        private BigDecimal tax3_after_whole_invoice_discount;      
			private BigDecimal qty_ordered;
	        private BigDecimal cost;
	        private String item_desc;
	        private Double line_discount_percent;
	        private Double line_discount_amount;
	        private Double whole_invoice_discount_amount;
	        private long  invoice_id;
	        private int row_id;
	        private boolean isMod;
	        private String note;
	        private boolean tax1_exempt, tax2_exempt,tax3_exempt;
	        private Map<String, Object> map;
	        public String getNote() {
				return note;
			}

			public void setNote(String note) {
				this.note = note;
			}

			public int getRow_id() {
				return row_id;
			}

			public void setRow_id(int row_id) {
				this.row_id = row_id;
			}

			public String getOrder_id() {
				return order_id;
			}

			public void setOrder_id(String order_id) {
				this.order_id = order_id;
			}

			public long getProduct_id() {
				return product_id;
			}

			public void setProduct_id(long product_id) {
				this.product_id = product_id;
			}

			public BigDecimal getCurrent_price() {
				return current_price;
			}

			public void setCurrent_price(BigDecimal current_price) {
				this.current_price = current_price;
			}

			public BigDecimal getCurrent_price_after_wholeinvoicediscount() {
				return current_price_after_wholeinvoicediscount;
			}

			public void setCurrent_price_after_wholeinvoicediscount(
					BigDecimal current_price_after_wholeinvoicediscount) {
				this.current_price_after_wholeinvoicediscount = current_price_after_wholeinvoicediscount;
			}

			public BigDecimal getTax1_after_whole_invoice_discount() {
				return tax1_after_whole_invoice_discount;
			}

			public void setTax1_after_whole_invoice_discount(
					BigDecimal tax1_after_whole_invoice_discount) {
				this.tax1_after_whole_invoice_discount = tax1_after_whole_invoice_discount;
			}

			public BigDecimal getTax2_after_whole_invoice_discount() {
				return tax2_after_whole_invoice_discount;
			}

			public void setTax2_after_whole_invoice_discount(
					BigDecimal tax2_after_whole_invoice_discount) {
				this.tax2_after_whole_invoice_discount = tax2_after_whole_invoice_discount;
			}

			public BigDecimal getTax3_after_whole_invoice_discount() {
				return tax3_after_whole_invoice_discount;
			}

			public void setTax3_after_whole_invoice_discount(
					BigDecimal tax3_after_whole_invoice_discount) {
				this.tax3_after_whole_invoice_discount = tax3_after_whole_invoice_discount;
			}

			public BigDecimal getQty_ordered() {
				return qty_ordered;
			}

			public boolean isMod() {
				return isMod;
			}

			public void setMod(boolean isMod) {
				this.isMod = isMod;
			}

			public void setQty_ordered(BigDecimal qty_ordered) {
				this.qty_ordered = qty_ordered;
			}

			public BigDecimal getCost() {
				return cost;
			}

			public void setCost(BigDecimal cost) {
				this.cost = cost;
			}

			public String getItem_desc() {
				return item_desc;
			}

			public void setItem_desc(String item_desc) {
				this.item_desc = item_desc;
			}

			public Double getLine_discount_percent() {
				return line_discount_percent;
			}

			public void setLine_discount_percent(Double line_discount_percent) {
				this.line_discount_percent = line_discount_percent;
			}

			public Double getLine_discount_amount() {
				return line_discount_amount;
			}

			public void setLine_discount_amount(Double line_discount_amount) {
				this.line_discount_amount = line_discount_amount;
			}

			public Double getWhole_invoice_discount_amount() {
				return whole_invoice_discount_amount;
			}

			public void setWhole_invoice_discount_amount(
					Double whole_invoice_discount_amount) {
				this.whole_invoice_discount_amount = whole_invoice_discount_amount;
			}

			public long getInvoice_id() {
				return invoice_id;
			}

			public void setInvoice_id(long invoice_id) {
				this.invoice_id = invoice_id;
			}

			
public boolean isTax1_exempt() {
				return tax1_exempt;
			}

			public void setTax1_exempt(boolean tax1_exempt) {
				this.tax1_exempt = tax1_exempt;
			}

			public boolean isTax2_exempt() {
				return tax2_exempt;
			}

			public void setTax2_exempt(boolean tax2_exempt) {
				this.tax2_exempt = tax2_exempt;
			}

			public boolean isTax3_exempt() {
				return tax3_exempt;
			}

			public void setTax3_exempt(boolean tax3_exempt) {
				this.tax3_exempt = tax3_exempt;
			}

			public Map<String, Object> getMap() {
				return map;
			}

			public void setMap(Map<String, Object> map) {
				this.map = map;
			}

public ArrayList<Order> converttoOrder(ArrayList<Map<String, Object>> data)
	{
		ArrayList<Order> orders = new ArrayList<Order>(); 
	        for(Map<String, Object> map: data)
	        {
	        	Order order = new Order();
	        	order.map = map;
	    		order.product_id = (long) map.get("product_id");
	    		order.current_price = (BigDecimal) map.get("current_price");	
	    		order.cost = (BigDecimal) map.get("cost");
	    		order.current_price_after_wholeinvoicediscount = (BigDecimal) map.get("current_price_after_wholeinvoicediscount");
	    		//if needed we have a tax before whole_invoice_discount in 'tax' map entry
	    		BigDecimal[] taxrate = (BigDecimal[]) map.get("item_tax_after_wholeinvoicediscount");
	    		
	    		
	    			boolean[] temp = new boolean[3];
	    			if(map.containsKey("tax_exempt")){
	    				temp = (boolean[]) map.get("tax_exempt");
	    			}
	    			order.tax1_exempt = temp[0];
	    			order.tax2_exempt = temp[1];
	    			order.tax3_exempt = temp[2];
	    			Log.d("tax123", Boolean.toString(temp[0]) + "\n" + Boolean.toString(temp[1]) + "\n" + Boolean.toString(temp[2]));
	    		
	    		order.tax1_after_whole_invoice_discount = taxrate[0];
	    		order.tax2_after_whole_invoice_discount = taxrate[1];
	    		order.tax3_after_whole_invoice_discount = taxrate[2];
	    		order.qty_ordered  = (BigDecimal) map.get("qty");
	    		order.item_desc = (String) map.get("item_desc");	    		
	    		order.line_discount_percent = (Double) map.get("percentage_discount");
	    		order.line_discount_amount =  (Double) map.get("dollar_discount");
	    		//whole_invoice_discount_percent =
	    		order.whole_invoice_discount_amount =  (Double) map.get("whole_invoice_discount_amount");
	    		if(map.containsKey("row")){
	    			order.row_id = (int) map.get("row");
	    			order.isMod = true;
	    		}else{
	    			order.row_id = 0;
	    			order.isMod = false;
	    		}
	    		order.note = (String) map.get("note");
//	    		Gson gson = new Gson();
//	    		String OrderObj = gson.toJson(order);
	    		
	    		orders.add(order);
	    	}
	        return orders;
	  }
}
