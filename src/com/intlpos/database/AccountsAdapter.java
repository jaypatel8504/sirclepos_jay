package com.intlpos.database;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.dialogs.Accounts.addCustomer;
import com.intlpos.sirclepos.CornerStorePOS;
import com.john.beans.Customer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AccountsAdapter  extends ArrayAdapter<Customer>{
	
	
	public AccountsAdapter(Context context, int resource, ArrayList<Customer> data, int id,boolean search,addCustomer add) {
		super(context, resource, data);
		this.context = context;
		this.data = data;
		this.ids = id;
		this.search = search;
		this.add = add;
	}

	private ArrayList<Customer> data;
	private AccountsAdapter adapter;
	private CornerStorePOS app;
	private Context context;
	private int ids;
	private boolean search;
	private addCustomer add;
	public void setAdapter(AccountsAdapter adapter){
		this.adapter = adapter;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 View rowView = inflater.inflate(R.layout.accountlist, parent, false);
		
		TextView custId = (TextView) rowView.findViewById(R.id.custidlist);
		TextView balance = (TextView) rowView.findViewById(R.id.balance);
		TextView name = (TextView) rowView.findViewById(R.id.custnamelist);
		TextView delete = (TextView) rowView.findViewById(R.id.deleteAccount);
		name.setText(data.get(position).getFirst_name() +" "+ data.get(position).getLast_name());
		double bal = data.get(position).getAccount_balance();
		balance.setText(""+bal);
		final String id= String.valueOf(data.get(position).getCustomer_id());
		custId.setText(id);
		app = (CornerStorePOS) ((Activity) context).getApplication();
		delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(data.get(position).getCustomer_id() != 1)
					deleteAccount(id, position,ids);
			}
		});
		return rowView;
	}
	
	
	private void deleteAccount(String id, final int pos,final int ids){
		final String methodName = "CustomerMaintenence/CustomerService.svc/deletecustomer";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        
        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        

        params.put("storedetail", storedetail);
        params.put("customer_id", id);

        JSONParser jParser = new JSONParser(new MyListener() {

                        @Override
                        public void onResult(JSONObject json) {

                                        try {
                                        	if(json.getBoolean("result")){
                                        		Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
                                        		if(data.get(pos).getCustomer_id() == ids){
                                        			add.setCustomerID(1);
                            						add.setName(app.CustomerList.get(0).getFirst_name() + " " + app.CustomerList.get(0).getLast_name());
                                        		}
                                        		if(!search){
	                                        		adapter.remove(data.get(pos));
	                                        		adapter.notifyDataSetChanged();
                                        		}else{
                                        			for(int i = 0; i < app.CustomerList.size(); i++){
                                        				if(app.CustomerList.get(i).getCustomer_id() == data.get(pos).getCustomer_id()){
                                        					app.CustomerList.remove(i);
                                        					break;
                                        				}
                                        			}
                                        			adapter.remove(data.get(pos));
	                                        		adapter.notifyDataSetChanged();
                                        		}
                                        	}else{
                                        		Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
                                        	}
                                        } catch (Exception e) {
                                        	JSONParser jParser = new JSONParser(new MyListener() {

                                                @Override
                                                public void onResult(JSONObject json) {

                                                                try {
                                                                	if(json.getBoolean("result")){
                                                                		Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
                                                                		adapter.remove(data.get(pos));
                                                                		adapter.notifyDataSetChanged();
                                                                	}else{
                                                                		Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
                                                                	}
                                                                } catch (Exception e) {
                                                                	
                                                                }             
                                                }
                                });

                                jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
                                        }             
                        }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);

	}
}
