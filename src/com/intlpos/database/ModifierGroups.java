package com.intlpos.database;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ModifierGroups {
	private int groupId;
	private String groupName;
	private String prompt;
	
	public static ArrayList<ModifierGroups> convertFromJSON(JSONObject json){
		ArrayList<ModifierGroups> groupList = new ArrayList<ModifierGroups>();
		try {
			JSONArray groupArray = json.getJSONArray("modifiergroup_list");
			for(int i = 0; i < groupArray.length(); i++){
				JSONObject groupObj = groupArray.getJSONObject(i);
				ModifierGroups group = new ModifierGroups();
				group.groupId = groupObj.getInt("modifier_group_id");
				group.groupName = groupObj.getString("modifier_group_name");
				group.prompt = groupObj.getString("modifier_group_prompt");
				groupList.add(group);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return groupList;
	}
	
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getPrompt() {
		return prompt;
	}
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}
	
}
