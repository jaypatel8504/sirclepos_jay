package com.intlpos.database;

import java.math.BigDecimal;

public class TaxExemption {
	
	boolean taxAll;//determines whether it is an item in the grid or whether it applies to the whole invoice
	boolean taxExempt[];//whether or not one of the 3 taxes has taxes included
	private BigDecimal tax[], oldTax[];//tax is the current tax, old tax is the tax after we remove it. 
	
	public TaxExemption(boolean taxAll, boolean[] taxExempt, BigDecimal[] tax,
			BigDecimal[] oldTax) {
		this.taxAll = taxAll;
		this.taxExempt = taxExempt;
		this.tax = tax;
		this.oldTax = oldTax;
	}
	
	public boolean isTaxAll() {
		return taxAll;
	}
	public void setTaxAll(boolean taxAll) {
		this.taxAll = taxAll;
	}
	public boolean[] getTaxExempt() {
		return taxExempt;
	}
	public void setTaxExempt(boolean[] taxExempt) {
		this.taxExempt = taxExempt;
	}
	public BigDecimal[] getTax() {
		return tax;
	}
	public void setTax(BigDecimal[] tax) {
		this.tax = tax;
	}
	public BigDecimal[] getOldTax() {
		return oldTax;
	}
	public void setOldTax(BigDecimal[] oldTax) {
		this.oldTax = oldTax;
	}
	
	
}
