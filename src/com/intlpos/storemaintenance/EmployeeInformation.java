package com.intlpos.storemaintenance;

import java.util.Date;

public class EmployeeInformation {
	
	private String firstName, lastName, middleName;
	private String email, password;
	private int id, access_id, jobcode;
	private boolean backoffice, admin, pos, disabled;
	private long birthday; 
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAccess_id() {
		return access_id;
	}

	public void setAccess_id(int access_id) {
		this.access_id = access_id;
	}

	public int getJobcode() {
		return jobcode;
	}

	public void setJobcode(int jobcode) {
		this.jobcode = jobcode;
	}

	public boolean isBackoffice() {
		return backoffice;
	}

	public void setBackoffice(boolean backoffice) {
		this.backoffice = backoffice;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isPos() {
		return pos;
	}

	public void setPos(boolean pos) {
		this.pos = pos;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	public long getbirth(){
		return birthday;
	}
	
	public EmployeeInformation(String firstName, String lastName,
			String middleName, String email, String password, int id,
			int access_id, int jobcode, boolean backoffice, boolean admin,
			boolean pos, boolean disabled,long birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleName = middleName;
		this.email = email;
		this.password = password;
		this.id = id;
		this.access_id = access_id;
		this.jobcode = jobcode;
		this.backoffice = backoffice;
		this.admin = admin;
		this.pos = pos;
		this.disabled = disabled;
		this.birthday = birthday;
	}
	
	
}
