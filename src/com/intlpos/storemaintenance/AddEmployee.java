package com.intlpos.storemaintenance;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

public class AddEmployee extends Fragment implements OnCheckedChangeListener, OnClickListener{
	
	TextView id, access, pass, email, admintext;
	CheckBox pos, backoffice, admin;
	EditText text[] = new EditText[7];
	Button save,delete,cancel;
	DatePicker birthday;
	EmployeeInformation emp;
	
	public AddEmployee(){
		
	}
	
	public AddEmployee(EmployeeInformation emp){
		this.emp = emp;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.addemployee, container, false);
		getWidgets(view);
		setWidgets();
		if(emp != null){
			setValues();
		}
		return view;
	}

	private void getWidgets(View v){
		id = (TextView) v.findViewById(R.id.employeeid);
		access = (TextView) v.findViewById(R.id.accessid1);
		admintext = (TextView) v.findViewById(R.id.adminText);
		email = (TextView) v.findViewById(R.id.emailText);
		pass = (TextView) v.findViewById(R.id.passText);
		
		pos = (CheckBox) v.findViewById(R.id.accesstopos);
		backoffice = (CheckBox) v.findViewById(R.id.accesstobo);
		admin = (CheckBox) v.findViewById(R.id.adminaccess);
		
		text[0] = (EditText) v.findViewById(R.id.firstname);
		text[1] = (EditText) v.findViewById(R.id.lastname);
		text[2] = (EditText) v.findViewById(R.id.middle);
		text[3] = (EditText) v.findViewById(R.id.setemail);
		text[4] = (EditText) v.findViewById(R.id.accessid);
		text[5] = (EditText) v.findViewById(R.id.emailid);
		text[6] = (EditText) v.findViewById(R.id.password);
		
		birthday = (DatePicker) v.findViewById(R.id.date);
		save = (Button) v.findViewById(R.id.saveEmployee);
		cancel = (Button) v.findViewById(R.id.cancelEmployee);
		delete = (Button) v.findViewById(R.id.deleteEmployee);
	}
	
	private void setWidgets(){
		pos.setOnCheckedChangeListener(this);
		backoffice.setOnCheckedChangeListener(this);
		admin.setOnCheckedChangeListener(this);
		
		save.setOnClickListener(this);
		cancel.setOnClickListener(this);
		delete.setOnClickListener(this);
	}
	
	private void setValues(){
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(emp.getbirth());
		Log.d("DATECAL", calendar.get(Calendar.YEAR)+ " "+ calendar.get(Calendar.MONTH) + " " + calendar.get(Calendar.DAY_OF_MONTH));
		birthday.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
		delete.setVisibility(View.VISIBLE);
		save.setText(getActivity().getResources().getString(R.string.update));
		id.setText(String.valueOf(emp.getId()));
		text[0].setText(emp.getFirstName());
		text[1].setText(emp.getLastName());
		text[2].setText(emp.getMiddleName());
		text[3].setText(emp.getEmail());
		
		if(emp.isPos()){
			pos.setChecked(true);
			String id = String.valueOf(emp.getAccess_id());
			text[4].setText(id);
			admin.setChecked(emp.isAdmin());
		}else{
			pos.setChecked(false);
			text[4].setText("");
			admin.setChecked(false);
		}
		
		if(emp.isBackoffice()){
			backoffice.setChecked(true);
			text[6].setText(emp.getPassword());
		}else{
			backoffice.setChecked(false);
		}
	}
	
	private void saveCustomer(){
		boolean callWebservice = false;
		
		for(int i = 0; i < 4; i++){
			if(text[i].getText().toString().isEmpty()){
				if(i == 0){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.firstfix), Toast.LENGTH_LONG).show();
					return;
				}else if(i == 1){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.lastfix), Toast.LENGTH_LONG).show();
					return;
				}else if(i == 2){
					callWebservice = true;
				}else if(i == 3){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.fixemail), Toast.LENGTH_LONG).show();
					return;
				}	
			}else{
				callWebservice = true;
			}
		}
		
		if(!text[3].getText().toString().contains("@") && text[3].getText().toString().length() < 5){
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.fixemail), Toast.LENGTH_LONG).show();
			return;
		}
		
		if(pos.isChecked()){
			if(text[4].getText().toString().isEmpty()){
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.accessfix), Toast.LENGTH_LONG).show();
				return;
			}else{
				callWebservice = true;
			}
		}
		if(backoffice.isChecked()){
			if(text[6].getText().toString().isEmpty()){
				Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.passfix), Toast.LENGTH_LONG).show();
				return;
			}else{
				if(text[6].getText().toString().length() < 7){
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.passfix), Toast.LENGTH_LONG).show();
					return;
				}else{
					if(!text[6].getText().toString().matches("^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$")){
						Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.passfix), Toast.LENGTH_LONG).show();
						return;
					}else{
						callWebservice = true;
					}
				}
				
			}
			
			
		}
		
		if(callWebservice){
			if(emp == null)
				executeSaveCustomer();
			else
				updateCustomer();
		}else{
			return;
		}
		
	}
	
	public void executeSaveCustomer(){
		
		int day = birthday.getDayOfMonth();
		int month = birthday.getMonth();
		int year = birthday.getYear();
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		
		long birthTime = calendar.getTimeInMillis();
		String birthdayDate = "/Date("+birthTime+")/";
		String dateOfPassword = "/Date("+System.currentTimeMillis()+")/";
		Log.d("birthdayDate", birthdayDate);
		Log.d("dateOfPassword", dateOfPassword);
		
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/saveemployee";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        LinkedHashMap<String, Object> employee = new LinkedHashMap<String, Object>();
        employee.put("employees_id", null);
        employee.put("email_id",text[3].getText().toString());
        employee.put("access_id",text[4].getText().toString());
        employee.put("card_swipeid","");
        employee.put("disabled",false);
        employee.put("first_name",text[0].getText().toString());
        employee.put("middle_name",text[2].getText().toString());
        employee.put("last_name",text[1].getText().toString());
        employee.put("contact_no",null);
        employee.put("birthday",birthdayDate);
        employee.put("picture",null);
        employee.put("job_code",1);
        employee.put("last_passchange",dateOfPassword);
        employee.put("reset_passflag",false);
        employee.put("admin_access", admin.isChecked());
        employee.put("access_to_pos", pos.isChecked());
        employee.put("access_to_backoffice", backoffice.isChecked());
        employee.put("password", text[6].getText().toString());
        
        params.put("storedetail", storedetail);
        params.put("employee", employee);
        
        JSONParser jParser = new JSONParser(new MyListener(){
        	
        	@Override
        	public void onResult(JSONObject json){
        		boolean result;
				try {
					result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        });
        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void updateCustomer(){
		int day = birthday.getDayOfMonth();
		int month = birthday.getMonth();
		int year = birthday.getYear();
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		
		long birthTime = calendar.getTimeInMillis();
		String birthdayDate = "/Date("+birthTime+")/";
		String dateOfPassword = "/Date("+System.currentTimeMillis()+")/";
		Log.d("birthdayDate", birthdayDate);
		Log.d("dateOfPassword", dateOfPassword);
		
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/updateemployee";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        LinkedHashMap<String, Object> employee = new LinkedHashMap<String, Object>();
        employee.put("employees_id", emp.getId());
        employee.put("email_id",text[3].getText().toString());
        employee.put("access_id",text[4].getText().toString());
        employee.put("card_swipeid","");
        employee.put("disabled",false);
        employee.put("first_name",text[0].getText().toString());
        employee.put("middle_name",text[2].getText().toString());
        employee.put("last_name",text[1].getText().toString());
        employee.put("contact_no",null);
        employee.put("birthday",birthdayDate);
        employee.put("picture",null);
        employee.put("job_code",1);
        employee.put("last_passchange",dateOfPassword);
        employee.put("reset_passflag",false);
        employee.put("admin_access", admin.isChecked());
        employee.put("access_to_pos", pos.isChecked());
        employee.put("access_to_backoffice", backoffice.isChecked());
        employee.put("password", text[6].getText().toString());
        
        params.put("storedetail", storedetail);
        params.put("employee", employee);
        
        JSONParser jParser = new JSONParser(new MyListener(){
        	
        	@Override
        	public void onResult(JSONObject json){
        		boolean result;
				try {
					result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        });
        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void deleteEmployee(){
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/deleteemployee";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        params.put("storedetail", storedetail);
        params.put("employees_id", emp.getId());
        
        JSONParser jParser = new JSONParser(new MyListener(){
        	
        	@Override
        	public void onResult(JSONObject json){
        		boolean result;
				try {
					result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
        });
        jParser.execute(CornerStorePOS.Url, methodName, params);
        
	}
	
	@Override
	public void onCheckedChanged(CompoundButton box, boolean checked) {
		switch(box.getId()){
			case R.id.accesstopos:
				if(checked){
					text[4].setVisibility(View.VISIBLE);
					admin.setVisibility(View.VISIBLE);
					admintext.setVisibility(View.VISIBLE);
					access.setVisibility(View.VISIBLE);
				}else{
					admintext.setVisibility(View.GONE);
					access.setVisibility(View.GONE);
					text[4].setVisibility(View.GONE);
					text[4].setText("");
					admin.setVisibility(View.GONE);
					admin.setChecked(false);
				}
				break;
			case R.id.accesstobo:
				if(checked){
					text[5].setVisibility(View.VISIBLE);
					text[6].setVisibility(View.VISIBLE);
					
					pass.setVisibility(View.VISIBLE);
					email.setVisibility(View.VISIBLE);
					
					text[5].setText(text[3].getText().toString());
					text[5].setEnabled(false);
				}else{
					text[5].setVisibility(View.GONE);
					text[6].setVisibility(View.GONE);
					pass.setVisibility(View.GONE);
					email.setVisibility(View.GONE);
					text[5].setText("");
					text[6].setText("");
				}
				break;
			case R.id.adminaccess:
				
				break;
		}
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch(arg0.getId()){
			case R.id.saveEmployee:
				saveCustomer();
				break;
			case R.id.deleteEmployee:
				 deleteEmployee();
				break;
			case R.id.cancelEmployee:
				getFragmentManager().popBackStack();
				break;
		}
	}
	
}
