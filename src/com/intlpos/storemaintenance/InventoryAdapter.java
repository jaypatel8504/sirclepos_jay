package com.intlpos.storemaintenance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Product;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class InventoryAdapter extends ArrayAdapter<HashMap<String, Object>>{
	
	private ArrayList<HashMap<String, Object>> products;
	private Context context; 
	
	public InventoryAdapter(Context context, int resource, ArrayList<HashMap<String, Object>> products) {
		super(context, resource, products);
		this.context = context;
		this.products = products;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 View rowView = inflater.inflate(R.layout.inventorylist, parent, false);
		 TextView item = (TextView) rowView.findViewById(R.id.Item);
		 TextView name = (TextView) rowView.findViewById(R.id.ItemName);
		 TextView tax1 = (TextView) rowView.findViewById(R.id.tax1);
		 TextView tax2 = (TextView) rowView.findViewById(R.id.tax2);
		 TextView tax3 = (TextView) rowView.findViewById(R.id.tax3);
		 TextView cost = (TextView) rowView.findViewById(R.id.Cost);
		 TextView price = (TextView) rowView.findViewById(R.id.Price);
		 TextView stock = (TextView) rowView.findViewById(R.id.Stock);
		 
		 
		 
		return rowView;
	}

}
