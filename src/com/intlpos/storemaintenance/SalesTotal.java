package com.intlpos.storemaintenance;

public class SalesTotal {
	
	private String sales, total;

	public String getSales() {
		return sales;
	}

	public void setSales(String sales) {
		this.sales = sales;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public SalesTotal(String sales, String total) {
		this.sales = sales;
		this.total = total;
	}
	
	
}
