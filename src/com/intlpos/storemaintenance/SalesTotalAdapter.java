package com.intlpos.storemaintenance;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.intlpos.sirclepos.R;

public class SalesTotalAdapter extends ArrayAdapter<SalesTotal>{
	private ArrayList<SalesTotal> sales;
	private Context context;
	
	public SalesTotalAdapter(Context context, int resource,
			ArrayList<SalesTotal> sales) {
		super(context, resource, sales);
		this.context = context;
		this.sales = sales;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 LayoutInflater inflater = (LayoutInflater) context
			        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View view = inflater.inflate(R.layout.salestotal, parent, false);
		TextView key = (TextView) view.findViewById(R.id.salesKey);
		TextView value = (TextView) view.findViewById(R.id.salesValue);
		key.setText(sales.get(position).getSales());
		value.setText(sales.get(position).getTotal());	
		return view;
	}
	
	
}
