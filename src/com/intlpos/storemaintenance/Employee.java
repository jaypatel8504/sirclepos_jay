package com.intlpos.storemaintenance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.AccountsAdapter;
import com.intlpos.dialogs.Accounts.addCustomer;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.Settings;
import com.john.beans.Customer;

public class Employee extends Fragment implements TextWatcher{
	private ListView employees;
	private EditText search;
	private Button add;
	private ProgressDialog ringProgressDialog;
	private EmployeeAdapter adapter;
	private ArrayList<EmployeeInformation>employeeInfo, searchEmployee;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.employee,container, false);
		View header = inflater.inflate(R.layout.employeeheader, null);
		getWidgets(view);
		setWidgets(header);
		getEmployees();
		return view;
	}
	
	private void getWidgets(View v){
		search = (EditText) v.findViewById(R.id.getFirstName);
		add = (Button) v.findViewById(R.id.add);
		employees = (ListView) v.findViewById(R.id.listofem);
	}
	
	private void setWidgets(View header){
		search.setEnabled(false);
		search.addTextChangedListener(this);
		
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				((Settings) getActivity()).addFrag(null);
			}
		});
		
		employees.addHeaderView(header,"",false);
	}
	
	private void getEmployees(){
		ringProgressDialog =  ProgressDialog.show(getActivity(),getActivity().getResources().getString(R.string.pleasewait), getActivity().getResources().getString(R.string.loading));
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/getlistofemployees";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);

        params.put("storedetail", storedetail);

        JSONParser jParser = new JSONParser(new MyListener() {

        	@Override
            public void onResult(JSONObject json) {
        		try {
        			employeeInfo = new ArrayList<EmployeeInformation>();
        			
					JSONArray employeelist = json.getJSONArray("employee_list");		
					Log.d("emply", employeelist.toString());
					for(int i = 0; i < employeelist.length(); i++){
						JSONObject employ = employeelist.getJSONObject(i);
						int access_id;
						if(employ.isNull("access_id")){
							access_id = 0;
						}else{
							access_id = employ.getInt("access_id");
						}
						
						boolean backoffice = employ.getBoolean("access_to_backoffice");
						boolean access_to_pos = employ.getBoolean("access_to_pos");
						boolean admin = employ.getBoolean("admin_access");
						String email = employ.getString("email_id");
						int id = employ.getInt("employees_id");
						String first = employ.getString("first_name");
						String last = employ.getString("last_name");
						String middle_name = employ.getString("middle_name");
						String password;
						
						if(employ.isNull("access_id")){
							password = "";
						}else{
							password = employ.getString("password");
						}
						int jobcode = employ.getInt("job_code");
						String dateStr = employ.getString("birthday");
						String temp = dateStr.replaceAll("/", "");
						temp =  temp.replaceAll("Date", "").replace("(", "").replace(")", "").replace("-", "");
						Log.d("DATE", temp);
						long birth = Long.valueOf(temp);
//						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//						Date birthDate = sdf.parse(dateStr);
//						Log.d("date", birthDate.toString());
				
						EmployeeInformation employeeObj = new EmployeeInformation(first,last,
																				  middle_name, email, password, id,
																				  access_id, jobcode, backoffice, admin,
																				  access_to_pos, false,birth);
						employeeInfo.add(employeeObj);
					}
					
					if(employeeInfo.size() > 0){
						search.setEnabled(true);
						adapter = new EmployeeAdapter(getActivity(),R.layout.employeelist, employeeInfo);
						employees.setAdapter(adapter);
						setListListener();
					}
					ringProgressDialog.dismiss();
				}catch (Exception e) {	
					ringProgressDialog.dismiss();
				}
           }
        });
        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	
	private void setListListener(){
		employees.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
		{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
					if(employeeInfo.get(position-1).getId()!= 1){
						EmployeeInformation info = employeeInfo.get(position-1);
						((Settings) getActivity()).addFrag(info);
					}
			   }
		});
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		if(cs.length() >= 1){
			searchEmployee = new ArrayList<EmployeeInformation>();
			for(EmployeeInformation e : employeeInfo){
				if(e.getFirstName().toLowerCase().contains(cs.toString().toLowerCase())){
					searchEmployee.add(e);
				}
			}
			adapter = new EmployeeAdapter(getActivity(),R.layout.employeelist, searchEmployee);
			employees.setAdapter(adapter);
			employees.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
			{
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {
						if(searchEmployee.get(position-1).getId()!= 1){
							EmployeeInformation info = searchEmployee.get(position-1);
							((Settings) getActivity()).addFrag(info);
						}
				   }
			});
		}else{
			adapter = new EmployeeAdapter(getActivity(),R.layout.employeelist, employeeInfo);
			employees.setAdapter(adapter);
			setListListener();
		}
	}
	
	

}
