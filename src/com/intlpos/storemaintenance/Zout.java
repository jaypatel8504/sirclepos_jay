package com.intlpos.storemaintenance;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.intlpos.WCFAccess.JSONArrayListener;
import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.JSONReport;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.database.Invoice;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.PreviousStartTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.InvoiceHome;
import com.intlpos.sirclepos.MainLogin;
import com.intlpos.sirclepos.RegisterTablet;
import com.intlpos.sirclepos.Star;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.john.bluetoothprinter.helper.PrinterCommands;
import com.john.bluetoothprinter.helper.ZoutReport;
import com.partner.pt100.printer.PrinterApiContext;
import com.partner.pt100.printer.PrinterManage;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManager;

public class Zout extends DialogFragment implements OnClickListener{
	private CheckBox printBreakDown;
	private Button print, cancel;
	private TextView startTimeText, endTimeText;
	private ListView salesTotal, department, payment;
	private SalesTotalAdapter adapter, departmentAdapter, paymentAdapter;
	private PrintDataService printDataService = null; 
	private ArrayList<SalesTotal> salesTotalValues;
	private ArrayList<SalesTotal> departTotalValues;
	private ArrayList<SalesTotal> paymentValues;
	private ZoutReport zprint;
	private String payTypes[];
	private List<String>employees;
	private boolean logout, zout;
	private String start, end,eid,station;
	private CornerStorePOS app;
	private PrinterApiContext printApiContext;
	private StarIoExtManager mStarIoExtManager;
	private Star star;
	public static Zout newInstance(boolean logout, boolean zout){
		Zout z = new Zout();
		Bundle bundle = new Bundle();
		bundle.putBoolean("log", logout);
		bundle.putBoolean("zout", zout);
		z.setArguments(bundle);
		return z;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		logout = getArguments().getBoolean("log");
		zout = getArguments().getBoolean("zout");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setCancelable(false);
		app = (CornerStorePOS)getActivity().getApplicationContext();
		employees =  employee();
		if(zout)
			  getDialog().setTitle("Z-Out Report");
		  else
			  getDialog().setTitle("X-Out Report");
		
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
		
		View view = inflater.inflate(R.layout.zoutreport, container, false);
		getWidgets(view);
		 List<String> sid = new ArrayList<String>();
	    sid.add(CornerStorePOS.StationId);
	    Gson gson = new Gson();
	    eid = gson.toJson(employees);
	    station = gson.toJson(sid);
		getReport();
		
		
		return view;
	}
	
	private List<String> employee(){
		List<String> employee = new ArrayList<String>();
		for(EmployeeInformation e: app.employeeInfo){
			employee.add(Integer.toString(e.getId()));
		}
		return employee;
	}
	
	private void getWidgets(View v){
		salesTotal = (ListView) v.findViewById(R.id.salesTotal);
		department = (ListView) v.findViewById(R.id.departSales);
		payment = (ListView) v.findViewById(R.id.paymentTotals);
		startTimeText = (TextView) v.findViewById(R.id.startTimeZ);
		endTimeText = (TextView) v.findViewById(R.id.endTimeZ);
		print = (Button) v.findViewById(R.id.PrintZout);
		cancel = (Button) v.findViewById(R.id.Cancel);
		printBreakDown = (CheckBox) v.findViewById(R.id.PrintR);
		print.setOnClickListener(this);
		cancel.setOnClickListener(this);
		payTypes = new String[7];
		for(int i = 0; i <7; i++){
		  	payTypes[i] = "0.00";
		}
	}
	
	private void getReport() {
		StartTime startTime = CSSharedPreferences.loadStartTime();
		EndTime endTime = CSSharedPreferences.loadEndTime();
		PreviousStartTime prevTime = CSSharedPreferences.loadPrevTime();
		
		if(zout){
			start = startTime.getTime();
//			Date date = new SimpleDateFormat("yyyy/MM/dd HH:mma").parse(start);
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String currentDateandTime = sdf.format(new Date());
//			String newstring = sdf.format(date);
//			start = newstring;
			end = currentDateandTime;
		}else{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String currentDateandTime = sdf.format(new Date());
			start = startTime.getTime();
			end = currentDateandTime;
		}
		DateFormat f1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat f2 = new SimpleDateFormat("MM/dd/yyyy h:mma");
		Date d1;
		try {
			d1 = f1.parse(start);
		
			Date d2 = f1.parse(end);
			startTimeText.setText(f2.format(d1));
			endTimeText.setText(f2.format(d2));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		final ProgressDialog ringProgressDialog =  ProgressDialog.show(getActivity(),getActivity().getResources().getString(R.string.pleasewait), getActivity().getResources().getString(R.string.loading));
		final String methodName = "Reports/DetailDailyReport.svc/displaydailyreport";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        params.put("storedetail", storedetail);
        params.put("stationids", station);
        params.put("employeeids", eid);
        params.put("startdate", start);
        params.put("enddate", end);
        
        JSONReport jParser = new JSONReport(new JSONArrayListener() {

            @Override
            public void onResult(JSONArray json) {
	            try {

	            	salesTotalValues = new ArrayList<SalesTotal>();
	            	for(int i = 0; i < json.length(); i++){
	            		JSONObject saleAmount = json.getJSONObject(i);
	            		Log.d("REPORT", saleAmount.toString());
	            		String key = saleAmount.getString("key");
	            		String value = saleAmount.getString("value");
	            		if(value.isEmpty())
	            			value = "0.00";
	            		String extraZeros[]= value.split("\\.");
	            		if(extraZeros[1].length() > 2){
	            			value = extraZeros[0] + "." + extraZeros[1].substring(0, 2);
	            		}
	            		
 	            		SalesTotal st = new SalesTotal(key,value);
	            		salesTotalValues.add(st);
	            	}
	            	Log.d("REPORT", "ADDED");
	            	adapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, salesTotalValues);
	            	salesTotal.setAdapter(adapter);
	            	setListViewHeightBasedOnChildren(salesTotal);
	            	getDepartment();
	            	getPayments();
	            	ringProgressDialog.dismiss();
	            }catch (JSONException e) {
	            	
	            	JSONReport jParser = new JSONReport(new JSONArrayListener() {

	                    @Override
	                    public void onResult(JSONArray json) {
	        	            try {
	        	            	Log.d("REPORT", json.toString());
	        	            	salesTotalValues = new ArrayList<SalesTotal>();
	        	            	for(int i = 0; i < json.length(); i++){
	        	            		JSONObject saleAmount = json.getJSONObject(i);
	        	            		Log.d("REPORT", saleAmount.toString());
	        	            		String key = saleAmount.getString("key");
	        	            		String value = saleAmount.getString("value");
	        	            		if(value.isEmpty())
	        	            			value = "0.00";
	        	            		String extraZeros[]= value.split("\\.");
	        	            		if(extraZeros[1].length() > 2){
	        	            			value = extraZeros[0] + "." + extraZeros[1].substring(0, 2);
	        	            		}
	        	            		
	         	            		SalesTotal st = new SalesTotal(key,value);
	        	            		salesTotalValues.add(st);
	        	            	}
	        	            	Log.d("REPORT", "ADDED");
	        	            	adapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, salesTotalValues);
	        	            	salesTotal.setAdapter(adapter);
	        	            	setListViewHeightBasedOnChildren(salesTotal);
	        	            	getDepartment();
	        	            	getPayments();
	        	            	ringProgressDialog.dismiss();
	        	            }catch (Exception e) {
	        	            	ringProgressDialog.dismiss();       
	        	            }             
	                   }
	                });

	                jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	            	
	            }             
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);

	}
	
	
	private void getDepartment(){
		final String methodName = "Reports/DetailDailyReport.svc/displaydeptreport";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);

        StartTime startTime = CSSharedPreferences.loadStartTime();
		EndTime endTime = CSSharedPreferences.loadEndTime();
        
        params.put("storedetail", storedetail);
        params.put("stationids", station);
        params.put("employeeids", eid);
        params.put("startdate", start);
        params.put("enddate", end);
        
        JSONReport jParser = new JSONReport(new JSONArrayListener() {

            @Override
            public void onResult(JSONArray json) {
	            try {
	            	Log.d("REPORT", json.toString());
	            	departTotalValues = new ArrayList<SalesTotal>();
//	            	ArrayList<SalesTotal> departSalesValues = new ArrayList<SalesTotal>();
	            	for(int i = 0; i < json.length(); i++){
	            		JSONObject saleAmount = json.getJSONObject(i);
	            		Log.d("REPORT", saleAmount.toString());
	            		JSONArray key = saleAmount.getJSONArray("Key");
	            		JSONArray value = saleAmount.getJSONArray("Value");
	            		
	            		for(int k = 0; k < key.length(); k++){
	            			JSONObject department = key.getJSONObject(k);
	            			String deptInfo = department.getString("key");
	            			String deptValue = department.getString("value");
	            			SalesTotal depart = new SalesTotal(deptInfo, deptValue);
	            			departTotalValues.add(depart);
	            		}
	            		for(int v = 0; v < value.length(); v++){
	            			JSONObject items = value.getJSONObject(v);
	            			String saleInfo = items.getString("key");
	            			String saleValue = items.getString("value");
	            			String extraZeros[]= saleValue.split("\\.");
		            		if(extraZeros[1].length() > 2){
		            			saleValue = extraZeros[0] + "." + extraZeros[1].substring(0, 2);
		            		}
	            			SalesTotal deptItems = new SalesTotal(saleInfo, saleValue);
	            			departTotalValues.add(deptItems);
	            		}
	            	}
	            	departmentAdapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, departTotalValues);
	            	department.setAdapter(departmentAdapter);
	            	setListViewHeightBasedOnChildren(department);
	            }catch (Exception e) {   
	            	JSONReport jParser = new JSONReport(new JSONArrayListener() {

	                    @Override
	                    public void onResult(JSONArray json) {
	        	            try {
	        	            	Log.d("REPORT", json.toString());
	        	            	departTotalValues = new ArrayList<SalesTotal>();
//	        	            	ArrayList<SalesTotal> departSalesValues = new ArrayList<SalesTotal>();
	        	            	for(int i = 0; i < json.length(); i++){
	        	            		JSONObject saleAmount = json.getJSONObject(i);
	        	            		Log.d("REPORT", saleAmount.toString());
	        	            		JSONArray key = saleAmount.getJSONArray("Key");
	        	            		JSONArray value = saleAmount.getJSONArray("Value");
	        	            		
	        	            		for(int k = 0; k < key.length(); k++){
	        	            			JSONObject department = key.getJSONObject(k);
	        	            			String deptInfo = department.getString("key");
	        	            			String deptValue = department.getString("value");
	        	            			SalesTotal depart = new SalesTotal(deptInfo, deptValue);
	        	            			departTotalValues.add(depart);
	        	            		}
	        	            		for(int v = 0; v < value.length(); v++){
	        	            			JSONObject items = value.getJSONObject(v);
	        	            			String saleInfo = items.getString("key");
	        	            			String saleValue = items.getString("value");
	        	            			String extraZeros[]= saleValue.split("\\.");
	        		            		if(extraZeros[1].length() > 2){
	        		            			saleValue = extraZeros[0] + "." + extraZeros[1].substring(0, 2);
	        		            		}
	        	            			SalesTotal deptItems = new SalesTotal(saleInfo, saleValue);
	        	            			departTotalValues.add(deptItems);
	        	            		}
	        	            	}
	        	            	departmentAdapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, departTotalValues);
	        	            	department.setAdapter(departmentAdapter);
	        	            	setListViewHeightBasedOnChildren(department);
	        	            }catch (Exception e) {    
	        	            }             
	                   }
	                });

	                jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	            }             
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void getPayments(){
		final String methodName = "Reports/DetailDailyReport.svc/displaybypaymenttypereport";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);

        StartTime startTime = CSSharedPreferences.loadStartTime();
		EndTime endTime = CSSharedPreferences.loadEndTime();
        
        params.put("storedetail", storedetail);
        params.put("stationids",station);
        params.put("employeeids", eid);
        params.put("startdate", start);
        params.put("enddate", end);
        
        JSONReport jParser = new JSONReport(new JSONArrayListener() {

            @Override
            public void onResult(JSONArray json) {
	            try {
	            	Log.d("REPORT", json.toString());
	            	paymentValues = new ArrayList<SalesTotal>();
	            	for(int i = 0; i < json.length(); i++){
	            		JSONObject saleAmount = json.getJSONObject(i);
	            		
	            		String key = saleAmount.getString("Key");
	            		String pay = Invoice.PAYMENT_TYPE[Integer.valueOf(key)-1];
	            		
	            		String value = saleAmount.getString("Value");
	            		
	            		SalesTotal st = new SalesTotal(pay,value);
	            		paymentValues.add(st);
	            	}
	            	
	            	Log.d("REPORT", "ADDED");
	            	paymentAdapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, paymentValues);
	            	payment.setAdapter(paymentAdapter);
	            	setListViewHeightBasedOnChildren(payment);
	            	getDepartment();
	            }catch (Exception e) {
	            	JSONReport jParser = new JSONReport(new JSONArrayListener() {

	                    @Override
	                    public void onResult(JSONArray json) {
	        	            try {
	        	            	Log.d("REPORT", json.toString());
	        	            	paymentValues = new ArrayList<SalesTotal>();
	        	            	for(int i = 0; i < json.length(); i++){
	        	            		JSONObject saleAmount = json.getJSONObject(i);
	        	            		
	        	            		String key = saleAmount.getString("Key");
	        	            		String pay = Invoice.PAYMENT_TYPE[Integer.valueOf(key)-1];
	        	            		
	        	            		String value = saleAmount.getString("Value");
	        	            		
	        	            		SalesTotal st = new SalesTotal(pay,value);
	        	            		paymentValues.add(st);
	        	            	}
	        	            	
	        	            	Log.d("REPORT", "ADDED");
	        	            	paymentAdapter = new SalesTotalAdapter(getActivity(), R.layout.salestotal, paymentValues);
	        	            	payment.setAdapter(paymentAdapter);
	        	            	setListViewHeightBasedOnChildren(payment);
	        	            	getDepartment();
	        	            }catch (Exception e) {
	        	            	
	        	            }             
	                   }
	                });

	                jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	            }             
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	
	 private void getSharedReference() {
		  SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		  String address = preference.getString("list_bluetooth_printer", "");
			if (address == ""){
				printDataService = null;
				Toast.makeText(getActivity().getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
				return;
			}
			else{
				printDataService = new PrintDataService(getActivity().getApplicationContext(),address);
				printDataService.connect();
				if(!printDataService.isConnected)
					printDataService = null;
			}
	  }
	  
	  @Override
	  public void onStart() {
	      super.onStart();
	      if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_printer", false) == true){
	    	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()); 
		    	String print = preferences.getString("printerType", "");
		    	if(print.equals("Partner Tech")){
		    		printApiContext = new PrinterManage();
		    		printApiContext.open();
		    		printApiContext.initPrinter();
		    	}else if(print.equals("Star mPop")){
		    		mStarIoExtManager = new StarIoExtManager(StarIoExtManager.Type.WithBarcodeReader, "BT:"+preferences.getString("list_bluetooth_printer", ""), "", 10000, getActivity());     // 10000mS!!!
			        star = new Star(mStarIoExtManager,getActivity());
			        mStarIoExtManager.setListener(star.mStarIoExtManagerListener);
		    		star.starIoExtManagerConnect();
		    		
		    	}else
		    		getSharedReference();
		  }
	  } 
	
	
	
	 public static void setListViewHeightBasedOnChildren(ListView listView) {
         ListAdapter listAdapter = listView.getAdapter();
       if (listAdapter == null) {
       // pre-condition
             return;
       }

       int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
       for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
               listItem.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
       }

       ViewGroup.LayoutParams params = listView.getLayoutParams();
       params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
                 listView.setLayoutParams(params);
   }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.PrintZout:
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()); 
				String print = preferences.getString("printerType", "");
				if(!print.equals("Partner Tech") && !print.equals("Star mPop")){
					if(printDataService == null){
					  	Toast.makeText(getActivity().getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
						return;
					}
				}
			  
			  ByteArrayOutputStream output = new ByteArrayOutputStream();
			  try {	
				  	if(zout)
				  		RegisterTablet.setPreferenceStartTime();
				  	for(int i = 0; i < paymentValues.size();i++){
				  		if(paymentValues.get(i).getSales().equals("Cash")){
				  			payTypes[0] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("Credit")){
				  			payTypes[1] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("Debit")){
				  			payTypes[2] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("Gift Card")){
				  			payTypes[3] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("EBT")){
				  			payTypes[4] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("To Account")){
				  			payTypes[5] = paymentValues.get(i).getTotal();
				  		}else if(paymentValues.get(i).getSales().equals("Check")){
				  			payTypes[6] = paymentValues.get(i).getTotal();
				  		}
				  		
				  	}
				  	for(int i = 0; i < payTypes.length; i++){
				  		if(!payTypes[i].contains(".")){
				  			payTypes[i] += ".00";
				  		}
				  	}
				  	String title;
				  	if(zout)
				  		title = "ZOUT REPORT";
				  	else
				  		title = "XOUT REPORT";	
				  	zprint = new ZoutReport(getActivity(), salesTotalValues, departTotalValues, payTypes, printBreakDown.isChecked(),  true, title,start,end);
				  
				  	if(print.equals("Star mPop")){
				  		for (ArrayList<String> arr: zprint.printout){
							star.sendCommands(arr.get(0).getBytes());
			        	}
			    		star.sendCommands(PrinterCommands.STARFEED_PAPER_AND_CUT);
				  	}else if(print.equals("Partner Tech")){
				  		for (ArrayList<String> arr: zprint.printout){
							printApiContext.printData(arr.get(0).getBytes(),arr.get(0).getBytes().length);
						}
						printApiContext.feedLines(2);
				  	}else{
			        	for (ArrayList<String> arr: zprint.printout){
			        		output.write(printDataService.sendwithCommand(arr.get(0), Integer.parseInt(arr.get(1))));
			        	}
			        	
			    		if (!printDataService.isConnected){
			    			Toast.makeText(getActivity(), getString(R.string.printer_not_connected), Toast.LENGTH_SHORT).show();
			    			return;
			    		}
			    		//printDataService.sendFeedCutCommand();
			    		printDataService.sendByteArray(output.toByteArray());
			    		printDataService.sendFeedCutCommand();
			    		PrintDataService.disconnect();
				  	}
		    		dismiss();
		    		if(logout){
			    		Intent zoutIntent = new Intent(getActivity(), MainLogin.class);
			    		startActivity(zoutIntent);
		    		}
			  }	
			  catch (IOException e) {
				
				e.printStackTrace();
			}
				break;
			case R.id.Cancel:
				if(printDataService != null){
			  		PrintDataService.disconnect();
			  	}
			  	
			  	dismiss();
			  	
			  	if(logout == true){ 
		    		Intent zoutIntent = new Intent(getActivity(), MainLogin.class);
		    		startActivity(zoutIntent);
			  	}
				break;
		}
	}
}
