package com.intlpos.storemaintenance;

import java.util.ArrayList;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DepartAdapter extends ArrayAdapter<Department>{
	
	private ArrayList<Department> departments;
	private Context context;
	
	public DepartAdapter(Context context, int resource, ArrayList<Department> departments) {
		super(context, resource, departments);
		this.departments = departments;
		this.context = context;
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	    View rowView = inflater.inflate(R.layout.departmentlist, parent, false);
		TextView department = (TextView) rowView.findViewById(R.id.departname);
		TextView ts = (TextView) rowView.findViewById(R.id.touchscreen);
		department.setText(departments.get(position).getDept_Name());
		ts.setText(String.valueOf(departments.get(position).getTSDisplay()));
		
		return rowView;
	}
	
}
