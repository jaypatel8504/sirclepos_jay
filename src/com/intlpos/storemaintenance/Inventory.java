package com.intlpos.storemaintenance;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Product;

public class Inventory extends Fragment implements TextWatcher{
	private Product product;
	private ListView list;
	private Button add;
	private EditText search;
	
	public Inventory(Product product){
		this.product = product;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.inventory, container, false);
		View header = inflater.inflate(R.layout.inventoryheader, null);
		
		return view;
	}
	
	private void getWidgets(View v){
		list = (ListView) v.findViewById(R.id.listofinventory);
		add = (Button) v.findViewById(R.id.addIn);
		search = (EditText) v.findViewById(R.id.searchIn);
	}
	
	private void setWidgets(View v){
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		
		list.addHeaderView(v, "", false);
		search.setEnabled(false);
		search.addTextChangedListener(this);
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}
	
}