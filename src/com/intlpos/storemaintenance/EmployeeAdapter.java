package com.intlpos.storemaintenance;

import java.util.ArrayList;
import java.util.List;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class EmployeeAdapter extends ArrayAdapter<EmployeeInformation>{
	private ArrayList<EmployeeInformation> employees;
	private Context context;
	public EmployeeAdapter(Context context, int resource, ArrayList<EmployeeInformation> objects) {
		super(context, resource, objects);
		this.employees = objects;
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 View rowView = inflater.inflate(R.layout.employeelist, parent, false);
		 
		 TextView id = (TextView) rowView.findViewById(R.id.accessid);
		 TextView email = (TextView) rowView.findViewById(R.id.email);
		 TextView first = (TextView) rowView.findViewById(R.id.first);
		 TextView last = (TextView) rowView.findViewById(R.id.last); 
		 int eid = employees.get(position).getId();
		 Log.d("VALUES", Integer.toString(eid) + " " + employees.get(position).getEmail() + " " + employees.get(position).getFirstName());
		
		 id.setText(Integer.toString(eid));
		 email.setText(employees.get(position).getEmail());
		 first.setText(employees.get(position).getFirstName());
		 last.setText(employees.get(position).getLastName());
		return rowView;
	}

}
