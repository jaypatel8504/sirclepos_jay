package com.intlpos.storemaintenance;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.sirclepos.CornerStorePOS;

public class AddEditDepartment extends Fragment implements OnClickListener{
	private TextView auto;
	private EditText name;
	private CheckBox touchScreen;
	private Button save, delete, cancel;
	private Department department; 
	
	public AddEditDepartment(Department department){
		this.department = department;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.adddepart, container, false);
		getWidgets(view);
		setWidgets();
		if(department != null){
			getDepartment();
		}
		return view;
	}
	
	private void getWidgets(View v){
		auto = (TextView) v.findViewById(R.id.auto);
		name = (EditText) v.findViewById(R.id.departname);
		touchScreen = (CheckBox) v.findViewById(R.id.tschecked);
		
		save = (Button) v.findViewById(R.id.saveDepart);
		delete = (Button) v.findViewById(R.id.deleteDepart);
		cancel = (Button) v.findViewById(R.id.cancelDepart);
	}

	private void setWidgets(){
		save.setOnClickListener(this);
		delete.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}
	
	private void getDepartment(){
		auto.setText(department.getDept_Id());
		name.setText(department.getDept_Name());
		touchScreen.setChecked(department.getTSDisplay());
		delete.setVisibility(View.VISIBLE);
	}
	
	private void saveDepartment(){
		final String methodName = "DepartmentMaintenence/DepartmentService.svc/savedepartment";
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        LinkedHashMap<String, Object> department = new LinkedHashMap<String, Object>();
        department.put("department_id", "");
        department.put("department_name", name.getText().toString());
        department.put("tsdisplay", touchScreen.isChecked());
        
        params.put("storedetail", storedetail);
        params.put("department",department);

        JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	 try {
					boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	 
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void updateDepartment(){
		final String methodName = "DepartmentMaintenence/DepartmentService.svc/updatedepartment";
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        LinkedHashMap<String, Object> department = new LinkedHashMap<String, Object>();
        department.put("department_id", this.department.getDept_Id());
        department.put("department_name", name.getText().toString());
        department.put("tsdisplay", touchScreen.isChecked());
        
        params.put("storedetail", storedetail);
        params.put("department",department);

        JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	 try {
					boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	 
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void deleteDepartment(){
		final String methodName = "DepartmentMaintenence/DepartmentService.svc/deletedepartment";
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);
        
        params.put("storedetail", storedetail);
        params.put("department_id",department.getDept_Id());

        JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	 try {
					boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result){
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
						getFragmentManager().popBackStack();
					}else{
						Toast.makeText(getActivity(), resultString, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	 
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.saveDepart:
				if(department == null)
					saveDepartment();
				else{
					updateDepartment();
				}
				break;
			case R.id.deleteDepart:
				deleteDepartment();
				break;
			case R.id.cancelDepart:
				getFragmentManager().popBackStack();
				break;
		}
	}
	
	
	
}
