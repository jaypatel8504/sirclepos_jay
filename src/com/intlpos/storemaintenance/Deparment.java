package com.intlpos.storemaintenance;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.Settings;

public class Deparment extends Fragment implements TextWatcher{
	private Button add;
	private EditText search;
	private ListView listofdepart;
	private ProgressDialog ringProgressDialog;
	private ArrayList<Department> searchdepart;
	private DepartAdapter adapter;
	private CornerStorePOS app;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		View view = inflater.inflate(R.layout.department, container, false);
		View header = inflater.inflate(R.layout.departmentheader, null);
		getWidgets(view);
		setWidgets(header);
		getDepartments();
		return view;
	}
	
	private void getWidgets(View v){
		app = (CornerStorePOS) getActivity().getApplication();
		add = (Button) v.findViewById(R.id.adddepart);
		search = (EditText) v.findViewById(R.id.getdepart);
		listofdepart = (ListView) v.findViewById(R.id.listofdepart);
	}
	
	private void setWidgets(View v){
		search.setEnabled(false);
		search.addTextChangedListener(this);
		
		add.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((Settings) getActivity()).addDepartment(null);
			}
		});
		
		listofdepart.addHeaderView(v,"",false);
	}
	
	private void addListener(){
		listofdepart.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
		{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
						Department department = app.DepartmentList.get(position-1);
						((Settings) getActivity()).addDepartment(department);
			   }
		});
	}
	
	private void getDepartments(){
		ringProgressDialog =  ProgressDialog.show(getActivity(),getActivity().getResources().getString(R.string.pleasewait), getActivity().getResources().getString(R.string.loading));
		final String methodName = "DepartmentMaintenence/DepartmentService.svc/getdepartments";

        final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

        LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
        storedetail.put("email_id", CornerStorePOS.email_id);
        storedetail.put("password", CornerStorePOS.password);
        storedetail.put("store_id", CornerStorePOS.StoreId);
        storedetail.put("station_id", CornerStorePOS.StationId);

        params.put("storedetail", storedetail);

        JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	try {
            		if (json.isNull("department_list")) {
                    } else {
                    	search.setEnabled(true);
                    	app.DepartmentList = new ArrayList<Department>();
                        app.DepartmentList = Department.convertJSONtodepartment(json);
                        adapter = new DepartAdapter(getActivity(),R.layout.departmentlist,app.DepartmentList);
                        listofdepart.setAdapter(adapter);
                        addListener();
                        ringProgressDialog.dismiss();
                    }
                    } catch (Exception e) {
                        JSONParser jParser = new JSONParser(new MyListener() {

                        @Override
                        public void onResult(JSONObject json) {

                        	try {
	                            if (json.isNull("department_list")) {
	                            } else {
	                            	search.setEnabled(true);
	                            	app.DepartmentList = new ArrayList<Department>();
	                                app.DepartmentList = Department.convertJSONtodepartment(json);
	                                adapter = new DepartAdapter(getActivity(),R.layout.departmentlist,app.DepartmentList);
	                                listofdepart.setAdapter(adapter);
	                                addListener();
	                                ringProgressDialog.dismiss();
	                            }
	                            
                            } catch (Exception e) {
                            	ringProgressDialog.dismiss();
                            }             
                            }});

                            jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
                     }             
           }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		if(cs.length() >= 1){
			
			searchdepart = new ArrayList<Department>();
			for(Department d : app.DepartmentList){
				if(d.getDept_Name().toLowerCase().contains(cs.toString().toLowerCase())){
					searchdepart.add(d);
				}
			}
			adapter = new DepartAdapter(getActivity(),R.layout.departmentlist,searchdepart);
			listofdepart.setAdapter(adapter);
			listofdepart.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
			{
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {
							Department department = searchdepart.get(position-1);
							((Settings) getActivity()).addDepartment(department);
				   }
			});
		}else{
			 adapter = new DepartAdapter(getActivity(),R.layout.departmentlist,app.DepartmentList);
             listofdepart.setAdapter(adapter);
             addListener();
		}
	}
	
	

}
