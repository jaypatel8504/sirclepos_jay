package com.intlpos.dialogs;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;






import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.AccountsAdapter;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.InvoiceHome;
import com.john.beans.Customer;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Accounts extends DialogFragment implements TextWatcher{
	private ListView list;
	private AccountsAdapter adapter;
	private CornerStorePOS app;
	private EditText search;
	private ArrayList<Customer> customerList;
	private int id;
	private addCustomer add;
	static public Accounts newInstance(int cust){
		Accounts newAccounts = new Accounts();
		Bundle args = new Bundle();
		args.putInt("id", cust);
		newAccounts.setArguments(args);
		return  newAccounts;
	}
	
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		id = getArguments().getInt("id");
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.accounts, container, false);
		View header = inflater.inflate(R.layout.accountheader, null);
		add = (addCustomer) getActivity();
		Button addCust = (Button) view.findViewById(R.id.addCustomer);
		search = (EditText) view.findViewById(R.id.searchForCustomer);
		search.setEnabled(false);
		search.addTextChangedListener(this);
		addCust.setOnClickListener(new View.OnClickListener() {//Press the add customer button to open add customer dialog using the addCustomerToList listener
			
			@Override
			public void onClick(View v) {
				add.addCustomerToList();
				dismiss();
			}
		});
		getDialog().setTitle(getActivity().getResources().getString(R.string.Customers));

		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
		
		app = (CornerStorePOS) ((Activity) getActivity()).getApplication();
		list = (ListView) view.findViewById(R.id.accountsListView);
		list.addHeaderView(header, null, false);
		FillCustomerList();//fills customer list
		
		
		return view;
	}
	
	
	public interface addCustomer{
		public void addCustomerToList();
		public void setCustomerID(int id);
		public void setName(String name);
	}
	
	private void setListener(){
		list.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
		   {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
					add = (addCustomer) getActivity();
					add.setCustomerID(app.CustomerList.get(position-1).getCustomer_id());
					add.setName(app.CustomerList.get(position-1).getFirst_name() + " " + app.CustomerList.get(position-1).getLast_name());
					dismiss();
				}
			   });
	}
	
	private void FillCustomerList()
  {
                  final String methodName = "CustomerMaintenence/CustomerService.svc/getlistofcustomers";

                  final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

                  LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
                  storedetail.put("email_id", CornerStorePOS.email_id);
                  storedetail.put("password", CornerStorePOS.password);
                  storedetail.put("store_id", CornerStorePOS.StoreId);
                  storedetail.put("station_id", CornerStorePOS.StationId);

                  params.put("storedetail", storedetail);

                  JSONParser jParser = new JSONParser(new MyListener() {

                                  @Override
                                  public void onResult(JSONObject json) {
                                	JSONArray getArray;
									try {//Get customer information
										
										getArray = json.getJSONArray("customer_list");
										app.CustomerList = new ArrayList<Customer>();
										for(int i = 0; i < getArray.length(); i++)
	                                  	{
	                                  		JSONObject objects = getArray.getJSONObject(i);
	                                  		Log.d("STRING", objects.toString());
	                                  		double balance = objects.getDouble("account_balance");
	                                  		String first = objects.getString("first_name");
	                                  		String last = objects.getString("last_name");
	                                  		int id = objects.getInt("customer_id");
	                                  		String address1 = objects.getString("address1");
	                                  		String address2 = objects.getString("address2"); 
	                                  		String city = objects.getString("city");
	                                  		String email = objects.getString("email");
	                                  		String phone = objects.getString("phone_1");
 	                             			String state = objects.getString("state");
 	                             			String zipcode = objects.getString("zipcode");
	                                  		Customer customer = new Customer(balance, id, address1, address2, city, email, first, last, phone, state, zipcode);
	                                  		app.CustomerList.add(customer);//Add to list
	                                  	}
										if(app.CustomerList.size() > 0){
											search.setEnabled(true);
										}
										adapter = new AccountsAdapter(getActivity(),R.layout.accountlist,app.CustomerList,id,false, add);
										adapter.setAdapter(adapter);
										
										
										
										list.setAdapter(adapter);
										setListener();
										
									} catch (Exception e) {
										// TODO Auto-generated catch block
										
										JSONParser jParser = new JSONParser(new MyListener() {

			                                  @Override
			                                  public void onResult(JSONObject json) {
			                                	JSONArray getArray;
												try {
													
													getArray = json.getJSONArray("customer_list");
													app.CustomerList = new ArrayList<Customer>();
													for(int i = 0; i < getArray.length(); i++)
				                                  	{
				                                  		JSONObject objects = getArray.getJSONObject(i);
				                                  		Log.d("STRING", objects.toString());
				                                  		double balance = objects.getDouble("account_balance");
				                                  		String first = objects.getString("first_name");
				                                  		String last = objects.getString("last_name");
				                                  		int id = objects.getInt("customer_id");
				                                  		String address1 = objects.getString("address1");
				                                  		String address2 = objects.getString("address2"); 
				                                  		String city = objects.getString("city");
				                                  		String email = objects.getString("email");
				                                  		String phone = objects.getString("phone_1");
			 	                             			String state = objects.getString("state");
			 	                             			String zipcode = objects.getString("zipcode");
				                                  		Customer customer = new Customer(balance, id, address1, address2, city, email, first, last, phone, state, zipcode);
				                                  		app.CustomerList.add(customer);
				                                  	}
													if(app.CustomerList.size() > 0){
														search.setEnabled(true);
													}
													adapter = new AccountsAdapter(getActivity(),R.layout.accountlist,app.CustomerList,id,false, add);
													adapter.setAdapter(adapter);
													
													
													
													list.setAdapter(adapter);
													setListener();
													
												} catch (Exception e) {
													// TODO Auto-generated catch block
													
												}
			                                }
					                  });
					                  jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
									
									}
                                }
                  });
                  jParser.execute(CornerStorePOS.Url, methodName, params);
  }



	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {//search for customer
		// TODO Auto-generated method stub
		if(cs.length() >= 1){
			customerList = new ArrayList<Customer>();
			for(Customer c:app.CustomerList){
				if(c.getLast_name().toLowerCase().contains(cs.toString().toLowerCase())){//make everything you search for lowercase
					customerList.add(c);// if matching string is found add to list
				}
			}
			adapter = new AccountsAdapter(getActivity(),R.layout.accountlist,customerList,id,true,add);
			adapter.setAdapter(adapter);
			list.setAdapter(adapter);
			list.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
			   {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1, int position,
							long arg3) {
						add = (addCustomer) getActivity();
						add.setCustomerID(customerList.get(position-1).getCustomer_id());
						add.setName(customerList.get(position-1).getFirst_name() + " " + customerList.get(position-1).getLast_name());
						dismiss();
					}
				   });
		}else{
			adapter = new AccountsAdapter(getActivity(),R.layout.accountlist,app.CustomerList,id,false,add);
			adapter.setAdapter(adapter);
			list.setAdapter(adapter);
			setListener();
		}
	}
}
