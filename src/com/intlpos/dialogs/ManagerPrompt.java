package com.intlpos.dialogs;

import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Employee;
import com.intlpos.database.Permission;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.GotToBackOffice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;


public class ManagerPrompt extends PopupWindow{
	  
	
	private ProgressDialog ringProgressDialog;
	Context context;
	View layout;
	Button[] btn;
	Button login;
	EditText AccessIdEditText;
	TextView pinForgot;
	private Employee employee;
	private Permission permission;
	public ManagerPrompt(Context context,View layout,int height, int width)
    {
		super(layout, width, height, true);
		this.context = context;
		this.layout = layout;
		btn = new Button[13];
		//pictureURl = new ArrayList<ProductImages>();
        setWidgetReferences();
		addWidgetListeners();
		// show popupwindow
    }
	
	public ProgressDialog getRingProgressDialog() {
		return ringProgressDialog;
	}
	
	private void setWidgetReferences() {
		AccessIdEditText = (EditText)layout.findViewById(R.id.numberpadtext);
		btn[0] = (Button)layout.findViewById(R.id.button1);
		btn[1] = (Button)layout.findViewById(R.id.button2);
		btn[2] = (Button)layout.findViewById(R.id.button3);
		btn[3] = (Button)layout.findViewById(R.id.button4);
		btn[4] = (Button)layout.findViewById(R.id.button5);
		btn[5] = (Button)layout.findViewById(R.id.button6);
		btn[6] = (Button)layout.findViewById(R.id.button7);
		btn[7] = (Button)layout.findViewById(R.id.button8);
		btn[8] = (Button)layout.findViewById(R.id.button9);
		btn[9] = (Button)layout.findViewById(R.id.button0);
		btn[10] = (Button)layout.findViewById(R.id.button00);
		btn[11] = (Button)layout.findViewById(R.id.deletebutton);
		btn[12] = (Button)layout.findViewById(R.id.clearbutton);

		login = (Button)layout.findViewById(R.id.loginbutton);
		
		pinForgot = (TextView)layout.findViewById(R.id.forgotyourpin);
		pinForgot.setText(context.getResources().getString(R.string.getManager));
		
	}
	
	public void show()
    {
    	layout.post(new Runnable() {
    		   public void run() {
    		     showAtLocation(layout, Gravity.CENTER, 0, 0);
    		   }
    		});
    }
	
	private void addWidgetListeners() {
		
		for(int i =0;i<13;i++){
			btn[i].setOnClickListener(new LoginButtonListener());
		}
	}
	
	
	private class LoginButtonListener implements OnClickListener {

		@Override
		public void onClick(View v){
		
		
		switch(v.getId()){
		case R.id.button1:
			addtoarray("1");
			break;
		case R.id.button2:
			addtoarray("2");
			break;
		case R.id.button3:
			addtoarray("3");
			break;
		case R.id.button4:
			addtoarray("4");
			break;
		case R.id.button5:
			addtoarray("5");
			break;
		case R.id.button6:
			addtoarray("6");
			break;
		case R.id.button7:
			addtoarray("7");
			break;
		case R.id.button8:
			addtoarray("8");
			break;
		case R.id.button9:
			addtoarray("9");
			break;
		case R.id.button0:
			addtoarray("0");
			break;
		case R.id.button00:
			addtoarray("00");
			break;
		case R.id.deletebutton:

			//get the length of input
			int length = AccessIdEditText.getText().length();
			if (length > 0) {
				AccessIdEditText.getText().delete(length - 1, length);
			}
			break;
		case R.id.clearbutton:
			//AccessIdEditText = (EditText)layout.findViewById(R.id.numberpadtext);
			AccessIdEditText.setText("");
			break;
		default:

		}
	}
	
	}
	public void addtoarray(String numbers){		
		//register TextBox 		
		AccessIdEditText.append(numbers);		
	}
	
	
	public Button getLogin(){
		return login;
	}
	public Employee getEmployee(){
		return employee;
	}
	public Permission getPermission(){
		return permission;
	}
	public String getAcess(){
		return AccessIdEditText.getText().toString();
	}
	
	public void close(boolean granted){
//		ringProgressDialog.dismiss();
		if(granted)
			dismiss();
	}
	
	public void getManagerPermission(){
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/getemployeebyid";
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);

		
		params.put("storedetail", storedetail);
		params.put("access_id",AccessIdEditText.getText());
		
		 JSONParser jParser = new JSONParser(new MyListener() {
			 
			 @Override
			 public void onResult(JSONObject json) {
				 try {
					if(json.getJSONObject("employee").getString("employees_id").isEmpty()){}
					else{
						 employee = Employee.convertjsontoemp(json);
						 permission = Permission.convertjsontopermission(json);
					
					}
					ringProgressDialog.dismiss();
					dismiss();
				 } catch (JSONException e) {
					 ringProgressDialog.dismiss();
					 dismiss();
						e.printStackTrace();
				 }
			 }
		});
		jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	}
}
