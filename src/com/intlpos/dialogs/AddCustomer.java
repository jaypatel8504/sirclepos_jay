package com.intlpos.dialogs;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLoginPopupWindow;
import com.intlpos.sirclepos.InvoiceHome;
import com.john.beans.Customer;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddCustomer extends DialogFragment implements OnClickListener{
	private EditText info[] = new EditText[9];
	private Button ok;
	public static AddCustomer newInstance(){
		return new AddCustomer();
	}
	
	public interface OpenAccounts{
		public void open();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.toaccount, null, false);
		getDialog().setTitle(R.string.addCust);
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
		setWidgets(view);
		return view;
	}
	
	private void setWidgets(View view){
		info[0] = (EditText) view.findViewById(R.id.custFirst);
		info[1] = (EditText) view.findViewById(R.id.custLast);
		info[2] = (EditText) view.findViewById(R.id.custAddress1);
		info[3] = (EditText) view.findViewById(R.id.custAddress2);
		info[4] = (EditText) view.findViewById(R.id.custCity);
		info[5] = (EditText) view.findViewById(R.id.custState);
		info[6] = (EditText) view.findViewById(R.id.custZip);
		info[7] = (EditText) view.findViewById(R.id.custPhoneNo);
		info[8] = (EditText) view.findViewById(R.id.custEmail);
		
		ok = (Button) view.findViewById(R.id.saveCust);
		ok.setOnClickListener(this);
	}
	
	private boolean checkifEmpty(){
		if(info[0].getText().toString().isEmpty()){
				Toast.makeText(getActivity(), "Please enter a value for First Name", Toast.LENGTH_LONG).show();
				return false;
		}
		if(info[1].getText().toString().isEmpty()){
			Toast.makeText(getActivity(), "Please enter a value for Last Name", Toast.LENGTH_LONG).show();
			return false;
		}
		if(info[8].getText().toString().isEmpty()){
			Toast.makeText(getActivity(), "Please enter a value for Email", Toast.LENGTH_LONG).show();
			return false;
		}
		
		return true;
	}
	
	
	private void saveCustomer(){
		String methodName = "CustomerMaintenence/CustomerService.svc/savecustomer";

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		
		LinkedHashMap<String, Object> customer = new LinkedHashMap<String, Object>();
		customer.put("first_name", info[0].getText().toString());
		customer.put("last_name", info[1].getText().toString());
		customer.put("address1", info[2].getText().toString());
		customer.put("address2", info[3].getText().toString());
		customer.put("city", info[4].getText().toString());
		customer.put("state", info[5].getText().toString());
		customer.put("zipcode", info[6].getText().toString());
		customer.put("phone_1", info[7].getText().toString());
		customer.put("email", info[8].getText().toString());

		params.put("storedetail", storedetail);
		params.put("customer",customer);
		
		JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	try {
					if(json.getBoolean("result")){
						OpenAccounts open = (OpenAccounts) getActivity();
						open.open();
						Toast.makeText(getActivity(), json.getString("resultString"),Toast.LENGTH_SHORT).show();
						dismiss();
					}else{
						Toast.makeText(getActivity(), json.getString("resultString"),Toast.LENGTH_SHORT).show();
						
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            		
            }
        });

        jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	
	private void backup(){
		String methodName = "CustomerMaintenence/CustomerService.svc/savecustomer";

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		
		LinkedHashMap<String, Object> customer = new LinkedHashMap<String, Object>();
		customer.put("first_name", info[0].getText().toString());
		customer.put("last_name", info[1].getText().toString());
		customer.put("address1", info[2].getText().toString());
		customer.put("address2", info[3].getText().toString());
		customer.put("city", info[4].getText().toString());
		customer.put("state", info[5].getText().toString());
		customer.put("zipcode", info[6].getText().toString());
		customer.put("phone_1", info[7].getText().toString());
		customer.put("email", info[8].getText().toString());

		params.put("storedetail", storedetail);
		params.put("customer",customer);
		
		JSONParser jParser = new JSONParser(new MyListener() {

            @Override
            public void onResult(JSONObject json) {
            	try {
					if(json.getBoolean("result")){
						OpenAccounts open = (OpenAccounts) getActivity();
						open.open();
						Toast.makeText(getActivity(), json.getString("resultString"),Toast.LENGTH_SHORT).show();
						dismiss();
					}else{
						Toast.makeText(getActivity(), json.getString("resultString"),Toast.LENGTH_SHORT).show();
						
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });

        jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch(arg0.getId()){
		case R.id.saveCust:
			try{
				if(checkifEmpty()){
					saveCustomer();
				}
			}catch(Exception e){
				if(checkifEmpty())
					backup();
			}
			break;
		}
	}
	
}
