package com.intlpos.dialogs;

import com.intlpos.sirclepos.R;
import com.intlpos.dialogs.Accounts.addCustomer;
import com.intlpos.sirclepos.CornerStorePOS;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SelectAccountFirst extends DialogFragment{

	public static SelectAccountFirst newInstance(){
		return new SelectAccountFirst();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.select, container, false);
		getDialog().setTitle(R.string.ToAccount);
		TextView title = (TextView)getDialog().findViewById( android.R.id.title );
		title.setTextColor( Color.parseColor("#000000") );
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		View titleDivider = getDialog().findViewById(titleDividerId);
		if (titleDivider != null)
		    titleDivider.setBackgroundColor(Color.parseColor("#2f6699"));
		return view;
	}
	
}
