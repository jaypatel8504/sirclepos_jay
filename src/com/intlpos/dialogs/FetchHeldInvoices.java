package com.intlpos.dialogs;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Order;
import com.intlpos.dialogs.Accounts.addCustomer;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.InvoiceHome;

public class FetchHeldInvoices extends DialogFragment{
	
	private CornerStorePOS app;
	private ListView list;
	private OrderAdapter adapter;
	
	public static FetchHeldInvoices newInstance(){
		return new FetchHeldInvoices();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().setTitle(getActivity().getResources().getString(R.string.FetchInvoice));
		
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
		
		View view = inflater.inflate(R.layout.heldinvoices, container, false);
		app = (CornerStorePOS) ((Activity) getActivity()).getApplication();
		list = (ListView) view.findViewById(R.id.invoicelist);
		
		adapter = new OrderAdapter(getActivity(),app.heldInvoices);
		adapter.setAdapter(app.heldInvoices);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() //If user clicks on message it will go to AddMessage class with all the data pertaining to that message.
		   {
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position,
						long arg3) {
					PullOrder add = (PullOrder) getActivity();
					add.getOrder(app.heldInvoices.get(position).getOrders(), position);
					dismiss();
				}
			   });
		return view;
	}
	
	public interface PullOrder{
		void getOrder(ArrayList<Order> orders, int pos);
	}
	
	
}
