package com.intlpos.dialogs;

import java.io.IOException;

import com.intlpos.sirclepos.R;
import com.intlpos.initsetup.StartScreen;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.InvoiceHome;
import com.intlpos.sirclepos.RegisterTablet;
import com.john.email.SendMailTLSTask;

import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Demo extends DialogFragment{
	public static Demo newInstance(){
		return new Demo();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.demo,container,false);
		getDialog().setTitle("Please Enter Your Details Below");
		final EditText text[] = new EditText[3];
		text[0] = (EditText) view.findViewById(R.id.name);
		text[1] = (EditText) view.findViewById(R.id.email);
		text[2] = (EditText)view.findViewById(R.id.phone);
		final Button submit = (Button) view.findViewById(R.id.submit);
		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				for(EditText et:text){
					if(et.getText().toString().isEmpty()){
						Toast.makeText(getActivity(), R.string.fillText, Toast.LENGTH_LONG).show();
						return;
					}
				}
				String temp = text[1].getText().toString().replaceAll("\\s+","");
				if(!RegisterTablet.isEmailValid(temp)){
					Toast.makeText(getActivity(), R.string.fixemailCust, Toast.LENGTH_LONG).show();
					return;
				}
				
				String html = null;
				try {
					html = InvoiceHome.convertStreamToString(getActivity().getAssets().open("demo.html"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				submit.setEnabled(false);
	    		SendMailTLSTask sendMailTask = new SendMailTLSTask(getActivity().getApplicationContext(),getActivity(),text[1].getText().toString(),text[0].getText().toString(),text[2].getText().toString()); 
   		     	sendMailTask.execute("sirclepos@gmail.com","yyydluzndsbkrinb", text[1].getText().toString(),"Thank you for trying out Sircle POS","smtp.gmail.com","587",html);
   		     	
	   		     	
	   		 	
			}
		});
		return view;
	}

}
