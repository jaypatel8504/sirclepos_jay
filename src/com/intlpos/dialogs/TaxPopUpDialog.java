package com.intlpos.dialogs;

import java.math.BigDecimal;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Tax;
import com.intlpos.database.TaxExempt;
import com.intlpos.database.TaxExemption;

public class TaxPopUpDialog extends Dialog implements OnCheckedChangeListener{
	
	private TaxExemption taxExemption;
	private TextView tv[];
	private CheckBox cb[];
	private Button ok;
	private Context context;
	private boolean taxExempt[];
	private String[] tax_desc = Tax.tax_desc;
	private BigDecimal taxValues[], oldValues[];
	
	public TaxPopUpDialog(Context context, TaxExemption taxExemption) {
		super(context);
		this.taxExemption = taxExemption;
		
	}
	
	private void setValues(){
		taxValues = new BigDecimal[3];
		taxValues = taxExemption.getTax(); // Get tax values
		
		if(taxExemption.getTaxExempt() == null){
			taxExempt = new boolean[3];
			for(int i = 0; i <3; i++){
				taxExempt[i] = false;//By default everything is false, therefore not tax exempt
			}
		}else{//Get tax exempt taxes
			boolean temp[] = taxExemption.getTaxExempt();
			taxExempt = new boolean[3];
			int i = 0;
			for(boolean getTemp:temp){//Need to copy by reference because we might alter it
				taxExempt[i] = getTemp;
				i++;
			}
			
		}
		
        if (taxExemption.getOldTax() == null){ //if the item/invoice has not been alter yet
        	oldValues = new BigDecimal[3];
        	for(int i = 0; i <3; i++){
        		oldValues[i] = BigDecimal.ZERO;
        	}
		}else{
			BigDecimal temp[] = taxExemption.getOldTax();//Get tax if it was altered
			oldValues = new BigDecimal[3];
			for(int i = 0; i < 3; i++){
				oldValues[i] = temp[i];
			}
		}
	}

	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.popup_tax);
        setValues();
        getWidgets();
        setWidgets();
        
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		switch (buttonView.getId()) {
		case R.id.tax1Check:
			if(!isChecked){// An item is tax exempt if it is true (box is unchecked)
				tv[0].setText(tax_desc[0] + " : 0.00");
				taxExempt[0] = true;
			}else{
				if(oldValues[0].compareTo(BigDecimal.ZERO) == 0){
					tv[0].setText(tax_desc[0] + " : " +  taxValues[0].toString());
				}else{
					tv[0].setText(tax_desc[0] + " : " + oldValues[0].toString());
				}
				taxExempt[0] = false;
			}
			break;
		case R.id.tax2Check:
			if(!isChecked){
				tv[1].setText(tax_desc[1] + " : 0.00");
				taxExempt[1] = true;
			}else{
				if(oldValues[1].compareTo(BigDecimal.ZERO) == 0){
					tv[1].setText(tax_desc[1] +  " : " + taxValues[1].toString());
				}else{
					tv[1].setText(tax_desc[1] +  " : " + oldValues[1].toString());
				}
				taxExempt[1] = false;
			}
			break;
		case R.id.tax3Check:
			if(!isChecked){
				tv[2].setText(tax_desc[2] + " : 0.00");
				taxExempt[2] = true;
			}else{
				if(oldValues[2].compareTo(BigDecimal.ZERO) == 0){
					tv[2].setText(tax_desc[2] + " : " + taxValues[2].toString());
				}else{
					tv[2].setText(tax_desc[2] + " : " +oldValues[2].toString());
				}
				taxExempt[2] = false;
			}
			break;
		}
	}
	
	public Button getOK(){// returns the button so we can retrieve values
		return ok;
	}
	
	private void getWidgets() {
		tv = new TextView[3];
		cb = new CheckBox[3];
		ok = (Button) findViewById(R.id.oktax);
		cb[0] = (CheckBox) findViewById(R.id.tax1Check);
		cb[1] = (CheckBox) findViewById(R.id.tax2Check);
		cb[2] = (CheckBox) findViewById(R.id.tax3Check);
	}
	
	public void setWidgets() {
		final ArrayList<TextView> tv_array = new ArrayList<TextView>();
		tv_array.add((TextView) findViewById(R.id.tax1));
		tv_array.add((TextView) findViewById(R.id.tax2));
		tv_array.add((TextView) findViewById(R.id.tax3));
		
		for (int i = 0; i < 3; i++) {
			tv[i] = tv_array.get(i);
			if(taxValues[i].compareTo(BigDecimal.ZERO) == 0){
				if(!taxExempt[i] && oldValues[i].compareTo(BigDecimal.ZERO)==0)
					cb[i].setVisibility(View.GONE);
				if(!taxExempt[i])
					cb[i].setChecked(true);
				else
					cb[i].setChecked(false);
				
				tv[i].setText(tax_desc[i] + " : 0.00");
				cb[i].setOnCheckedChangeListener(this);
			}else{
				tv[i].setText(tax_desc[i] + " : " +  taxValues[i].toString());
				if(!taxExempt[i])
					cb[i].setChecked(true);
				else
					cb[i].setChecked(false);
				cb[i].setOnCheckedChangeListener(this);
			}
		}
		
	}

	public boolean[] getTaxExempt() {
		return taxExempt;
	}

	public BigDecimal[] getOldValues() {
		return oldValues;
	}

	
}
	