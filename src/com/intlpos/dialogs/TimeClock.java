package com.intlpos.dialogs;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.intlpos.sirclepos.R;

public class TimeClock extends Dialog implements View.OnClickListener{
	private Context context;
	private Button buttons[];
	
	public TimeClock(Context context) {
		super(context);
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.timeclock);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.width = 300;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        getWindow().setAttributes(lp);
		getWidets();
	}
	
	private void getWidets(){
		buttons = new Button[4];
		buttons[0] = (Button) findViewById(R.id.clockin);
		buttons[1] = (Button) findViewById(R.id.clockout);
		buttons[2] = (Button) findViewById(R.id.takebreak);
		buttons[3] = (Button) findViewById(R.id.breakend);
		
		for(int i = 0; i < 4; i++)
			buttons[i].setOnClickListener(this);
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mma");
		switch(arg0.getId()){
			case R.id.clockin:
				String in = sdf.format(new Date());
				Toast.makeText(context, "Clocked in at " + in, Toast.LENGTH_SHORT).show();
				break;
			case R.id.clockout:
				String out = sdf.format(new Date());
				Toast.makeText(context, "Clocked out at " + out, Toast.LENGTH_SHORT).show();
				break;
			case R.id.takebreak:
				String bt = sdf.format(new Date());
				Toast.makeText(context, "Break started at " + bt, Toast.LENGTH_SHORT).show();
				break;
			case R.id.breakend:
				String be = sdf.format(new Date());
				Toast.makeText(context, "Break finished at " + be, Toast.LENGTH_SHORT).show();
				break;
		}
	}
	
	
	

}
