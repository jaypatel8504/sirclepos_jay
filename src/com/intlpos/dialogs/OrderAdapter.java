package com.intlpos.dialogs;

import java.util.ArrayList;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Order;
import com.intlpos.database.SaveInvoice;
import com.intlpos.sirclepos.CornerStorePOS;
import com.john.beans.Customer;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

	public class OrderAdapter extends ArrayAdapter<SaveInvoice>{
	private Context context;
	private ArrayList<SaveInvoice> data;
	
	public OrderAdapter(Context context, ArrayList<SaveInvoice> data) {
		super(context, R.layout.orderlist, data);
		this.context = context;
		this.data = data;
	}
	
	public void setAdapter(ArrayList<SaveInvoice> data){
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	     
		LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 
	    View rowView = inflater.inflate(R.layout.orderlist, parent, false);
	    TextView date = (TextView) rowView.findViewById(R.id.Date);
	    TextView holdId = (TextView) rowView.findViewById(R.id.HoldId);
	    date.setText(data.get(position).getDate());
	    holdId.setText(data.get(position).getOrderNameId());
		return rowView;
	}
}
