package com.intlpos.dialogs;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Employee;
import com.intlpos.database.Permission;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ManagerPromptDialog extends Dialog implements View.OnClickListener{
	
	Context context;
	View layout;
	Button[] btn;
	Button login;
	EditText AccessIdEditText;
	TextView pinForgot;
	private Employee employee;
	private Permission permission;
	
	public ManagerPromptDialog(Context context) {
		super(context);
		this.context= context;
		btn = new Button[13];
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.employee_login);
		setWidgetReferences();
		addWidgetListeners();
	}



	private void setWidgetReferences() {
		AccessIdEditText = (EditText)findViewById(R.id.numberpadtext);
		btn[0] = (Button)findViewById(R.id.button1);
		btn[1] = (Button)findViewById(R.id.button2);
		btn[2] = (Button)findViewById(R.id.button3);
		btn[3] = (Button)findViewById(R.id.button4);
		btn[4] = (Button)findViewById(R.id.button5);
		btn[5] = (Button)findViewById(R.id.button6);
		btn[6] = (Button)findViewById(R.id.button7);
		btn[7] = (Button)findViewById(R.id.button8);
		btn[8] = (Button)findViewById(R.id.button9);
		btn[9] = (Button)findViewById(R.id.button0);
		btn[10] = (Button)findViewById(R.id.button00);
		btn[11] = (Button)findViewById(R.id.deletebutton);
		btn[12] = (Button)findViewById(R.id.clearbutton);

		login = (Button)findViewById(R.id.loginbutton);
		
		pinForgot = (TextView)findViewById(R.id.forgotyourpin);
		pinForgot.setText(context.getResources().getString(R.string.getManager));
		
	}
	
	private void addWidgetListeners() {
		
		for(int i =0;i<13;i++){
			btn[i].setOnClickListener(this);
		}
	}
	
	public void addtoarray(String numbers){		
			//register TextBox 		
			AccessIdEditText.append(numbers);		
	}
	

	public Button getLogin(){
		return login;
	}

	public String getAcess(){
		return AccessIdEditText.getText().toString();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.button1:
			addtoarray("1");
			break;
		case R.id.button2:
			addtoarray("2");
			break;
		case R.id.button3:
			addtoarray("3");
			break;
		case R.id.button4:
			addtoarray("4");
			break;
		case R.id.button5:
			addtoarray("5");
			break;
		case R.id.button6:
			addtoarray("6");
			break;
		case R.id.button7:
			addtoarray("7");
			break;
		case R.id.button8:
			addtoarray("8");
			break;
		case R.id.button9:
			addtoarray("9");
			break;
		case R.id.button0:
			addtoarray("0");
			break;
		case R.id.button00:
			addtoarray("00");
			break;
		case R.id.deletebutton:
			int length = AccessIdEditText.getText().length();
			if (length > 0) {
				AccessIdEditText.getText().delete(length - 1, length);
			}
			break;
		case R.id.clearbutton:
			AccessIdEditText.setText("");
			break;
		}
	}
}
