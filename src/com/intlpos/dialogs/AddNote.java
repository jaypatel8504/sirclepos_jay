package com.intlpos.dialogs;

import java.util.ArrayList;
import java.util.Set;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;

public class AddNote extends DialogFragment implements View.OnClickListener{
	private ListView list;
	private LinearLayout add;
	private Button newMessage, save;
	private EditText message;
	private ArrayList<String> savedMessages;
	private ArrayAdapter<String> listofMessages;
	private Set<String> saved;
	public static AddNote newInstance(){
		return new AddNote();
	}
	
	public interface AddNoteToGrid{
		public void noteToGrid(String note);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		View view = inflater.inflate(R.layout.addnote, null,false);
		getDialog().setTitle("Add a Note");
		saved = CSSharedPreferences.getMess();
		getWidgets(view);
		setWidgets();
		if(!saved.isEmpty()){
			for(String x:saved){
				Log.d("mess", x);
			}
			String[] messages = saved.toArray(new String[saved.size()]);
			listofMessages = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, messages);
			list.setAdapter(listofMessages);
		}
		
		return view;
	}
	
	private void getWidgets(View v){
		list = (ListView) v.findViewById(R.id.notelist);
		add = (LinearLayout) v.findViewById(R.id.newmessage);
		newMessage = (Button) v.findViewById(R.id.create);
		save = (Button) v.findViewById(R.id.saveMessage);
		message = (EditText)v.findViewById(R.id.message);
	}
	
	private void setWidgets(){
		newMessage.setOnClickListener(this);
		save.setOnClickListener(this);
		list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                    long arg3) {
            	String mess = (String) arg0.getAdapter().getItem(arg2);
            	AddNoteToGrid grid = (AddNoteToGrid) getActivity();
				grid.noteToGrid(mess);
            	dismiss();
            }
        });

	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.create:
			add.setVisibility(View.VISIBLE);
			break;
		case R.id.saveMessage:
			if(!message.getText().toString().isEmpty()){
				String getMessage = message.getText().toString();
				saved.add(getMessage);
				for(String x:saved){
					Log.d("mess", x);
				}
				CSSharedPreferences.setMessages(saved);
				String[] messages = saved.toArray(new String[saved.size()]);
				listofMessages = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, messages);
				list.setAdapter(listofMessages);
				AddNoteToGrid grid = (AddNoteToGrid) getActivity();
				grid.noteToGrid(getMessage);
				dismiss();
			}
			break;
		}
	}

}
