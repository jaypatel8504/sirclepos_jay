package com.intlpos.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.EmployeeLogin;

public class WhatsNewDialog {
	 
    private String EULA_PREFIX = "eula_";
    private Activity mActivity;
 
    public WhatsNewDialog(Activity context) {
        mActivity = context;
    }
 
    private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
             pi = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi;
    }
 
     public void show() {
        PackageInfo versionInfo = getPackageInfo();
 
        // the eulaKey changes every time you increment the version number in the AndroidManifest.xml
        final String eulaKey = EULA_PREFIX + versionInfo.versionCode;
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        boolean hasBeenShown = prefs.getBoolean(eulaKey, false);
        if(hasBeenShown == false){
 
            // Show the Eula
            String title = mActivity.getString(R.string.app_name) + " v" + versionInfo.versionName + versionInfo.versionCode;
 
            //Includes the updates as well so users know what changed.
            String message = mActivity.getString(R.string.whats_new); // mActivity.getString(R.string.updates) + "\n\n" + 
//            String message = "abc";
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {
 
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Mark this version as read.
                            dialogInterface.dismiss();
                        }
                    });
            builder.create().show();
        }
    }
 
}