package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class InvoiceSql extends SQLiteOpenHelper{

	private final static String DATABASE_NAME = "invoice.db";
	private final static int VERSION_NAME = 1;
	
	public final static String INVOICE_TABLE = "invoice";
	public final static String COLUMN_ID = "_id";
	public final static String INVOICE_ID = "invoice_id";
	public final static String UNDISCOUNT_SUB = "undiscounted_sub_total"; 
	public final static String SUBTOTAL = "sub_total";
	public final static String TOTAL_TAX1 = "total_tax1";
	public final static String TOTAL_TAX2 = "total_tax2";
	public final static String TOTAL_TAX3 = "total_tax3";
	public final static String WHOLE_INVOICE_DISCOUNT_PERCENT = "whole_invoice_discount_percent";
	public final static String WHOLE_INVOICE_DISCOUNT_AMOUNT = "whole_invoice_discount_amount";
	public final static String GRAND_TOTAL = "grand_total";
	public final static String INVOICE_DATE = "invoice_date";
	public final static String PAYMENT_TYPE = "payment_type";
	public final static String CHANGE_AMT = "change_amt";
	public final static String S_CASHIER_ID = "s_cashier_id";
	public final static String STATION_ID = "station_id";
	public final static String STATUS = "status";
	public final static String REF_NO = "ref_no";
	public final static String AUTH_CODE = "auth_code";
	public final static String TRANS_ID = "transaction_id";
	public final static String LAST_FOUR = "last_four";
	public final static String CARD_HOLDER_NAME = "card_holder_name"; 
	public final static String CUSTOMER_ID = "customer_id";
	
	
	private final static String CREATE_TABLE = "create table " + INVOICE_TABLE + "(" +
			INVOICE_ID+ " integer primary key not null, " + UNDISCOUNT_SUB+ " real not null default '0', " + SUBTOTAL + " real not null default '0', "+
			TOTAL_TAX1 + " real not null default '0', "+ TOTAL_TAX2 + " real not null default '0', " + TOTAL_TAX3 + " real not null default '0', "+
			WHOLE_INVOICE_DISCOUNT_PERCENT+ " real not null default '0', " + WHOLE_INVOICE_DISCOUNT_AMOUNT + " real not null default '0', " +
			GRAND_TOTAL + " decimal not null default '0', " + INVOICE_DATE + " text not null, " + PAYMENT_TYPE + " integer not null, "+
			CHANGE_AMT+ " decimal not null, " + S_CASHIER_ID + " text not null, " + "foreign key ('" + S_CASHIER_ID +"') references "+ EmployeesSql.TABLE_EMPLOYEES + " ('" + EmployeesSql.EMPLOYEES_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION,"  + STATION_ID + " integer not null, " + 
			STATUS + " text not null  default 'S', " + REF_NO  + " text not null, " + AUTH_CODE + " text default null, " + 
			TRANS_ID + " text default null, " + LAST_FOUR+ " text default null, " + CARD_HOLDER_NAME + " text default null, " + CUSTOMER_ID + " text not null default '1', "+
			"foreign key ('" + CUSTOMER_ID +"') references "+ CustomerSql.CUSTOMER_TABLE+ " ('" + CustomerSql.CUSTOMERID + "') ON DELETE NO ACTION ON UPDATE NO ACTION" +");";
	
	public InvoiceSql(Context context) {
		super(context, DATABASE_NAME, null, VERSION_NAME);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP IF TABLE EXISTS " + INVOICE_TABLE);
		onCreate(db);
	}

}
