package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class OrdersSql extends SQLiteOpenHelper{
	
	public static String TABLE_ORDERS = "orders";
	private static String DATABASENAME = "orders.db";
	public static int VERSION = 1;
	
	public static String ORDER_ID = "order_id";
	public static String PRODUCT_ID = "product_id";
	public static String CURRENT_PRICE = "current_price";
	public static String CURRENT_PRICE_AFTER_WHOLE_INVOICE_DISCOUNT = "current_price_after_wholeinvoicediscount";
	public static String TAX1_AFTER_WHOLE_INVOICE_DISCOUNT = "tax1_after_whole_invoice_discount";
	public static String TAX2_AFTER_WHOLE_INVOICE_DISCOUNT = "tax2_after_whole_invoice_discount";
	public static String TAX3_AFTER_WHOLE_INVOICE_DISCOUNT = "tax3_after_whole_invoice_discount";
	public static String QTY_ORDERED = "qty_ordered";
	public static String COST = "cost";
	public static String ITEM_DESC = "item_desc";
	public static String LINE_DISCOUNT_PERCENT = "line_discount_percent";
	public static String LINE_DISCOUNT_AMOUNT = "line_discount_amount";
	public static String WHOLE_INVOICE_DISCOUNT_PERCENT = "whole_invoice_discount_percent";
	public static String WHOLE_INVOICE_DISCOUNT_AMOUNT = "whole_invoice_discount_amount";
	public static String INVOICE_ID = "invoice_id";
	public static String ROW_ID = "row_id";
	
	private static String CREATE_TABLE = "create table "+ TABLE_ORDERS + "(' " + ORDER_ID +" integer primary key not null, "+ 
			PRODUCT_ID + " integer not null, " + "foreign key ('" + PRODUCT_ID +"') references "+ ProductsSql.PRODUCTS_TABLE + " ('" + ProductsSql.PRODUCT_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION," +
			CURRENT_PRICE + " real not null default '0', " + CURRENT_PRICE_AFTER_WHOLE_INVOICE_DISCOUNT + " real  not null default '0', " + TAX1_AFTER_WHOLE_INVOICE_DISCOUNT + " real  not null default '0', "+
			TAX2_AFTER_WHOLE_INVOICE_DISCOUNT + " real  not null default '0', " + TAX3_AFTER_WHOLE_INVOICE_DISCOUNT + " real  not null default '0', " +
			QTY_ORDERED + " real  not null default '0', " +  COST + " real  not null default '0', " + ITEM_DESC + "text not null" +
			LINE_DISCOUNT_PERCENT + " real  not null default '0', " + WHOLE_INVOICE_DISCOUNT_AMOUNT + " real  not null default '0', " + INVOICE_ID + " integer not null, " + "foreign key ('" + INVOICE_ID +"') references "+ InvoiceSql.INVOICE_TABLE + " ('" + InvoiceSql.INVOICE_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION," +
			ROW_ID + " integer not null default '0'');";
	
	public OrdersSql(Context context) {
		super(context, DATABASENAME, null, VERSION);
	}
	
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(OrdersSql.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
