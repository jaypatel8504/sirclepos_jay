package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class RecieptSql extends SQLiteOpenHelper{
	
	private static String DATABASENAME = "receiptdetail.db";
	private static int VERSION = 1;
	
	public static String RECIEPT_TABLE = "receiptdetail";
	public static String RECEIPTDETAIL_ID = "receiptdetail_id";
	public static String STORE_NO = "store_no";
	public static String STORE_NAME = "store_name";
	public static String ADDRESS1 = "address1";
	public static String ADDRESS2 = "address2";
	public static String CONTACT_NO = "contact_no";
	public static String EMAIL_ID = "email_id";
	public static String PICTURE = "picture";
	public static String FOOTERLINE1 = "footerline1";
	public static String FOOTERLINE2 = "footerline2";
	public static String FOOTERLINE3 = "footerline3";
	public static String FOOTERLINE4 = "footerline4";
	public static String FOOTERLINE5 = "footerline5";
	public static String FOOTERLINE6 = "footerline6";
	public static String FOOTERLINE7 = "footerline7";
	public static String FOOTERLINE8 = "footerline8";
	public static String FOOTERLINE9 = "footerline9";
	public static String FOOTERLINE10 = "footerline10";
	
	private static String CREATETABLE = "create table " + RECIEPT_TABLE + " (' " + RECEIPTDETAIL_ID + " integer primary key not null, "+
			STORE_NO + " text not null, " + STORE_NAME + " text not null, "+ ADDRESS1 + " text default null, " + ADDRESS2 + " text default null, " + 
			CONTACT_NO + " text default null, " + EMAIL_ID + " text default null, " + PICTURE + " text default null, " + FOOTERLINE1 + " text default null, "+
			FOOTERLINE2 + " text default null, " + FOOTERLINE3 + " text default null, " + FOOTERLINE4 + " text default null, " + FOOTERLINE5 + " text default null, " +
			FOOTERLINE6 + " text default null, " + FOOTERLINE7 + " text default null, " + FOOTERLINE8 + " text default null, " + FOOTERLINE9 + " text default null, " +
			FOOTERLINE10 + " text default null');";
	
	public RecieptSql(Context context) {
		super(context, DATABASENAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(RecieptSql.CREATETABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
