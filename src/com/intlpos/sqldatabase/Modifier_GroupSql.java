package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Modifier_GroupSql extends SQLiteOpenHelper{
	
	private final static String DATABASENAME = "modifier_group.db";
	private final static int VERSION = 1;
	
	public final static String TABLE_NAME = "modifier_group";
	public final static String MODIFIER_GROUP_ID = "modifier_group_id";
	public final static String MODIFIER_GROUP_NAME = "modifier_group_name";
	public final static String MODIFIER_GROUP_PROMPT = "modifier_group_prompt";
	
	public final static String CREATETABLE = "create table " + TABLE_NAME + " ( " + 
			MODIFIER_GROUP_ID + " integer primary key not null, " + MODIFIER_GROUP_NAME + " text not null, " +
			MODIFIER_GROUP_PROMPT + " text not null);";
	
	public Modifier_GroupSql(Context context) {
		super(context, DATABASENAME, null, VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATETABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
