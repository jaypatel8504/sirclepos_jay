package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DepartmentSql extends SQLiteOpenHelper{
	
	private static final String DATABASENAME = "Department.db";
	private static final int VERSION = 1;
	
	public static final String TABLE_DEPARTMENT = "departments";
	public static final String COLUMN_ID = "_id";
	public static final String DEPT_ID = "dept_id";
	public static final String DEPT_NAME = "dept_name";
	public static final String DESCRIPTION = "description";
	public static final String TS = "tsdisplay";
	public static final String URL = "url";
	
	private static final String CREATE_TABLE = "create table " +TABLE_DEPARTMENT + "(" 
			+ DEPT_ID + " integer primary key not null, " + DEPT_NAME + " text not null default 'blank' unique, "+ DESCRIPTION + " text default null, " + URL + " text default null, "+TS + " integer not null default '1');";
	
	public DepartmentSql(Context context) {
		super(context, DATABASENAME, null, VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEPARTMENT);
		 onCreate(db);
	}

}
