package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class CustomerSql extends SQLiteOpenHelper{
	
	private final static String DATABASE_NAME = "Customer.db";
	private final static int VERSIONNAME = 1;
	
	public final static String CUSTOMER_TABLE = "customers";
	public final static String COLUMN_ID = "_id";
	public final static String CUSTOMERID = "customer_id";
	public final static String FIRST_NAME = "first_name";
	public final static String LAST_NAME = "last_name";
	public final static String ADDRESS1 = "address1";
	public final static String ADDRESS2 = "address2";
	public final static String CITY = "city";
	public final static String STATE = "state";
	public final static String ZIPCODE = "zipcode";
	public final static String PHONE= "phone_1";
	public final static String EMAIL = "email";
	public final static String ACCOUNT_BALANCE = "account_balance";

	private final static String CREATE_TABLE = "create table " + CUSTOMER_TABLE + "(" +
			CUSTOMERID+ " text primary key not null, " + FIRST_NAME + " text not null, " + LAST_NAME + " text not null, "+
			ADDRESS1 + " text default null, " + ADDRESS2 + " text default null, " + CITY + " text default null, "+
			STATE+ " text default null, " + ZIPCODE + " text default null, " + PHONE + " text default null, " +
			EMAIL + " text not null unique, " + ACCOUNT_BALANCE + " real default '0.00');";
	
	public CustomerSql(Context context) {
		super(context, DATABASE_NAME, null, VERSIONNAME);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + CUSTOMER_TABLE);
		onCreate(db);
	}

}
