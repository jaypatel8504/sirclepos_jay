package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class PermissionSql extends SQLiteOpenHelper{
	
	private static final String DATABASE_NAME = "permissions.db";
	private static final int DATABASE_VERSION = 1;
	
	public static final String TABLE_PERMISSIONS= "permissions";
	public static final String PERMISSION_ID = "permissions_id";
	public static final String PERMISSION_NAME = "permissions_name";
	public static final String PRICE_CHANGE = "price_change";
	public static final String QTY_CHANGE = "qty_change";
	public static final String DELETE_BUTTON = "delete_button";
	public static final String SHIFTREPORT = "shiftreport_atshiftclose";
	public static final String REQCLOCKIN = "reqclockin";
	public static final String DISCOUNT = "discount";
	public static final String SELL_NON_INVENTORY = "sell_non_inventory";
	public static final String VOID_INVOICE = "void_invoice";
	public static final String OPEN_CASHDRAWER = "open_cashdrawer";
	public static final String NEGATE_INVOICE = "negate_invoice";
	
	private static final String CREATE_TABLE = "create table " + TABLE_PERMISSIONS + " ("+ 
			  PERMISSION_ID+ " integer primary key not null, " + PERMISSION_NAME + " text not null default 'Cashier' unique, " + PRICE_CHANGE + " integer not null default '1', "
			+ QTY_CHANGE + " integer not null default '1', " + DELETE_BUTTON + " integer not null default '1', " + SHIFTREPORT+ " integer not null default '1', "
			+ REQCLOCKIN + " integer not null default '1', " + DISCOUNT + " integer not null default '1', " + SELL_NON_INVENTORY + " integer not null default '1', "
			+ VOID_INVOICE + " integer not null default '1', " + OPEN_CASHDRAWER + " integer not null default '1', " + NEGATE_INVOICE + " integer not null default '1');";
	
	
	public PermissionSql(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERMISSIONS);
		 onCreate(db);
	}

}
