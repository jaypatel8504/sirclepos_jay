package com.intlpos.sqldatabase;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.intlpos.database.Employee;
import com.intlpos.database.Permission;

public class PermissionDataSource {

	private SQLiteDatabase database;
	private PermissionSql dbHelper;
	
	public PermissionDataSource(Context context){
		this.dbHelper = new PermissionSql(context); 
	}
	
	String all[] = {PermissionSql.PERMISSION_ID,PermissionSql.PERMISSION_NAME,PermissionSql.PRICE_CHANGE,
			PermissionSql.QTY_CHANGE,PermissionSql.DELETE_BUTTON,PermissionSql.SHIFTREPORT,PermissionSql.REQCLOCKIN,
			PermissionSql.DISCOUNT,PermissionSql.SELL_NON_INVENTORY,PermissionSql.VOID_INVOICE,PermissionSql.OPEN_CASHDRAWER,
			PermissionSql.NEGATE_INVOICE};
	
	public void createRow(Permission p){
		database = dbHelper.getWritableDatabase();
		database.execSQL("PRAGMA foreign_keys = ON;");
		ContentValues values = new ContentValues();
		values.put(PermissionSql.PERMISSION_ID, p.permissions_id);
		values.put(PermissionSql.PERMISSION_NAME, p.permissions_name);
		values.put(PermissionSql.PRICE_CHANGE, p.price_change);
		values.put(PermissionSql.QTY_CHANGE, p.qty_change);
		values.put(PermissionSql.DELETE_BUTTON, p.delete_button);
		values.put(PermissionSql.SHIFTREPORT, p.shiftreport_atshiftclose);
		values.put(PermissionSql.REQCLOCKIN, p.reqclockin);
		values.put(PermissionSql.DISCOUNT, p.discount);
		values.put(PermissionSql.SELL_NON_INVENTORY, p.sell_non_inventory);
		values.put(PermissionSql.VOID_INVOICE, p.void_invoice);
		values.put(PermissionSql.OPEN_CASHDRAWER, p.open_cashdrawer);
		values.put(PermissionSql.NEGATE_INVOICE, p.negate_invoice);
		
		database.insert(PermissionSql.TABLE_PERMISSIONS, null, values);
		dbHelper.close();
	}
	
	public void readTables(){
		database = dbHelper.getReadableDatabase();
		ArrayList<Permission>permission = new ArrayList<Permission>();
		Cursor cursor = database.query(PermissionSql.TABLE_PERMISSIONS, all,null, null, null, null, null);
		cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	    	Permission p = cursorToPermission(cursor);
	    	permission.add(p);
	      cursor.moveToNext();
	    }
	    // make sure to close the cursor
	    cursor.close();
	    
	    for(Permission p: permission){
	    	Log.d("Permission p", Integer.toString(p.permissions_id));
	    }
	}
	
	private Permission cursorToPermission(Cursor cursor){
		Permission p = new Permission();
		p.permissions_id = cursor.getInt(0);
		p.permissions_name = cursor.getString(1);
		p.price_change = cursor.getInt(2);
		p.qty_change = cursor.getInt(3);
		p.delete_button = cursor.getInt(4);
		p.shiftreport_atshiftclose = cursor.getInt(5);
		p.reqclockin = cursor.getInt(6);
		p.discount = cursor.getInt(7);
		p.sell_non_inventory = cursor.getInt(8);
		p.void_invoice = cursor.getInt(9);
		p.open_cashdrawer = cursor.getInt(10);
		p.negate_invoice = cursor.getInt(11);
		
		return p;
	}
	
}
