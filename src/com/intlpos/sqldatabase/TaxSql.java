package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class TaxSql extends SQLiteOpenHelper{

	private static String DATABASENAME = "taxes.db";
	private static int VERSION = 1;
	
	public static String TAX_TABLE = "taxes";
	public static String TAX_RATE_ID = "tax_rate_id";
	public static String TAX_RATE1 = "tax_rate1";
	public static String TAX_RATE2 = "tax_rate2";
	public static String TAX_RATE3 = "tax_rate3";
	public static String TAX_DESC1 = "tax_desc1";
	public static String TAX_DESC2 = "tax_desc2";
	public static String TAX_DESC3 = "tax_desc3";
	
	
	private static String CREATETABLE = "create table " + TaxSql.TAX_TABLE + " (' " + 
			TaxSql.TAX_RATE_ID + " integer primary key not null, " + TaxSql.TAX_RATE1 + " real not null default '0.0000', "+
			TaxSql.TAX_RATE2 + " real not null default '0.0000'," + TaxSql.TAX_RATE3 + " real not null default '0.0000', " +
			TaxSql.TAX_DESC1 + " text not null, " + TaxSql.TAX_DESC2 + " text not null, " + TaxSql.TAX_DESC3 + " text not null ')";
	
	public TaxSql(Context context) {
		super(context, TaxSql.DATABASENAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
