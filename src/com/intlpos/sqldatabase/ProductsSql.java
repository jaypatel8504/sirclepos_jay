package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class ProductsSql extends SQLiteOpenHelper{
	
	private final static String DATABASE_NAME = "products.db";
	private final static int VERISON_NUMBER = 1;
	
	public final static String PRODUCTS_TABLE = "products";
	public final static String PRODUCT_ID = "product_Id";
	public final static String ITEM_NO = "item_no";
	public final static String ITEM_NAME = "item_name";
	public final static String PRICE = "price";
	public final static String COST = "cost";
	public final static String ITEM_STOCK = "item_stock";
	public final static String REORDER_LEVEL = "re_order_level";
	public final static String TAX_RATE1 = "tax_rate1";
	public final static String TAX_RATE2 = "tax_rate2";
	public final static String TAX_RATE3 = "tax_rate3";
	public final static String PRODUCT_TSDISPLAY = "product_tsdisplay";
	public final static String PROMPT_ITEMS_ID = "prompt_items_id";
	public final static String PROMPT_PRICE = "prompt price";
	public final static String DEPT_ID = "department_id";
	public final static String PICTURE = "picture";
	public final static String TAXINCLUSIVE = "tax_inclusive";
	public final static String HASMODS = "hasmodifier";
	public final static String ISMODS = "ismodifier";
	public final static String FOODSTAMPABLE = "foodstampable";
	public final static String BULK_PRICE = "bulk_price";
	public final static String BULK_QTY = "bulk_qty";
	public final static String CHECKIDELEVEL = "checkidelevel";
	public final static String TAG_ALONG_ITEMS = "tag_along_items";
	public final static String DATE_CREATED = "date_created";
	public final static String DISABLE_STOCK_COUNT = "disable_stock_count";
	public final static String PRODUCT_POSITION = "product_position";
	
	public final static String CREATE = "create table " + PRODUCTS_TABLE +" (" + PRODUCT_ID +" integer primary key not null, "+
			ITEM_NO + " text not null default 'blank', " + ITEM_NAME + " text not null, " + PRICE + " real not null default '0', " + COST + 
			" real not null default '0', " + ITEM_STOCK + " real not null default '0', " + REORDER_LEVEL + " real not null default '10', " +
			TAX_RATE1 + " integer not null default '1', " + TAX_RATE2 + " integer not null default '0',"+ 
			TAX_RATE3 + " integer not null default '0', " + PRODUCT_TSDISPLAY+ " integer not null default '1', " + 
			PROMPT_ITEMS_ID + " string default null, " + PROMPT_PRICE + " integer not null default '0', " + 
			PICTURE+ " string default null, "+  FOODSTAMPABLE + " integer default '0', "+ DEPT_ID + " integer default null, "+ "foreign key ('" + DEPT_ID +"') references "+ DepartmentSql.TABLE_DEPARTMENT  + " ('" + DepartmentSql.DEPT_ID+ "')" +
			TAXINCLUSIVE + " integer not null default '0', " + HASMODS + " not null default '0', " + 
			ISMODS + " integer not null default '0', "  + BULK_PRICE + " string, "+ BULK_QTY + " decimal not null default '0', "  + CHECKIDELEVEL + " integer not null default '0', " +
			TAG_ALONG_ITEMS + " integer not null default '0', " + DATE_CREATED + " string not null, " +
			DISABLE_STOCK_COUNT + " integer not null default '0', " + PRODUCT_POSITION + " integer not null default '-1');";
	
	
	public ProductsSql(Context context) {
		super(context, DATABASE_NAME, null, VERISON_NUMBER);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + PRODUCTS_TABLE);
		 onCreate(db);
	}

}
