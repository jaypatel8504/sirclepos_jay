package com.intlpos.sqldatabase;

import java.util.ArrayList;

import com.intlpos.database.Employee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class EmployeeDataSource {
	
	private SQLiteDatabase database;
	private EmployeesSql dbHelper;
	private String[] all = {EmployeesSql.EMPLOYEES_ID,EmployeesSql.EMAIL_ID,EmployeesSql.ACCESS_ID,EmployeesSql.FIRSTNAME,EmployeesSql.LASTNAME, EmployeesSql.JOBCODE};
	
	public EmployeeDataSource(Context context){
		this.dbHelper = new EmployeesSql(context);
	}
	
	
	
	public void createRow(Employee emp){
		database = dbHelper.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(EmployeesSql.EMPLOYEES_ID, emp.employees_id);
		values.put(EmployeesSql.EMAIL_ID, emp.email_id);
		values.put(EmployeesSql.ACCESS_ID, emp.access_id);
		values.put(EmployeesSql.CARD_SWIPEID, emp.card_swipeid);
		values.put(EmployeesSql.DISABLED, 0);
		values.put(EmployeesSql.FIRSTNAME, emp.first_name);
		values.put(EmployeesSql.MIDDLENAME, emp.middle_name);
		values.put(EmployeesSql.LASTNAME, emp.last_name);
		values.put(EmployeesSql.CONTACT_NO, emp.contact_no);
		values.put(EmployeesSql.BIRTHDAY, "");
		values.put(EmployeesSql.PICTURE, emp.picture);
		values.put(EmployeesSql.JOBCODE, emp.job_code);
		values.put(EmployeesSql.RESET_PASSFLAG, 0);
		int pos = (emp.access_to_pos) ? 1 : 0;
		values.put(EmployeesSql.ACCESS_TO_POS, pos);
		pos = (emp.access_to_backoffice) ? 1 : 0;
		values.put(EmployeesSql.ACCESS_TO_BACKOFFICE, pos);
		pos = (emp.admin_access) ? 1:0;
		values.put(EmployeesSql.ADMIN_ACCESS, pos);
		
		database.insert(EmployeesSql.TABLE_EMPLOYEES, null, values);
		dbHelper.close();
	}
	
	
	public void readTables(){
		database = dbHelper.getReadableDatabase();
		
		 ArrayList<Employee> emp = new ArrayList<Employee>(); 
		 Cursor cursor = database.query(EmployeesSql.TABLE_EMPLOYEES,
			        all, null, null, null, null, null);
		 cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Employee employee = cursorToComment(cursor);
		      emp.add(employee);
		      cursor.moveToNext();
		    }
		    // make sure to close the cursor
		    cursor.close();
		    
		    for(Employee employee: emp){
		    	Log.d("EMPLOYEE", Integer.toString(employee.job_code));
		    	
		    }
	}
	
	private Employee cursorToComment(Cursor cursor){
		Employee employee = new Employee();
		employee.employees_id = cursor.getString(0);
		employee.email_id = cursor.getString(1);
		employee.access_id = cursor.getString(2);
		employee.first_name = cursor.getString(3);
		employee.last_name = cursor.getString(4);
		int id = cursor.getInt(5);
		employee.job_code = id;
		Log.d("ID", Integer.toString(id));
		return employee;
		
	}
}
