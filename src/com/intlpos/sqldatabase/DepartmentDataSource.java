package com.intlpos.sqldatabase;

import java.util.ArrayList;

import com.intlpos.database.Department;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DepartmentDataSource {
	
	private DepartmentSql dbHelper;
	private SQLiteDatabase databse;
	
	private String all[] = {DepartmentSql.DEPT_ID,DepartmentSql.DEPT_NAME,DepartmentSql.URL,DepartmentSql.TS};
	
	public DepartmentDataSource(Context context){
		this.dbHelper = new DepartmentSql(context);
	}
	
	public void createTables(Department d){
		databse = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(DepartmentSql.DEPT_ID, d.getDept_Id());
		values.put(DepartmentSql.DEPT_NAME, d.getDept_Name());
		values.put(DepartmentSql.URL, d.getUrl());
		int ts = (d.getTSDisplay()) ? 1:0;
		values.put(DepartmentSql.TS, ts);
		
		databse.insert(DepartmentSql.TABLE_DEPARTMENT, null, values);
		dbHelper.close();
	}
	
	public void readTables(){
		databse = dbHelper.getReadableDatabase();
		ArrayList<Department> dept = new ArrayList<Department>();
		Cursor cursor = databse.query(DepartmentSql.TABLE_DEPARTMENT, all, null, null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()){
			Department d = getCursor(cursor);
			dept.add(d);
			cursor.moveToNext();
		}
		cursor.close();
		for(Department d:dept){
			Log.d("Depart", d.getDept_Name());
		}
	}
	
	private Department getCursor(Cursor cursor){
		Department d = new Department();
		d.setDept_Id(cursor.getString(0));
		d.setDept_Name(cursor.getString(1));
		d.setUrl(cursor.getString(2));
		int temp  = cursor.getInt(3);
		if(temp == 0)
			d.setTSDisplay(false);
		else
			d.setTSDisplay(true);
		
		return d;
	}
	
}
