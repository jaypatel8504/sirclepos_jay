package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class Modifier_Groups_ModifiersSql extends SQLiteOpenHelper{

	private final static String DATABASENAME = "modifier_groups_modifiers.db";
	private final static int VERSION = 1;
	
	public final static String TABLENAME = "modifier_groups_modifiers";
	public final static String MODIFIER_GROUPS_MODIFIERS_ID = "modifier_groups_modifiers_id";
	public final static String MODIFIER_GROUP_ID = "modifier_group_id";
	public final static String MODIFIER_ID = "modifier_id";
	
	public final static String CREATETABLE = "create table " + TABLENAME + " ( "+
			MODIFIER_GROUPS_MODIFIERS_ID + " integer primary key not null, " + 
			MODIFIER_GROUP_ID + " integer not null, " + "foreign key ('" + MODIFIER_GROUP_ID +"') references "+ Modifier_GroupSql.TABLE_NAME + " ('" + Modifier_GroupSql.MODIFIER_GROUP_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION, " +
			MODIFIER_ID + " integer not null, " + "foreign key ('" + MODIFIER_ID +"') references "+ ProductsSql.PRODUCTS_TABLE + " ('" + ProductsSql.PRODUCT_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION);";
			
	public Modifier_Groups_ModifiersSql(Context context) {
		super(context, DATABASENAME, null, VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATETABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
