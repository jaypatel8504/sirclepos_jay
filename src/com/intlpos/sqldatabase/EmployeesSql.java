package com.intlpos.sqldatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class EmployeesSql extends SQLiteOpenHelper{
	
	public static final String TABLE_EMPLOYEES = "employees";
	public static final String EMPLOYEES_ID = "employees_id";
	public static final String EMAIL_ID= "email_id";
	public static final String ACCESS_ID = "access_id";
	public static final String CARD_SWIPEID = "card_swipeid";
	public static final String DISABLED = "disabled";
	public static final String FIRSTNAME = "first_name";
	public static final String MIDDLENAME = "middle_name";
	public static final String LASTNAME = "last_name";
	public static final String CONTACT_NO = "contact_no";
	public static final String BIRTHDAY = "birthday";
	public static final String PICTURE = "picture";
	public static final String JOBCODE = "job_code";
	public static final String RESET_PASSFLAG = "reset_passflag";
	public static final String LAST_PASSCHANGE = "last_passchange";
	public static final String ACCESS_TO_POS = "access_to_pos";
	public static final String ACCESS_TO_BACKOFFICE="access_to_backoffice";
	public static final String ADMIN_ACCESS = "admin_access";
	
	 private static final String DATABASE_NAME = "employees.db";
	 private static final int DATABASE_VERSION = 1;
	
	private static final String DATABASE_CREATE = "create table " + TABLE_EMPLOYEES + "(" + EMPLOYEES_ID+ " text primary key not null, " + EMAIL_ID + " text not null unique, " + ACCESS_ID + " text default null unique, "
		+ CARD_SWIPEID+ " text default null, " + DISABLED+ " integer not null default '0', " + FIRSTNAME + " text not null, " + MIDDLENAME + " text default null, "
		+ LASTNAME + " text not null, " +  CONTACT_NO + " text default null, " + BIRTHDAY + " text default null, "
		+ PICTURE + " text default null, " +  RESET_PASSFLAG + " integer default 0, "
		+ LAST_PASSCHANGE + " text default null default '0', " + ACCESS_TO_POS + " integer not null default '0', " + ACCESS_TO_BACKOFFICE+ " integer not null default '0', "
		+ ADMIN_ACCESS + " integer not null default '1', " + JOBCODE+ " integer default '1', " + "foreign key ('" + JOBCODE +"') references "+ PermissionSql.TABLE_PERMISSIONS + " ('" + PermissionSql.PERMISSION_ID + "') ON DELETE NO ACTION ON UPDATE NO ACTION );";
	
	public EmployeesSql(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override//called if db is accessed but not created
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("PRAGMA foreign_keys = ON;");
//		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		 db.execSQL("DROP TABLE IF EXISTS " + TABLE_EMPLOYEES);
		 onCreate(db);
	}

}
