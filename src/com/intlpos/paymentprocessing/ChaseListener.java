package com.intlpos.paymentprocessing;


public interface ChaseListener {

	public abstract void onResult(String[] result);
}
