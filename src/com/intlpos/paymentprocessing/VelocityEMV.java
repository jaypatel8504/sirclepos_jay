package com.intlpos.paymentprocessing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

public class VelocityEMV extends AsyncTask<Object, Void, String[]> {
	String dstAddress;
	int dstPort;
	String TransactionType;
	String Amt;
	String TenderType;
	String[] response;
	Context context;
	String[] velocityResponse;
	ProgressDialog progressDialog;
	SharedPreferences pref;
	String resources[];

	private PaymentListener listener;

	public VelocityEMV(Context context, PaymentListener listener,
			String resources[]) {
		StoreDetail sd = CSSharedPreferences.getIpandPort();
		this.dstAddress = sd.getIp(); // pref.getString("ip", "10.0.0.20");
		this.dstPort = sd.getPort();// pref.getInt("port",6200);
		Log.d("CSPOS-ipport", dstAddress + dstPort);
		this.listener = listener;
		this.context = context;
		this.resources = resources;
	}

	@Override
	protected void onPreExecute() {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(resources[0]);
		progressDialog.setCancelable(false);
		progressDialog.setTitle(resources[1]);
		progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, resources[4],
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						AlertDialog alertDialog = new AlertDialog.Builder(
								context).create();

						alertDialog.setTitle(resources[2]);

						alertDialog.setMessage(resources[3]);

						alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
								"OK", new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog,
											int id) {

									}
								});
						alertDialog.show();
					}
				});

		progressDialog.show();
	}

	@Override
	protected String[] doInBackground(Object... params) {
		// TODO Auto-generated method stub
		String[] result = new String[] { "failed", "failed", "0", "0", "0" };

		Log.d("velocityEMV", "in velocity EMV");
		TransactionType = (String) params[0];
		TenderType = (String) params[1];
		Amt = (String) params[2];
		String Approval_Code = "9999";
		HttpPost httppost = new HttpPost("http://" + dstAddress + ":" + dstPort
				+ "/");
		StringEntity se;
		try {
			se = new StringEntity("<DETAIL><TRAN_TYPE>" + TransactionType
					+ "</TRAN_TYPE><AMOUNT>" + Amt + "</AMOUNT></DETAIL>");
			// se.setContentType("text/xml");
			httppost.setHeader("Content-Type", "application/xml");
			httppost.setEntity(se);

			HttpClient httpclient = new DefaultHttpClient();
			BasicHttpResponse httpResponse = (BasicHttpResponse) httpclient
					.execute(httppost);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			InputSource s = new InputSource(new InputStreamReader(httpResponse
					.getEntity().getContent()));
			Document doc = dBuilder.parse(s);
			doc.getDocumentElement().normalize();

			// String root_eliment = doc.getDocumentElement().getNodeName();
			if (doc.getDocumentElement().getNodeName()
					.equalsIgnoreCase("RESPONSE")) {
				NodeList nList = doc.getElementsByTagName("RESPONSE");

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;

						NodeList nl = eElement
								.getElementsByTagName("AUTH_RESP");
						if (nl.getLength() > 0) {
							Approval_Code = eElement
									.getElementsByTagName("AUTH_RESP").item(0)
									.getTextContent();

						}

						if (!Approval_Code.equals("9999")) {
							String LastFour_string = eElement
									.getElementsByTagName(
											"AUTH_MASKED_ACCOUNT_NBR").item(0)
									.getTextContent();
							Log.d("velocityEMV-result[0] ", Approval_Code);
							if (Approval_Code.equals("00")) {

								result[0] = "Approved"; // need to check for
														// partial approval
							} else if (Approval_Code.equals("10")) {
								result[0] = "Partially approved";
							}
							result[1] = eElement
									.getElementsByTagName("AUTH_GUID").item(0)
									.getTextContent();
							result[2] = eElement
									.getElementsByTagName("AUTH_AMOUNT")
									.item(0).getTextContent();
							result[3] = LastFour_string
									.substring(LastFour_string.lastIndexOf('X') + 1);

							Log.d("velocityEMV-result[1]-result[2]-result[3] ",
									result[1] + "-" + result[2] + "-"
											+ result[3]);

						}

					}
				}
			}
			// else
			// {
			// //transaction canceled by the terminal or something else
			// }

			// response.put("HTTPStatus",httpResponse.getStatusLine().toString());
		} catch (IOException | ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@Override
	protected void onPostExecute(String[] result) {
		// textResponse.setText(response);
		super.onPostExecute(result);
		if (listener != null) {
			progressDialog.dismiss();
			listener.onResult(result);

		}
	}
}
