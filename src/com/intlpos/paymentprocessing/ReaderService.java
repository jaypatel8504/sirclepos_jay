package com.intlpos.paymentprocessing;

import java.math.BigDecimal;

import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.PaymentProcessing;
import com.intlpos.sirclepos.PaymentFragmentListener;
import com.magtek.mobile.android.scra.MagTekSCRA;
import com.usaepay.sdk.Gateway;
import com.usaepay.sdk.classes.CreditCardData;
import com.usaepay.sdk.classes.CurrencyAmount;
import com.usaepay.sdk.classes.OnGatewayResultListener;
import com.usaepay.sdk.classes.TransactionRequest;
import com.usaepay.sdk.classes.TransactionResponse;
import com.usaepay.sdk.enums.Command;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Handler.Callback;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class ReaderService {
	private Context context = null;
	private String deviceAddress = null;
	private BluetoothAdapter bluetoothAdapter = BluetoothAdapter
			.getDefaultAdapter();
	private BluetoothDevice device = null;
	public boolean isConnected = false;
	private MagTekSCRA mMTSCRA;
	private Handler mSCRADataHandler;
	//private String transactionType;
	
	ProgressDialog progressDoalog;
	OnGatewayResultListener mlistener ;
	
	ReaderListener listener;
	SharedPreferences pref;
	public ReaderService(Context context,ProgressDialog pd,ReaderListener readerListener) {
		// TODO Auto-generated constructor stub
		super();
		this.context = context;			
		this.mSCRADataHandler = new Handler(new SCRAHandlerCallback());
		this.progressDoalog = pd;
		//this.progressDoalog.setCancelable(false);
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		this.deviceAddress = pref.getString("list_bluetooth_card_reader", "");
		Log.d("CSPOS-MAC", this.deviceAddress);
		if (this.deviceAddress == ""){
			Toast.makeText(context, R.string.cantBTC, Toast.LENGTH_SHORT).show();
			return;
		}
		//this.transactionType = TransactionType;

		this.listener = readerListener;
		
		mMTSCRA = new MagTekSCRA(mSCRADataHandler);
		try
		{
			this.device = this.bluetoothAdapter.getRemoteDevice(this.deviceAddress);
		}
		catch(IllegalArgumentException e){
			Toast.makeText(context, R.string.manInvalid, Toast.LENGTH_SHORT).show();
		}
		
	}
	

	public boolean connect() {
		
		if (!this.isConnected) {
			try {
//				progressDoalog = new ProgressDialog(this.context);
				progressDoalog.setMessage(context.getResources().getString(R.string.swipe));
			    progressDoalog.setTitle(context.getResources().getString(R.string.swipe));
			     
			    progressDoalog.show();
			      
				mMTSCRA.setDeviceType(MagTekSCRA.DEVICE_TYPE_BLUETOOTH);
				// if you know the address, you can directly specify here
				// in that case you would not need a UI to show the list
				// of BT devices
				mMTSCRA.setDeviceID(deviceAddress);
				mMTSCRA.openDevice();
				
				this.isConnected = true;
				Log.d("CSPOS","Magtek connection successfull");
				
				
				
				if (this.bluetoothAdapter.isDiscovering()) {
					System.out.println("Close Adapter!");
					this.bluetoothAdapter.isDiscovering();
				}
			} catch (Exception e) {
				Toast.makeText(this.context, R.string.connect_failed_please_reconnect, 1).show();
				return false;
			}
			Toast.makeText(this.context, this.device.getName() + "Connect successed!",
					Toast.LENGTH_SHORT).show();
			
			return true;
		} else {
			return true;
		}
	}
	
	private class SCRAHandlerCallback implements Callback {
        @Override
		public boolean handleMessage(Message msg) 
        {
        	Log.d("CSPOS", "I am here at Magtek call handler");
        	try
        	{
            	Log.d("CSPOS", msg.what+"");
            	switch (msg.what) 
            	{

    			case MagTekSCRA.DEVICE_MESSAGE_STATE_CHANGE:
    				switch (msg.arg1) {
    				case MagTekSCRA.DEVICE_STATE_CONNECTED:
    					//mIntCurrentStatus = STATUS_IDLE;
    					//mIntCurrentDeviceStatus = MagTekSCRA.DEVICE_STATE_CONNECTED;    					
    					//maxVolume();
    					//setStatus(R.string.title_connected, Color.GREEN);
    					break;
    				case MagTekSCRA.DEVICE_STATE_CONNECTING:
    					//mIntCurrentDeviceStatus = MagTekSCRA.DEVICE_STATE_CONNECTING;
    					//setStatus(R.string.title_connecting, Color.YELLOW);
    					break;
    				case MagTekSCRA.DEVICE_STATE_DISCONNECTED:
    					//mIntCurrentDeviceStatus = MagTekSCRA.DEVICE_STATE_DISCONNECTED;
    					//setStatus(R.string.title_not_connected, Color.RED);
    					//minVolume();
    					break;
    				}
    				break;
    			case MagTekSCRA.DEVICE_MESSAGE_DATA_START:
    	        	if (msg.obj != null) 
    	        	{
    	        		//debugMsg("Transfer started");
    	        		//mCardDataEditText.setText("Card Swiped...");
    	        		progressDoalog.setMessage("Card Swiped...");
    	                return true;
    	            }
    				break;  
    			case MagTekSCRA.DEVICE_MESSAGE_DATA_CHANGE:    				
    	        	if (msg.obj != null) 
    	        	{
    	        		Log.d("CSPOS-Status","DEVICE_MESSAGE_DATA_CHANGE");
    	        		String card_data = mMTSCRA.getResponseData();
    	        		String[] result = new String[]{"full response data","track2","KSN"} ;
    	        		result[0] = mMTSCRA.getResponseData();
    	        		result[1] = mMTSCRA.getTrack2();
    	        		result[2] = mMTSCRA.getKSN();
    	        		
    	        		if(listener!= null)
    	        		{
    	        			progressDoalog.setMessage(context.getResources().getString(R.string.transProcess));
    	        			closeDevice();
    	        			//progressDoalog.dismiss();
    	        			listener.onResult(result);
    	        		}
    	        		
 
    	        	
    	        		msg.obj=null;
//    	        		if(mStringLocalConfig.length() > 0)
//    	        		{
//    						setConfigurationLocal(mStringLocalConfig);//optional but can be useful to retrieve from locally and get it from server only certain times
//    						mStringLocalConfig="";
//    	        		}

    	                return true;
    	            }
    				break;  
    			case MagTekSCRA.DEVICE_MESSAGE_DATA_ERROR:
	        	//	mCardDataEditText.setText("Card Swipe Error... Please Swipe Again.\n");
    				progressDoalog.setMessage(context.getResources().getString(R.string.swipeError));
	                return true;
    			default:
    	        	if (msg.obj != null) 
    	        	{
    	                return true;
    	            }
    				break;
            	};
        		
        	}
        	catch(Exception ex)
        	{
        		
        	}
        	
            return false;
        	
        	
        }

		
    }	
	private void closeDevice()
	{
		if(mMTSCRA.isDeviceConnected())
		{
		mMTSCRA.closeDevice();
		}
	}

   
}
