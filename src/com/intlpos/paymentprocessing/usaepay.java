package com.intlpos.paymentprocessing;

import java.math.BigDecimal;

import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.PaymentProcessing;
import com.usaepay.sdk.Gateway;
import com.usaepay.sdk.classes.CreditCardData;
import com.usaepay.sdk.classes.CurrencyAmount;
import com.usaepay.sdk.classes.OnGatewayResultListener;
import com.usaepay.sdk.classes.TransactionRequest;
import com.usaepay.sdk.enums.Command;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Base64;
import android.util.Log;

public class usaepay {

	OnGatewayResultListener listener;
	Context context;
	ProgressDialog progressDialog;
	private CurrencyAmount bal;
	String gateway;
	String PIN;
	String source_key;
	boolean SANDBOX_MODE = false;	

	public usaepay(Context context,ProgressDialog progressDialog,OnGatewayResultListener listener)
	{
		this.listener = listener;
		this.context = context;
		this.progressDialog = progressDialog;	
		PaymentProcessing payment = CSSharedPreferences.getPayment();
		this.gateway = payment.getGateway();
		this.PIN = payment.getPassword();
		this.source_key = payment.getId();
	}
	public String[] runTransaction(final String TransactionType,final BigDecimal Amt) {
		// TODO Auto-generated method stub		

		String[] result = new String[]{"failed","failed","0","0"} ;
		bal = new CurrencyAmount(Amt.toString());
		try
		{
			ReaderService readerservice = new ReaderService(context,progressDialog, new ReaderListener() {

				@Override
				public void onResult(String[] result) {
					// TODO Auto-generated method stub

					String card_data = result[0];
					TransactionRequest mRequest = new TransactionRequest();
					switch(TransactionType)
					{
					case "sale":
						mRequest.setCommand(Command.CC_SALE); // credit card sale 
					case "return":
						break;
					case "void":
						break;
					}
					mRequest.setCommand(Command.CC_SALE); // credit card sale 	        			  
					mRequest.setTotal(bal) ;
					CreditCardData ccd = new CreditCardData();    			          			                    			             
					ccd.setMagSupport("yes");
					String converted = Base64.encodeToString(card_data.getBytes(), Base64.DEFAULT);
					ccd.setSwipeData("enc://"+converted);

					//   ccd.setSwipeData(card_data);
					mRequest.setCreditCardData(ccd);    			                		
					Log.d("CSPOS-Cred",source_key+" "+PIN);
					Gateway gateway = new Gateway(source_key,PIN,listener);
					gateway.setSandboxMode(SANDBOX_MODE);
					gateway.runTransaction(mRequest);
					progressDialog.setMessage(context.getResources().getString(R.string.transProcess));

				}
			});
			readerservice.connect();	  

		}
		catch(Exception ex)
		{}

		return result;
	}
}
