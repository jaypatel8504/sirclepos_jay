package com.intlpos.paymentprocessing;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import javax.swing.text.html.CSS;

import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

import com.intlpos.sirclepos.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

public class chasepaymentech extends AsyncTask<Object, Void, String[]>{

	  String dstAddress;
	  int dstPort;
	  String TransactionType;
	  BigDecimal Amt;
	  String TenderType;
	  String[] response;  
	  Context context;
	  String[] chaseResponse;
	  ProgressDialog progressDialog;
	  SharedPreferences pref;
	  String resources[];
	/*2 bytes TRNSACTION_TYPE -> {0x30,0x30} purchase
	  							 {0x30,0x33}return/refund,
	  							 {0x30,0x35}void
	*/	
	  byte TRANSACTION_TYPE_BYTE1 ;
	  byte TRANSACTION_TYPE_BYTE2;
	  /*1 byte*/ byte FS = 0x1c; //Field separator
	  
	  /*3 bytes  TAG ->{0x30,0x30,0x31}ECR Transaction Amount
	  					 {0x30,0x30,0x32}ECR Tender Type
	  					 {0x30,0x30,0x33}ECR Clerk ID*/
	  byte AMOUNT_TAG_BYTE1 = 0x30;
	  byte AMOUNT_TAG_BYTE2 = 0x30;
	  byte AMOUNT_TAG_BYTE3 = 0x31;
	  
	  byte TENDER_TAG_BYTE1 = 0x30;
	  byte TENDER_TAG_BYTE2 = 0x30;
	  byte TENDER_TAG_BYTE3 = 0x32;
	  
	  /*1 byte TENDER_TYPE -> 0x30 All Card Type
		  0x31 Debit
		  0x32 Credit
		  0x33 Gift
		  0x34 EBT
		  0x37 Cash*/
		  
	  byte TENDER_TYPE ;
	  
	  /*1 byte */ byte HB = 0x11; //Heart Beat
	  
	  private PaymentListener listener;
	  public chasepaymentech(Context context ,PaymentListener listener, String resources[]){
	   StoreDetail sd = CSSharedPreferences.getIpandPort();
	   this.dstAddress = sd.getIp(); //pref.getString("ip", "10.0.0.20");
	   this.dstPort = sd.getPort();//pref.getInt("port",4554);
	   Log.d("CSPOS-ipport",dstAddress+dstPort); 
   	   this.listener = listener;
   	   this.context = context;
   	   this.resources = resources;
   	  }
	@Override
	protected void onPreExecute()	  
	{
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(resources[0]);
		progressDialog.setCancelable(false);
	    progressDialog.setTitle(resources[1]);
	    progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, resources[4], new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    	AlertDialog alertDialog = new AlertDialog.Builder(context).create();

			    alertDialog.setTitle(resources[2]);

			    alertDialog.setMessage(resources[3]);

			    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", new DialogInterface.OnClickListener() {

			      public void onClick(DialogInterface dialog, int id) {
			        
			    }}); 
			    alertDialog.show();
		    }
		});
	     
	    progressDialog.show();
	}
	@Override
	protected String[] doInBackground(Object... params) {
		// TODO Auto-generated method stub
		 Socket socket = null;		
		 response = new String[]{"50","Trnasaction failed"};
		 
	     TransactionType = (String)params[0];
	     TenderType = (String)params[1];
	     switch(TransactionType)
	     {
	     case "purchase":
	    	 TRANSACTION_TYPE_BYTE1 = 0x30; // (byte)0
	  		 TRANSACTION_TYPE_BYTE2 = 0x30; // (byte)0
	     break;
	     case "return":
	    	 TRANSACTION_TYPE_BYTE1 = 0x30; // (byte)0
	  		 TRANSACTION_TYPE_BYTE2 = 0x33; // (byte)3
	    	 break;
	     case "void":
	    	 TRANSACTION_TYPE_BYTE1 = 0x30;// (byte)0
	  		 TRANSACTION_TYPE_BYTE2 = 0x35;// (byte)5
	    	 break;
	     }
	  	 
	  	  switch(TenderType)
	  	  {
	  	  case "debit":
	  		  TENDER_TYPE =  0x31;// (byte)1
	  		  break;
	  	  case "credit":
	  		  TENDER_TYPE =  0x32;// (byte)2
	  	  break;
	  	  case "gift":
	  		  TENDER_TYPE =  0x33;// (byte)3
	  	  break;
	  	  }
	  	 
	     Amt = (BigDecimal)params[2];
	     String amount = Amt.toString().replace(".", "");
	     byte[] amount_bytes = amount.getBytes();
	     
  	   try {  		
  		   	
  	    Socket clientSocket = new Socket(dstAddress, dstPort);
  	    DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
  	    byte[] data = {TRANSACTION_TYPE_BYTE1,TRANSACTION_TYPE_BYTE2,FS,TENDER_TAG_BYTE1,TENDER_TAG_BYTE2,TENDER_TAG_BYTE3,TENDER_TYPE,FS,AMOUNT_TAG_BYTE1,AMOUNT_TAG_BYTE2,AMOUNT_TAG_BYTE3};
  	    byte[] destination = new byte[data.length + amount_bytes.length];
    	System.arraycopy(data, 0, destination, 0, data.length);
    	System.arraycopy(amount_bytes, 0, destination, data.length, amount_bytes.length);
    	Log.d("CSPOS-Array",destination.toString());
  	    outToServer.write(destination);   
  	    final DataInputStream in = new DataInputStream(clientSocket.getInputStream());
    	byte[] bytes2 = new byte[100];
        char[] first_two = new char[2];
  	int p = 0;
  	for (int b = 0; ((b = in.read()) >= 0);)
  	{
  		if(b != (byte)0x11)
		  {
  					 first_two[p] = (char)b;
  					Log.d("CSPOS-first_two",""+(char)b);
  					if(p == 1)
  					{break;}
  					 p++;
  					 
		  }
  	}
  	Log.d("CSPOS-FirstTwo",String.copyValueOf(first_two));//STRINGS!
	//\\chaseResponse = context.getResources().getStringArray(R.array.)
  	switch(String.copyValueOf(first_two))
  	{  	
  	case "00": // Approved
  		response[0] = "Approved";
  		in.readFully(bytes2); // reads from the third byte
	  	Log.d("CSPOS-CHARARRAY-Out",new BigInteger(1, bytes2).toString(16));
	  	response[1] = new BigInteger(1, bytes2).toString(16);
  		break; 
  	case "01": // Partially approved 
  		response[0] = "Partially approved";
  		in.readFully(bytes2); // reads from the third byte
	  	Log.d("CSPOS-CHARARRAY-Out",new BigInteger(1, bytes2).toString(16));
	  	response[1] = new BigInteger(1, bytes2).toString(16);
  		break;
  	case "10": // Declined by acquirer
  		response[0] = "Declined by acquirer";
  		break;
  	case "11": // Communication error
  		response[0] = "Communication error";
  		break;
  	case "12":  // Cancelled by use
  		response[0] = "Cancelled by user";
  		break;
  	case "13": // Timed out on user input
  		response[0] = "Timed out on user input";
  		break;
  	case "14": // Transaction not completed
  		response[0] = "Timed out on user input";
  		break;
  	case "15": // Batch empty
  		response[0] = "Batch empty";
  		break;
  	case "16": // Declined by merchant
  		response[0] = "Declined by merchant";
  		break;
  	case "30":  // Invalid ECR parameter
  		response[0] = "Invalid ECR parameter";
  		break; 
  	default:
  		// Transaction failed
  		response[0] = "Transaction failed";
  		
  	}
	 
  	  clientSocket.close();
 
  	   } catch (UnknownHostException e) {
  	    // TODO Auto-generated catch block
  	    e.printStackTrace();
  	    response[0] = "UnknownHostException: " + e.toString();
  	   } catch (IOException e) {
  	    // TODO Auto-generated catch block
  	    e.printStackTrace();
  	    response[0] = "IOException: " + e.toString();
  	   }finally{
  	    if(socket != null){
  	     try {
  	      socket.close();
  	     } catch (IOException e) {
  	      // TODO Auto-generated catch block
  	      e.printStackTrace();
  	     }
  	    }	
  	   }
  	   return response;
  	  }

	 @Override
	  protected void onPostExecute(String[] result) {
	  // textResponse.setText(response);
	   super.onPostExecute(result);
	   if (listener != null) {			
		   progressDialog.dismiss();
		      listener.onResult(result);	
		      
		    }
	  }
}
