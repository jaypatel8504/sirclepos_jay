package com.intlpos.paymentprocessing;

public interface ReaderListener {
	public abstract void onResult(String[] result);
}
