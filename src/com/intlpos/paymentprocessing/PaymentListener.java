package com.intlpos.paymentprocessing;

public interface PaymentListener {
	public abstract void onResult(String[] result);
}
