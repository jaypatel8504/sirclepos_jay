package com.intlpos.sirclepos;

import java.util.LinkedHashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.ReceiptDetail;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.PaymentProcessing;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Other extends DialogFragment  implements OnItemSelectedListener, OnClickListener{
	private Spinner processor; 
	private EditText urlPrimary, secureKey, pin, merchantID, state;
	private Button ok, can;
	private String payType, url, key, getPin;
	private LinearLayout pinLay,urlLay, keyLay, velLay;
	PaymentProcessing pp = CSSharedPreferences.getPayment();
	public static Other newInstance() {
		Other frag = new Other();
	     return frag;
	}
	
	
	 @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
		 
		 View v = inflater.inflate(R.layout.other, container, false);
		 setCancelable(false);
		 getDialog().setTitle(R.string.PaymentProcessingInfo);
		 
		 int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		 InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
			
		 processor = (Spinner)v.findViewById(R.id.paymentProcessor);
		 
		 ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
			        R.array.payTypes, android.R.layout.simple_spinner_item);
		 
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		
		processor.setAdapter(adapter);
		processor.setOnItemSelectedListener(this);
		
		
		pinLay = (LinearLayout)v.findViewById(R.id.pinLay);
		urlLay = (LinearLayout)v.findViewById(R.id.urlLay);
		keyLay = (LinearLayout)v.findViewById(R.id.KeyLay);
		velLay = (LinearLayout)v.findViewById(R.id.velLay);
		
		 urlPrimary = (EditText)v.findViewById(R.id.getUrl);
		 secureKey = (EditText)v.findViewById(R.id.getKey);
		 pin = (EditText)v.findViewById(R.id.getpin);
		 merchantID = (EditText)v.findViewById(R.id.getID);
		 state = (EditText)v.findViewById(R.id.state);
		 
		 if(!pp.getGateway().isEmpty()){
			 if(pp.getGateway().equals("velocity")){
				 merchantID.setText(pp.getId());
			 }else{
				 urlPrimary.setText(pp.getUrl());
				 secureKey.setText(pp.getId());
				 pin.setText(pp.getPassword());
			 }
			 processor.setSelection(getIndex(processor, pp.getGateway()));
		 }
		 
		 ok = (Button)v.findViewById(R.id.okPay);
		 can = (Button)v.findViewById(R.id.cancelPay);
		 ok.setOnClickListener(this);
		 can.setOnClickListener(this);
		 return v;
	 }

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		payType =  arg0.getItemAtPosition(arg2).toString();
		if(payType.equals("chasepaymentech")){
			urlLay.setVisibility(View.GONE);
				pinLay.setVisibility(View.GONE);
				keyLay.setVisibility(View.GONE);	
				velLay.setVisibility(View.GONE);
		}else if(payType.equals("usaepay")){
			urlLay.setVisibility(View.GONE);
			pinLay.setVisibility(View.VISIBLE);
			keyLay.setVisibility(View.VISIBLE);
			velLay.setVisibility(View.GONE);
		}else if(payType.equals("velocity")|| payType.equals("velocityemv")){
			velLay.setVisibility(View.VISIBLE);
			urlLay.setVisibility(View.GONE);
			pinLay.setVisibility(View.GONE);
			keyLay.setVisibility(View.GONE);	
		}else{
			velLay.setVisibility(View.GONE);	
			pinLay.setVisibility(View.VISIBLE);
			urlLay.setVisibility(View.VISIBLE);
			keyLay.setVisibility(View.VISIBLE);
		}
		
		 if(!pp.getUrl().isEmpty() && !pp.getPassword().isEmpty() && !pp.getId().isEmpty()){
			 if(!pp.getGateway().equals(payType)){
				 urlPrimary.setText("");
				 secureKey.setText("");
				 pin.setText("");
			 }else{
				 urlPrimary.setText(pp.getUrl());
				 secureKey.setText(pp.getId());
				 if(payType.equals("velocity")||payType.equals("velocityemv")){
					 merchantID.setText(pp.getId());
					 state.setText(pp.getPassword());
				 }else{
					 pin.setText(pp.getPassword());
				 }
			 }
		 }
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
		if(!pp.getUrl().isEmpty() && !pp.getPassword().isEmpty() && !pp.getId().isEmpty()){
			payType =  arg0.getItemAtPosition(getIndex(processor, pp.getGateway())).toString(); 
		 }
		else{
			payType =  arg0.getItemAtPosition(0).toString();
		}
	}

	private boolean checkInput(){
		if(payType.equals("velocity") || payType.equals("velocityemv")){
			if(key.isEmpty() || state.getText().toString().isEmpty()){
				Toast.makeText(getActivity(), "Every Textfield must be filled!", Toast.LENGTH_SHORT).show();
				return false;
			}else
				return true;
		}
		if(key.length()>50){
			Toast.makeText(getActivity(), "Key can only be 50 characters long!", Toast.LENGTH_SHORT).show();
			return false;
		}
		if(getPin.length()>20){
			Toast.makeText(getActivity(), "Pin can only be 20 characters long!", Toast.LENGTH_SHORT).show();
			return false;
		}
		if(!payType.equals("chasepaymentech")  ){
			if(url.isEmpty() || key.isEmpty() || getPin.isEmpty()){
				Toast.makeText(getActivity(), "Every Textfield must be filled!", Toast.LENGTH_SHORT).show();
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.okPay:
			
			if(payType.equals("chasepaymentech")){
				url = "InternationalPOS";
				key = "InternationalPOS";
				getPin = "InternationalPOS";
			}else if(payType.equals("usaepay")){
				url = "InternationalPOS";
				key = secureKey.getText().toString();
				getPin = pin.getText().toString();
			}else if(payType.equals("velocity")|| payType.equals("velocityemv")){
				url = "InternationalPOS";
				key = merchantID.getText().toString();
				getPin = state.getText().toString().toUpperCase();
			}
			else{
				url = urlPrimary.getText().toString();
				key = secureKey.getText().toString();
				getPin = pin.getText().toString();
			}
			if(checkInput() == true){
				updateProcessingData();
				
			}
			break;
		case R.id.cancelPay:
			dismiss();
			break;
		}
	}
	
	private void updateProcessingData() {
		// TODO Auto-generated method stub
		String methodName = "PaymentProcessing/PaymentService.svc/updatepaymentprocessingdata";

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	
		LinkedHashMap<String, Object> payment = new LinkedHashMap<String, Object>();
		payment.put("gateway_name", payType);
		payment.put("merchant_id", key);
		payment.put("merchant_password", getPin);
		payment.put("url", url);
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);

		params.put("storedetail", storedetail);
		params.put("payment", payment);

		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {
				Log.d("RESULT", json.toString());
				try {
					Boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if(result == true)
					{
						PaymentProcessing pp = new PaymentProcessing();
						pp.setGateway(payType);
						pp.setId(key);
						pp.setPassword(getPin);
						pp.setUrl(url);
						
						CSSharedPreferences.setPaymentProcessing(pp);
						Intent i = new Intent(getActivity(), Settings.class);
						startActivity(i);
						getActivity().finish();
						dismiss();
					}
					else{
						Toast.makeText(getActivity(),
								R.string.NotUpdated, Toast.LENGTH_LONG).show();
						dismiss();
					}
				}
				catch (JSONException e) {
					Log.e("JSONException", "" + e.toString());
					dismiss();
				}
			}
			
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private int getIndex(Spinner spinner, String myString){

        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).equals(myString)){
                index = i;
            }
        }
        return index;
}
}
