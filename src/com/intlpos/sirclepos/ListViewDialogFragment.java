package com.intlpos.sirclepos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.database.InventoryParcelable;
import com.intlpos.database.ItemListViewAdapter;
import com.intlpos.database.ModifierGroupProducts;
import com.intlpos.database.ModifierGroups;
import com.intlpos.database.ModifierProducts;
import com.intlpos.initsetup.ReceiptDetails.OnFragmentSwitchListener;
import com.intlpos.sirclepos.TestFragment.OnMyButtonClickListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class ListViewDialogFragment extends DialogFragment implements OnItemSelectedListener  {
	private ArrayList<InventoryParcelable> data, temp;
	private OnItemSelectedListener mListener;
	private ListView listview;
	private ItemListViewAdapter adapter;
	private EditText idSearch,nameSearch;
	private Button cleanId,cleanName,searchId,searchName, mods, choice, all;
	private TextView itemId,itemName,itemPrice,itemStock;
	private Spinner departs, groups;
    private CornerStorePOS app;
    static ListViewDialogFragment newInstance(ArrayList<InventoryParcelable> data) {
        ListViewDialogFragment newFragment = new ListViewDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("map", data);
		newFragment.setArguments(bundle);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.data = args.getParcelableArrayList("map");
        this.temp = this.data;
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().getWindow().setTitle("Select the Inventory you want:");
		getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_white));
		TextView title = (TextView)getDialog().findViewById( android.R.id.title );
		title.setTextColor( Color.parseColor("#000000") );
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		View titleDivider = getDialog().findViewById(titleDividerId);
		if (titleDivider != null)
		    titleDivider.setBackgroundColor(Color.parseColor("#2f6699"));
		app = (CornerStorePOS) getActivity().getApplication();
		View view = inflater.inflate(R.layout.listview_dialogfragment, container, false);
		View header = inflater.inflate(R.layout.listitem_invoicehome_header, null);
		listview = (ListView) view.findViewById(R.id.searchitems_listview);
		idSearch = (EditText) view.findViewById(R.id.item_id_edit);
		nameSearch = (EditText) view.findViewById(R.id.item_name_edit);
		itemId = (TextView) header.findViewById(R.id.item_id);
		itemName = (TextView) header.findViewById(R.id.item_name);
		itemPrice = (TextView) header.findViewById(R.id.item_price);
		itemStock = (TextView) header.findViewById(R.id.item_stock);
		
		HeaderClickListener listener = new HeaderClickListener();
		itemId.setOnClickListener(listener);
		itemName.setOnClickListener(listener);
		itemPrice.setOnClickListener(listener);
		itemStock.setOnClickListener(listener);
		Collections.sort(data,new NameComparator());
		adapter = new ItemListViewAdapter(getActivity(), data);
		listview.addHeaderView(header,null,false);
		
		

//		idSearch.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // Listview name of the class
//            	adapter.getFilter().filter("id"+s);
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                    int after) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                // TODO Auto-generated method stub
//
//            }
//        });
//		
//		nameSearch.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // Listview name of the class
//            	adapter.getFilter().filter("nm"+s);
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                    int after) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                // TODO Auto-generated method stub
//
//            }
//        });
		cleanId = (Button) view.findViewById(R.id.clean_item_id);
		cleanName = (Button) view.findViewById(R.id.clean_item_name);
		searchId = (Button) view.findViewById(R.id.search_item_id);
		searchName = (Button) view.findViewById(R.id.search_item_name);
		mods = (Button) view.findViewById(R.id.mods);
		groups = (Spinner) view.findViewById(R.id.groups);
		choice = (Button) view.findViewById(R.id.choice);
		departs = (Spinner) view.findViewById(R.id.depart);
		all = (Button) view.findViewById(R.id.all);
		String departsA[] = new String[app.DepartmentList.size()+1];
		departsA[0] = " ";
		int i = 1;
		for(Department d:app.DepartmentList){
			departsA[i] = d.getDept_Name();
			i++;
		}
		
		ArrayAdapter<String> departAdapter = new ArrayAdapter(getActivity(),
				android.R.layout.simple_spinner_item, departsA);
		
		departs.setAdapter(departAdapter);
		departs.setOnItemSelectedListener(this);
		String groupsMod[] = new String[app.ModifierGroup.size()+1];
		groupsMod[0] = " ";
		i = 1;
		for(ModifierGroups g :app.ModifierGroup){
			groupsMod[i] = g.getGroupName();
			i++;
		}
		
		ArrayAdapter<String> modGroupAdapter = new ArrayAdapter(getActivity(),
				android.R.layout.simple_spinner_item, groupsMod);
		groups.setAdapter(modGroupAdapter);
		groups.setOnItemSelectedListener(this);
		
		all.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mods.setBackgroundResource(R.drawable.loginbutton);
				choice.setBackgroundResource(R.drawable.loginbutton);
				all.setBackgroundResource(R.drawable.bg_deptbutton_selected);
				data = temp;
				adapter = new ItemListViewAdapter(getActivity(), data);
				listview.setAdapter(adapter);
			}
		});
		mods.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				choice.setBackgroundResource(R.drawable.loginbutton);
				all.setBackgroundResource(R.drawable.loginbutton);
				mods.setBackgroundResource(R.drawable.bg_deptbutton_selected);
				ArrayList<InventoryParcelable> modifs = new ArrayList<InventoryParcelable>();
				for(int i = 0; i < data.size(); i++){
					if((boolean) data.get(i).map.get("ismodifier")){
						modifs.add(data.get(i));
					}
				}
				adapter = new ItemListViewAdapter(getActivity(), modifs);
				listview.setAdapter(adapter);
			}
		});
		
		choice.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				choice.setBackgroundResource(R.drawable.bg_deptbutton_selected);
				mods.setBackgroundResource(R.drawable.loginbutton);
				all.setBackgroundResource(R.drawable.loginbutton);
				ArrayList<InventoryParcelable> prompt = new ArrayList<InventoryParcelable>();
				for(int i = 0; i < data.size(); i++){
					if(!data.get(i).map.get("prompt_items_id").toString().equals("null")){
						prompt.add(data.get(i));
					}
				}
				adapter = new ItemListViewAdapter(getActivity(), prompt);
				listview.setAdapter(adapter);
			}
		});
		
		cleanId.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				idSearch.getText().clear();
				adapter.getFilter().filter("id"+idSearch.getText().toString());
			}
			
		});		
		searchId.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				adapter.getFilter().filter("id"+idSearch.getText().toString());
			}
			
		});
		cleanName.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				nameSearch.getText().clear();
				adapter.getFilter().filter("nm"+nameSearch.getText().toString());
			}
			
		});		
		searchName.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				adapter.getFilter().filter("nm"+nameSearch.getText().toString());
			}
			
		});
		
		adapter = new ItemListViewAdapter(getActivity(), data);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				mListener.onInvoiceSelected(parent.getAdapter().getItem(position));
				dismiss();
			            }
			        });
        return view;
    }
	


    @Override
    public void onPause() {
        super.onPause();
        idSearch.getText().clear();
        nameSearch.getText().clear();
    }
    
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnItemSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}


	public interface OnItemSelectedListener {
		// TODO: Update argument type and name
		public void onInvoiceSelected(Object object);
	}
	

    public int convertToPx(int input) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (input * scale + 0.5f);
    }
    
    class NameComparator implements Comparator<InventoryParcelable>{
		@Override
		public int compare(InventoryParcelable lhs, InventoryParcelable rhs) {
			return lhs.map.get("item_name").toString().compareToIgnoreCase(rhs.map.get("item_name").toString());
		}
    }
    
    class IdComparator implements Comparator<InventoryParcelable>{
		@Override
		public int compare(InventoryParcelable lhs, InventoryParcelable rhs) {
			return lhs.map.get("item_no").toString().compareToIgnoreCase(rhs.map.get("item_no").toString());
		}
    }
    
    class PriceComparator implements Comparator<InventoryParcelable>{
		@Override
		public int compare(InventoryParcelable lhs, InventoryParcelable rhs) {
			Double price1 = Double.valueOf(((BigDecimal)lhs.map.get("price")).toString());
			Double price2 = Double.valueOf(((BigDecimal)rhs.map.get("price")).toString());
			return (int) (price1 - price2);
		}
    }
    
    class StockComparator implements Comparator<InventoryParcelable>{
		@Override
		public int compare(InventoryParcelable lhs, InventoryParcelable rhs) {
			Double price1 = Double.valueOf(lhs.map.get("item_stock").toString());
			Double price2 = Double.valueOf(rhs.map.get("item_stock").toString());
			return (int) (price1 - price2);
		}
    }
    
    class HeaderClickListener implements View.OnClickListener{
		@Override
		public void onClick(View v) {
			int id = v.getId();
			Log.d("click", id+"");
			switch(id){
			case R.id.item_id:
				Collections.sort(data,new IdComparator());
				adapter = new ItemListViewAdapter(getActivity(), data);
				adapter.notifyDataSetChanged();
				listview.setAdapter(adapter);
				break;
			case R.id.item_name:
				Collections.sort(data,new NameComparator());
				adapter = new ItemListViewAdapter(getActivity(), data);
				adapter.notifyDataSetChanged();
				listview.setAdapter(adapter);
				break;
			case R.id.item_price:
//				Collections.sort(data,new PriceComparator());
//				adapter = new ItemListViewAdapter(getActivity(), data);
//				adapter.notifyDataSetChanged();
//				listview.setAdapter(adapter);
				break;
			case R.id.item_stock:
				Collections.sort(data,new StockComparator());
				adapter = new ItemListViewAdapter(getActivity(), data);
				adapter.notifyDataSetChanged();
				listview.setAdapter(adapter);
				break;				
			}
			
		}
    }


	@Override
	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
		// TODO Auto-generated method stub
		mods.setBackgroundResource(R.drawable.loginbutton);
		choice.setBackgroundResource(R.drawable.loginbutton);
		all.setBackgroundResource(R.drawable.loginbutton);
		switch(parent.getId()){
		case R.id.depart:
			String department = parent.getItemAtPosition(pos).toString();
		
			ArrayList<InventoryParcelable> deparmentIn = new ArrayList<InventoryParcelable>();
			for(int i = 0; i < data.size(); i++){
				if(data.get(i).map.get("department_name").toString().equals(department)){
					deparmentIn.add(data.get(i));
				}
			}
			if(!department.equals(" ")){
				adapter = new ItemListViewAdapter(getActivity(), deparmentIn);
				listview.setAdapter(adapter);
				data = deparmentIn;
			}
			break;
		case R.id.groups:
			String groupName = parent.getItemAtPosition(pos).toString();
			int gid;
			ArrayList<InventoryParcelable>groupIn = new ArrayList<InventoryParcelable>();
			for(ModifierGroups g :app.ModifierGroup){
				if(g.getGroupName().equals(groupName)){
					gid = g.getGroupId();
					Log.d("gid", Integer.toString(gid));
					for(ModifierGroupProducts keys: app.GroupProducts){
						Log.d("gid keys.getKey()", Integer.toString(keys.getKey()));
						if(keys.getKey() == gid){
							for(ModifierProducts mp:keys.getModProducts()){
								for(int i = 0; i < data.size(); i++){
									if(data.get(i).map.get("product_id").toString().equals(Integer.toString(mp.getProductId()))){
										groupIn.add(data.get(i));
									}
								}
							}
						}
					}
					adapter = new ItemListViewAdapter(getActivity(), groupIn);
					listview.setAdapter(adapter);
					data = groupIn;
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		adapter = new ItemListViewAdapter(getActivity(), data);
		listview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
}
