package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.HelpActivity.WebClientClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class GotToBackOffice extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.backoffice);
		WebView myWebView = (WebView) findViewById(R.id.back);
		myWebView.setWebViewClient(new WebViewClient());
		
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.loadUrl("http://backoffice.sirclepos.com/");
		 WebClientClass webViewClient = new WebClientClass();
		 myWebView.setWebViewClient(webViewClient);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, EmployeeLogin.class);
		startActivity(intent);
	}
	
	public class WebClientClass extends WebViewClient {
		  ProgressDialog pd = null;

		  @Override
		  public void onPageStarted(WebView view, String url, Bitmap favicon) {
		   super.onPageStarted(view, url, favicon);
		   pd = new ProgressDialog(GotToBackOffice.this);
		   pd.setTitle("Please wait");
		   pd.setMessage("Page is loading..");
		   
		  }

		  @Override
		  public void onPageFinished(WebView view, String url) {
		   super.onPageFinished(view, url);
		   
		  }
		  
		  @Override
		    public boolean shouldOverrideUrlLoading(WebView webview, String url)
		    {	
			  	if(url.endsWith(".pdf")){
				  	Intent intent = new Intent(GotToBackOffice.this, PdfViewer.class);
				  	intent.putExtra("result", url);
					startActivity(intent);
			    }
			  	else{
			  		webview.loadUrl(url);
			  	}
		    	
		        return true;
		    }
		  	
		 }
}
