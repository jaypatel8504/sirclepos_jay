
package com.intlpos.sirclepos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ImageLoader;
import com.intlpos.database.InventoryParcelable;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.sirclepos.TestFragment.OnMyButtonClickListener;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView;

public class ItemsFragment extends Fragment {
	private int btnWidth;
	private int btnHeight;
    private static final String TAG = "TestFragment";
    //private String[] sTitle = null;// = "hello android";
    private int[] bg_color = null;
    private OnMyProductButtonClickListener pListener;
    private ArrayList<HashMap<String,Object>> items = null;
    private String[] item_name = null;
    private String[] product_id = null;
    
    public interface OnMyProductButtonClickListener{
		public void onMyProductButtonClick(HashMap<String,Object> myproduct);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
		pListener = (OnMyProductButtonClickListener) activity;
		} catch(ClassCastException e){
			throw new ClassCastException(activity.toString() 
					+ "must implement OnMyButtonClickListener();");
		}
	}
    
    // Rinkal - added while Items Lookup
    static ItemsFragment newInstance(int width, int height, ArrayList<InventoryParcelable> item,int[] bg_color) {
    	ItemsFragment newFragment = new ItemsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("btnWidth", width);
        bundle.putInt("btnHeight", height);
        bundle.putIntArray("bg_color", bg_color);
        bundle.putParcelableArrayList("value", item);
        newFragment.setArguments(bundle);
        return newFragment;
    }
  
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null){
        	
        
        Bundle args = getArguments();
        bg_color = (int[]) args.getIntArray("bg_color");
        ArrayList<InventoryParcelable> itemsList = args.getParcelableArrayList("value");
        items = new ArrayList<HashMap<String,Object>>();
        for(InventoryParcelable ip: itemsList){
        	items.add(ip.map);
        	
        }
        
        btnWidth = (int) args.getInt("btnWidth");
        btnHeight = (int) args.getInt("btnHeight");
//	        if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("ui", false)){
//	        	DisplayMetrics metrics = new DisplayMetrics();
//	        	getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//	        	if(metrics.widthPixels >1200){
//	        		btnHeight += 75;
//		    		btnWidth += 75;
//	        	}else{
//		    		btnHeight += 40;
//		    		btnWidth += 40;
//	        	}
//	    	}else{
////	    		btnWidth += 10;
//	    		btnHeight += 10;
//	    	}
        }
        
        
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
   
        MyViewGroup view = (MyViewGroup) inflater.inflate(R.layout.pager_item, container, false);
        int len = items == null ? 0 : items.size();
        //int len = item_name == null ? 0 : item_name.length;
        MyImageButton[] button = new MyImageButton[len];
        int count = 0;
//        Button[] button = new Button[len];
    	
        for(int i = 0 ; i<len;i++){
        	boolean pic = false;
        	button[i] = new MyImageButton(this.getActivity());        
        	MyViewGroup.LayoutParams param = new MyViewGroup.LayoutParams(btnWidth,LayoutParams.WRAP_CONTENT);
        	button[i].setMinimumHeight(btnHeight);
        	button[i].setMinimumWidth(btnWidth);
        	button[i].setLayoutParams(param);
        	if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
	        	if(!items.get(i).get("picture").toString().isEmpty()){
	        		try{
	        			pic = true;
	        			//Muhammad- This loads the image button. 
	        			ImageLoader imageLoader;
	        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight, getActivity());
	        			
	        			imageLoader.DisplayImage(items.get(i).get("picture").toString(), button[i]);
	        		}catch(NullPointerException e){
	        				
	        		}	
	        	}
        	}
        	
        	button[i].setBackgroundResource(R.drawable.bg_inventorybutton_states);
        	button[i].setText(items.get(i).get("item_name").toString());
        	Log.d("items",items.get(i).get("item_name").toString());
        	Log.d("items ui",Integer.toString(i));
        	button[i].setPrice(((BigDecimal) items.get(i).get("price")).setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
        	button[i].setTextColor(Color.BLACK);button[i].setId(i);
        	button[i].setTag(items.get(i));
        	view.setBackgroundResource(R.drawable.shape);
        	view.addView(button[i]);

        	final String barcode = (String) items.get(i).get("item_no");
        	        	button[i].setOnLongClickListener(new Button.OnLongClickListener() {
        					@Override
        					public boolean onLongClick(View v) {
        	
        						PriceCheckDialog dialog = new PriceCheckDialog(getActivity(),CornerStorePOS.PMap,barcode);
        						dialog.setTitle(R.string.CheckItem);
        						TextView title = (TextView)dialog.findViewById( android.R.id.title );
        						title.setTextColor( Color.parseColor("#000000") );
        						int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
        						View titleDivider = dialog.findViewById(titleDividerId);
        						if (titleDivider != null)
        						    titleDivider.setBackgroundColor(Color.parseColor("#2f6699"));
        		                dialog.show();	       
        						return false;
        					}
        	        		
        	       	});
        	

            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    	HashMap<String,Object> product =  (HashMap<String, Object>) v.getTag();
            	    	pListener.onMyProductButtonClick(product);
            	       }
            	     });
        	}
        
        
        return view;
    }
	

    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.d(TAG, "TestFragment-----onDestroy");
    }
    
    
}
