package com.intlpos.sirclepos;


import java.util.ArrayList;
import java.util.List;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ButtonParam;

import android.R.color;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class PromptDialogFragment extends DialogFragment {
	private DisplayMetrics dm;
//	private Context context = this;
	private final static int VIEW_MARGIN=2;
	private final static int BUTTON_MARGIN=10;
	private static final String TAG = "123";
	public final static int btnWidth = 100;
	public final static int btnHeight = 50;
	// var for viewPager
	private ArrayList<Fragment> fragmentsList;
	private ArrayList<ArrayList<ButtonParam>> btn_list;
	private int viewTag;
	private MyFragmentPagerAdapter adapter= null;
	private RadioGroup dotGroupButton = null;
	private ArrayList<ArrayList<Button>> btns_list;
	private ViewPager viewPager;
	private List<View> viewList;
	private View view;
	private String[] item_name;
	private int[] item_id;
	private String[] pic;
    private Button submit,cancel;
    private int selected[], no[];
	//private OnViewUpdateListener mCallback;
	private boolean hasMeasured;
	private boolean mods;
	int screenWidth;
	int screenHeight;
	private String dialogTitle;
    private OnMySubmitButtonClickListener mListener;
    private String prices[];
    private int row;
    private boolean edit;
    public static PromptDialogFragment newInstance(String[] item_name,int[] item_id,String dialogTitle, String pic[], boolean mods, int row, String prices[]) {
    	PromptDialogFragment newFragment = new PromptDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("item_name", item_name);
        bundle.putIntArray("item_id", item_id);
        bundle.putString("dialogTitle", dialogTitle);
        bundle.putStringArray("pic", pic);
        bundle.putBoolean("mods", mods);
        bundle.putInt("row",row);
        bundle.putStringArray("prices", prices);
        bundle.putBoolean("edit", false);
        newFragment.setArguments(bundle);
        return newFragment;
    }
    
    public static PromptDialogFragment newInstance(String[] item_name,int[] item_id,String dialogTitle, String pic[], boolean mods, int row, int selected[], int no[], String prices[], boolean edit) {
    	PromptDialogFragment newFragment = new PromptDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("item_name", item_name);
        bundle.putIntArray("item_id", item_id);
        bundle.putString("dialogTitle", dialogTitle);
        bundle.putStringArray("pic", pic);
        bundle.putBoolean("mods", mods);
        bundle.putInt("row",row);
        bundle.putIntArray("selected", selected);
        bundle.putIntArray("no", no);
        bundle.putStringArray("prices", prices);
        bundle.putBoolean("edit", edit);
        newFragment.setArguments(bundle);
        return newFragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey("item_name")){
        	item_name = args.getStringArray("item_name");
        	item_id = args.getIntArray("item_id");
        	dialogTitle = args.getString("dialogTitle");
        	pic = args.getStringArray("pic");
        	mods = args.getBoolean("mods");
        	row = args.getInt("row");
        	prices = args.getStringArray("prices");
        	edit = args.getBoolean("edit");
        	if(args.containsKey("selected")){
        		selected  = args.getIntArray("selected");
        		no = args.getIntArray("no"); 
        	}
        }
        else 
        	item_name = new String[3];
        
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mydialogfragment, container, false);
        screenWidth  = convertToPx(500);
        screenHeight  = convertToPx(500-80);
		Log.d("DIM", "Wid = "+screenWidth+" Hei = "+ screenHeight);
			initFragViewParams(view,item_name,item_id,pic);
        	initFragView(view);
       return view;
	}
	

	public void initFragViewParams(View v,String[] item_name, int[] item_id, String pic[]){
		getDialog().getWindow().setTitle(dialogTitle);
		getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.shape));
		getDialog().setCanceledOnTouchOutside(false);
		
		int buttonsPerPage = (screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)) *
				(screenHeight/(btnHeight+VIEW_MARGIN));
		
		viewList = new ArrayList<View>();

        final String sTitle[] = item_name;
		int total_buttons = sTitle.length;
		int pages = (int) Math.ceil((double) total_buttons/buttonsPerPage);
	
	     // create buttons dynamically
		Log.d(TAG, "Btn#= "+ total_buttons + " Btn/Page= "+buttonsPerPage+ " Page#= "+pages);
		
		btn_list = new ArrayList<ArrayList<ButtonParam>>();
		ArrayList<ButtonParam> btn_params = new ArrayList<ButtonParam>();
        int count = 0;
        for (int i = 0; i<sTitle.length; i++){
        	if(prices == null)
        		btn_params.add(new ButtonParam(sTitle[i], i,item_id[i], pic[i], Color.MAGENTA));
        	else
        		btn_params.add(new ButtonParam(sTitle[i], Float.valueOf(prices[i]),item_id[i], pic[i], Color.MAGENTA));
			count++;
			if (count == buttonsPerPage){
				btn_list.add(btn_params);
				btn_params = new ArrayList<ButtonParam>();
				count = 0;
			}
		}
        
        //add the last several strings
        if (btn_params.size() != 0)
        	btn_list.add(btn_params);

	}
	public void initFragView(View v){
		viewPager = (ViewPager) v.findViewById(R.id.testViewPager);//Layout of viewPager
		Log.d(TAG, "ID of ViewPager = "+ viewPager.getId());
		dotGroupButton = (RadioGroup) v.findViewById(R.id.dotGroupButton);
		fragmentsList = new ArrayList<Fragment>();
		for (int i = 0; i < btn_list.size();i++){
			// Create a MyViewGroup
			ArrayList<ButtonParam> list = btn_list.get(i);
			int size = list.size();
			
			String[] str_arr = new String[size];
			int[] img_arr = new int[size];
			int[] color_arr = new int[size];
			float[] prices = new float[size];
			int[] id_arr = new int[size];
			String pic[] = new String[size];
			for (int j = 0 ; j < size; j++){
				str_arr[j] = list.get(j).getText();
				id_arr[j] = list.get(j).getItem_id();
				img_arr[j] = list.get(j).getDrawable();
				color_arr[j] = list.get(j).getBgColor();
				prices[j] = list.get(j).getPrice();
				pic[j] = list.get(j).getLink();
			}
//			Log.d(TAG, "Len of String arr = "+ array.length);
			Log.d("CODE", "IS HERE");
			Fragment mFragment = null;
			if(!mods)
				mFragment = TestDialogFragment.newInstance(btnWidth,btnHeight,str_arr,id_arr,prices,img_arr,color_arr,pic);
			else{
				mFragment = SelectModsFragment.newInstance(btnWidth,btnHeight,str_arr,id_arr,prices,img_arr,color_arr,pic,selected,no);
			}
			//set button for each page
			final RadioButton dotButton = new RadioButton(v.getContext());
			dotButton.setId(i);
			dotButton.setLayoutParams(new RadioGroup.LayoutParams(
					RadioGroup.LayoutParams.WRAP_CONTENT,
					RadioGroup.LayoutParams.WRAP_CONTENT));
			dotButton.setPadding(20, 20, 20, 20);
//			dotButton.setBackgroundResource(R.drawable.dot_bg);//background of each page
			dotButton.setButtonDrawable(R.drawable.dot_bg);
			
			dotButton.setTag(i);//set tag for each page's listener
			
			dotButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						viewPager.setCurrentItem((Integer)dotButton.getTag(),true);
					}
				}
			});
			Log.d(TAG, "dotGroupButton = "+ dotGroupButton);
			dotGroupButton.addView(dotButton);
			dotGroupButton.check(0);
			fragmentsList.add(mFragment);
		}
		
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {				

			@Override
			public void onPageSelected(int position) {
				((RadioButton)dotGroupButton.getChildAt(position)).setChecked(true);
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		// submit and cancel button 

        submit = (Button) v.findViewById(R.id.submit_select);
        cancel = (Button) v.findViewById(R.id.cancel_select);
        submit.setOnClickListener(new Button.OnClickListener() {
    	    @Override
    	        public void onClick(View v) {
	    	    	if(!mods){
		    	    	ArrayList<Integer> itemIdList = new ArrayList<Integer>();
		    	    	for (Fragment frag:fragmentsList){
		    	    		MyImageButton[] btn_list = ((TestDialogFragment) frag).button;
		    	    		for (MyImageButton btn:btn_list){
		    	    			Log.d("All_Id", btn.getItem_id()+"");
		    	    			if (btn.selected)
		    	    				itemIdList.add(btn.getItem_id());
		    	    		}    	    		
		    	    	}
		
		    	    	mListener.onMySubmitButtonClick(itemIdList);
		    	    	dismiss();
	    	    	}else{
	    	    		onModsSubmitted mod = (onModsSubmitted) getActivity();
	    	    		ArrayList<Integer> itemIdList = new ArrayList<Integer>();
	    	    		ArrayList<Integer> noList = new ArrayList<Integer>();
	    	    		ArrayList<Integer> remove = new ArrayList<Integer>();
		    	    	for (Fragment frag:fragmentsList){
		    	    		MyImageButton[] btn_list = ((SelectModsFragment) frag).button;
		    	    		for (MyImageButton btn:btn_list){
		    	    			if (btn.selected && btn.isYes())
		    	    				itemIdList.add(btn.getItem_id());
		    	    			if(btn.selected && !btn.isYes())
		    	    				noList.add(btn.getItem_id());
		    	    			if(!btn.selected && btn.isEdit()){
		    	    				remove.add(btn.getItem_id());
		    	    			}
		    	    		}    	    		
		    	    	}
	    	    		mod.onMyModButtonClick(itemIdList,noList, remove,row,selected,no,edit);
	    	    		dismiss();
	    	    	}
    	       }

    	     });
        cancel.setOnClickListener(new Button.OnClickListener() {
    	    @Override
    	        public void onClick(View v) {
	    	    	if(!mods)
	    	    		dismiss();
	    	    	else{
	    	    		dismiss();
	    	    		
	    	    	}
    	       }

    	     });
	}
	

	    @Override
	    public void onViewCreated(View view, Bundle savedInstanceState) {
	        super.onViewCreated(view, savedInstanceState);

	        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.testViewPager);
	        adapter = new MyFragmentPagerAdapter(getChildFragmentManager(),fragmentsList);
	        mViewPager.setAdapter(adapter);
	    }
	    
	    public void updateFragView(String[] item_name){
	    	this.item_name = item_name;
	    }
	    
	    public int convertToPx(int input) {
            // Get the screen's density scale
            final float scale = getResources().getDisplayMetrics().density;
            // Convert the dps to pixels, based on density scale
            return (int) (input * scale + 0.5f);
    }
	@Override
    public void onResume()
	    {
	        super.onResume();
	        screenWidth  = convertToPx(500);
	        screenHeight  = convertToPx(500);
	        Window window = getDialog().getWindow();
	        window.setLayout(screenWidth, screenHeight);
	        window.setGravity(Gravity.CENTER);
	        //TODO:
	    } 
	
	public interface OnMySubmitButtonClickListener{
		public void onMySubmitButtonClick(ArrayList<Integer> mylist);		
	}
	
	public interface onModsSubmitted{
		public void onMyModButtonClick(ArrayList<Integer> mylist, ArrayList<Integer> nolist, ArrayList<Integer> remove, int row, int selected[], int no[],boolean edit);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
		mListener = (OnMySubmitButtonClickListener) activity;
		} catch(ClassCastException e){
			throw new ClassCastException(activity.toString() 
					+ "must implement OnMyButtonClickListener();");
		}
	}
}


