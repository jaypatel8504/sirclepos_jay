package com.intlpos.sirclepos;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HelpActivity extends Activity{
	private WebView myWebView;
	String pdf = "https://docs.google.com/viewer?url=";
	String save;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		/*super.onCreate(savedInstanceState);
		/*WebView webView = new WebView(this);
		  webView.setClickable(true);
		  webView.setFocusableInTouchMode(true);
		  webView.getSettings().setJavaScriptEnabled(true);
		  webView.loadUrl("http://smart360pos.com/sirclereg/downloads.aspx");
		  WebClientClass webViewClient = new WebClientClass();
		  webView.setWebViewClient(webViewClient);
		  WebChromeClient webChromeClient=new WebChromeClient();
		  webView.setWebChromeClient(webChromeClient);
		  setContentView(webView);*/
		/*setContentView(R.layout.backoffice);
		myWebView = (WebView) findViewById(R.id.back);
		myWebView.setWebViewClient(new WebViewClient());
		
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.loadUrl("http://smart360pos.com/sirclereg/downloads.aspx");
		myWebView.setWebViewClient(new HelloWebViewClient());
	}
	private class HelloWebViewClient extends WebViewClient {
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView webview, String url)
	    {
	    	Log.d("THEURLDUMMY", url);
	    
	    	if(url.endsWith("pdf")){
	    		webview.loadUrl("https://docs.google.com/viewer?url="+url);
	    	}
	    	else{
	    		webview.loadUrl(url);
	    	}
	        return true;
	    }
	}
*/
		
		 
		  super.onCreate(savedInstanceState);
		  WebView webView = new WebView(this);
		  webView.setClickable(true);
		  webView.setFocusableInTouchMode(true);
		  webView.getSettings().setJavaScriptEnabled(true);
		  webView.loadUrl("http://smart360pos.com/sirclereg/downloads.aspx");
		  WebClientClass webViewClient = new WebClientClass();
		  webView.setWebViewClient(webViewClient);
		  
		  setContentView(webView);
		 }

		 public class WebClientClass extends WebViewClient {
		  ProgressDialog pd = null;

		  @Override
		  public void onPageStarted(WebView view, String url, Bitmap favicon) {
		   super.onPageStarted(view, url, favicon);
		   pd = new ProgressDialog(HelpActivity.this);
		   pd.setTitle("Please wait");
		   pd.setMessage("Page is loading..");
		   pd.show();
		  }

		  @Override
		  public void onPageFinished(WebView view, String url) {
		   super.onPageFinished(view, url);
		   pd.dismiss();
		  }
		  
		  @Override
		    public boolean shouldOverrideUrlLoading(WebView webview, String url)
		    {	
			  	if(url.endsWith(".pdf")){
				  	Intent intent = new Intent(HelpActivity.this, PdfViewer.class);
				  	intent.putExtra("result", url);
					startActivity(intent);
			    }
			  	else{
			  		webview.loadUrl(url);
			  	}
		    	
		        return true;
		    }
		  	
		 }
		 
	
		 		
	}
