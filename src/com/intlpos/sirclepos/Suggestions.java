package com.intlpos.sirclepos;

import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.intlpos.sirclepos.R;
import com.intlpos.initsetup.PostRequestTask;
import com.john.email.SendMailTLSTask;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Suggestions extends DialogFragment{
	private EditText suggest;
	private Button send;
	protected static final String GET_EMAIL_URL = "Settings/SettingsService.svc/getemailsettings";
	private CornerStorePOS app;
	public static Suggestions newInstance(){
		Suggestions sg = new Suggestions();
		return sg;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.contactus, container, false);
		setCancelable(true);
		app = (CornerStorePOS) getActivity().getApplication();
		getDialog().setTitle(R.string.contact);
		suggest = (EditText) v.findViewById(R.id.suggestion);
		send = (Button) v.findViewById(R.id.sendSuggest);
		
		send.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!suggest.getText().toString().isEmpty()){
					Gson gson = new Gson();
					GetEmailRequest ger = new GetEmailRequest(app.storedetail);
					String request = gson.toJson(ger,GetEmailRequest.class);
//					Log.d("Request", request);
					PostRequestTask postTask = new PostRequestTask(); 
					postTask.execute(CornerStorePOS.Url+GET_EMAIL_URL,request);
					String response = null;
					try {
						response = postTask.get();
					} catch (InterruptedException | ExecutionException e) {
						
						e.printStackTrace();
					}
					JSONObject json = null;
					try {
						 json = new JSONObject(response);
//						 Log.d("Response", response);
						if(!json.getString("username").equals("test@smart360pos.com")){
							 SendMailTLSTask sendMailTask = new SendMailTLSTask(); 
						     sendMailTask.execute(json.getString("username"),json.getString("password"),"tech@internationalpointofsale.com","SUGGESTION",json.getString("smtp_server"),json.getString("smtp_port"),suggest.getText().toString());
					     }else{
					    	 Toast.makeText(getActivity(), R.string.setupEmail, Toast.LENGTH_LONG).show();
					     }
						
					} catch (JSONException e) {			
						e.printStackTrace();
					}  	
					dismiss();
				}else{
					Toast.makeText(getActivity(), R.string.pleaseEnter, Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		return v;
	}
	
	
}
