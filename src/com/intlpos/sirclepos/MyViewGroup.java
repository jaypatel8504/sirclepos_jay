package com.intlpos.sirclepos;



import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("Instantiatable")
public class MyViewGroup extends ViewGroup {
    private final static String TAG = "123";
    
    private final static int VIEW_MARGIN=2;
    
    private boolean isFull = false;

    public boolean isFull() {
		return isFull;
	}

	public void setFull(boolean isFull) {
		this.isFull = isFull;
	}

	public MyViewGroup(Context context, AttributeSet attrs){
        super(context, attrs);
    }
    
    public MyViewGroup(Context context) {
        super(context);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        Log.d(TAG, "widthMeasureSpec = "+widthMeasureSpec+" heightMeasureSpec = "+heightMeasureSpec);
        
        for (int index = 0; index < getChildCount(); index++) {
            final View child = getChildAt(index);
            // measure
            child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
    private int margin = 10;//margin between buttons
    @Override
    protected void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
//        Log.d(TAG, "changed = "+arg0+" left = "+arg1+" top = "+arg2+" right = "+arg3+" botom = "+arg4);
        final int count = getChildCount();
        int row=0;// which row lay you view relative to parent
        int lengthX=arg1 ;    // right position of child relative to parent
        int lengthY=arg2;    // bottom position of child relative to parent
        for(int i=0;i<count;i++){
            
            final View child = this.getChildAt(i);
            int width = child.getMeasuredWidth();
            int height = child.getMeasuredHeight();
            lengthX+=width+VIEW_MARGIN+margin;//margin between buttons
            
            lengthY=row*(height+VIEW_MARGIN)+VIEW_MARGIN+height+arg2;
            //if it can't drawing on a same line , skip to next line
            if(lengthX>arg3){
                lengthX=width+VIEW_MARGIN+margin+arg1;
                row++;
                lengthY=row*(height+VIEW_MARGIN)+VIEW_MARGIN+height+arg2;
                if (lengthY > arg4){
                	break;
                }
                	
            }
            child.layout(lengthX-width, lengthY-height, lengthX, lengthY);
        }
    }
}