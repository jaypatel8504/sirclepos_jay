package com.intlpos.sirclepos;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

import android.R.string;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.velocity.VelocityPaymentTransaction;
import com.android.velocity.VelocityProcessor;
import com.google.zxing.client.android.AmbientLightManager;
import com.intlpos.sirclepos.R;
import com.intlpos.database.PaymentAdapter;
import com.intlpos.database.PaymentType;
import com.intlpos.dialogs.Accounts;
import com.intlpos.dialogs.SelectAccountFirst;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.paymentprocessing.PaymentListener;
import com.intlpos.paymentprocessing.ReaderListener;
import com.intlpos.paymentprocessing.ReaderService;
import com.intlpos.paymentprocessing.VelocityEMV;
import com.intlpos.paymentprocessing.chasepaymentech;
import com.intlpos.paymentprocessing.usaepay;
import com.intlpos.paymentprocessing.velocity;
import com.intlpos.sirclepos.PaymentFragment.OnPaymentFinishedListener;
import com.sun.mail.iap.ByteArray;
import com.usaepay.sdk.Gateway;
import com.usaepay.sdk.classes.OnGatewayResultListener;
import com.usaepay.sdk.classes.TransactionResponse;
import com.velocity.verify.response.VelocityResponse;

public class PaymentFragmentListener implements OnClickListener {
	private static String[] PAYMENT_TYPE = { "Cash", "Credit", "Debit",
			"Gift Card", "EBT", "To Account", "Check" };
	private static String EMPTY = "0.00";
	private static BigDecimal FIVE = new BigDecimal(5),
			TEN = new BigDecimal(10), TWENTY = new BigDecimal(20),
			FIFTY = new BigDecimal(50);
	private Context context;
	private BigDecimal balance;
	private EditText balance_edittext;
	private ArrayList<PaymentType> payments;
	private OnPaymentFinishedListener mListener;
	private PaymentAdapter mAdapter;
	private Button roundup;
	private TextView balanceRemain;
	private TextView amountRemaining;
	private KeyPadFragment keypadFrag;
	private boolean flag_change;
	private boolean expresscheckout;
	private String paymentprocessor;
	private SharedPreferences preference;
	private String nickel;
	private PaymentFragment paymentFragment;
	private int custId;
	private boolean open = false;
	String custName;
	BigDecimal bal = BigDecimal.ZERO;
	PaymentType pt;
	ProgressDialog progressDialog;

	private VelocityProcessor velocityProcessor = null;
	private VelocityPaymentTransaction velocityPaymentTransaction = null;
	private VelocityResponse velocityResponse;
	

	public PaymentFragmentListener(Context context, BigDecimal balance,
			KeyPadFragment keypadFrag, PaymentFragment paymentFragment,
			OnPaymentFinishedListener mListener,
			ArrayList<PaymentType> payments, PaymentAdapter mAdapter,
			int custId, String custName) {
		super();
		this.context = context;
		this.keypadFrag = keypadFrag;
		this.balance_edittext = keypadFrag.getEdittext();
		this.balance = balance;
		this.mListener = mListener;
		this.payments = payments;
		this.mAdapter = mAdapter;
		this.roundup = paymentFragment.roundup;
		this.balanceRemain = paymentFragment.balanceRemaine;
		this.amountRemaining = paymentFragment.amount;
		this.flag_change = balance.compareTo(BigDecimal.ZERO) == -1 ? true
				: false;
		this.expresscheckout = CSSharedPreferences.getExpressCheckout()
				.getExpress();
		this.paymentprocessor = CSSharedPreferences.getPayment().getGateway();
		this.paymentFragment = paymentFragment;
		this.custId = custId;
		this.custName = custName;
		Log.d("paymentprocessor", paymentprocessor);
		// Log.d();
	}

	public PaymentFragmentListener(BigDecimal balance) {
		this.balance = balance;
	}

	private static String hexToASCII(String hexValue) {
		StringBuilder output = new StringBuilder("");
		for (int i = 0; i < hexValue.length(); i += 2) {
			String str = hexValue.substring(i, i + 2);
			output.append((char) Integer.parseInt(str, 16));
		}
		return output.toString();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		payments = mAdapter.getData();
		preference = PreferenceManager.getDefaultSharedPreferences(context
				.getApplicationContext());
		nickel = preference.getString("nickel", "");
		switch (v.getId()) // Start Switch Case for Payment Types
		{
		case R.id.fivedollar:
			// mAdapter.addToRow(0, FIVE, flag_change);
			BigDecimal tempBal0 = balance;
			if (balance.compareTo(BigDecimal.ZERO) < 0) {
				Log.d("NEGATE", FIVE.negate().toString());
				pt = new PaymentType(PAYMENT_TYPE[0], FIVE.negate(), "", "0",
						"");
				payments.add(pt);
			} else {
				pt = new PaymentType(PAYMENT_TYPE[0], FIVE, "", "0", "");
				payments.add(pt);
			}
			balance = flag_change ? balance.add(FIVE) : balance.subtract(FIVE);
			if (balance.compareTo(BigDecimal.ZERO) <= 0) {

				if (nickel.equals("1"))
					addRoundingtoGrid(tempBal0, FIVE);
			}
			balance_edittext.setText(getBalanceText(balance));
			break;

		case R.id.tendollar:
			BigDecimal tempBal1 = balance;
			if (balance.compareTo(BigDecimal.ZERO) < 0) {
				pt = new PaymentType(PAYMENT_TYPE[0], TEN.negate(), "", "0", "");
				payments.add(pt);
			} else {
				pt = new PaymentType(PAYMENT_TYPE[0], TEN, "", "0", "");
				payments.add(pt);
			}
			balance = flag_change ? balance.add(TEN) : balance.subtract(TEN);
			if (balance.compareTo(BigDecimal.ZERO) <= 0) {
				if (nickel.equals("1"))
					addRoundingtoGrid(tempBal1, TEN);
			}
			balance_edittext.setText(getBalanceText(balance));

			break;
		case R.id.twentydollar:
			BigDecimal tempBal2 = balance;
			if (balance.compareTo(BigDecimal.ZERO) < 0) {
				pt = new PaymentType(PAYMENT_TYPE[0], TWENTY.negate(), "", "0",
						"");
				payments.add(pt);
			} else {
				pt = new PaymentType(PAYMENT_TYPE[0], TWENTY, "", "0", "");
				payments.add(pt);
			}
			balance = flag_change ? balance.add(TWENTY) : balance
					.subtract(TWENTY);
			if (balance.compareTo(BigDecimal.ZERO) <= 0) {
				if (nickel.equals("1"))
					addRoundingtoGrid(tempBal2, TWENTY);
			}
			balance_edittext.setText(getBalanceText(balance));
			break;
		case R.id.fiftydollar:
			BigDecimal tempBal3 = balance;
			if (balance.compareTo(BigDecimal.ZERO) < 0) {
				pt = new PaymentType(PAYMENT_TYPE[0], FIFTY.negate(), "", "0",
						"");
				payments.add(pt);
			} else {
				pt = new PaymentType(PAYMENT_TYPE[0], FIFTY, "", "0", "");
				payments.add(pt);
			}
			balance = flag_change ? balance.add(FIFTY) : balance
					.subtract(FIFTY);
			if (balance.compareTo(BigDecimal.ZERO) <= 0) {
				if (nickel.equals("1"))
					addRoundingtoGrid(tempBal3, FIFTY);
			}
			balance_edittext.setText(getBalanceText(balance));
			break;
		case R.id.roundup:
			BigDecimal tempBal4 = balance;
			bal = balance.setScale(0, BigDecimal.ROUND_UP);
			pt = new PaymentType(PAYMENT_TYPE[0], bal, "", "0", "");
			payments.add(pt);
			balance = flag_change ? balance.add(bal.abs()) : balance
					.subtract(bal.abs());
			if (balance.compareTo(BigDecimal.ZERO) <= 0) {
				if (nickel.equals("1"))
					addRoundingtoGrid(tempBal4, bal);
			}
			// mAdapter.addToRow(0, bal, flag_change);
			break;
		case R.id.cash:
			// Start Cash PaymentType
			preference = PreferenceManager.getDefaultSharedPreferences(context
					.getApplicationContext());
			nickel = preference.getString("nickel", "");
			bal = getEdittextValue(balance_edittext);
			if (nickel.equals("1")) {
				if (balance.compareTo(BigDecimal.ZERO) > 0
						&& bal.compareTo(BigDecimal.ZERO) < 0) {
					toast_message(context.getResources().getString(
							R.string.enterPos));
					return;
				}
				if (balance.compareTo(BigDecimal.ZERO) < 0
						&& bal.compareTo(BigDecimal.ZERO) < 0) {
					bal = checkNegative(bal);
				}
				String bd = bal.toString();
				if (!bd.equals("0.00")) {
					BigDecimal tempBal = balance;
					String trunc = bd.substring(bd.length() - 1);
					if (trunc.equals("0") || trunc.equals("5")) {
						BigDecimal temp = balance;
						balance = nearestnickel(balance);
						if (balance.compareTo(BigDecimal.ZERO) < 0
								&& bal.compareTo(BigDecimal.ZERO) < 0) {
							bal = checkNegative(bal);
						}
						if (bal.compareTo(BigDecimal.ZERO) > 0
								&& balance.compareTo(BigDecimal.ZERO) < 0) {
							pt = new PaymentType(PAYMENT_TYPE[0], bal.negate(),
									"", "0", "");
							payments.add(pt);
						} else {
							pt = new PaymentType(PAYMENT_TYPE[0], bal, "", "0",
									"");
							payments.add(pt);
						}

						balance = flag_change ? balance.add(bal) : balance
								.subtract(bal);
						Log.d("balNick", balance.toString());
						if (balance.compareTo(BigDecimal.ZERO) <= 0) {
							addRoundingtoGrid(tempBal);
						}
					} else {
						Toast.makeText(context,
								"Cents need to end with a Zero or Five",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(
							context,
							"You cannot ring up Zero dollar transactions when you have the Round Nickel option selected",
							Toast.LENGTH_LONG).show();
					return;
				}
			} else {
				if (balance.compareTo(BigDecimal.ZERO) > 0
						&& bal.compareTo(BigDecimal.ZERO) < 0) {
					toast_message(context.getResources().getString(
							R.string.enterPos));
					return;
				}
				if (balance.compareTo(BigDecimal.ZERO) < 0
						&& bal.compareTo(BigDecimal.ZERO) < 0) {
					bal = checkNegative(bal);
				}
				if (bal.compareTo(BigDecimal.ZERO) > 0
						&& balance.compareTo(BigDecimal.ZERO) < 0) {
					pt = new PaymentType(PAYMENT_TYPE[0], bal.negate(), "",
							"0", "");
					payments.add(pt);
				} else {
					pt = new PaymentType(PAYMENT_TYPE[0], bal, "", "0", "");
					payments.add(pt);
				}
				balance = flag_change ? balance.add(bal) : balance
						.subtract(bal);

			}

			// End Cash PaymentType
			break;
		default:
			bal = getEdittextValue(balance_edittext);
			if ((!flag_change && balance.subtract(bal).compareTo(
					BigDecimal.ZERO) == -1)
					|| (flag_change && balance.subtract(bal).compareTo(
							BigDecimal.ZERO) == 1)) {
				Toast.makeText(context, R.string.cantProcess, Toast.LENGTH_LONG)
						.show();
			} else {
				Log.d("CSPOS-payment", paymentprocessor);

				switch (v.getId()) {
				case R.id.credit:
					if (expresscheckout != true) // when express checkout is not
													// true
					{
						// start when bal is negative
						if (balance_edittext.getText().toString().charAt(0) == '('
								|| getEdittextValue(balance_edittext)
										.compareTo(BigDecimal.ZERO) < 0) {
							switch (paymentprocessor) {
							case "velocity":
								progressDialog = new ProgressDialog(context);
								velocity velocity = new velocity(context,
										progressDialog, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												progressDialog.dismiss();
												Log.d("velocity result - ",
														result[0] + ""
																+ result[1]);
												if (result[0] != null) {
													if (result[0].equals("AP")) {
														update_balance(
																bal,
																PAYMENT_TYPE[1],
																result[1], "",
																"0");
													} else if (result[0]
															.equals("PARTIAL AP")) {
														BigDecimal Approved_Amount = new BigDecimal(
																result[2]);
														update_balance(
																Approved_Amount,
																PAYMENT_TYPE[1],
																result[1], "",
																"0");
													} else {// toast_message(response.getResult());
														alert_message(result[0]);
													}
												} else {
													alert_message("Transaction Falied");
												}

											}
										});
								velocity.runTransaction("P2PE ReturnUnlinked",
										bal, null);
								break;
							case "chasepaymentech":
								chasepaymentech chase = new chasepaymentech(
										context, new PaymentListener() {
											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":
													extract_response(result[1]);
													break;
												case "Partially approved":
													extract_response(result[1]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								chase.execute("return", "credit", bal);
								break;
								
							case "velocityemv":
								VelocityEMV velocityemv = new VelocityEMV(
										context, new PaymentListener() {
											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":												
													update_balance(bal,PAYMENT_TYPE[1],result[1], "",result[3]);
													break;
												case "Partially approved":
													BigDecimal Approved_Amount = new BigDecimal(result[2]);
													update_balance(Approved_Amount,PAYMENT_TYPE[1],result[1], "",result[3]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								velocityemv.execute("CCR9", "credit", bal.toString());
								break;
								
								
							default:
								Toast.makeText(context, R.string.negval,
										Toast.LENGTH_SHORT).show();
								return;
							}

						} // //end when bal is negative
						else // start when bal is posiitive
						{
							switch (paymentprocessor) {
							case "usaepay":
								progressDialog = new ProgressDialog(context);
								OnGatewayResultListener gateResultListner = new OnGatewayResultListener() {

									@Override
									public void onGatewayResult(
											int requestCode, int resultCode,
											Bundle data) {
										Bundle b = new Bundle();

										if (resultCode == Gateway.RESULT_OK) {
											b.putParcelable(
													"TRANSACTION_RESPONSE",
													data.getParcelable(Gateway.EXTRA_RESULT));
											final TransactionResponse response = b
													.getParcelable("TRANSACTION_RESPONSE");

											if (response.getResult()
													.equalsIgnoreCase(
															"Approved")) {
												update_balance(bal,
														PAYMENT_TYPE[1],
														response.getAuthCode(),
														String.valueOf(response
																.getRefNum()),
														"0");
											} else {
												// toast_message(response.getResult());
												alert_message(response
														.getResult());
											}

										} else { // resultCode ==
													// Gateway.RESULT_ERROR
											b.putParcelable(
													"GATEWAY_EXCEPTION",
													data.getParcelable(Gateway.EXTRA_EXCEPTION));
											// Log.d("CSPOS-EXTRA_EXCEPTION",data.getParcelable(Gateway.EXTRA_EXCEPTION).toString());
											// toast_message(Gateway.EXTRA_EXCEPTION.toString());
											alert_message(Gateway.EXTRA_EXCEPTION
													.toString());
										}
										// closeDevice();
										progressDialog.dismiss();
									}
								};
								usaepay usaepay = new usaepay(context,
										progressDialog, gateResultListner);
								usaepay.runTransaction("sale", bal);

								break;
							case "chasepaymentech":
								Log.d("paymentprocessor", paymentprocessor);
								chasepaymentech chase = new chasepaymentech(
										context, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":
													extract_response(result[1]);
													break;
												case "Partially approved":
													extract_response(result[1]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								chase.execute("purchase", "credit", bal);
								break;
							case "velocity":
								progressDialog = new ProgressDialog(context);
								velocity velocity = new velocity(context,
										progressDialog, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												progressDialog.dismiss();
												Log.d("velocity result",
														result[0] + ""
																+ result[1]);
												if (result[0] != null) {
													if (result[0].equals("AP")) {
														update_balance(
																bal,
																PAYMENT_TYPE[1],
																result[1], "",
																"0");

													} else if (result[0]
															.equals("PARTIAL AP")) {
														BigDecimal Approved_Amount = new BigDecimal(
																result[2]);
														update_balance(
																Approved_Amount,
																PAYMENT_TYPE[1],
																result[1], "",
																"0");
													} else {// toast_message(response.getResult());
														alert_message(result[0]);
													}
												} else {
													alert_message("Transaction Falied");
												}
											}
										});
								velocity.runTransaction(
										"P2PE AuthorizeAndCapture", bal, null);
								// velocity.runTransaction("Undo",bal);
								break;
							case "velocityemv":
								Log.d("paymentprocessor", paymentprocessor);
								VelocityEMV velocityemv = new VelocityEMV(
										context, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":
													update_balance(bal,PAYMENT_TYPE[1],result[1], "",result[3]);
													break;
												case "Partially approved":
													BigDecimal Approved_Amount = new BigDecimal(result[2]);
													update_balance(Approved_Amount,PAYMENT_TYPE[1],result[1], "",result[3]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								velocityemv.execute("CCR1", "credit", bal.toString());
								break;
							default:
								pt = new PaymentType(PAYMENT_TYPE[1], bal, "",
										"0", "");
								payments.add(pt);
								balance = flag_change ? balance.add(bal)
										: balance.subtract(bal);
								break;
							}
						}// end when bal is posiitive

					} else // when expresscheckout is true
					{
						if (balance.compareTo(BigDecimal.ZERO) > 0
								&& bal.compareTo(BigDecimal.ZERO) < 0) {
							toast_message(context.getResources().getString(
									R.string.enterPos));
							return;
						}
						BigDecimal bal1 = getEdittextValue(balance_edittext);
						if (balance.compareTo(BigDecimal.ZERO) < 0
								&& bal1.compareTo(BigDecimal.ZERO) < 0) {
							bal1 = checkNegative(bal1);
						}
						if (bal1.compareTo(BigDecimal.ZERO) > 0
								&& balance.compareTo(BigDecimal.ZERO) < 0) {
							pt = new PaymentType(PAYMENT_TYPE[1],
									bal1.negate(), "", "0", "");
							payments.add(pt);
						} else {
							pt = new PaymentType(PAYMENT_TYPE[1], bal1, "",
									"0", "");
							payments.add(pt);
						}
						balance = flag_change ? balance.add(bal1) : balance
								.subtract(bal1);

					}
					break;
				case R.id.debit:
					// mAdapter.addToRow(2, bal, flag_change);
					if (expresscheckout != true) {
						if (balance_edittext.getText().toString().charAt(0) == '('
								|| getEdittextValue(balance_edittext)
										.compareTo(BigDecimal.ZERO) < 0) {
							switch (paymentprocessor) {

							case "chasepaymentech":
								chasepaymentech chase1 = new chasepaymentech(
										context, new PaymentListener() {
											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":
													extract_response(result[1]);
													break;
												case "Partially approved":
													extract_response(result[1]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								chase1.execute("return", "debit", bal);
								break;
							case "velocityemv":
								VelocityEMV velocityemv = new VelocityEMV(
										context, new PaymentListener() {
											@Override
											public void onResult(String[] result) {
												// TODO Auto-generated method
												// stub
												switch (result[0]) {
												case "Approved":
													update_balance(bal,PAYMENT_TYPE[2],result[1], "",result[3]);
													break;
												case "Partially approved":
													BigDecimal Approved_Amount = new BigDecimal(result[2]);
													update_balance(Approved_Amount,PAYMENT_TYPE[2],result[1], "",result[3]);
													break;
												default:
													alert_message(result[0]);

												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								velocityemv.execute("DB01", "debit", bal.toString());
								break;
							default:
								Toast.makeText(context, R.string.negval,
										Toast.LENGTH_SHORT).show();
								return;
							}
						} else // for positive balance
						{
							if (paymentprocessor.equals("chasepaymentech")) {

								chasepaymentech chase = new chasepaymentech(
										context, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												switch (result[0]) {
												case "Approved":
													extract_response(result[1]);
													break;
												case "Partially approved":
													extract_response(result[1]);
													break;
												default:
													alert_message(result[0]);
												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								chase.execute("purchase", "debit", bal);
							}
							else if (paymentprocessor.equals("velocityemv")) {

								VelocityEMV velocityemv = new VelocityEMV(
										context, new PaymentListener() {

											@Override
											public void onResult(String[] result) {
												switch (result[0]) {
												case "Approved":
													update_balance(bal,PAYMENT_TYPE[2],result[1], "",result[3]);
													break;
												case "Partially approved":
													BigDecimal Approved_Amount = new BigDecimal(result[2]);
													update_balance(Approved_Amount,PAYMENT_TYPE[1],result[1], "",result[3]);
													break;
												default:
													alert_message(result[0]);
												}

											}
										}, context.getResources()
												.getStringArray(
														R.array.chaseResources));
								velocityemv.execute("DB00", "debit", bal.toString());
							}
							else {
								pt = new PaymentType(PAYMENT_TYPE[2], bal, "",
										"0", "");
								payments.add(pt);
								balance = flag_change ? balance.add(bal)
										: balance.subtract(bal);
							}
						}
					} else // when express checkout is not true
					{
						if (balance.compareTo(BigDecimal.ZERO) > 0
								&& bal.compareTo(BigDecimal.ZERO) < 0) {
							toast_message(context.getResources().getString(
									R.string.enterPos));
							return;
						}
						BigDecimal bal1 = getEdittextValue(balance_edittext);
						if (balance.compareTo(BigDecimal.ZERO) < 0
								&& bal1.compareTo(BigDecimal.ZERO) < 0) {
							bal1 = checkNegative(bal1);
						}
						if (bal1.compareTo(BigDecimal.ZERO) > 0
								&& balance.compareTo(BigDecimal.ZERO) < 0) {
							pt = new PaymentType(PAYMENT_TYPE[2],
									bal1.negate(), "", "0", "");
							payments.add(pt);
						} else {
							pt = new PaymentType(PAYMENT_TYPE[2], bal1, "",
									"0", "");
							payments.add(pt);
						}
						balance = flag_change ? balance.add(bal1) : balance
								.subtract(bal1);

					}
					break;
				case R.id.giftcard:
					// mAdapter.addToRow(3, bal, flag_change);
					if (balance.compareTo(BigDecimal.ZERO) > 0
							&& bal.compareTo(BigDecimal.ZERO) < 0) {
						toast_message(context.getResources().getString(
								R.string.enterPos));
						return;
					}
					BigDecimal bal1 = getEdittextValue(balance_edittext);
					if (balance.compareTo(BigDecimal.ZERO) < 0
							&& bal1.compareTo(BigDecimal.ZERO) < 0) {
						bal1 = checkNegative(bal1);
					}

					balance = flag_change ? balance.add(bal1) : balance
							.subtract(bal1);

					if (bal1.compareTo(BigDecimal.ZERO) > 0
							&& balance.compareTo(BigDecimal.ZERO) < 0) {
						bal1 = bal1.negate();
					}

					pt = new PaymentType(PAYMENT_TYPE[3], bal, "", "0", "");
					payments.add(pt);
					break;
				case R.id.ebt:
					if (balance.compareTo(BigDecimal.ZERO) > 0
							&& bal.compareTo(BigDecimal.ZERO) < 0) {
						toast_message(context.getResources().getString(
								R.string.enterPos));
						return;
					} else if (balance
							.compareTo(getEdittextValue(balance_edittext)) != 0) {
						toast_message("Checks must be equal to total balance!");
						return;
					}
					AlertDialog.Builder alert = new AlertDialog.Builder(context);
					alert.setTitle("Enter Check Number");
					// Set an EditText view to get user input
					final EditText input = new EditText(context);

					alert.setView(input);

					alert.setPositiveButton("Submit", null);

					alert.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// Canceled.
									dialog.dismiss();
								}
							});

					final AlertDialog alertDialog = alert.create();
					alertDialog.show();
					Button b = alertDialog
							.getButton(AlertDialog.BUTTON_POSITIVE);
					b.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							if (input.getText().toString().isEmpty()) {
								Toast.makeText(context,
										"Check Number cannot be empty",
										Toast.LENGTH_LONG).show();
							} else if (input.getText().toString().length() > 20) {
								Toast.makeText(
										context,
										"Check Number cannot be greater than 20",
										Toast.LENGTH_LONG).show();
							} else {
								alertDialog.dismiss();
								BigDecimal bal1 = getEdittextValue(balance_edittext);
								if (balance.compareTo(BigDecimal.ZERO) < 0
										&& bal1.compareTo(BigDecimal.ZERO) < 0) {
									bal1 = checkNegative(bal1);
								}

								balance = flag_change ? balance.add(bal1)
										: balance.subtract(bal1);

								if (bal1.compareTo(BigDecimal.ZERO) > 0
										&& balance.compareTo(BigDecimal.ZERO) < 0) {
									bal1 = bal1.negate();
								}

								pt = new PaymentType(PAYMENT_TYPE[6], bal,
										input.getText().toString(), "0", "");
								payments.add(pt);

								if ((flag_change && balance
										.compareTo(BigDecimal.ZERO) != -1)
										|| (!flag_change && balance
												.compareTo(BigDecimal.ZERO) != 1)) {
									payments = mAdapter.getData();
									boolean hasCash = false;
									for (PaymentType p : payments) {
										if (p.type == "Cash"
												&& p.amount
														.compareTo(BigDecimal.ZERO) != 0) {
											hasCash = true;
										}
									}
									if (!hasCash)
										mListener.onPaymentFinished(true,
												balance, false);
									else {
										if (nickel.equals("1")) {
											mListener.onPaymentFinished(true,
													nearestnickel(balance),
													true);
										} else
											mListener.onPaymentFinished(true,
													balance, true);
									}

								}
								return;
							}

						}
					});
					break;
				case R.id.toaccount:
					if (custId == 1 || custName.equals("Guest Customer")) {
						((Activity) context).runOnUiThread(new Runnable() {
							@Override
							public void run() {
								DialogFragment newFragment = SelectAccountFirst
										.newInstance();
								newFragment.show(
										paymentFragment.getFragmentManager(),
										"SelectAccountFirst");
							}
						});

						return;
					}
					if (balance.compareTo(BigDecimal.ZERO) > 0
							&& bal.compareTo(BigDecimal.ZERO) < 0) {
						toast_message(context.getResources().getString(
								R.string.enterPos));
						return;
					}
					// mAdapter.addToRow(5, bal, flag_change);
					bal1 = getEdittextValue(balance_edittext);
					if (balance.compareTo(BigDecimal.ZERO) < 0
							&& bal1.compareTo(BigDecimal.ZERO) < 0) {
						bal1 = checkNegative(bal1);
					}

					balance = flag_change ? balance.add(bal) : balance
							.subtract(bal);
					if (bal1.compareTo(BigDecimal.ZERO) > 0
							&& balance.compareTo(BigDecimal.ZERO) < 0) {
						bal1 = bal1.negate();
					}

					pt = new PaymentType(PAYMENT_TYPE[5], bal, "", "0", "");
					payments.add(pt);
					break;
				}

			}
			break;

		} // End Switch Case for Payment Types

		preference = PreferenceManager.getDefaultSharedPreferences(context
				.getApplicationContext());
		nickel = preference.getString("nickel", "");

		if ((flag_change && balance.compareTo(BigDecimal.ZERO) != -1)
				|| (!flag_change && balance.compareTo(BigDecimal.ZERO) != 1)) {
			payments = mAdapter.getData();
			boolean hasCash = false;
			for (PaymentType p : payments) {
				if (p.type == "Cash"
						&& p.amount.compareTo(BigDecimal.ZERO) != 0) {
					hasCash = true;
				}
			}
			if (!hasCash)
				mListener.onPaymentFinished(true, balance, false);
			else {
				if (nickel.equals("1")) {
					mListener.onPaymentFinished(true, nearestnickel(balance),
							true);
				} else
					mListener.onPaymentFinished(true, balance, true);
			}

		}
		roundup.setText(getBalanceText(balance.setScale(0, BigDecimal.ROUND_UP)));
		keypadFrag.str = "0.00";
		balance_edittext.setText(getBalanceText(balance));

		if (nickel.equals("1")) {
			BigDecimal result = nearestnickel(balance);

			amountRemaining.setText("Amount Remaining/Cash Remaining:");
			balanceRemain.setText("      " + getBalanceText(balance) + "/"
					+ getBalanceText(result));
		} else {
			balanceRemain.setText(getBalanceText(balance));
		}
		mAdapter.setData(payments);
		mAdapter.notifyDataSetChanged();
		mAdapter.scrollListViewToBottom();
	}

	public BigDecimal nearestnickel(BigDecimal amount) {

		// toast_message("beginning"+amount.toString());
		BigDecimal newamount = new BigDecimal(0);
		if (amount.compareTo(BigDecimal.ZERO) == -1) {
			newamount = amount.multiply(new BigDecimal(-1));
			// toast_message("in"+newamount.toString());
		} else {
			newamount = amount;
		}
		BigDecimal p = newamount.multiply(new BigDecimal(100))
				.remainder(new BigDecimal(100)).remainder(new BigDecimal(5));
		BigDecimal q = new BigDecimal(5).subtract(p);
		BigDecimal result = new BigDecimal(0);
		if (q.compareTo(p) == 1) {
			result = newamount.subtract(p.divide(new BigDecimal(100)));
		} else {
			result = newamount.add(q.divide(new BigDecimal(100)));
		}

		if (amount.compareTo(BigDecimal.ZERO) == -1) {
			result = result.multiply(new BigDecimal(-1));
		}

		// toast_message(result.toString());
		return result;
		// toast_message(result.toString());

	}

	protected void extract_response(String response) {
		int lastIndex = response.lastIndexOf("2a"); // ****
		String last_four = response.substring(lastIndex + 2, lastIndex + 10);
		// Log.d("CSPOS-RR","lastfour"+hexToASCII(last_four));
		lastIndex = response.indexOf("1c343030"); // FS 400
		String Auth_code_string = response.substring(lastIndex + 8,
				response.length());
		String Auth_code = Auth_code_string.substring(0,
				Auth_code_string.indexOf("1c"));
		// Log.d("CSPOS-RRAuth_code",hexToASCII(Auth_code));
		lastIndex = response.indexOf("1c313039"); // Tag 109
		String Approved_amount_first = response.substring(lastIndex + 8,
				response.length());
		String Approved_amount = Approved_amount_first.substring(0,
				Approved_amount_first.indexOf("1c"));
		// Log.d("CSPOS-ApprovedAmount",Approved_amount);
		new BigDecimal(hexToASCII(Approved_amount)).divide(new BigDecimal(100));
		// Log.d("CSPOS-Amount",""+new
		// BigDecimal(hexToASCII(Approved_amount)).divide(new BigDecimal(100)));

		/*
		 * Customer Card Type 00 Debit 01 Visa 02 MasterCard 03 Amex 04 Diners
		 * Club 05 Discover Card 06 JCB 07 Union Pay Card 08 Other Credit Card
		 * 09 Gift Card 10 Cash 11 EBT Food Stamp 12 EBT Cash Benefit
		 */
		lastIndex = response.indexOf("1c333030");// Tag 300
		String CardType = hexToASCII(response.substring(lastIndex + 8,
				lastIndex + 12));

		String PaymentType = "Credit";
		if (CardType.equals("00")) {
			PaymentType = "Debit";

		}

		update_balance(
				new BigDecimal(hexToASCII(Approved_amount)).divide(new BigDecimal(
						100)), PaymentType, hexToASCII(Auth_code), "0",
				hexToASCII(last_four));

	}

	protected void alert_message(final String message) {
		try {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(message)
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.dismiss();
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		} catch (RuntimeException e) {
			((Activity) context).runOnUiThread(new Runnable() {
				public void run() {
					// Toast.makeText(context, R.string.failed,
					// Toast.LENGTH_LONG).show();
					AlertDialog.Builder builder = new AlertDialog.Builder(
							context);
					builder.setMessage(R.string.failed)
							.setCancelable(false)
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.dismiss();
										}
									});
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
		}
	}

	protected void toast_message(final String message) {
		// TODO Auto-generated method stub
		((Activity) context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		});
	}

	protected void update_balance(final BigDecimal bal,
			final String payment_type, final String Auth_code,
			final String Ref_no, final String last_four) {
		// TODO Auto-generated method stub
		((Activity) context).runOnUiThread(new Runnable() {
			public void run() {
				Toast.makeText(context, R.string.trans, Toast.LENGTH_LONG)
						.show();

				balance = flag_change ? balance.add(bal) : balance
						.subtract(bal);
				Log.d("CSPOS:Lastfour", last_four);
				PaymentType pt = new PaymentType(payment_type, bal, Auth_code,
						last_four, Ref_no);
				payments.add(pt);
				if ((flag_change && balance.compareTo(BigDecimal.ZERO) != -1)
						|| (!flag_change && balance.compareTo(BigDecimal.ZERO) != 1)) {
					payments = mAdapter.getData();
					if (payments.size() == 0
							|| payments.get(0).amount.equals(BigDecimal.ZERO))
						mListener.onPaymentFinished(true, balance, false);
					else
						mListener.onPaymentFinished(true, balance, true);
				}
				roundup.setText(getBalanceText(balance.setScale(0,
						BigDecimal.ROUND_UP)));
				keypadFrag.str = "0.00";
				balance_edittext.setText(getBalanceText(balance));
				balanceRemain.setText(getBalanceText(balance));
				mAdapter.setData(payments);
				mAdapter.notifyDataSetChanged();
				mAdapter.scrollListViewToBottom();
				// Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show();
				// pd.dismiss();
			}
		});
	}

	private BigDecimal getEdittextValue(EditText edittext) {
		// TODO Auto-generated method stub
		String str = edittext.getText().toString();
		if (str.charAt(0) == '(') {
			str = str.substring(1, str.length() - 1);
		}
		return new BigDecimal(str);
	}

	public static String getBalanceText(BigDecimal num) {
		// TODO Auto-generated method stub
		if (num.compareTo(BigDecimal.ZERO) != -1) {
			return num.toString();
		} else {
			return "(" + num.negate().toString() + ")";
		}
	}

	public BigDecimal checkNegative(BigDecimal balEditText) {
		if (balEditText.compareTo(BigDecimal.ZERO) < 0) {
			balEditText = balEditText.negate();
		}

		return balEditText;
	}

	public interface NickelGrid {
		public void nickel(BigDecimal amount);
	}

	public void addRoundingtoGrid(BigDecimal tempBal) {

		NickelGrid nick = (NickelGrid) context;
		if (balance.compareTo(BigDecimal.ZERO) < 0) {
			BigDecimal change = getEdittextValue(balance_edittext).subtract(
					nearestnickel(tempBal));
			Log.d("nickchange", change.toString());
			BigDecimal nickel = nearestnickel(
					getEdittextValue(balance_edittext)).subtract(tempBal);
			Log.d("nickchange1", nickel.toString());
			nickel = nickel.subtract(change);
			nick.nickel(nickel);
		} else {
			Log.d("nick2",
					tempBal.toString() + " "
							+ getEdittextValue(balance_edittext).toString());
			if (tempBal
					.compareTo(nearestnickel(getEdittextValue(balance_edittext))) > 0) {
				BigDecimal nickel = nearestnickel(
						getEdittextValue(balance_edittext)).subtract(tempBal);
				Log.d("nick1", nickel.toString());
				nick.nickel(nickel);
			} else {
				BigDecimal nickel = nearestnickel(
						getEdittextValue(balance_edittext)).subtract(tempBal);
				Log.d("nick", nickel.toString());
				if (tempBal.compareTo(BigDecimal.ZERO) != 0)// could be temp
					nick.nickel(nickel);
			}
		}

	}

	public void addRoundingtoGrid(BigDecimal tempBal, BigDecimal whole) {

		NickelGrid nick = (NickelGrid) context;
		if (balance.compareTo(BigDecimal.ZERO) < 0) {
			BigDecimal change = whole.subtract(nearestnickel(tempBal));
			Log.d("nickchange", change.toString());
			BigDecimal nickel = nearestnickel(whole).subtract(tempBal);
			Log.d("nickchange1", nickel.toString());
			nickel = nickel.subtract(change);
			nick.nickel(nickel);
		} else {

			if (tempBal.compareTo(nearestnickel(whole)) > 0) {
				BigDecimal nickel = nearestnickel(whole).subtract(tempBal);
				Log.d("nick1", nickel.toString());
				nick.nickel(nickel);
			} else {
				BigDecimal nickel = nearestnickel(whole).subtract(tempBal);
				Log.d("nick", nickel.toString());
				if (tempBal.compareTo(BigDecimal.ZERO) != 0)// could be temp
					nick.nickel(nickel);
			}
		}

	}

}