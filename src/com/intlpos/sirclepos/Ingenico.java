package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Ingenico  extends DialogFragment {
	private EditText ip, port;
	private Button save;
	public static Ingenico newInstance() {
		Ingenico frag = new Ingenico();
	     return frag;
	}
	
	 @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
		  
		 View v = inflater.inflate(R.layout.ingenico, container, false);
		 getDialog().setTitle(R.string.ingenicoiCF250);
		 setCancelable(true);
		 ip = (EditText)v.findViewById(R.id.ipaddress);
		 port = (EditText)v.findViewById(R.id.port);
		 ip.setText(CSSharedPreferences.getIpandPort().getIp());
		 port.setText(Integer.toString(CSSharedPreferences.getIpandPort().getPort()));
		 save = (Button)v.findViewById(R.id.saveIPPORT);
		 save.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(ip.getText().toString().isEmpty() || port.getText().toString().isEmpty()){
					Toast.makeText(getActivity(), R.string.fillText, Toast.LENGTH_SHORT).show();
				}else{
					StoreDetail sd = new StoreDetail();
					Log.d("ip", ip.getText().toString() +"\n" + port.getText().toString());
					sd.setIp(ip.getText().toString());
					sd.setPort(Integer.parseInt(port.getText().toString()));
					CSSharedPreferences.saveIpAndPort(sd);
					dismiss();
				}
			}
		});
		 return v;
	 }
	
}
