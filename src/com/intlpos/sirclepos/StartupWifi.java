package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class StartupWifi extends DialogFragment implements OnClickListener{
	 private Button yes, no;
	 
	 public static WifiPopup newInstance() {
		 WifiPopup frag = new WifiPopup();
	     return frag;
	}
	 
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
		 super.onCreate(savedInstanceState);
	 }
	 
	 @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
		 getDialog().setTitle(R.string.offline);
		 View v = inflater.inflate(R.layout.startupwifi, container, false);
		 setCancelable(false);
		 yes = (Button)v.findViewById(R.id.ouiButton);
		 yes.setOnClickListener(this);
		 no =(Button)v.findViewById(R.id.nonButton);
		 no.setOnClickListener(this);
		 
		return v;
	 }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.ouiButton:
			startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
			dismiss();
			break;
		case R.id.nonButton:
			dismiss();
			break;
		}
	}
}
