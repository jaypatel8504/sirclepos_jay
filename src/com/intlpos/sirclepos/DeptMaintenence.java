package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.Toast;

public class DeptMaintenence extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dept_maintenence);
		
			
		FlowLayout flowLayout = (FlowLayout)findViewById(R.id.deptmaintenece);
	
		
		for (int i = 1; i <= 20; i++) {
			LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    Button btn = new Button(this);
		    btn.setId(i);
		    final int id_ = btn.getId();
		    btn.setText("button " + id_);
		    btn.setBackgroundColor(Color.rgb(70, 80, 90));
		    flowLayout.addView(btn, lp);
		    Button btn1 = ((Button) findViewById(id_));
		    btn1.setOnClickListener(new View.OnClickListener() {
		        @Override
				public void onClick(View view) {
		            Toast.makeText(view.getContext(),
		                    "Button clicked index = " + id_, Toast.LENGTH_SHORT)
		                    .show();
		        }
		    });
		}
		
		
		
	}

}
