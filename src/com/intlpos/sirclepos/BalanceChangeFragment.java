package com.intlpos.sirclepos;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class BalanceChangeFragment extends Fragment {
	private BigDecimal change;
	private DecimalFormat form = new DecimalFormat("0.00");
	public BalanceChangeFragment() {
		// Required empty public constructor
	}
	public BalanceChangeFragment(BigDecimal change) {
		this.change = change.negate();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		// Inflate the layout for this fragment 
		final View view = inflater.inflate(R.layout.change_fragment,
				container, false);	
		view.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
            	if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //do something
                    return true;
                }
				return false;
            }
    });
		TextView tv = (TextView) view.findViewById(R.id.change_amt);
		tv.setText(change.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
		return view;
	}	
	
}


