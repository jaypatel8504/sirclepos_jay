package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.database.Employee;
import com.intlpos.database.Invoice;
import com.intlpos.database.ModifierGroupProducts;
import com.intlpos.database.ModifierGroups;
import com.intlpos.database.Order;
import com.intlpos.database.Permission;
import com.intlpos.database.Product;
import com.intlpos.database.ProductModifierGroups;
import com.intlpos.database.SaveInvoice;
import com.intlpos.dialogs.TimeClock;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.ImageHelper;
import com.intlpos.mysharedpreferences.PaymentProcessing;
import com.intlpos.sqldatabase.EmployeeDataSource;
import com.intlpos.sqldatabase.PermissionDataSource;
import com.intlpos.storemaintenance.EmployeeAdapter;
import com.intlpos.storemaintenance.EmployeeInformation;
import com.john.beans.Customer;


public class EmployeeLoginPopupWindow extends PopupWindow{
	private static String Connect = "CONNECTING";
	
    public ProgressDialog getRingProgressDialog() {
		return ringProgressDialog;
	}
	private ProgressDialog ringProgressDialog;
	private EmployeeLoginPopupWindow popupwindows = this;
    Context context;
	View layout;
	Button[] btn;
	EditText AccessIdEditText;
	boolean fromLogin, manager;
	private CornerStorePOS app;
	private int check = 0;
	private TextView pinForgot;
	private ArrayList<ImageHelper> images;
	int count = 0;
	private boolean success;
	public EmployeeLoginPopupWindow(Context context,View layout,int height, int width, boolean fromLogin, boolean manager)
    {
		super(layout, width, height, true);
		this.context = context;
		app = (CornerStorePOS) ((Activity) context).getApplication();
		this.layout = layout;
		this.fromLogin = fromLogin;
		this.manager = manager;
		btn = new Button[15];
        setWidgetReferences();
		addWidgetListeners();
		if(CSSharedPreferences.loadDemo()){
			 SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
			 SharedPreferences.Editor editor = settings.edit();
			 editor.putBoolean("ui",true);
			 editor.commit();
			 pinForgot.setText("ACCESS ID FOR DEMO IS 7777");
			 pinForgot.setClickable(false);
		 }
		// show popupwindow
    }
	
	public EmployeeLoginPopupWindow(Context context){
		this.context = context;
		app = (CornerStorePOS) ((Activity) context).getApplication();
	}
	
	public void showProgressBar() {
		ringProgressDialog = ProgressDialog.show(context,context.getResources().getString(R.string.pleasewait), context.getResources().getString(R.string.loading));
	}
	private void setWidgetReferences() {
		AccessIdEditText = (EditText)layout.findViewById(R.id.numberpadtext);
		btn[0] = (Button)layout.findViewById(R.id.button1);
		btn[1] = (Button)layout.findViewById(R.id.button2);
		btn[2] = (Button)layout.findViewById(R.id.button3);
		btn[3] = (Button)layout.findViewById(R.id.button4);
		btn[4] = (Button)layout.findViewById(R.id.button5);
		btn[5] = (Button)layout.findViewById(R.id.button6);
		btn[6] = (Button)layout.findViewById(R.id.button7);
		btn[7] = (Button)layout.findViewById(R.id.button8);
		btn[8] = (Button)layout.findViewById(R.id.button9);
		btn[9] = (Button)layout.findViewById(R.id.button0);
		btn[10] = (Button)layout.findViewById(R.id.button00);
		btn[12] = (Button)layout.findViewById(R.id.loginbutton);
		btn[11] = (Button)layout.findViewById(R.id.deletebutton);
		btn[13] = (Button)layout.findViewById(R.id.clearbutton);
		btn[14] = (Button)layout.findViewById(R.id.timeclock);
		pinForgot = (TextView)layout.findViewById(R.id.forgotyourpin);
		
		if(!manager)
			pinForgot.setText(Html.fromHtml("<u>"+context.getResources().getString(R.string.forgotyourpin)+"</u> "));
		else{
			pinForgot.setText(context.getResources().getString(R.string.getManager));
			
		}
	}
	
    public void show()
    {
    	layout.post(new Runnable() {
    		   public void run() {
    		     showAtLocation(layout, Gravity.CENTER, 0, 0);
    		   }
    		});
    }
	
	public EditText getAccessIdEditText() {
		return AccessIdEditText;
	}
	private void addWidgetListeners() {
		
		for(int i =0;i<15;i++){
			btn[i].setOnClickListener(new LoginButtonListener());
		}
		pinForgot.setOnClickListener(new LoginButtonListener());

	}
	
	private class LoginButtonListener implements OnClickListener {

		@Override
		public void onClick(View v){
		
		
		switch(v.getId()){
		case R.id.forgotyourpin:
			Intent back = new Intent(layout.getContext(), GotToBackOffice.class);
			layout.getContext().startActivity(back);
			break;
		case R.id.button1:
			addtoarray("1");
			break;
		case R.id.button2:
			addtoarray("2");
			break;
		case R.id.button3:
			addtoarray("3");
			break;
		case R.id.button4:
			addtoarray("4");
			break;
		case R.id.button5:
			addtoarray("5");
			break;
		case R.id.button6:
			addtoarray("6");
			break;
		case R.id.button7:
			addtoarray("7");
			break;
		case R.id.button8:
			addtoarray("8");
			break;
		case R.id.button9:
			addtoarray("9");
			break;
		case R.id.button0:
			addtoarray("0");
			break;
		case R.id.button00:
			addtoarray("00");
			break;
		case R.id.loginbutton:
			if(AccessIdEditText.length()>0)
			{
				showProgressBar();
				connecttolocaldb();
				
			}
			break;
		case R.id.deletebutton:

			//get the length of input
			int length = AccessIdEditText.getText().length();
			if (length > 0) {
				AccessIdEditText.getText().delete(length - 1, length);
			}
			break;
		case R.id.clearbutton:
			//AccessIdEditText = (EditText)layout.findViewById(R.id.numberpadtext);
			AccessIdEditText.setText("");
			break;
		case R.id.timeclock:
			TimeClock tc = new TimeClock(context);
			tc.show();
			break;
		default:

		}
	}
	
	}
	public void addtoarray(String numbers){		
		//register TextBox 		
		AccessIdEditText.append(numbers);		
	}

	
	private void connecttolocaldb ()
	{	
		
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/getemployeebyid";

		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);

		params.put("storedetail", storedetail);
		params.put("access_id",AccessIdEditText.getText());
		 
		 final JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {
					if(json.getJSONObject("employee").getString("employees_id").isEmpty())
					{
						Animation shake = AnimationUtils.loadAnimation(layout.getContext(),R.anim.shake);
						AccessIdEditText.startAnimation(shake);
						AccessIdEditText.setText("");
					}
					else
					{
						Log.d("EMPLOYEE", json.toString());
						app.employee = Employee.convertjsontoemp(json, app);
//						app.permission = Permission.convertjsontopermission(json);
						if(app.employee.access_to_pos == true)
						{	

							AccessIdEditText.setText("");
							if(fromLogin){
								 
								
								app.heldInvoices = new ArrayList<SaveInvoice>();
								FillDepartmentList();
								FillProductList();
								getModifierGroups();
								FillCustomerList();
								getProcessingData();
								getEmployees();
							}else{
								ringProgressDialog.dismiss();
								success = true;
								dismiss();
							}
						}
						else
						{
							Animation shake = AnimationUtils.loadAnimation(layout.getContext(),R.anim.shake);
							AccessIdEditText.startAnimation(shake);
							AccessIdEditText.setText("");
						}
					}
				}
				catch(Exception e)
				{
					success = false;
					JSONParser js = new JSONParser(new MyListener() {
						@Override
						public void onResult(JSONObject json) {

							try {
								 
								if(json.getJSONObject("employee").getString("employees_id").isEmpty())
								{
									Animation shake = AnimationUtils.loadAnimation(layout.getContext(),R.anim.shake);
									AccessIdEditText.startAnimation(shake);
									AccessIdEditText.setText("");
								}
								else
								{
									app.employee = Employee.convertjsontoemp(json, app);
									
									if(app.employee.access_to_pos == true)
									{	

										AccessIdEditText.setText("");
										if(fromLogin){
											SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
											if(!prefs.contains("TOKEN")){
												CSSharedPreferences.setToken(1);
											}
											
											FillDepartmentList();
											FillProductList();
											getModifierGroups();
											FillCustomerList();
											getProcessingData();
											getEmployees();
											
											
																		
										}else{
											ringProgressDialog.dismiss();
											success = true;
											dismiss();
										}
									}
									else
									{
										
										Animation shake = AnimationUtils.loadAnimation(layout.getContext(),R.anim.shake);
										AccessIdEditText.startAnimation(shake);
										AccessIdEditText.setText("");
									}
								}
							}
							catch(Exception e)
							{
								ringProgressDialog.dismiss();
								Toast.makeText(context, R.string.fail, Toast.LENGTH_LONG).show();
								Animation shake = AnimationUtils.loadAnimation(layout.getContext(),R.anim.shake);
								AccessIdEditText.startAnimation(shake);
								AccessIdEditText.setText("");
							}
						}

						
						}); 
					js.execute(CornerStorePOS.BackupUrl, methodName, params);

				}
			}

			
			});

		jParser.execute(CornerStorePOS.Url, methodName, params);
		
	}
	
	private void getProcessingData() {
		// TODO Auto-generated method stub
		final String methodName = "PaymentProcessing/PaymentService.svc/getpaymentprocessingdata";

		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);

		params.put("storedetail", storedetail);
		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {
				
				try {
					String merchant_id = json.getString("merchant_id");
					String getUrl = json.getString("url");
					String gateway = json.getString("gateway_name");
					String merchant_password = json.getString("merchant_password");
					if(!merchant_id.isEmpty() && !getUrl.isEmpty() && !merchant_password.isEmpty())
					{
						PaymentProcessing pp = new PaymentProcessing();
						pp.setGateway(gateway);
						pp.setId(merchant_id);
						pp.setPassword(merchant_password);
						pp.setUrl(getUrl);
						
						CSSharedPreferences.setPaymentProcessing(pp);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					JSONParser jParser = new JSONParser(new MyListener() {

						@Override
						public void onResult(JSONObject json) {
							Log.d("RESULT", json.toString());
							try {
								String merchant_id = json.getString("merchant_id");
								String getUrl = json.getString("url");
								String gateway = json.getString("gateway_name");
								String merchant_password = json.getString("merchant_password");
								if(!merchant_id.isEmpty() && !getUrl.isEmpty() && !merchant_password.isEmpty())
								{
									PaymentProcessing pp = new PaymentProcessing();
									pp.setGateway(gateway);
									pp.setId(merchant_id);
									pp.setPassword(merchant_password);
									pp.setUrl(getUrl);
									
									CSSharedPreferences.setPaymentProcessing(pp);
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
				}
			}
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void FillProductList()
	{
		final String methodName = "InventoryMaintenence/InventoryService.svc/getproductsdeptids";

		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
//		
//		
		
		params.put("storedetail", storedetail);
		
		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {
				if(json.getJSONArray("product_list")!= null)	 
				{	
					

					Product product = new Product();
					ArrayList<Product> productsList = product.convertjsontoproduct(json);
					
					app.PMap = new HashMap<String,ArrayList<HashMap<String,Object>>>();
					
					app.PList = new ArrayList<HashMap<String,Object>>(); 
				
					String imageProductId;
					StringBuilder sb = new StringBuilder();
					StringBuilder sbOldUrl = new StringBuilder();
					ArrayList<String> newProducts = new ArrayList<String>();
					for(int i=0;i < productsList.size();i++)
					{	
							ArrayList<HashMap<String,Object>> PList = new ArrayList<HashMap<String,Object>>();
							Product p = productsList.get(i);
							
							HashMap<String, Object> map=new HashMap<String, Object>();
							map.put("product_id",p.getProduct_Id());
							map.put("item_no", p.getItem_no());
							map.put("item_name", p.getItem_name());
							map.put("price",p.getPrice());
							map.put("current_price",p.getPrice());
							map.put("cost",p.getCost());
						    map.put("before_discount_current_price",p.getPrice());
						    map.put("percentage_discount",0.00);
							map.put("dollar_discount",0.00);
							map.put("tax1",p.isTax_rate1());
							map.put("tax2",p.isTax_rate2());
							map.put("tax3",p.isTax_rate3());
							map.put("tsdisplay", p.isProduct_tsdisplay());
							map.put("prompt_items_id",p.getPrompt_items_id());
							map.put("prompt_message",p.getPrompt_message());
							map.put("department_id", p.getDept_id());
							map.put("department_name", p.getDepartment_name());
							map.put("dept_tsdisplay",p.isDept_tsdisplay());
							map.put("item_stock", p.getItem_stock());
							map.put("re_order_level", p.getRe_order_level());
							map.put("picture", p.getPicture());
							map.put("tax_inclusive", p.isTaxInclusive());
							map.put("hasmodifier",p.isHasMods());
							map.put("ismodifier", p.isMods());
							map.put("foodstampable",p.isFoodstampable());
							
							app.PList.add(map);		
							if( app.PMap.containsKey(p.getItem_no()))
							{
								ArrayList<HashMap<String,Object>> oldlist = app.PMap.get(p.getItem_no());
								oldlist.add(map);
								app.PMap.put(p.getItem_no(),oldlist);
							}
							else
							{
								PList.add(map);
								app.PMap.put(p.getItem_no(),PList);
							}
				
					}
					
					Log.d("Product List", "Product List loaded");
					Intent i=new Intent(layout.getContext(),StartPage.class);						
					layout.getContext().startActivity(i);

						ringProgressDialog.dismiss();
				}
				}
				catch(Exception ex)
				{
					JSONParser jParser = new JSONParser(new MyListener() {

						@Override
						public void onResult(JSONObject json) {

							try {
							if(json.getJSONArray("product_list")!= null)	 
							{	
								
								Log.d("product_list",""+json.getJSONArray("product_list").toString());		
								Product product = new Product();
								ArrayList<Product> productsList = product.convertjsontoproduct(json);
								
								app.PMap = new HashMap<String,ArrayList<HashMap<String,Object>>>();
								
								app.PList = new ArrayList<HashMap<String,Object>>(); 
							
								String imageProductId;
								StringBuilder sb = new StringBuilder();
								StringBuilder sbOldUrl = new StringBuilder();
								ArrayList<String> newProducts = new ArrayList<String>();
								for(int i=0;i < productsList.size();i++)
								{	
										ArrayList<HashMap<String,Object>> PList = new ArrayList<HashMap<String,Object>>();
										Product p = productsList.get(i);
										HashMap<String, Object> map=new HashMap<String, Object>();
										map.put("product_id",p.getProduct_Id());
										map.put("item_no", p.getItem_no());
										map.put("item_name", p.getItem_name());
										map.put("price",p.getPrice());
										map.put("current_price",p.getPrice());
										map.put("cost",p.getCost());
									    map.put("before_discount_current_price",p.getPrice());
									    map.put("percentage_discount",0.00);
										map.put("dollar_discount",0.00);
										map.put("tax1",p.isTax_rate1());
										map.put("tax2",p.isTax_rate2());
										map.put("tax3",p.isTax_rate3());
										map.put("tsdisplay", p.isProduct_tsdisplay());
										map.put("prompt_items_id",p.getPrompt_items_id());
										map.put("prompt_message",p.getPrompt_message());
										map.put("department_id", p.getDept_id());
										map.put("department_name", p.getDepartment_name());
										map.put("dept_tsdisplay",p.isDept_tsdisplay());
										map.put("item_stock", p.getItem_stock());
										map.put("re_order_level", p.getRe_order_level());
										map.put("picture", p.getPicture());
										map.put("tax_inclusive", p.isTaxInclusive());
										map.put("hasmodifier",p.isHasMods());
										map.put("ismodifier", p.isMods());
										map.put("foodstampable",p.isFoodstampable());
//										if(!p.getPicture().equals("")){
//											Log.d("URL", p.getPicture());
//											new GetAndSaveImage(p.getProduct_Id(), p.getPicture()).execute();
//											imageProductId = Long.toString(p.getProduct_Id());
//											sb.append(imageProductId).append(",");
//											sbOldUrl.append(imageProductId + p.getPicture()).append(",");
//										}
										
										app.PList.add(map);		
										if( app.PMap.containsKey(p.getItem_no()))
										{
											ArrayList<HashMap<String,Object>> oldlist = app.PMap.get(p.getItem_no());
											oldlist.add(map);
											app.PMap.put(p.getItem_no(),oldlist);
										}
										else
										{
											PList.add(map);
											app.PMap.put(p.getItem_no(),PList);
										}
							
								}
								
//								new DeleteImage(sb).execute();
//								Log.d("CSPOSProductsList",""+GlobalSetup.PList.toString());		
//								Log.d("CSPOSProductsMap",""+GlobalSetup.PMap.toString());
								Log.d("Product List", "Product List loaded");
								Intent i=new Intent(layout.getContext(),StartPage.class);						
								layout.getContext().startActivity(i);

									// dismiss the Progress Bar
									ringProgressDialog.dismiss();
							}
							}
							catch(Exception ex)
							{
								
							}
						}
					});
					
					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
				}
			}
		});
		
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
    private void FillDepartmentList()
    {
                    final String methodName = "DepartmentMaintenence/DepartmentService.svc/getdepartments";

                    final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

                    LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
                    storedetail.put("email_id", CornerStorePOS.email_id);
                    storedetail.put("password", CornerStorePOS.password);
                    storedetail.put("store_id", CornerStorePOS.StoreId);
                    storedetail.put("station_id", CornerStorePOS.StationId);

                    params.put("storedetail", storedetail);

                    JSONParser jParser = new JSONParser(new MyListener() {

                                    @Override
                                    public void onResult(JSONObject json) {

                                                    try {
                                                    	if (json.isNull("department_list")) {
                                                        } else {
                                                        	app.DepartmentList = new ArrayList<Department>();
                                                            app.DepartmentList = Department.convertJSONtodepartment(json);
//                                                            DepartmentDataSource dds = new DepartmentDataSource(context);
//                                                            for(Department d: app.DepartmentList){
//                                                            	dds.createTables(d);
//                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                    	JSONParser jParser = new JSONParser(new MyListener() {

                                                            @Override
                                                            public void onResult(JSONObject json) {

                                                                            try {
                                                                            	if (json.isNull("department_list")) {
                                                                                } else {
                                                                                	app.DepartmentList = new ArrayList<Department>();
                                                                                    app.DepartmentList = Department.convertJSONtodepartment(json);
                                                                                }
                                                                            } catch (Exception e) {
                                                                            	
                                                                            }             
                                                            }
                                            });

                                            jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
                                                    }             
                                    }
                    });

                    jParser.execute(CornerStorePOS.Url, methodName, params);
   

   }
  
    
  public void getModifierGroups(){
	  final String methodName = "InventoryMaintenence/ModifierGroups/ModifierGroupService.svc/getmodifiergroups";
	  final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

      LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
      storedetail.put("email_id", CornerStorePOS.email_id);
      storedetail.put("password", CornerStorePOS.password);
      storedetail.put("store_id", CornerStorePOS.StoreId);
      storedetail.put("station_id", CornerStorePOS.StationId);

      params.put("storedetail", storedetail);
      
      JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {
				if(json != null){
					app.ModifierGroup = ModifierGroups.convertFromJSON(json);
					getProductModifier();
				}else{
					return;
				}
			}
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);
		
  }
  
  private void getProductModifier(){
	  final String methodName = "InventoryMaintenence/ModifierGroups/ModifierGroupService.svc/getproductandmodifiergroups";
	  final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

      LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
      storedetail.put("email_id", CornerStorePOS.email_id);
      storedetail.put("password", CornerStorePOS.password);
      storedetail.put("store_id", CornerStorePOS.StoreId);
      storedetail.put("station_id", CornerStorePOS.StationId);

      params.put("storedetail", storedetail);
      
      JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {
				app.ProductModifier = ProductModifierGroups.products(json);
				getGroupProductModifier();
			}
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);
  }
  
  private void getGroupProductModifier(){
	  final String methodName = "InventoryMaintenence/ModifierGroups/ModifierGroupService.svc/getmodifiergroupandmodifiers";
	  final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

      LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
      storedetail.put("email_id", CornerStorePOS.email_id);
      storedetail.put("password", CornerStorePOS.password);
      storedetail.put("store_id", CornerStorePOS.StoreId);
      storedetail.put("station_id", CornerStorePOS.StationId);

      params.put("storedetail", storedetail);
      
      JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {
				app.GroupProducts = ModifierGroupProducts.getModandProducts(json);
			}
		});
		jParser.execute(CornerStorePOS.Url, methodName, params);
  }
    
  private void FillCustomerList()
  {
                  final String methodName = "CustomerMaintenence/CustomerService.svc/getlistofcustomers";

                  final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

                  LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
                  storedetail.put("email_id", CornerStorePOS.email_id);
                  storedetail.put("password", CornerStorePOS.password);
                  storedetail.put("store_id", CornerStorePOS.StoreId);
                  storedetail.put("station_id", CornerStorePOS.StationId);

                  params.put("storedetail", storedetail);

                  JSONParser jParser = new JSONParser(new MyListener() {

                                  @Override
                                  public void onResult(JSONObject json) {
                                	JSONArray getArray;
									try {
										
										getArray = json.getJSONArray("customer_list");
										app.CustomerList = new ArrayList<Customer>();
										for(int i = 0; i < getArray.length(); i++)
	                                  	{
	                                  		JSONObject objects = getArray.getJSONObject(i);
	                                  		Log.d("STRING", objects.toString());
	                                  		double balance = objects.getDouble("account_balance");
	                                  		String first = objects.getString("first_name");
	                                  		String last = objects.getString("last_name");
	                                  		int id = objects.getInt("customer_id");
	                                  		String address1 = objects.getString("address1");
	                                  		String address2 = objects.getString("address2"); 
	                                  		String city = objects.getString("city");
	                                  		String email = objects.getString("email");
	                                  		String phone = objects.getString("phone_1");
 	                             			String state = objects.getString("state");
 	                             			String zipcode = objects.getString("zipcode");
	                                  		Customer customer = new Customer(balance, id, address1, address2, city, email, first, last, phone, state, zipcode);
	                                  		app.CustomerList.add(customer);
	                                  	}
										
									} catch (Exception e) {
										 JSONParser jParser = new JSONParser(new MyListener() {

			                                  @Override
			                                  public void onResult(JSONObject json) {
			                                	JSONArray getArray;
												try {
													
													getArray = json.getJSONArray("customer_list");
													app.CustomerList = new ArrayList<Customer>();
													for(int i = 0; i < getArray.length(); i++)
				                                  	{
				                                  		JSONObject objects = getArray.getJSONObject(i);
				                                  		Log.d("STRING", objects.toString());
				                                  		double balance = objects.getDouble("account_balance");
				                                  		String first = objects.getString("first_name");
				                                  		String last = objects.getString("last_name");
				                                  		int id = objects.getInt("customer_id");
				                                  		String address1 = objects.getString("address1");
				                                  		String address2 = objects.getString("address2"); 
				                                  		String city = objects.getString("city");
				                                  		String email = objects.getString("email");
				                                  		String phone = objects.getString("phone_1");
			 	                             			String state = objects.getString("state");
			 	                             			String zipcode = objects.getString("zipcode");
				                                  		Customer customer = new Customer(balance, id, address1, address2, city, email, first, last, phone, state, zipcode);
				                                  		app.CustomerList.add(customer);
				                                  	}
													
												} catch (Exception e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
			                                  }
					                  });
					                  jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
									}
                                  }
                  });
                  jParser.execute(CornerStorePOS.Url, methodName, params);
  }
    
  private void getEmployees(){
		
		final String methodName = "EmployeeMaintenence/EmployeeService.svc/getlistofemployees";

      final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

      LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
      storedetail.put("email_id", CornerStorePOS.email_id);
      storedetail.put("password", CornerStorePOS.password);
      storedetail.put("store_id", CornerStorePOS.StoreId);
      storedetail.put("station_id", CornerStorePOS.StationId);

      params.put("storedetail", storedetail);

      JSONParser jParser = new JSONParser(new MyListener() {

      	@Override
          public void onResult(JSONObject json) {
      		try {
      			app.employeeInfo = new ArrayList<EmployeeInformation>();
      			
					JSONArray employeelist = json.getJSONArray("employee_list");		
					Log.d("emply", employeelist.toString());
					for(int i = 0; i < employeelist.length(); i++){
						JSONObject employ = employeelist.getJSONObject(i);
						int access_id;
						if(employ.isNull("access_id")){
							access_id = 0;
						}else{
							access_id = employ.getInt("access_id");
						}
						
						boolean backoffice = employ.getBoolean("access_to_backoffice");
						boolean access_to_pos = employ.getBoolean("access_to_pos");
						boolean admin = employ.getBoolean("admin_access");
						String email = employ.getString("email_id");
						int id = employ.getInt("employees_id");
						String first = employ.getString("first_name");
						String last = employ.getString("last_name");
						String middle_name = employ.getString("middle_name");
						String password;
						
						if(employ.isNull("access_id")){
							password = "";
						}else{
							password = employ.getString("password");
						}
						int jobcode = employ.getInt("job_code");
						String dateStr = employ.getString("birthday");
						String temp = dateStr.replaceAll("/", "");
						temp =  temp.replaceAll("Date", "").replace("(", "").replace(")", "").replace("-", "");
						Log.d("DATE", temp);
						long birth = Long.valueOf(temp);
				
						EmployeeInformation employeeObj = new EmployeeInformation(first,last,
																				  middle_name, email, password, id,
																				  access_id, jobcode, backoffice, admin,
																				  access_to_pos, false,birth);
						app.employeeInfo.add(employeeObj);
					}
					
					
				}catch (Exception e) {	
				}
         }
      });
      jParser.execute(CornerStorePOS.Url, methodName, params);
	}
  
}
