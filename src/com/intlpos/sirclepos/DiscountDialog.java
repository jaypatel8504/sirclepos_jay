package com.intlpos.sirclepos;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import com.intlpos.sirclepos.R;
import com.intlpos.global.KeyPad;
import com.intlpos.global.KeyPad2;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupWindow.OnDismissListener;
/**
 * Customized Dialog for the discount page, including percentage and amount
 * @author John
 *
 */
public class DiscountDialog extends Dialog{
	private int position;
	public int getPosition() {
		return position;
	}
	private DiscountDialog dialog = this;
	private BigDecimal price;
	private EditText amount;
	public Double getAmount() {
		return Double.parseDouble(amount.getText().toString());
	}
	public Double getPercentage() {
		return Double.parseDouble(percentage.getText().toString());
	}
	private EditText percentage;
	private EditText focusedEditText;
	private DecimalFormat form = new DecimalFormat("0.00");

	private TextWatcher ptw, atw;
	private Button submitBtn = (Button) findViewById(R.id.submit);
	public Button getSubmitBtn() {
		return submitBtn;
	}
	private Button cancelBtn = (Button) findViewById(R.id.cancel);
	protected KeyPad keypad;
	private Context context;
	private KeyPadFragment keypadFrag;

	private Button button_1;
	private Button button_2;
	private Button button_3;
	private Button button_4;
	private Button button_5;
	private Button button_6;
	private Button button_7;
	private Button button_8;
	private Button button_9;
	private Button button_0;
	private Button button_00;
	private Button button_clean;
	private Button button_ok;
	
	
	public DiscountDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Passing the TextView of price in its parent View, in order to calculate 
	 * the discount percentage of full price. 
	 * @param position 
	 * @param TextView price
	 * @param Context context
	 */
    public DiscountDialog(BigDecimal subtotal, int position, Context context){
        super(context);
        this.context = context;
        this.price = subtotal;
        this.position = position;
    }
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.popup_discount);
		getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_white));
        WindowManager.LayoutParams lp=dialog.getWindow().getAttributes();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
     		 Display display = wm.getDefaultDisplay();
     		 DisplayMetrics metrics = new DisplayMetrics();
     		 display.getMetrics(metrics);
     		 final int displaywidth = metrics.widthPixels;
     		 final int displayheight = metrics.heightPixels;	
        if(displaywidth < 1920 && displayheight < 1200){
	        lp.width = 300;
	        lp.height = 500;
        }else{
        	lp.width = 600;
        	lp.height = 900;
        }
        dialog.getWindow().setAttributes(lp);
        amount = (EditText) findViewById(R.id.amount);
        percentage = (EditText) findViewById(R.id.percent);
        focusedEditText = percentage;
        Double value = price.doubleValue();
        atw = new AmountWatcher(percentage,value);
        ptw = new PercentWatcher(amount,value);
        amount.addTextChangedListener(atw);
        amount.setInputType(InputType.TYPE_NULL);
        percentage.addTextChangedListener(ptw);
        percentage.setInputType(InputType.TYPE_NULL);
        amount.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {  
    		@Override  
    		public void onFocusChange(View v, boolean hasFocus) {
    		    if(hasFocus) {
    			// When focus on
    		    	focusedEditText = amount;
    		    	amount.setText("");
    			} else {
    			// When focus off
    			}
    			    }
    			});
            percentage.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {  
    		@Override  
    		public void onFocusChange(View v, boolean hasFocus) {
    		    if(hasFocus) {
    			// When focus on
    		    	focusedEditText = percentage;
    		    	percentage.setText("");
    			} else {
    			// When focus off
    			}
    			    }
    			});
            percentage.requestFocus();
        submitBtn = (Button) findViewById(R.id.submit);
        cancelBtn = (Button) findViewById(R.id.cancel);
//        submitBtn.setOnClickListener(new View.OnClickListener() {
//        /** After click OK, update the price TextView         *  
//         */
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//            	Double price_value = price.doubleValue();
//            	price_value -= Double.parseDouble(amount.getText().toString());
//            	dialog.dismiss();
//            }
//        });
        
        cancelBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
            	dialog.dismiss();
            }
        });
        
        addListenerOnButton();
    }

    
    private class AmountWatcher implements TextWatcher {
		private EditText percentage;
		private final BigDecimal price;
		private BigDecimal amt;
		
		public AmountWatcher(EditText edittext, Double price) {
	        this.percentage = edittext;
	        this.price = new BigDecimal(price);
	        
	    }
        
        @Override    
        public void afterTextChanged(Editable s) {     
            // TODO Auto-generated method stub     
            Log.d("TAG","afterTextChanged--------------->");
            percentage.addTextChangedListener(ptw);
    		final String price_str = new DecimalFormat("###,###,###.##").format(price);
            if (!s.toString().matches("")){
            	Log.d("TAG","Amount = "+amt +" Price = "+ price);
	        	if (amt.compareTo(price) == 1)        		
	                s.replace(0, s.length(), price_str);
            }
            else{
            	amount.setText("0.00");
            	Selection.setSelection(amount.getText(), 4, 4);
            }
        }   
          
        @Override 
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub  
            Log.d("TAG","beforeTextChanged--------------->");
        }  
 
         @Override    
        public void onTextChanged(CharSequence s, int start, int before,     
                int count) {        	
            Log.d("TAG","onTextChanged--------------->");
            percentage.removeTextChangedListener(ptw);
            amount.removeTextChangedListener(atw);
            try {  
                //if ((heighText.getText().toString())!=null)
            	amt = new BigDecimal(s.toString());
            	amt = amt.multiply(new BigDecimal(10)).compareTo(price) == 1 ?
            			price : amt.multiply(new BigDecimal(10));
            	amount.setText(form.format(amt.doubleValue()));
            	Selection.setSelection(amount.getText(), 4, 4);
//            	Log.d("123", "percentage = "+amt.divide(price).multiply(new BigDecimal(100)).toString());
            	percentage.setText(amt.divide(price, 2 ,BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(100)).toString());
            	
            } catch (Exception e) {  
                // TODO: handle exception  
//            	Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            	Log.d("123", e.toString());
//            	String str = amt.compareTo(price) == -1 ? "0":"100";
//                percentage.setText(str);
            }  finally{
            	amount.addTextChangedListener(atw);

            }
                              
        }                    
    }
    
    
    private class PercentWatcher implements TextWatcher {
		EditText amount;
		Double price;
		
		public PercentWatcher(EditText edittext, Double price) {
	        this.amount = edittext;
	        this.price = price;
	    }
        
        @Override    
        public void afterTextChanged(Editable s) {     
            // TODO Auto-generated method stub     
            Log.d("TAG","afterTextChanged--------------->");
            amount.addTextChangedListener(atw);
            if ( !s.toString().matches("")){
            	Double per = Double.parseDouble(s.toString());
        	if (per > 100)
                s.replace(0, s.length(), "100");
            }
            else{
            	percentage.setText("0");
            	Selection.setSelection(percentage.getText(), 0, 1);
            }
        }
          
        @Override 
        public void beforeTextChanged(CharSequence s, int start, int count,  
                int after) {  
            // TODO Auto-generated method stub  
            Log.d("TAG","beforeTextChanged--------------->");  
        }  
 
         @Override    
        public void onTextChanged(CharSequence s, int start, int before,     
                int count) {        	
            Log.d("TAG","onTextChanged--------------->");
            amount.removeTextChangedListener(atw);
            try {  
                //if ((heighText.getText().toString())!=null)
            	Double per = Double.parseDouble(s.toString());            	
            	amount.setText((form.format(price*per/100)));
                  
            } catch (Exception e) {  
                // TODO: handle exception  
            }  
                              
        }                    
    }
    
    public static double multi(double v1,double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.multiply(b2).doubleValue();
    }
    
    private void addListenerOnButton() {
		// TODO Auto-generated method stub
		button_1 = (Button) findViewById(R.id.keypad_1);
		button_2 = (Button) findViewById(R.id.keypad_2);
		button_3 = (Button) findViewById(R.id.keypad_3);
		button_4 = (Button) findViewById(R.id.keypad_4);
		button_5 = (Button) findViewById(R.id.keypad_5);
		button_6 = (Button) findViewById(R.id.keypad_6);
		button_7 = (Button) findViewById(R.id.keypad_7);
		button_8 = (Button) findViewById(R.id.keypad_8);
		button_9 = (Button) findViewById(R.id.keypad_9);
		button_0 = (Button) findViewById(R.id.keypad_0);
		button_00 = (Button) findViewById(R.id.keypad_00);
		button_clean = (Button) findViewById(R.id.keypad_clean);
//		button_ok = (Button) findViewById(R.id.keypad_ok);
		
		button_1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {	
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"1"));
//            	Toast.makeText(getContext(), "123", Toast.LENGTH_SHORT).show();
            }
    });
		button_2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"2"));
            }
    });
		button_3.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"3"));
            }
    });
		button_4.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"4"));
            }
    });
		button_5.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"5"));
            }
    });
		button_6.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"6"));
            }
    });
		button_7.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"7"));
            }
    });
		button_8.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"8"));
            }
    });
		button_9.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"9"));
            }
    });
		button_0.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"0"));
            }
    });
		
		button_00.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"0"));
            	focusedEditText.setText(getText(focusedEditText.getText().toString(),"0"));
            }
    });
		button_clean.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
            	focusedEditText.setText("");
            }
    });
//		button_ok.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v)
//            {
//               Toast.makeText(getContext(), focusedEditText.getText().toString() , Toast.LENGTH_SHORT).show();
//               dismiss();
//            }
//    });
		
	}
    
    private String getText(String str, String chr){
    	Log.d("String", str);
    	if(str.equals("0"))
    		return chr;
    	else
    		return str+chr;    	
    }
}
