package com.intlpos.sirclepos;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import POSAPI.POSInterfaceAPI;
import POSAPI.POSWIFIAPI;
import POSSDK.POSSDK;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abhi.barcode.frag.libv2.BarcodeFragment;
import com.abhi.barcode.frag.libv2.IScanResultHandler;
import com.abhi.barcode.frag.libv2.ScanResult;
import com.google.gson.Gson;
import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.JSONParserNew;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.WCFAccess.StarListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Department;
import com.intlpos.database.InventoryParcelable;
import com.intlpos.database.Invoice;
import com.intlpos.database.InvoiceParcelable;
import com.intlpos.database.ModifierGroups;
import com.intlpos.database.Order;
import com.intlpos.database.PaymentType;
import com.intlpos.database.Permission;
import com.intlpos.database.Product;
import com.intlpos.database.ProductAdapter;
import com.intlpos.database.ProductAdapter.GroupIds;
import com.intlpos.database.ProductAdapter.getProducts;
import com.intlpos.database.ProductAdapter.setTempTax;
import com.intlpos.database.ProductModifierValues;
import com.intlpos.database.SaveInvoice;
import com.intlpos.database.TaxExempt;
import com.intlpos.database.TaxExemption;
import com.intlpos.dialogs.Accounts;
import com.intlpos.dialogs.Accounts.addCustomer;
import com.intlpos.dialogs.AddCustomer;
import com.intlpos.dialogs.AddCustomer.OpenAccounts;
import com.intlpos.dialogs.AddNote;
import com.intlpos.dialogs.AddNote.AddNoteToGrid;
import com.intlpos.dialogs.FetchHeldInvoices;
import com.intlpos.dialogs.FetchHeldInvoices.PullOrder;
import com.intlpos.dialogs.ManagerPrompt;
import com.intlpos.dialogs.TaxPopUpDialog;
import com.intlpos.global.KeyPad2;
import com.intlpos.initsetup.PostRequestTask;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.sirclepos.ItemsFragment.OnMyProductButtonClickListener;
import com.intlpos.sirclepos.ListViewDialogFragment.OnItemSelectedListener;
import com.intlpos.sirclepos.PaymentFragmentListener.NickelGrid;
import com.intlpos.sirclepos.PromptDialogFragment.onModsSubmitted;
import com.intlpos.sirclepos.SelectModsFragment.AddNotetoItem;
import com.intlpos.sirclepos.TestFragment.OnMyButtonClickListener;
import com.john.beans.Customer;
import com.john.beans.SaveCustomer;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.john.bluetoothprinter.helper.PrinterCommands;
import com.john.bluetoothprinter.helper.ReceiptBuilder;
import com.john.bluetoothprinter.helper.TestPrintInfo;
import com.john.email.SendMailTLSTask;
import com.partner.pt100.cashdrawer.CashDrawerApiContext;
import com.partner.pt100.cashdrawer.CashDrawerManage;
import com.partner.pt100.display.DisplayLineApiContext;
import com.partner.pt100.display.DisplayManager;
import com.partner.pt100.printer.PrinterApiContext;
import com.partner.pt100.printer.PrinterManage;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.stario.StarPrinterStatus;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManager;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManagerListener;
import com.zj.usbsdk.UsbController;


@SuppressLint("NewApi")
public class InvoiceHome extends FragmentActivity  implements  PaymentFragment.OnPaymentFinishedListener,
		OnMyButtonClickListener,OnMyProductButtonClickListener,PromptDialogFragment.OnMySubmitButtonClickListener,OnItemSelectedListener
		, IScanResultHandler, InvoicesDialogFragment.OnInvoiceSelectedListener, InvoiceItemsDialogFragment.OnPrintRecallInvoiceListener, setTempTax, addCustomer, OpenAccounts,onModsSubmitted,GroupIds, AddNotetoItem, AddNoteToGrid, NickelGrid, PullOrder,getProducts{
	private static final String GET_EMAIL_URL = "Settings/SettingsService.svc/getemailsettings";
	private static final byte[][] SELECTLINE = {
		{0x10,0x00},
		{0x10,0x14},
	};
	private static final int POSPORT = 9100; 	// The port handle of Processing instruction 
	private static final int STATEPORT = 4000; 	// The port handle of Query State
	private String POSIP = ""; 
	private static final String DISPLAY_FORMAT = "%-13s%6.2f";
	private Context context;
	private ImageButton mSearchButton,mKeypadButton,mCameraScannerButton,mSyncButton;
	private Button lastInvoice,openDrawer,addItem,checkPrice, holdInvoice, fetchHeldInvoice, accounts, kitchenOrder;
	private EditText mSearchEditText;
	private View dummyitem;
	private KeyPadFragment keypadFrag;
	private PopupWindow itemSearchPopupWindow;
	//private GridView gridvieItems;
	private ListView mlistviewItems = null;
	private String toPrint;
	private ArrayList<Map<String, Object>> productList = new ArrayList<Map<String,Object>>();
	private Runnable viewParts;
	private ProductAdapter m_adapter;
	private TableLayout m_keypad;
	private TableLayout m_header;
	private KeyPad2 keypad;
	private boolean hasKeypad = false;
	private BroadcastReceiver mybroadcast;
	EditText ItemQty;
	EditText ItemPrice;
	Button btn[] = new Button[14];
	String Flag = "NULL";
	BigDecimal[] total_tax;
	private TextView total_subtotal_text,total_tax_text,total_balance_text;
	private Button payButton;
	private ArrayList<HashMap<String, Object>> mylist;
	private ArrayList<HashMap<String, Object>> allProductList;
	private ListViewDialogFragment listPopupWindow;
	private InvoicesDialogFragment invoicesDialogFragment;
	private InvoiceItemsDialogFragment invoiceitemsDialogFragment;
	private DepartmentLookupFragment deptFrag;
	private ItemLookupFragment itemFrag;
	private PaymentFragment dialogFragment;
	private UsbController usbCtrl;
	private UsbDevice dev;
	private PrintDataService printDataService = null; 
	private Resources res;
	private String invoiceNo;
	private BarcodeFragment barcodeScannerFrag;
	private CornerStorePOS app;
	private ToneGenerator toneG;
	BluetoothAdapter mBluetoothAdapter;
	private String customerName = "";
	//USB Device Receiver
	PendingIntent mPermissionIntent;
	//TestPrint variable
	private TestPrintInfo testprint;
	public static POSSDK pos_wifi = null;	
	private int error_code = 0;
	int rowID;
	private boolean isTaxed[];
	private static final String ACTION_USB_PERMISSION =
		    "com.android.example.USB_PERMISSION";
	private Socket socket;
	private static final int SERVERPORT = 8080;
	private String SERVER_IP="";
	protected static final String TAG = "InvocieHome";
		private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

		    public void onReceive(Context context, Intent intent) {
		        String action = intent.getAction();
		        if (ACTION_USB_PERMISSION.equals(action)) {
		            synchronized (this) {
		                UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

		                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
		                    if(device != null){
		                      //call method to set up device communication
		                   }
		                } 
		                else {
		                    Log.d(TAG, "permission denied for device " + device);
		                }
		            }
		        }
		    }
		};
		protected boolean keypadFragFlag;
		private final BroadcastReceiver wifiBroad = new MyWiFiStateListener();
		private Button moreDetails;
		private BigDecimal[] oldTaxes;
		private boolean[] taxExempt;
		private BigDecimal[] tempTax = null;  
		private boolean admin;
		private Permission permission;
		protected POSInterfaceAPI interface_wifi;
		protected POSWIFIAPI interface_state;
		protected POSSDK pos_state;
		private ArrayList<ProductModifierValues>groupIds;
		private String CustomerName;
		private DisplayLineApiContext mPTTApiContext;
		private PrinterApiContext printApiContext;
		private CashDrawerApiContext cashDrawerApi;
		private StarIoExtManager mStarIoExtManager;
		private int token;
		StarIOPort ports;
		private ArrayList<Product>prods = new ArrayList<Product>();
		private String ServerEmail;
		IdleTimer idleTimer;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("ui", false)){
			setContentView(R.layout.quickserviceui);
		}else
			setContentView(R.layout.invoice_home_withkeypad);
		//for keep screen open
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); 
		IntentFilter filter = new IntentFilter();
	    filter.addAction("android.net.wifi.STATE_CHANGE");
	    registerReceiver(wifiBroad, filter);  
	    
		if(!Locale.getDefault().toString().equals("en_US")){
	    	Locale.setDefault(Locale.US);
	    	Log.d(TAG, ""+Locale.getDefault().toString());
	    }
		
		app = (CornerStorePOS) getApplication();
		toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 100);
		// start a Ring Dialog to wait everything ready.
//		launchRingDialog();
		
		res = this.getResources();
		//Register for Broadcast Receiver
		mybroadcast = new ScreenOnReceiver();
		registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_ON));
//		PaymentFragment paymentFrag = PaymentFragment.newInstance(BigDecimal.TEN);
//		fm.beginTransaction().add(R.id.RelativeLayout2, paymentFrag, "10").commit();
		
		setWidgetReferences();
		addWidgetListeners();
		initFragment();
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		token = CSSharedPreferences.getToken();
		String printerpref = prefs.getString("kitchentprint", "");
		
		if(!printerpref.equals("None")){
			connectToWifiPrinter(printerpref);
		}else{
			kitchenOrder.setVisibility(View.GONE);
		}
	//	HashMap<String,ArrayList<HashMap<String,Object>>> LocalPMap = GlobalSetup.PMap;
		//GridView gridview = (GridView) findViewById(R.id.gridviewProducts);  
	    //gridview.setAdapter(new DataAdapter(this));
		
		m_adapter = new ProductAdapter(this, productList, total_subtotal_text,total_tax_text,total_balance_text,getSupportFragmentManager(),mlistviewItems,InvoiceHome.this);
		ItemHighlighterListener listener = new ItemHighlighterListener(this.getBaseContext());
		mlistviewItems.setOnItemClickListener(listener);
		mlistviewItems.setAdapter(m_adapter);
		//set more details button
		FragmentManager fm = getSupportFragmentManager();
		final SubTotalFragment subtotalFrag = (SubTotalFragment) fm.findFragmentById(R.id.fragment_subtotal);
		moreDetails = subtotalFrag.getMoreDetails();
    	Log.d("more", (subtotalFrag != null)+"");
        moreDetails.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String[] details = subtotalFrag.getDetails();
            	details[2] = getString(R.string.total_qty)+getWholeInvoiceQty();
            	details[0] = getString(R.string.undiscounted_total)+m_adapter.getTotalBalance();
            	details[1] = getString(R.string.discounted) + CornerStorePOS.dec_format.format(m_adapter.getWholeInvoiceDiscountPer())+"%) : "+CornerStorePOS.dec_format.format(m_adapter.getWholeInvoiceDiscountAmt());
            	subtotalFrag.setDetails(details);
            	subtotalFrag.getListPopupWindow().show();
            }

			
        });
        
        
       
	}
	

	private void connectToWifiPrinter(String printer) {
		// connect to wifi printer
				if(printer.equals("SNBC/Partner Tech")){
					StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			        StrictMode.setThreadPolicy(policy);
					final int POS_SUCCESS = 1000;
					POSIP = PreferenceManager.getDefaultSharedPreferences(this).getString("wifi_printer_ip", "");
					interface_wifi = new POSWIFIAPI();
					error_code = interface_wifi.OpenDevice(POSIP, POSPORT);
					if(error_code != POS_SUCCESS){
						Toast.makeText(this, "Failed to connect device, please try again.1", Toast.LENGTH_LONG).show();
					}else{
						pos_wifi = new POSSDK(interface_wifi);
						interface_state = new POSWIFIAPI();
						error_code = interface_state.OpenDevice(POSIP, STATEPORT);
						if (error_code == POS_SUCCESS) {
							pos_state = new POSSDK(interface_state);					
							Toast.makeText(this, "Connection has been established you can send the data.", Toast.LENGTH_LONG)
									.show();
						}else{
							Toast.makeText(this, "Failed to connect device, please try again.", Toast.LENGTH_LONG).show();
							interface_wifi = null;
							
						}
						new CheckWriteSocketConnectionState().start();
					}
				}else{
					POSIP = PreferenceManager.getDefaultSharedPreferences(this).getString("wifi_printer_ip", "");
				    StarEthernet star = new StarEthernet(new StarListener(){

						@Override
						public void getStar(StarIOPort port) {
							// TODO Auto-generated method stub
							ports= port;
							if(ports == null)
								Log.d("did not","connect");
						}
				    	
				    });
				    star.execute(POSIP);
				}
		
	}


	private double getWholeInvoiceQty() {
				
				ArrayList<Map<String, Object>> data = m_adapter.getData();
				BigDecimal counter = BigDecimal.ZERO;
				for(Map<String, Object> map: data){
					counter = counter.add((BigDecimal) map.get("qty"));
				}
				return counter.doubleValue();
			}
	private void initFragment() {
		
		FragmentManager fm = this.getSupportFragmentManager();
		barcodeScannerFrag = new BarcodeFragment();
		barcodeScannerFrag.setScanResultHandler(this);
		// Support for adding decoding type 
//		barcodeScannerFrag.setDecodeFor(EnumSet.of(BarcodeFormat.CODE_128));
		
		if (!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("ui", false)){
			if(app.permission.sell_non_inventory == 1){
				keypadFrag = KeyPadFragment.newInstance(BigDecimal.ZERO);
				fm.beginTransaction().add(R.id.main_keypad_fragment,keypadFrag, "keypad").show(keypadFrag).commit();
			}
		}else{
			mKeypadButton.setVisibility(View.GONE);
		}
		// get first department items
		ArrayList<InventoryParcelable> mylist = new ArrayList<InventoryParcelable>();
    	
		for (int i= 0 ; i< app.PList.size(); i++)
		{			
			HashMap<String,Object> h = app.PList.get(i);
			if( h.get("tsdisplay").equals(true))
			{				
				if(h.get("department_id").equals("1")) // first department
				{
					InventoryParcelable ip = new InventoryParcelable();
					//HashMap<String,Object> myHash = new HashMap<String, Object>();
					//myHash.put("product_id", h.get("product_id"));
					//myHash.put("item_name", h.get("item_name"));
					//ip.map = myHash;
					ip.map = h;
					mylist.add(ip);         			
				}
			}
		}
		itemFrag = ItemLookupFragment.newInstance(mylist);
		deptFrag = DepartmentLookupFragment.newInstance();
		fm.beginTransaction().setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right).add(R.id.fragment_department_lookup, deptFrag, "empty").show(deptFrag).commit();
		fm.beginTransaction().setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right).add(R.id.fragment_item_lookup, itemFrag, "empty").show(itemFrag).commit();
		if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_camera_scanner", false)){
			fm.beginTransaction()
			  .add(R.id.RelativeLayout2, barcodeScannerFrag).hide(barcodeScannerFrag).commit();
		}
	
	}
	
	private void setWidgetReferences() {
//		dummyitem = (View) this.findViewById(R.id.dummyitem);
		mSearchEditText = (EditText) this.findViewById(R.id.Search);
		mSearchButton = (ImageButton) this.findViewById(R.id.buttonSearch);
		mKeypadButton = (ImageButton) this.findViewById(R.id.buttonKeypad);
		mCameraScannerButton = (ImageButton) this.findViewById(R.id.buttonCamera);
		mSyncButton = (ImageButton) this.findViewById(R.id.buttonSync);
		if(!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_camera_scanner", false)){
			mCameraScannerButton.setVisibility(View.GONE);
		}
		lastInvoice = (Button) this.findViewById(R.id.lastInvoice);
		openDrawer = (Button) this.findViewById(R.id.open_cashdrawer);
		addItem = (Button) this.findViewById(R.id.additem);
		checkPrice = (Button) this.findViewById(R.id.price_check);
		holdInvoice = (Button) this.findViewById(R.id.hold_invoice);
		
		fetchHeldInvoice = (Button)this.findViewById(R.id.fetchInvoice);
		kitchenOrder = (Button)this.findViewById(R.id.kitchen_order);
		mlistviewItems = (ListView)this.findViewById(R.id.list);
		
		m_header = (TableLayout)this.findViewById(R.id.header);
		total_subtotal_text = (TextView) findViewById(R.id.subtotal_amt);
		total_tax_text = (TextView) findViewById(R.id.tax_amt);
		total_balance_text = (TextView) findViewById(R.id.grandtotal_amt);
		payButton = (Button) findViewById(R.id.balance);
		accounts =(Button)this.findViewById(R.id.accounts);
		accounts.setText("1 - Guest Customer");
	}
	
	private void addWidgetListeners() {
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		try {
		Class<EditText> cls = EditText.class;
		Method setSoftInputShownOnFocus = cls.getMethod("setSoftInputShownOnFocus", boolean.class);
		setSoftInputShownOnFocus.setAccessible(true);
		setSoftInputShownOnFocus.invoke(mSearchEditText, false);
		} catch (Exception e) {
			
		}
		mSearchEditText.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				hideSoftInput(v.getWindowToken());
			}
			
		});
		mSearchEditText.setOnKeyListener(new OnKeyListener(){

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				
				if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN){
					onSearchEvent(mSearchEditText.getText().toString());
//		        	mSearchEditText.setText("");
				}
				return false;
			}
			
		});
		mSearchEditText.addTextChangedListener(new TextWatcher() {

	        @Override
	        public void afterTextChanged(Editable s) {
	            
//                Log.d("fragListener", "Press outside of payment Frag");
	        	// close Payment Dialog if scan something.
	        	if (dialogFragment !=null && dialogFragment.getUserVisibleHint()){
	        		FragmentManager fm = getSupportFragmentManager();
	              	fm.beginTransaction()
	            	  .setCustomAnimations(android.R.anim.slide_in_left,
	                            android.R.anim.slide_out_right)
	            	  .hide(dialogFragment)
	            	  .show(deptFrag).show(itemFrag)
	            	  .commit();
	            	if(keypadFrag != null)
	            		fm.beginTransaction().show(keypadFrag).commit();
	        	}
	        }

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				
				
			}

	    });
		mSearchButton.setOnClickListener(new SearchButtonListener());	
		mKeypadButton.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				if(app.permission.sell_non_inventory == 1){
					FragmentManager fm = getSupportFragmentManager();
					if (!keypadFragFlag){
	//					Log.d("keypadFrag", "keypad shown");
						keypadFragFlag = true;
						fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_EXIT_MASK).hide(keypadFrag).commit();
						fm.addOnBackStackChangedListener(null);
					}	
					else{
	//					Log.d("keypadFrag", "keypad hidden");
						keypadFragFlag = false;
						fm.beginTransaction()
						  .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
						  .show(keypadFrag).commit();
						keypadFrag.getEdittext().requestFocus();
					}
				}
			}
		});
		mSyncButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
//				listPopupWindow = null;
				if(productList.isEmpty()){
					
					FillProductList();
				}else{
					Toast.makeText(context, "Please finish this Invoice.", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		// set listener for Camera Scanner Button
		mCameraScannerButton.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				FragmentManager fm = getSupportFragmentManager();
				if (!barcodeScannerFrag.isHidden()){
					fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_EXIT_MASK).hide(barcodeScannerFrag).commit();
					fm.addOnBackStackChangedListener(null);
				}	
				else{
					fm.beginTransaction()
					  .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
					  .show(barcodeScannerFrag).commit();
				}
			}	 
		});
		// set listener for print last invoice button
		lastInvoice.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(app.employee.admin_access == true){
					byte[] data = readFile();
					/*if (printer_type == 1){
						usbCtrl.sendByte(data, dev);
					}*/
					if (printer_type == 0){
						SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
				    	String print = preferences.getString("printerType", "");
				    	if(print.equals("Partner Tech")){
				    		printApiContext.printData(data, data.length);
				    		printApiContext.feedLines(2);
				    	}else if(print.equals("Star mPop")){
				    		if(mStarIoExtManager != null){
					    		sendCommands(data);
					    		sendCommands(PrinterCommands.STARFEED_PAPER_AND_CUT);
				    		}
				    	}else{
							printDataService.sendByteArray(data);
							printDataService.sendFeedCutCommand();
							printDataService.sendFeedCutCommand();
				    	}
					}
				}else{
					Toast.makeText(getApplicationContext(), "Only Employees That Have Admin Acess Can Print Last Receipt", Toast.LENGTH_SHORT).show();
				}
			}

			private byte[] readFile() {
				
				File file = new File(Environment.getExternalStorageDirectory(),"last_invoice.txt");
				if (!file.exists()){
					Toast.makeText(getBaseContext(), R.string.file_does_not_exist, Toast.LENGTH_SHORT).show();
					return null;
				}
			    int size = (int) file.length();
			    byte[] bytes = new byte[size];
			    try {
			        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
			        buf.read(bytes, 0, bytes.length);
			        buf.close();
			    } catch (FileNotFoundException e) {
			        
			        e.printStackTrace();
			    } catch (IOException e) {
			        
			        e.printStackTrace();
			    }
				return bytes;
			}
			
		});
		// set listner for open cash drawer
		openDrawer.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				if(app.permission.open_cashdrawer == 1)
					openCashDrawer("");
				else if(app.permission.open_cashdrawer == 2){
					final ManagerPrompt windows;
					View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
					windows = new ManagerPrompt(context,layout ,400,400);
					
				    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
				    windows.getLogin().setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							getManagerPermission(windows.getAcess(),"open",windows,v);
						}
					});
				}else
					Toast.makeText(getApplicationContext(), "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
			}
		});
		addItem.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				AddItemDialog dialog = new AddItemDialog(context);
				dialog.setTitle(R.string.QuickAdd);
				TextView title = (TextView)dialog.findViewById( android.R.id.title );
				title.setTextColor( Color.parseColor("#000000") );
				int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
				View titleDivider = dialog.findViewById(titleDividerId);
				if (titleDivider != null)
				    titleDivider.setBackgroundColor(Color.parseColor("#da6637"));
                dialog.show();	       
               
			}
		});
		checkPrice.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				PriceCheckDialog dialog = new PriceCheckDialog(context,CornerStorePOS.PMap);
				dialog.setTitle(R.string.CheckItem);
                dialog.show();	       
			}
		});
		
		accounts.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				DialogFragment dialog = Accounts.newInstance(m_adapter.getCustId());
				dialog.show(getSupportFragmentManager(), "account");
			}
		});
		
		holdInvoice.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				if(m_adapter.getCount() == 0){
					if(!app.heldInvoices.isEmpty()){
						DialogFragment fetch = FetchHeldInvoices.newInstance();
						fetch.show(getSupportFragmentManager(), "fetch");
					}else{
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.NoInvoices), Toast.LENGTH_SHORT).show();
					}
				}else{
					getOrderId();
				}
			}					
		});
		kitchenOrder.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v){
				testprint = new TestPrintInfo();
        		final ByteArrayOutputStream output = new ByteArrayOutputStream();	
        		ArrayList<Map<String, Object>> data = m_adapter.getData();
        		Order order = new Order();
        		orders = order.converttoOrder(data);
        		AlertDialog.Builder alert = new AlertDialog.Builder(context);

        		alert.setTitle("Note");
        		alert.setMessage("Please write the requirment.");

        		// Set an EditText view to get user input 
        		final EditText input = new EditText(context);
        		alert.setView(input);

        		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
        		public void onClick(DialogInterface dialog, int whichButton) {
        		  String value = input.getText().toString()+"\n";

  				ReceiptBuilder rb = new ReceiptBuilder(orders,value,accounts.getText().toString(), context,0);
  				try {
  					String printerpref = PreferenceManager.getDefaultSharedPreferences(context).getString("kitchentprint", "");
  					if(printerpref.equals("SNBC/Partner Tech")){
		  	        	for (ArrayList<String> arr: rb.printout){
		  					output.write(arr.get(0).getBytes());
		  	        	}
		  	        	output.write(PrinterCommands.FEED_PAPER_AND_CUT);
		  				pos_wifi = new POSSDK(interface_wifi);
		  				byte[] arr = output.toByteArray();
		  				output.close();
		  				
		  				pos_wifi.textPrint(arr, arr.length);
  					}else if(printerpref.equals("Star Micronics")){
  						try{
  						if(ports !=null){
		  					for (ArrayList<String> arr: rb.printout){
		  							ports.writePort(arr.get(0).getBytes(), 0, arr.get(0).getBytes().length);
		  					}
		  					ports.writePort(PrinterCommands.STARFEED_PAPER_AND_CUT, 0, PrinterCommands.STARFEED_PAPER_AND_CUT.length);
  						}
	  					}catch (StarIOPortException e)
	  					{
	  						Log.d("NEVER", "CONNECTED!");
	  					//There was an error writing to the port
	  					}
  					}
		  		} catch (IOException e) {
		  			e.printStackTrace();
		  		}
	        	}
        		});

        		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
        		  public void onClick(DialogInterface dialog, int whichButton) {
        		    // Canceled.
        		  }
        		});
//				error_code = testprint.TestPrintText(pos_wifi, 0, print, print.getBytes().length, 0, 0, 
//						0,100, 20, 10, 1, 1);
        		alert.show();
			}
		});
		fetchHeldInvoice.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v){			
				if(app.permission.void_invoice == 0){
					Toast.makeText(getApplicationContext(), "You Do Not Have Access To This Feature", Toast.LENGTH_SHORT).show();
				}else if(app.permission.void_invoice == 1){
					recallInvoice();
				}else{
					final ManagerPrompt windows;
					View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
					windows = new ManagerPrompt(context,layout ,400,400);
					
				    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
				    windows.getLogin().setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							getManagerPermission(windows.getAcess(),"recall",windows,null);
						}
					});
				}
            	
			}
		});
		
		// set listener for the discount Button
				Button discountAll = (Button) InvoiceHome.this.findViewById(R.id.discount_all);

				discountAll.setOnClickListener(new OnClickListener() {		
					@Override
					public void onClick(final View v) { 
						if(app.permission.discount == 1){
							discountAll(v);
						}else if(app.permission.discount == 0){
							Toast.makeText(getApplicationContext(), "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
						}else{
							final ManagerPrompt windows;
							View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
							windows = new ManagerPrompt(context,layout ,400,400);
							
						    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
						    windows.getLogin().setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									getManagerPermission(windows.getAcess(),"discount",windows,v);
								}
							});
						}
					}	 
				});
				// set listener for the delete all Button
				Button delAll = (Button)  InvoiceHome.this.findViewById(R.id.del_all);
				delAll.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						if(app.permission.delete_button == 1){
							if(mPTTApiContext != null){
								mPTTApiContext.clearText();
								mPTTApiContext.lineDisplay(1, "Voided Invoice");
							}
							cleanWholeInvoice();
							if(!SERVER_IP.isEmpty()){
								sendData("");
							}
						}else if(app.permission.delete_button == 0){
							Toast.makeText(getApplicationContext(), "You Do Not Have Access for this feature.", Toast.LENGTH_SHORT).show();
						}else{
							final ManagerPrompt windows;
							View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
							windows = new ManagerPrompt(context,layout ,400,400);
							
						    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
						    windows.getLogin().setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									getManagerPermission(windows.getAcess(),"delAll",windows,null);
								}
							});
						}
					}

					 
				});
		
				// set listener for tax_all button
				final Button taxAll = (Button) InvoiceHome.this.findViewById(R.id.tax_all);
				final BigDecimal[] temp = new BigDecimal[3];
				final BigDecimal[] old= new BigDecimal[3];
				final String[] str_arr = new String[3];
				taxAll.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						EditTax(taxAll);
					}
					});

				
				payButton.setOnClickListener(new View.OnClickListener() {
		            public void onClick(View v) {
		            	
		            	if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true){
		    		    	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
		    		    	String print = preferences.getString("printerType", "");
		    		    	if(print.equals("Partner Tech")){
		    		    		displayMange("",m_adapter.getTotalGrandBalanceAfterWholeInvoiceDiscount().toString());
		    		    	}
		            	}
		            	
		            	if(app.permission.negate_invoice == 0 || app.permission.negate_invoice == 2){
			            	for(Map<String,Object>map:m_adapter.getData()){
			            		BigDecimal price = (BigDecimal) map.get("current_price");
			            		BigDecimal qty = (BigDecimal) map.get("qty"); 
			            		if(price.compareTo(BigDecimal.ZERO) < 0 || qty.compareTo(BigDecimal.ZERO)< 0){
			            			if(app.permission.negate_invoice == 2){
					            		final ManagerPrompt windows;
										View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
										windows = new ManagerPrompt(context,layout ,400,400);
										
									    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
									    windows.getLogin().setOnClickListener(new View.OnClickListener() {
											
											@Override
											public void onClick(View v) {
												getManagerPermission(windows.getAcess(),"negate",windows,null);
											}
										});
									    break;
			            			}else{
			            				Toast.makeText(getApplicationContext(), "You do not have permission to ring up negative sales!", Toast.LENGTH_LONG).show();
			            			}
			            		}
			            	}
		            	}
		            	if (m_adapter.getCount() < 1){
		            		Toast.makeText(context, getString(R.string.please_purchase_something), Toast.LENGTH_SHORT).show();
		            	}else
		            		if(dialogFragment==null||!dialogFragment.isVisible())
		            			showFragmentDialog();
		            }
		        });				
		
	}
	
	private void EditTax(Button taxAll){
		final TaxPopUpDialog taxdialog;
		LayoutInflater inflater = (LayoutInflater)InvoiceHome.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View taxPop = inflater.inflate(R.layout.popup_tax, null);
		total_tax = m_adapter.getSubTax();//Get the total tax amount for each of the 3 taxes
		final BigDecimal individual_totalTax[] = new BigDecimal[3];//Create a copy of each of the 3 total taxes
		
		for (int i = 0; i< total_tax.length; i++){
			total_tax[i] = total_tax[i].setScale(2, BigDecimal.ROUND_HALF_EVEN);
			individual_totalTax[i] = total_tax[i];
		}
		
		if(isTaxed == null){//This means that we have yet to alter any tax in the invoice. oldTaxes is what the tax was before it was altered
			TaxExemption isTaxExempt = new TaxExemption(true, null, individual_totalTax, null);
			taxdialog = new TaxPopUpDialog(context,isTaxExempt);
		}else{
			m_adapter.setOldTax(isTaxed);
			TaxExemption isTaxExempt = new TaxExemption(true, isTaxed, individual_totalTax, oldTaxes);
			taxdialog = new TaxPopUpDialog(context,isTaxExempt);
		}
		
		taxdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = taxdialog.getWindow();
		WindowManager.LayoutParams wlp = window.getAttributes();

		wlp.gravity = Gravity.TOP | Gravity.LEFT;
		wlp.x = (int) taxAll.getX() - 130;   //x position
		wlp.y = (int) taxAll.getY() - 5;   //y position
		taxdialog.show();
		taxdialog.setCancelable(false);
		taxdialog.getOK().setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setsaveOldTaxes(individual_totalTax);
				setExemptTax(taxdialog.getTaxExempt());
				m_adapter.alterAllTaxes(taxdialog.getTaxExempt(),taxdialog);
				taxdialog.dismiss();
			}
		});	
		
	}
	
	private void setsaveOldTaxes(BigDecimal tax[]){
		oldTaxes = new BigDecimal[3];
		for(int i = 0; i < 3; i++){
			oldTaxes[i]= tax[i];
		}
	}
	
	
	private void setExemptTax(boolean tax[]){
		isTaxed = tax;
	}
	private void FillProductList()
	{

		FillDepartmentList();
		final String methodName = "InventoryMaintenence/InventoryService.svc/getproductsdeptids";

		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
//		
//		
		
		params.put("storedetail", storedetail);
		
		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {
				if(json.getJSONArray("product_list")!= null)	 
				{								
					Product product = new Product();
					ArrayList<Product> productsList = product.convertjsontoproduct(json);
					
					CornerStorePOS.PMap = new HashMap<String,ArrayList<HashMap<String,Object>>>();
					
					app.PList = new ArrayList<HashMap<String,Object>>(); 
				
				
					for(int i=0;i < productsList.size();i++)
					{	
							ArrayList<HashMap<String,Object>> PList = new ArrayList<HashMap<String,Object>>();
							Product p = productsList.get(i);
							HashMap<String, Object> map=new HashMap<String, Object>();
							map.put("product_id",p.getProduct_Id());
							map.put("item_no", p.getItem_no());
							map.put("item_name", p.getItem_name());
							map.put("price",p.getPrice());
							map.put("current_price",p.getPrice());
							map.put("cost",p.getCost());
						    map.put("before_discount_current_price",p.getPrice());
						    map.put("percentage_discount",0.00);
							map.put("dollar_discount",0.00);
							map.put("tax1",p.isTax_rate1());
							map.put("tax2",p.isTax_rate2());
							map.put("tax3",p.isTax_rate3());
							map.put("tsdisplay", p.isProduct_tsdisplay());
							map.put("prompt_items_id",p.getPrompt_items_id());
							map.put("prompt_message",p.getPrompt_message());
							map.put("department_id", p.getDept_id());
							map.put("department_name", p.getDepartment_name());
							map.put("dept_tsdisplay",p.isDept_tsdisplay());
							map.put("item_stock", p.getItem_stock());
							map.put("re_order_level", p.getRe_order_level());
							map.put("picture", p.getPicture());
							map.put("tax_inclusive", p.isTaxInclusive());
							map.put("hasmodifier",p.isHasMods());
							map.put("ismodifier", p.isMods());
							map.put("foodstampable",p.isFoodstampable());
							
							app.PList.add(map);		
							if( CornerStorePOS.PMap.containsKey(p.getItem_no()))
							{
								ArrayList<HashMap<String,Object>> oldlist = CornerStorePOS.PMap.get(p.getItem_no());
								oldlist.add(map);
								CornerStorePOS.PMap.put(p.getItem_no(),oldlist);
							}
							else
							{
								PList.add(map);
								CornerStorePOS.PMap.put(p.getItem_no(),PList);
							}
				
					}
					EmployeeLoginPopupWindow emp = new EmployeeLoginPopupWindow(context);
					emp.getModifierGroups();
					
//					Log.d("CSPOSProductsList",""+GlobalSetup.PList.toString());		
//					Log.d("CSPOSProductsMap",""+GlobalSetup.PMap.toString());
//					Log.d("Product List", "Product List loaded");
					
					Intent intent = getIntent();
					overridePendingTransition(0, 0);
					finish();
					overridePendingTransition(0, 0);
					startActivity(intent);

				}
				}
				catch(Exception ex)
				{
					JSONParser jParser = new JSONParser(new MyListener() {

						@Override
						public void onResult(JSONObject json) {

							try {
							if(json.getJSONArray("product_list")!= null)	 
							{								
								Product product = new Product();
								ArrayList<Product> productsList = product.convertjsontoproduct(json);
								
								CornerStorePOS.PMap = new HashMap<String,ArrayList<HashMap<String,Object>>>();
								
								app.PList = new ArrayList<HashMap<String,Object>>(); 
							
							
								for(int i=0;i < productsList.size();i++)
								{	
										ArrayList<HashMap<String,Object>> PList = new ArrayList<HashMap<String,Object>>();
										Product p = productsList.get(i);
										HashMap<String, Object> map=new HashMap<String, Object>();
										map.put("product_id",p.getProduct_Id());
										map.put("item_no", p.getItem_no());
										map.put("item_name", p.getItem_name());
										map.put("price",p.getPrice());
										map.put("current_price",p.getPrice());
										map.put("cost",p.getCost());
									    map.put("before_discount_current_price",p.getPrice());
									    map.put("percentage_discount",0.00);
										map.put("dollar_discount",0.00);
										map.put("tax1",p.isTax_rate1());
										map.put("tax2",p.isTax_rate2());
										map.put("tax3",p.isTax_rate3());
										map.put("tsdisplay", p.isProduct_tsdisplay());
										map.put("prompt_items_id",p.getPrompt_items_id());
										map.put("prompt_message",p.getPrompt_message());
										map.put("department_id", p.getDept_id());
										map.put("department_name", p.getDepartment_name());
										map.put("dept_tsdisplay",p.isDept_tsdisplay());
										map.put("item_stock", p.getItem_stock());
										map.put("re_order_level", p.getRe_order_level());
										map.put("picture", p.getPicture());
										app.PList.add(map);		
										if( CornerStorePOS.PMap.containsKey(p.getItem_no()))
										{
											ArrayList<HashMap<String,Object>> oldlist = CornerStorePOS.PMap.get(p.getItem_no());
											oldlist.add(map);
											CornerStorePOS.PMap.put(p.getItem_no(),oldlist);
										}
										else
										{
											PList.add(map);
											CornerStorePOS.PMap.put(p.getItem_no(),PList);
										}
							
								}
							
								
//								Log.d("CSPOSProductsList",""+GlobalSetup.PList.toString());		
//								Log.d("CSPOSProductsMap",""+GlobalSetup.PMap.toString());
//								Log.d("Product List", "Product List loaded");
								
								Intent intent = getIntent();
								overridePendingTransition(0, 0);
								finish();
								overridePendingTransition(0, 0);
								startActivity(intent);

							}
							}
							catch(Exception ex)
							{
								Log.d("CSPOS",""+ex);
							}
							// dismiss the Progress Bar
//							ringProgressDialog.dismiss();
						}
					});
					
					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
				}
				// dismiss the Progress Bar
//				ringProgressDialog.dismiss();
			}
		});
		
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void FillDepartmentList()
    {
                    final String methodName = "DepartmentMaintenence/DepartmentService.svc/getdepartments";

                    final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();

                    LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
                    storedetail.put("email_id", CornerStorePOS.email_id);
                    storedetail.put("password", CornerStorePOS.password);
                    storedetail.put("store_id", CornerStorePOS.StoreId);
                    storedetail.put("station_id", CornerStorePOS.StationId);

                    params.put("storedetail", storedetail);

                    JSONParser jParser = new JSONParser(new MyListener() {

                                    @Override
                                    public void onResult(JSONObject json) {

                                                    try {
                                                    	if (json.isNull("department_list")) {
                                                        } else {
                                                        	app.DepartmentList = new ArrayList<Department>();
                                                            app.DepartmentList = Department.convertJSONtodepartment(json);
                                                        }
                                                    } catch (Exception e) {
                                                    	JSONParser jParser = new JSONParser(new MyListener() {

                                                            @Override
                                                            public void onResult(JSONObject json) {

                                                                            try {
                                                                            	if (json.isNull("department_list")) {
                                                                                } else {
                                                                                	app.DepartmentList = new ArrayList<Department>();
                                                                                    app.DepartmentList = Department.convertJSONtodepartment(json);
                                                                                }
                                                                            } catch (Exception e) {
                                                                             	Log.d("GetDept", e.toString());
                                                                            }             
                                                            }
                                            });

                                            jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
                                                    }             
                                    }
                    });

                    jParser.execute(CornerStorePOS.Url, methodName, params);
   

    }

	protected void openCashDrawer(String change) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
    	String toOpen = preferences.getString("cash_types", "");
    	if(!toOpen.equals("None")){
			if(toOpen.equals("Printer Driven")){
				if(printDataService != null){
					printDataService.openCashDrawer();
				}
			}else if(toOpen.equals("Partner Tech")){
				if(!change.isEmpty()){
					if(mPTTApiContext!= null){
						BigDecimal nc = new BigDecimal(change).negate();
						change = nc.toString();
						mPTTApiContext.clearText();
						String space = "";
						if(change.length() == 4){
							space = "          ";
						}
						else if(change.length() == 5){
							space = "        ";
						}else if(change.length() == 6){
							space = "      ";
						}else if(change.length() == 7){
							space = "    ";
						}
						mPTTApiContext.lineDisplay(2, "Change"+ space+change);
					}
				}
				cashDrawerApi.OpenCashDrawerA();
			}else if(toOpen.equals("Star mPOP")){
				byte [] array = {0x07};
				if(mStarIoExtManager != null)
					sendCommands(array);
			}
    	}
	}


	protected void showFragmentDialog() {  
		FragmentManager fm = getSupportFragmentManager();
	    // Create and show the dialog.
		String bal = total_balance_text.getText().toString();
		//Log.d("balanceDue", bal);
		boolean ebt = false;
		BigDecimal ebtTotal = BigDecimal.ZERO;
		BigDecimal taxTotals[] = {BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO};
		for(Map<String,Object>map:m_adapter.getData()){
			boolean food = (boolean) map.get("foodstampable");
			if(food){
				ebt = true;
				ebtTotal = ebtTotal.add((BigDecimal) map.get("item_subtotal"));
				BigDecimal[] tax_value = (BigDecimal[])map.get("tax");
				for (int i = 0; i < 3; i++){
					taxTotals[i] = taxTotals[i].add(tax_value[i]);
					
				}
			}
		}
		
		String tax[] = new String[3];
		
		for(int i = 0; i < taxTotals.length; i++){
			tax[i] = taxTotals[i].toString();
			Log.d("TAXW", tax[i]);
		}
		
		String ebtString = ebtTotal.toString();
		dialogFragment = PaymentFragment.newInstance(new BigDecimal(bal), m_adapter.getCustId(), customerName,ebt, ebtString,tax);		
//		Fragment keypad = fm.findFragmentById(R.id.keypad_fragment);
		fm.beginTransaction()
		  .setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right)
		  .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
		  .replace(R.id.RelativeLayout2, dialogFragment).commit();
		
	}
	
	private void cleanWholeInvoice() {
		holdInvoice.setText(getResources().getString(R.string.fetch_invoice));
		oldTaxes = null;
		taxExempt = null;
		isTaxed = null;
		tempTax = null;
		customerName = "";
		productList.clear();
		m_adapter.resetTheView();
		m_adapter.setTotalBalance(BigDecimal.ZERO);
		m_adapter.setTotalGrandBalance(BigDecimal.ZERO);
		m_adapter.setTotalGrandBalanceAfterWholeInvoiceDiscount(BigDecimal.ZERO);
		m_adapter.setTotalTax(BigDecimal.ZERO);
		m_adapter.setCustId(1);
		m_adapter.setWholeInvoiceDiscountAmt(0.00);
		m_adapter.setWholeInvoiceDiscountPer(0.00);
		total_subtotal_text.setText("0.00");
		total_tax_text.setText("0.00");
        m_adapter.notifyDataSetChanged();
        if (keypadFrag != null){
        	keypadFrag.clean();
        }
        accounts.setText("1 - Guest Customer");
        total_balance_text.setText("0.00");
		m_adapter.setSubTax(new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO});
		m_adapter.setTotalTaxAfterWholeInvoiceDiscount(new BigDecimal[]{BigDecimal.ZERO,BigDecimal.ZERO,BigDecimal.ZERO});
	}	
	
	public void addtoarray(String numbers){		
		//register TextBox 		
		if(Flag == "ItemQty")
		{			
			ItemQty.append(numbers);
		}
		else
		{
			ItemPrice.append(numbers);			
		}
				
	}
	private long lastClickTime;
	private Thread alermThread;
	
	private class SearchButtonListener implements OnClickListener
	{


		@Override
		public void onClick(View v) {
			// For Testing ONLY
			
            if (SystemClock.elapsedRealtime() - lastClickTime > 1000){
    			onSearchEvent(mSearchEditText.getText().toString());
            }

            lastClickTime = SystemClock.elapsedRealtime();
		}				
	}
	
	
	

	public void checkfor_promptitems(HashMap<String, Object> LocalPMap) {
		
		String prompt_items_id = LocalPMap.get("prompt_items_id").toString();
		if(prompt_items_id.equals("null"))
		{
			AddtoGrid(LocalPMap);
			sendData("");
			boolean mod = (boolean) LocalPMap.get("hasmodifier");
			if(mod){
				getMods(LocalPMap);
			}
		}
		else // check for prompt items
		{
			mylist = new ArrayList<HashMap<String,Object>>() ;
			
					for (Map.Entry<String, ArrayList<HashMap<String, Object>>> entry : CornerStorePOS.PMap.entrySet())
					{				   
					   ArrayList<HashMap<String, Object>> sListValue = entry.getValue();
					   for (int a =0; a<sListValue.size();a++)
				        {
						    HashMap<String, Object> tmpData = (HashMap<String, Object>) sListValue.get(a);
						    
						    if (tmpData.get("prompt_items_id").toString().equals(prompt_items_id))
							   {
							      mylist.add(tmpData);										      
							   }				          
				        }       				   					   
					}
					String[] name = new String[mylist.size()];
					int[] id = new int[mylist.size()];
					String title = mylist.get(0).get("prompt_message").toString();
					String picURL[] = new String[mylist.size()];
					for(int j = 0, len = mylist.size(); j < len; j++){
						name[j] = mylist.get(j).get("item_name").toString();
						id[j] = Integer.parseInt(mylist.get(j).get("product_id").toString());
						picURL[j] = mylist.get(j).get("picture").toString();
					}
					FragmentManager fm = getSupportFragmentManager();
//					FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
					  PromptDialogFragment alert = PromptDialogFragment.newInstance(name,id,title,picURL,false,0,null);
//					  ft.add(R.layout.activity_main, alert);
					  alert.show(fm, "Alert_Dialog");
					//ArrayList<HashMap<String, Object>> new_mylist = prompt_items_popup(mylist);
					//AddtoArray(new_myList);
					
					Log.d("CSPOSL",mylist.toString());
		}
		
	}

	public void onSearchEvent(String Barcode) {
//		final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "Please wait...",	"Loading...");
//		ringProgressDialog.setCancelable(false);
		final String barcode = new String(Barcode);
		Log.d("barcode", barcode);
	
			 		if(CornerStorePOS.PMap.get(barcode) != null)
					{
					HashMap<String, Object> LocalPMap = CornerStorePOS.PMap.get(barcode).get(0);	
					String prompt_items_id = LocalPMap.get("prompt_items_id").toString();
					if(prompt_items_id.equals("null"))
					{
						AddtoGrid(LocalPMap);
						boolean mod = (boolean) LocalPMap.get("hasmodifier");
						if(mod){
							getMods(LocalPMap);
						}
						mlistviewItems.setSelection(m_adapter.getCount() - 1);
					}
					else // check for prompt items
					{
					checkfor_promptitems(LocalPMap);
					}
					}
					else
					{
//						Toast.makeText(context, Barcode, Toast.LENGTH_SHORT).show();
						if(barcode.isEmpty()){
							
							FragmentManager fm = getSupportFragmentManager();
							if(listPopupWindow == null){
								// init popup window for search item 
								initiatePopupWindow();
							}
							listPopupWindow.show(
									fm, "Alert_Dialog");
							
						}else{
							if(barcode.startsWith("{BI")){
								Toast.makeText(this, "This is a invoice No", Toast.LENGTH_SHORT).show();
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();

							    alertDialog.setTitle("Recall Invoice");

							    alertDialog.setMessage("Please select action you want");

							    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Display Invoice", new DialogInterface.OnClickListener() {

							      public void onClick(DialogInterface dialog, int id) {
							        

							    } }); 

							    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.return_item), new DialogInterface.OnClickListener() {

							      public void onClick(DialogInterface dialog, int id) {
							        

							    }}); 

//							    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Button 3 Text", new DialogInterface.OnClickListener() {
	//
//							      public void onClick(DialogInterface dialog, int id) {
	//
//							        //...
	//
//							    }});
							    alertDialog.show();
							}else{
//								ringProgressDialog.dismiss();
								
//							Toast.makeText(InvoiceHome.this, R.string.ItemNotFound, Toast.LENGTH_LONG).show();
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									context);
							
								final View view = new View(context);
								alertDialogBuilder.setView(view);
//								// set title
//								alertDialogBuilder.setTitle("Setting up your System");

								// set dialog message
								alertDialogBuilder
									.setTitle(getString(R.string.ItemNotFound))
									.setCancelable(false)
									.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog,int id) {
											// if this button is clicked, closes
											dialog.dismiss();
										}
									  });
								
									// create alert dialog
									final AlertDialog alertDialog = alertDialogBuilder.create();
									alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
									    @Override
									    public void onShow(DialogInterface dialog) {
									        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
									        btnPositive.setHeight(100);
									        btnPositive.setTextSize(20);
									        btnPositive.setText("OK");
									        view.setFocusable(true);
									        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
									        params.height = 0;
									        params.width = 0;
											view.setLayoutParams(params);
											view.requestFocus();
									    }
									});
									alermThread = new Thread(new Runnable() {
										@Override
										public void run() {
											while(alertDialog.isShowing()){
												toneG.startTone(ToneGenerator.TONE_SUP_ERROR, 300); 
												try {
													Thread.sleep(500);
												} catch (InterruptedException e) {
													
													e.printStackTrace();
												}
											}
										}
									});

									// show it
									alertDialog.show();
									alermThread.start();
						}
					}

				}

					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
							
								
//										ringProgressDialog.dismiss();
										
							} catch (Exception e) {
							}
						}
					}).start();		
		mSearchEditText.setText("");
	}

	
	private void AddtoGrid(final HashMap<String,Object> product)
	{	
		FragmentManager fm = getSupportFragmentManager();
		if (changeFragment != null && changeFragment.isVisible()){
			fm.beginTransaction()
			  .setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right)
              .hide(changeFragment)
              .commit();					
		}
		if (new BigDecimal(product.get("item_stock").toString()).compareTo(new BigDecimal(product.get("re_order_level").toString())) == 1){
			toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
			m_adapter.addRow(product);
			mlistviewItems.setSelection(productList.size());
			mSearchEditText.setText("");
			mSearchEditText.setFocusable(true);
		}else{	
		
			boolean promtStock = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_stock_prompt", true);
		
			if(promtStock && !product.containsKey("row")){
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);
		
				// set title
				alertDialogBuilder.setTitle(getString(R.string.re_order_level_reached));
		
				// set dialog message
				alertDialogBuilder
					.setMessage(getString(R.string.do_you_want_to_add_this_item))
					.setCancelable(false)
					.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, close
							toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
							m_adapter.addRow(product);
							mlistviewItems.setSelection(productList.size());
							mSearchEditText.setText("");
							mSearchEditText.setFocusable(true);
							dialog.dismiss();
							sendData("");
						}
					  })
					.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							// if this button is clicked, just close
							// the dialog box and do nothing
							dialog.dismiss();
						}
					});
		
					// create alert dialog
					final AlertDialog alertDialog = alertDialogBuilder.create();
					alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
					    @Override
					    public void onShow(DialogInterface dialog) {
					        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
					        btnPositive.setHeight(100);
					        btnPositive.setTextSize(20);
		
					        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
					        btnNegative.setHeight(100);
					        btnNegative.setTextSize(20);
					    }
					});
					// show it
					alertDialog.show();		
				
			}
			else{
				toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
				m_adapter.addRow(product);
				mlistviewItems.setSelection(productList.size());
				mSearchEditText.setText("");
				mSearchEditText.setFocusable(true);
			}
			
		
		}
	}




	private void AddtoGrid(final HashMap<String,Object> product, final boolean VariablePrice)
	{
		FragmentManager fm = getSupportFragmentManager();
		if (changeFragment != null && changeFragment.isVisible()){
			fm.beginTransaction()
			  .setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right)
              .hide(changeFragment)
              .commit();					
		}
		if (new BigDecimal(product.get("item_stock").toString()).compareTo(new BigDecimal(product.get("re_order_level").toString())) == 1){
			toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
			m_adapter.addRowAfterCheck(product,VariablePrice);
			mlistviewItems.setSelection(productList.size());
			mSearchEditText.setText("");
			mSearchEditText.setFocusable(true);
		}else{
		
			boolean promtStock = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_stock_prompt", true);
			if(promtStock){
			
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
		
					// set title
					alertDialogBuilder.setTitle(R.string.re_order_level_reached);
		
					// set dialog message
					alertDialogBuilder
						.setMessage(R.string.do_you_want_to_add_this_item)
						.setCancelable(false)
						.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, close
								toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
								m_adapter.addRowAfterCheck(product,VariablePrice);
								mlistviewItems.setSelection(productList.size());
								mSearchEditText.setText("");
								mSearchEditText.setFocusable(true);
								dialog.dismiss();
							}
						  })
						.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.dismiss();
							}
						});
		
						// create alert dialog
						final AlertDialog alertDialog = alertDialogBuilder.create();
						alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
						    @Override
						    public void onShow(DialogInterface dialog) {
						        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
						        btnPositive.setHeight(100);
						        btnPositive.setTextSize(20);
		
						        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
						        btnNegative.setHeight(100);
						        btnNegative.setTextSize(20);
						    }
						});
						// show it
						alertDialog.show();
			}
			else{
				toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 200); 
				m_adapter.addRowAfterCheck(product,VariablePrice);
				mlistviewItems.setSelection(productList.size());
				mSearchEditText.setText("");
				mSearchEditText.setFocusable(true);
			}
			
				
		}
	}
	
	private void initiatePopupWindow()
	{			
			allProductList = new ArrayList<HashMap<String, Object>>();
			ArrayList<InventoryParcelable> allProductParcelableList = new ArrayList<InventoryParcelable>();
			for (ArrayList<HashMap<String, Object>> arr: CornerStorePOS.PMap.values()){
				for(HashMap<String, Object> m: arr){
					allProductList.add(m);
					InventoryParcelable ip = new InventoryParcelable();
					ip.map = m;
					allProductParcelableList.add(ip);
				}
			}
						
			listPopupWindow = ListViewDialogFragment.newInstance(allProductParcelableList);
	}
	
	private OnClickListener cancel_button_click_listener = new OnClickListener() 
	{
		
		@Override
		public void onClick(View v) {
			
			itemSearchPopupWindow.dismiss();		
		}
	};
	private int printer_type;
	private ReceiptBuilder rb;
	private ArrayList<Order> orders;
	private Invoice invoice;
	private BalanceChangeFragment changeFragment;

	@Override
	public void onMyButtonClick(ArrayList<InventoryParcelable> mylist) {
		
		
				FragmentManager fm = getSupportFragmentManager();
				Fragment oldFrag = (Fragment) fm.findFragmentById(R.id.fragment_item_lookup);
				Fragment newFrag = ItemLookupFragment.newInstance(mylist);
				Log.d("item size", "size = "+mylist.size());
				if (oldFrag != newFrag){
					oldFrag = newFrag;
					String Tag = (mylist == null || mylist.isEmpty()) ? "empty":mylist.get(0).toString();
					fm.beginTransaction()
					  .replace(R.id.fragment_item_lookup, newFrag, Tag)
					  .commit();

				}
	}


@Override
public void onMyProductButtonClick(HashMap<String, Object> myproduct) 
{
	holdInvoice.setText(getResources().getString(R.string.hold_invoice));
	String prompt_items_id = myproduct.get("prompt_items_id").toString();
	if(prompt_items_id.equals("null"))
	{	
		String name =  myproduct.get("item_name").toString();
//		Product prod= new Product("1",name);
//		prods.add(prod);
		
		
		if(keypadFrag !=null && !keypadFrag.getEdittext().getText().toString().equals("0.00"))
		{
		//myproduct.put("price", new BigDecimal(keypadFrag.getEdittext().getText().toString()));
			if(keypadFrag.priceQ.getText().toString().equals("P")){
				AddtoGrid(myproduct);
				m_adapter.ChangePrice(m_adapter.getCount()-1, new BigDecimal(keypadFrag.getEdittext().getText().toString()));
				keypadFrag.clean();
				sendData("");
				boolean mod = (boolean) myproduct.get("hasmodifier");
				if(mod){
					getMods(myproduct);
				}
			}else{
				AddtoGrid(myproduct);
				m_adapter.ChangeQty(m_adapter.getCount()-1, new BigDecimal(keypadFrag.getEdittext().getText().toString()));
				keypadFrag.clean();
				sendData("");
			}
		}
		else
		{
			AddtoGrid(myproduct);
			sendData("");
			boolean mod = (boolean) myproduct.get("hasmodifier");
			if(mod){
				getMods(myproduct);
			}
		}
	}
	else // check for prompt items
	{
	checkfor_promptitems(myproduct);
	}
	
	
	
}


private void getMods(HashMap<String, Object> myproduct){
	rowID = m_adapter.getCount();
	long Id = (long) myproduct.get("product_id");
	int productId = (int) Id;
	for(int i = 0; i < app.ProductModifier.size();i++){
		if(app.ProductModifier.get(i).getProductKey() == productId){
			groupIds = new ArrayList<ProductModifierValues>(app.ProductModifier.get(i).getProductValues());
			break;
		}
	}
	
	if(groupIds != null && !groupIds.isEmpty()){
		if(groupIds.get(0).isAutoRingUp()){
			autoRingup(myproduct);
		}else{
			String[] item_name = null;
			int[] item_id = null;
			String[] prices = null;
			String dialogTitle = "";
			String pic[] = null;
			
			
			for(int i = 0; i < app.GroupProducts.size(); i++){
				if(app.GroupProducts.get(i).getKey() == groupIds.get(0).getModifierGroupId()){
					Log.d("SIZE", Integer.toString(app.GroupProducts.get(i).getModProducts().size()));
					item_name = new String[app.GroupProducts.get(i).getModProducts().size()];
					item_id = new int[app.GroupProducts.get(i).getModProducts().size()];
					pic = new String[app.GroupProducts.get(i).getModProducts().size()];
					prices = new String[app.GroupProducts.get(i).getModProducts().size()];
					
					for(int a = 0; a < app.GroupProducts.get(i).getModProducts().size(); a++){
						item_name[a] = app.GroupProducts.get(i).getModProducts().get(a).getItemName();
						item_id[a] = app.GroupProducts.get(i).getModProducts().get(a).getProductId();
						prices[a] = app.GroupProducts.get(i).getModProducts().get(a).getPrice();
						pic[a] = app.GroupProducts.get(i).getModProducts().get(a).getPic();
					}
					
					Log.d("group", Integer.toString(groupIds.get(0).getModifierGroupId()));
					for(ModifierGroups mg: app.ModifierGroup){
						if(mg.getGroupId() == groupIds.get(0).getModifierGroupId()){
							dialogTitle = mg.getPrompt();
							break;
						}
					}
					groupIds.remove(0);
					if(dialogTitle.isEmpty())
						dialogTitle = "Mods";
					FragmentManager fm = getSupportFragmentManager();
					PromptDialogFragment alert = PromptDialogFragment.newInstance(item_name,item_id,dialogTitle,pic,true,rowID,prices);
					alert.show(fm, "Alert_Dialog");
					break;
				}
			}
			
		}
	}
	
	
}

private void autoRingup(HashMap<String, Object> myproduct){
	int[] item_id = null;
	for(int i = 0; i < app.GroupProducts.size(); i++){
		if(app.GroupProducts.get(i).getKey() == groupIds.get(0).getModifierGroupId()){
			item_id = new int[app.GroupProducts.get(i).getModProducts().size()];
			for(int a = 0; a < app.GroupProducts.get(i).getModProducts().size(); a++){
				item_id[a] = app.GroupProducts.get(i).getModProducts().get(a).getProductId();
			}
			break;
		}
	}
	if(item_id != null && item_id.length > 0){
		for (Map.Entry<String, ArrayList<HashMap<String, Object>>> entry : CornerStorePOS.PMap.entrySet())
		{				   
		   ArrayList<HashMap<String, Object>> sListValue = entry.getValue();
		   for (int a =0; a<sListValue.size();a++)
	        {
			    HashMap<String, Object> tmpData = (HashMap<String, Object>) sListValue.get(a);
			    for(int i = 0; i < item_id.length; i++){
				    if (tmpData.get("product_id").toString().equals(Integer.toString(item_id[i])))
					{
				    	tmpData.put("row", rowID);
				    	AddtoGrid(tmpData);									      
					}
			    }
	        }
		}
		
   }
	
	groupIds.remove(0);
	if(!groupIds.isEmpty()){
		if(groupIds.get(0).isAutoRingUp()){
			autoRingup(myproduct);
		}else{
			String[] item_name = null;
			int[] productId = null;
			String dialogTitle ="";
			String pic[] = null;
			String prices[] = null;
			for(int i = 0; i < app.GroupProducts.size(); i++){
				if(app.GroupProducts.get(i).getKey() == groupIds.get(0).getModifierGroupId()){
					item_name = new String[app.GroupProducts.get(i).getModProducts().size()];
					productId = new int[app.GroupProducts.get(i).getModProducts().size()];
					pic = new String[app.GroupProducts.get(i).getModProducts().size()];
					prices = new String[app.GroupProducts.get(i).getModProducts().size()];
					for(int a = 0; a < app.GroupProducts.get(i).getModProducts().size(); a++){
						item_name[a] = app.GroupProducts.get(i).getModProducts().get(a).getItemName();
						prices[a] = app.GroupProducts.get(i).getModProducts().get(a).getPrice();
						productId[a] = app.GroupProducts.get(i).getModProducts().get(a).getProductId();
						pic[a] = app.GroupProducts.get(i).getModProducts().get(a).getPic();
					}
					Log.d("group", Integer.toString(groupIds.get(0).getModifierGroupId()));
					for(ModifierGroups mg: app.ModifierGroup){
						if(mg.getGroupId() == groupIds.get(0).getModifierGroupId()){
							dialogTitle = mg.getPrompt();
							break;
						}
					}
					groupIds.remove(0);
					if(dialogTitle.isEmpty())
						dialogTitle = "Mods";
					FragmentManager fm = getSupportFragmentManager();
					PromptDialogFragment alert = PromptDialogFragment.newInstance(item_name,productId,dialogTitle,pic,true,rowID,prices);
					alert.show(fm, "Alert_Dialog");
					break;
				}
			}
			
		}
	} 

}

@Override
public void onMySubmitButtonClick(ArrayList<Integer> itemIdList) {
	
	for(HashMap<String, Object> map: mylist){
		if (itemIdList.contains(Integer.parseInt(map.get("product_id").toString()))){
			AddtoGrid(map);
			sendData("");
		}
	}
}



@Override
public void onInvoiceSelected(Object object) {
	
	AddtoGrid(((InventoryParcelable) object).map);
	boolean mod = (boolean) ((InventoryParcelable) object).map.get("hasmodifier");
	if(mod){
		getMods(((InventoryParcelable) object).map);
	}
}

@Override
public void onPaymentFinished(boolean finished,BigDecimal change, boolean hasCash) {
	FragmentManager fm = getSupportFragmentManager();
	Fragment oldFrag = fm.findFragmentById(R.id.RelativeLayout2);
	fm.beginTransaction()
	  .setCustomAnimations(android.R.anim.slide_in_left,
                android.R.anim.slide_out_right)
	  .hide(oldFrag)
	  .show(deptFrag).show(itemFrag)
	  .commit();
//	if(keypadFrag != null)
//		fm.beginTransaction().show(keypadFrag).commit();
	if(finished){
		saveinvoice(change);
		
		
		if (hasCash)
			openCashDrawer(change.toString());
		
		
		
	}
	//Toast.makeText(context, change.toString(), Toast.LENGTH_SHORT).show();
	

	
}

private void printReceiptPopUP(final int temp) {
	
	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			context);

		// set title
		alertDialogBuilder.setTitle(getString(R.string.print_receipt));

		// set dialog message
		alertDialogBuilder
			.setMessage(getString(R.string.do_you_want_to_print_receipt))
			.setCancelable(false)
			.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, close
					
					rb = new ReceiptBuilder(orders, invoice, invoiceNo,false, context,CustomerName,temp);
					
					printreceipt(rb);
					dialog.dismiss();
				}
			  })
			.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					// if this button is clicked, just close
					// the dialog box and do nothing
					dialog.dismiss();
				}
			});

			// create alert dialog
			final AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			    @Override
			    public void onShow(DialogInterface dialog) {
			        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
			        btnPositive.setHeight(100);
			        btnPositive.setTextSize(20);

			        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
			        btnNegative.setHeight(100);
			        btnNegative.setTextSize(20);
			    }
			});
			// show it
			alertDialog.show();
			
			
}



public void printreceipt(ReceiptBuilder receiptBuilder) {
	
	/*if (printer_type == 1){
		if (dev == null){
			Toast.makeText(context, getString(R.string.printer_not_connected), Toast.LENGTH_SHORT).show();
			return;
		}
		String msg = "";
//        String lang = getString(R.string.strLang);
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		initPrintCase(0);
    	byte[] cmd = new byte[3];
	    cmd[0] = 0x1b;
	    cmd[1] = 0x21;

		try {
        	for (ArrayList<String> arr: rb.printout){
        		if (Integer.parseInt(arr.get(1)) == 3){
        			cmd[2] |= 0x10; 
        		}else{
        			cmd[2] &= 0xEF;
        		}
        		//send data to printer
        		usbCtrl.sendByte(cmd, dev);                		
        		usbCtrl.sendMsg(arr.get(0),"utf-8", dev);
        		output.write(cmd);
        		output.write(arr.get(0).getBytes("utf-8"));
        	}  
        	PrintDataService.writeToFile(output.toByteArray());
    	    output.close();
    	    if(rb.printReceiptWithSign){
            	for (ArrayList<String> arr: rb.printout){
            		if (Integer.parseInt(arr.get(1)) == 3){
            			cmd[2] |= 0x10; 
            		}else{
            			cmd[2] &= 0xEF;
            		}
            		//send data to printer
            		usbCtrl.sendByte(cmd, dev);                		
            		usbCtrl.sendMsg(arr.get(0),"utf-8", dev);
            	}  
        		usbCtrl.sendByte(cmd, dev);                		
        		usbCtrl.sendMsg(rb.SIGNATURE[0],"utf-8", dev);
    	    }
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}else */
	
	if (printer_type == 0){
//		boolean flag = printDataService.connect();
//    	if (!flag){
//    		Toast.makeText(context, "Failed to connect to Printer", Toast.LENGTH_SHORT);
//    		return;
//    	}
//    	printDataService.send(TESTING);
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
    	String print = preferences.getString("printerType", "");
    	if(print.equals("Partner Tech")){
    		ByteArrayOutputStream output = new ByteArrayOutputStream();	
			for (ArrayList<String> arr: receiptBuilder.printout){
				printApiContext.printData(arr.get(0).getBytes(),arr.get(0).getBytes().length);
				try {
					output.write(arr.get(0).getBytes());
				} catch (NumberFormatException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			printApiContext.feedLines(2);
			
			try {
				PrintDataService.writeToFile(output.toByteArray());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
    	}else if(print.equals("Star mPop")){
    		ByteArrayOutputStream output = new ByteArrayOutputStream();	
    		ArrayList<Byte> array = new ArrayList<Byte>();
    		for (ArrayList<String> arr: receiptBuilder.printout){
    			sendCommands(arr.get(0).getBytes());
    			try {
					output.write(arr.get(0).getBytes());
				} catch (NumberFormatException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	}
    		sendCommands(PrinterCommands.STARFEED_PAPER_AND_CUT);;
    	}
    	else{
			if(printDataService == null){
				Toast.makeText(getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
				return;
			}
	    	int type = 0;
	//    	initPrintCase(type);
			ByteArrayOutputStream output = new ByteArrayOutputStream();	
		
	   	 // Sheet Type: 0 = small, 1 = large;
	    	try {	          
	    		
	        	for (ArrayList<String> arr: receiptBuilder.printout){
	        		output.write(printDataService.sendwithCommand(arr.get(0), Integer.parseInt(arr.get(1))));
	        		
	        	}
	        	//Printing Barcode // 322 40 for small // 482 70 for large // CAN PRINT AS LONG AS 32 BYTES!
	    		int width = 432;
	    		int height = 45;
	//    		output.write(printDataService.sendBarcodeParam(ReceiptDetail.store_no, rb.getInvoiceNum()));
				PrintDataService.writeToFile(output.toByteArray());
	//			output.write(PrinterCommands.FEED_PAPER_AND_CUT);
				output.close();
	    		if (!printDataService.isConnected){
	    			Toast.makeText(context, getString(R.string.printer_not_connected), Toast.LENGTH_SHORT).show();
	    			return;
	    		}
	    		
	    		printDataService.sendByteArray(output.toByteArray());
				if(receiptBuilder.printReceiptWithSign){
					printDataService.sendByteArray(output.toByteArray());
					for (int i = 0; i < 2; i++){
	//					printDataService.sendFeedCutCommand();
					}
					printDataService.sendByteArray(printDataService.sendwithCommand(receiptBuilder.SIGNATURE[type],receiptBuilder.NORMAL));
					printDataService.sendByteArray(printDataService.sendwithCommand(receiptBuilder.SPACE[type],receiptBuilder.NORMAL));
				}
				
				printDataService.sendFeedCutCommand();
			} catch (FileNotFoundException e1) {
				
				Toast.makeText(getBaseContext(), "File Not Found", Toast.LENGTH_SHORT).show();
				e1.printStackTrace();
			} catch (NumberFormatException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
    	}
    	
    	if(!receiptBuilder.duplicateCopy){
	    	
		    	boolean kitchen = preferences.getBoolean("printSame", false);
			    	if(kitchen)
						try {
							printKitchen();
						} catch (StarIOPortException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	    	
    	}
	}
}

//private void initPrintCase(int sheetType) {
//	    	    
//	  //Print Test Cases
//	Calendar cal = Calendar.getInstance();
//  	//String dateStr = CornerStorePOS.dateFormat.format(cal.getTime());
//  	//double subtotal = Double.parseDouble(total_subtotal_text.getText().toString());
//  	//double tax = Double.parseDouble(total_tax_text.getText().toString());
//  //	double bal = Double.parseDouble(CornerStorePOS.dec_format.format(subtotal+tax));
//	 rb = new ReceiptBuilder(orders, invoice, invoiceNo, context);
//}
private void saveinvoice(final BigDecimal change) 
{
	payButton.setEnabled(false);
	for(Customer customer:app.CustomerList){
		if(customer.getCustomer_id() == m_adapter.getCustId()){
			CustomerName = customer.getFirst_name() + " " + customer.getLast_name();
		}
	}
	final String methodName = "InvoiceMaintenence/InvoiceService.svc/saveinvoice";

	final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		
//	StoreDetail storedetail = new StoreDetail();
	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
	storedetail.put("email_id", CornerStorePOS.email_id);
	storedetail.put("password", CornerStorePOS.password);
	storedetail.put("store_id",CornerStorePOS.StoreId);
	storedetail.put("station_id", CornerStorePOS.StationId);
//	storedetail.setemail_id(GlobalSetup.email_id);
//	storedetail.setpassword(GlobalSetup.password);
//	storedetail.setstoreid(GlobalSetup.StoreId);
//	storedetail.setstationid(GlobalSetup.StationId);
//	Gson gson = new Gson();
//	String StoreObj = gson.toJson(storedetail);
	params.put("storedetail", storedetail);
	
	ArrayList<Map<String, Object>> data = m_adapter.getData();
	Order order = new Order();
	orders = order.converttoOrder(data);
	params.put("orders",orders );
	
	invoice = new Invoice();	
    Calendar cal = Calendar.getInstance();
    String today_datetime = CornerStorePOS.dateFormat.format(cal.getTime());
    ArrayList<PaymentType> splitinvoice = dialogFragment.getPayments(); 
    BigDecimal change_amt = change.negate();
    invoice = invoice.converttoInvoice(m_adapter.getTotalBalance(),m_adapter.getTotalBalanceAfterWholeInvoiceDiscount(),m_adapter.getTotalTaxAfterWholeInvoiceDiscount(),m_adapter.getWholeInvoiceDiscountAmt(),m_adapter.getWholeInvoiceDiscountPer(),m_adapter.getTotalGrandBalanceAfterWholeInvoiceDiscount(),today_datetime,splitinvoice,change_amt,app.employee.employees_id,CornerStorePOS.StationId,m_adapter.getCustId());

    params.put("invoice", invoice);
    final int temp; 
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
	boolean kitchen = preferences.getBoolean("printSame", false);
	if(kitchen){
		temp = token;
	}else
		temp = 0;
    //Log.d("CSPOS-invoice",invoice.getSplit_invoice_list().get(0).getLast_four());
   // Log.d("CSPOS-invoice",""+invoice.getLast_four()+""+invoice.getAuth_code()+""+invoice.getRef_no());
	JSONParserNew jParser = new JSONParserNew(new MyListener() {
		
		@Override
		public void onResult(JSONObject json) {
			boolean webserviceisfinished = false;
			try {
			if(json.getBoolean("result")== true)	 
			{								
					// lets store it in a log invoice_id and grandtotal
					payButton.setEnabled(true);
					webserviceisfinished = true;
					if(webserviceisfinished == true){ 
					invoiceNo = json.getString("resultString");
					FragmentManager fm = getSupportFragmentManager();
					changeFragment = new BalanceChangeFragment(change);
					fm.beginTransaction()		  
						  .setCustomAnimations(android.R.anim.slide_in_left,
			                android.R.anim.slide_out_right)
			      		  .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
			      		  .replace(R.id.balance_container, changeFragment)
			      		  .commit();
					//Popup window for sending email
					boolean sendEmail = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_email_receipt", false);
//					Log.d("Email", "send email "+ sendEmail);

					// if get invoiceNO, print the receipt
					
					if (printer_type == 0){
						int printType = Integer.parseInt(app.print_receipt);
						if (toPrint.equals("0")){
							rb = new ReceiptBuilder(orders, invoice, invoiceNo,false, context,CustomerName,temp);
							printreceipt(rb);
						}
						else if(toPrint.equals("2")){
							printReceiptPopUP(temp);
						}
					}
					if (sendEmail){
						sendEmail();
					}
					if(ServerEmail != null && !ServerEmail.isEmpty()){
						sendEmail(ServerEmail);
					}
					
					// clean the UI after printing receipt
						
						cleanWholeInvoice();
						if(!SERVER_IP.isEmpty()){
							String temp = change.negate().toString();
							sendData(temp);
						}
						
					}
					// TODO disconnect printer and connect to display
			}
			else
			{
				Log.d("SAVEINVOICE",json.getString("resultString"));
				//lets store it in a log Invoice,Order objects  /write some message saying failed etc...
				failedToSaveInvoice(change);
			}
			}
			catch(Exception ex)
			{
				Log.d("CSPOS",""+ex);
				JSONParserNew jParser = new JSONParserNew(new MyListener() {
					
					@Override
					public void onResult(JSONObject json) {
						boolean webserviceisfinished = false;
						try {
						if(json.getBoolean("result")== true)	 
						{								
								// lets store it in a log invoice_id and grandtotal
								payButton.setEnabled(true);
								webserviceisfinished = true;
								if(webserviceisfinished == true){ 
								invoiceNo = json.getString("resultString");
								FragmentManager fm = getSupportFragmentManager();
								changeFragment = new BalanceChangeFragment(change);
								fm.beginTransaction()		  
									  .setCustomAnimations(android.R.anim.slide_in_left,
						                android.R.anim.slide_out_right)
						      		  .setTransition(FragmentTransaction.TRANSIT_ENTER_MASK)
						      		  .replace(R.id.balance_container, changeFragment)
						      		  .commit();
								//Popup window for sending email
								boolean sendEmail = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_email_receipt", false);
//								Log.d("Email", "send email "+ sendEmail);

								// if get invoiceNO, print the receipt
								
								if (printer_type != 1){
								int printType = Integer.parseInt(app.print_receipt);
								if (toPrint.equals("0")){
									rb = new ReceiptBuilder(orders, invoice, invoiceNo,false, context,CustomerName,temp);
									printreceipt(rb);
								}
								else if(toPrint.equals("2")){
									printReceiptPopUP(temp);
									}
								}
								if (sendEmail){
									sendEmail();
								}
									cleanWholeInvoice();
									if(!SERVER_IP.isEmpty()){
										Log.d("CHANGE", Boolean.toString(change.toString().isEmpty()));
										String temp = change.toString();
										sendData(temp);
									}
									
								}
						}
						else
						{
							Log.d("SAVEINVOICE",json.getString("resultString"));
							failedToSaveInvoice(change);
						}
						}
						catch(Exception ex)
						{
							Toast.makeText(context, "Failed to save invoice. Please ensure you are connected to the internet.", Toast.LENGTH_LONG).show();
							payButton.setEnabled(true);
							cleanWholeInvoice();
						}
					}
				});
				
				jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
			}
		}
	});
	
	jParser.execute(CornerStorePOS.Url, methodName, params);


}
//Muhammad
private void failedToSaveInvoice(final BigDecimal change){
	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle("Your invoice failed to save!");
	alert.setMessage("Please select one of the following options:");
	alert.setPositiveButton("Resend", null);

	alert.setNegativeButton("Clear Invoice, and Retry", new DialogInterface.OnClickListener() {
	  public void onClick(DialogInterface dialog, int whichButton) {
	    // Canceled.
		  payButton.setEnabled(true);
		  cleanWholeInvoice();
		  dialog.dismiss();
	  }
	});
	
	final AlertDialog alertDialog = alert.create();
	alertDialog.show();
	Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
    b.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        	saveinvoice(change);	
    		alertDialog.dismiss();
    	  }
        	
    		
        
    });
}

private void holdInvoice(){
	if (m_adapter.getCount() < 1){
		Toast.makeText(context, getString(R.string.please_purchase_something), Toast.LENGTH_SHORT).show();
	}else{
		cleanWholeInvoice();
		HoldInvoicePopup dialog = new HoldInvoicePopup(context);
		dialog.setTitle(R.string.hold_invoice);
        dialog.show();	  
	}
}

protected void sendEmail() {
	
	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle("Send your receipt via email");
	alert.setMessage("Please type your email address:");

	final String[] emailList = new String[app.CustomerList.size()];
	for(int i = 0; i < app.CustomerList.size(); i++){
		emailList[i] = app.CustomerList.get(i).getEmail();
	}
	// Set an EditText view to get user input 
	final AutoCompleteTextView input = new AutoCompleteTextView(this);
	 ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
             android.R.layout.simple_dropdown_item_1line, emailList);
	 input.setAdapter(adapter);
	alert.setView(input);

	alert.setPositiveButton("Ok", null);

	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	  public void onClick(DialogInterface dialog, int whichButton) {
	    // Canceled.
		  dialog.dismiss();
	  }
	});
	
	final AlertDialog alertDialog = alert.create();
	alertDialog.show();
	Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
    b.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        	if(input.getText().toString().length() < 5){
    			Toast.makeText(getApplicationContext(), "Email Cannot Be Less Than 5 Characters", Toast.LENGTH_LONG).show();
    		}else{
    		String sendToEmail = input.getText().toString();
    		// save email address
    		if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_save_email", false)){
    			if(!Arrays.asList(emailList).contains(sendToEmail)){
    				String response = SaveCustomer.saveEmail(sendToEmail);
    				Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
    			}
    		}
    		
    		
    	  // Do something with value!
//    	  Toast.makeText(context, value, Toast.LENGTH_SHORT).show();
    		StringBuilder sb = new StringBuilder("<pre>");
    		if (rb == null)
//    			initPrintCase(1);
    			rb = new ReceiptBuilder(orders, invoice, invoiceNo,false, context,CustomerName,0);
    		for (ArrayList<String> arr: rb.printout){
      		if (Integer.parseInt(arr.get(1)) == 3){
          		sb.append("<b>"+arr.get(0)+"</b><br>");
      		}else{
          		sb.append(arr.get(0)+"<br>");
      		}
    		}  
      		sb.append("</pre>");
      		String html = null;
    		try {
    			html = convertStreamToString(getAssets().open("index.html"));
//    			Toast.makeText(getApplicationContext(), html, Toast.LENGTH_SHORT).show();
    		} catch (IOException e) {
    			
    			e.printStackTrace();
    		} catch (Exception e) {
    			
    			e.printStackTrace();
    		}
    		Gson gson = new Gson();
    		GetEmailRequest ger = new GetEmailRequest(app.storedetail);
    		String request = gson.toJson(ger,GetEmailRequest.class);
//    		Log.d("Request", request);
    		PostRequestTask postTask = new PostRequestTask(); 
    		postTask.execute(CornerStorePOS.Url+GET_EMAIL_URL,request);
    		String response = null;
    		try {
    			response = postTask.get();
    		} catch (InterruptedException | ExecutionException e) {
    			
    			e.printStackTrace();
    		}
    		JSONObject json = null;
    		try {
    			 json = new JSONObject(response);
//    			 Log.d("Response", response);
    			int index = html.indexOf("FILLTABLEHERE");
    			if (index != -1){
    				html = html.substring(0, index) + sb.toString() + html.substring(index + "FILLTABLEHERE".length());
    			}
    			index = html.indexOf("FILLEMAILHERE");
    			if (index != -1){
    				html = html.substring(0, index) + json.getString("email_subject") + html.substring(index + "FILLEMAILHERE".length());
    			}
    			 SendMailTLSTask sendMailTask = new SendMailTLSTask(); 
    		     sendMailTask.execute(json.getString("username"),json.getString("password"),sendToEmail,json.getString("email_subject"),json.getString("smtp_server"),json.getString("smtp_port"),html);

    		} catch (JSONException e) {			
    			e.printStackTrace();
    		}  	
    		alertDialog.dismiss();
    		}
        	
    	  }
        	
    		
        
    });
}

private void sendEmail(String sendToEmail) {
	StringBuilder sb = new StringBuilder("<pre>");
	if (rb == null)
//		initPrintCase(1);
		rb = new ReceiptBuilder(orders, invoice, invoiceNo,false, context,CustomerName,0);
	for (ArrayList<String> arr: rb.printout){
		if (Integer.parseInt(arr.get(1)) == 3){
  		sb.append("<b>"+arr.get(0)+"</b><br>");
		}else{
  		sb.append(arr.get(0)+"<br>");
		}
	}  
		sb.append("</pre>");
		String html = null;
	try {
		html = convertStreamToString(getAssets().open("index.html"));
//		Toast.makeText(getApplicationContext(), html, Toast.LENGTH_SHORT).show();
	} catch (IOException e) {
		
		e.printStackTrace();
	} catch (Exception e) {
		
		e.printStackTrace();
	}
	Gson gson = new Gson();
	GetEmailRequest ger = new GetEmailRequest(app.storedetail);
	String request = gson.toJson(ger,GetEmailRequest.class);
//	Log.d("Request", request);
	PostRequestTask postTask = new PostRequestTask(); 
	postTask.execute(CornerStorePOS.Url+GET_EMAIL_URL,request);
	String response = null;
	try {
		response = postTask.get();
	} catch (InterruptedException | ExecutionException e) {
		
		e.printStackTrace();
	}
	JSONObject json = null;
	try {
		 json = new JSONObject(response);
//		 Log.d("Response", response);
		int index = html.indexOf("FILLTABLEHERE");
		if (index != -1){
			html = html.substring(0, index) + sb.toString() + html.substring(index + "FILLTABLEHERE".length());
		}
		index = html.indexOf("FILLEMAILHERE");
		if (index != -1){
			html = html.substring(0, index) + json.getString("email_subject") + html.substring(index + "FILLEMAILHERE".length());
		}
		 SendMailTLSTask sendMailTask = new SendMailTLSTask(); 
	     sendMailTask.execute(json.getString("username"),json.getString("password"),sendToEmail,json.getString("email_subject"),json.getString("smtp_server"),json.getString("smtp_port"),html);
	}catch(Exception e){
		
	}
}


public static String convertStreamToString(InputStream is) throws Exception {
	BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	StringBuilder sb = new StringBuilder();
	String line = null;
	while ((line = reader.readLine()) != null) {
		sb.append(line+"\n");
	}
	is.close();
	return sb.toString();
}

@Override
public void onResume() {
    super.onResume();
   
    if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_camera_scanner", false) && barcodeScannerFrag.getView() != null){
    	barcodeScannerFrag.getView().setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
            	if(event.getAction() == MotionEvent.ACTION_DOWN){
                    //do something
                    return true;
                }
				return false;
            }
    });
    }
    if (keypadFrag != null)
    	keypadFrag.getEdittext().setText("0.00");
    
    
    IntentFilter filter = new IntentFilter();
    filter.addAction("android.net.wifi.STATE_CHANGE");
    registerReceiver(wifiBroad, filter);
   
    
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
    if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true){
    	toPrint = preferences.getString("list_print_receipt", "");
    	String print = preferences.getString("printerType", "");
    	if(print.equals("Generic") || print.equals("Star Micronics")){
    		getSharedReference(); 
    	}
    	else if(print.equals("Partner Tech")){
    		try{
	    		printApiContext = new PrinterManage();
	    		printApiContext.open();
	    		printApiContext.initPrinter();
	    		printer_type = 0;
    		}catch(Exception e){
    			
    		}
    	}else if(print.equals("Star mPop")){
    		mStarIoExtManager = new StarIoExtManager(StarIoExtManager.Type.WithBarcodeReader, "BT:"+preferences.getString("list_bluetooth_printer", ""), "", 10000, this);     // 10000mS!!!
	        mStarIoExtManager.setListener(mStarIoExtManagerListener);
    		starIoExtManagerConnect();
    	}
    }
    else{
    	printer_type = 1; 
    }
    
    String toOpen = preferences.getString("cash_types", "");
    if(toOpen.equals("Partner Tech")){
    	cashDrawerApi = new CashDrawerManage();
    }
    
    String display =  preferences.getString("pole_types", "");
    if(display.equals("Partner Tech")){
    	try{
	    	mPTTApiContext = new DisplayManager();
			mPTTApiContext.open();
			mPTTApiContext.clearText();
			displayMange("","");
    	}catch(Exception e){
    		
    	}
    }else if(display.equals("Sircle POS CFD")){
    	SERVER_IP = preferences.getString("cfd_ip", "");
    	  idleTimer = new IdleTimer(300000, new IIdleCallback() {
            @Override
            public void inactivityDetected() {
            	runOnUiThread(new Runnable() {
            		  public void run() {
	                      	HashMap<String, Object> LocalPMap = new HashMap(CornerStorePOS.PMap.get("000000000000").get(0));	
	              			LocalPMap.put("changeView",true);
	              			ArrayList<Map<String, Object>> temp = new ArrayList<Map<String, Object>>();
	              			temp.add(LocalPMap);
	              			new Thread(new ClientThread(temp)).start();
	              			idleTimer.restartIdleTimer();

            		  }
            		});
            	            }
        });
        idleTimer.startIdleTimer();
    }
} 
@Override
public void onDestroy() {
	super.onDestroy();
//	Log.d("InvoiceHome", "onStop");
	try {
		unregisterReceiver(mybroadcast);
	}catch(NullPointerException e){
		Log.d(TAG, "NULL");}
	
}
@Override
public void onPause(){
	super.onPause();
	SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()); 
	String display =  preferences.getString("pole_types", "");
	if(display.equals("Sircle POS CFD")){
		if(idleTimer != null)
			idleTimer.stopIdleTimer();
	}	
	if (printDataService != null && printDataService.isConnected)
		PrintDataService.disconnect();
	unregisterReceiver(wifiBroad);
}

private void getSharedReference() {
	SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
//	toPrint = preference.getString("list_print_receipt", "");
	printer_type = 0; 
	/*if (printer_type == 1){
		int[] usb_port = app.printer_usb_port;
		usbCtrl = new UsbController(this,mHandler);
		dev = usbCtrl.getDev(usb_port[0],usb_port[1]);
		if(dev != null){
			if( !(usbCtrl.isHasPermission(dev))){
				//Log.d("usb debug","request USB device permission.");
				usbCtrl.getPermission(dev);
			}else{
				Toast.makeText(getApplicationContext(), getString(R.string.connecte_to_usb_printer_successfully),
                        Toast.LENGTH_SHORT).show();
			}
		}else{
			Toast.makeText(getApplicationContext(), getString(R.string.cannot_find_usb_printer),
                    Toast.LENGTH_SHORT).show();
		}
	}*/ if (printer_type == 0){
		String address = preference.getString("list_bluetooth_printer", "");
		if (address == ""){
			Toast.makeText(getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
			printer_type = 1;
			return;
		}
		printDataService = new PrintDataService(getApplicationContext(),address);
		printDataService.connect();
		
		if(!printDataService.isConnected){
			printer_type = 1;
		}
	}
}

private final Handler mHandler = new Handler() {
	@Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
        case UsbController.USB_CONNECTED:
        	Toast.makeText(getApplicationContext(), getString(R.string.msg_getpermission),
                    Toast.LENGTH_SHORT).show();
        	break;
        default:
        	break;
        }
    }
};
public long lastWriteTime;

@Override
public boolean dispatchKeyEvent(KeyEvent event) {
	int keyCode = event.getKeyCode();
//	Log.d("KeyDown", "Key Event");
    switch (keyCode) {
    case KeyEvent.KEYCODE_HOME:
    	return super.dispatchKeyEvent(event);
    case KeyEvent.KEYCODE_BACK:
    	if (m_adapter.getCount() > 0){
    		Toast.makeText(context, getString(R.string.please_finish_this_invoice_before_logout), Toast.LENGTH_SHORT).show();
    		return true;
    	}else{
    		finish();
    		super.onBackPressed();
    	}
    case KeyEvent.KEYCODE_MENU:
    	return super.dispatchKeyEvent(event);
    case KeyEvent.KEYCODE_ENTER:
//		Toast.makeText(context, "Enter Pressed", Toast.LENGTH_SHORT).show();
    	return super.dispatchKeyEvent(event);
    case KeyEvent.KEYCODE_DEL:
    	return super.dispatchKeyEvent(event);
    case KeyEvent.KEYCODE_POWER:
    	if (m_adapter.getCount() > 0){
    		Toast.makeText(context, getString(R.string.please_finish_this_invoice_before_logout), Toast.LENGTH_SHORT).show();
    		return true;
    	}else{
    		return super.dispatchKeyEvent(event);
    	}
    default:
    	return super.dispatchKeyEvent(event);
}
}

@Override
public boolean dispatchTouchEvent(MotionEvent event) {
  // I only care if the event is an UP action
  if ( event.getAction() == MotionEvent.ACTION_DOWN) 
  {	
      //and only is the ListFragment shown.
      if (dialogFragment !=null && dialogFragment.getUserVisibleHint())
      {   

      	final FragmentManager fm = getSupportFragmentManager();
          // create a rect for storing the fragment window rect
          Rect r = new Rect ( 0, 0, 0, 0 );
          // retrieve the fragment's windows rect
          dialogFragment.getView().getHitRect(r);
          // check if the event position is inside the window rect
          boolean intersects = r.contains ( (int) event.getX (), (int) event.getY () );
          // if the event is not inside then we can close the fragment
          if ( intersects ) {
        	  if(CSSharedPreferences.getExpressCheckout().getExpress()){
        		  fm.beginTransaction()
              	  .setCustomAnimations(android.R.anim.slide_in_left,
                              android.R.anim.slide_out_right)
              	  .hide(dialogFragment)
              	  .show(deptFrag).show(itemFrag)
              	  .commit();
            	if(keypadFrag != null)
            		fm.beginTransaction().show(keypadFrag).commit();
        	  }
  			else if(dialogFragment.isVisible()){
  	          	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
              	alertDialogBuilder.setTitle(R.string.warning);
              	alertDialogBuilder
  				.setMessage(R.string.paymentNotVoid)
  				.setCancelable(false)
  				.setPositiveButton(R.string.iknow,new DialogInterface.OnClickListener() {//If the message is sent, the message is added to the database
  					public void onClick(DialogInterface dialog,int id) {		//Also, it is added to the listview
  						fm.beginTransaction()
  		          	  .setCustomAnimations(android.R.anim.slide_in_left,
  		                          android.R.anim.slide_out_right)
  		          	  .hide(dialogFragment)
  		          	  .show(deptFrag).show(itemFrag)
  		          	  .commit();
  		        	if(keypadFrag != null)
  		        		fm.beginTransaction().show(keypadFrag).commit();
  					}
  				  })
              	.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {//User cancels and nothing happens
  					public void onClick(DialogInterface dialog,int id) {
  						
  					}
  				});
   
  				// create alert dialog
  				AlertDialog alertDialog = alertDialogBuilder.create();
   
  				// show it
  				alertDialog.show();
  				return false;
  			}
          }
      }
  }
  //let the system handle the event
  return super.dispatchTouchEvent(event);
}

@Override
public void scanResult(ScanResult result) {
        Toast.makeText(this, result.getRawResult().getText(), Toast.LENGTH_LONG).show();
		onSearchEvent(result.getRawResult().getText().toString());
        barcodeScannerFrag.restart();
}

public void scanAgain(View v){
        barcodeScannerFrag.restart();
}

private void hideSoftInput(IBinder token) {
    if (token != null) {
        InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(token,
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}


@Override
public void onInvoiceSelected(final InvoiceParcelable invoice) {

	invoiceitemsDialogFragment = InvoiceItemsDialogFragment.newInstance(invoice);
    FragmentManager fm = getSupportFragmentManager();
	if(invoiceitemsDialogFragment != null){
		invoiceitemsDialogFragment.show(
		fm, "Alert_Dialog");
	}
}


@Override
public void printRecallInvoice(ReceiptBuilder rb) {
	printreceipt(rb);	
}


//@Override
//public void setTaxIfChanged(BigDecimal[] taxes) {
//	// TODO Auto-generated method stub
//	tempTax = taxes;
//}
//
//
//@Override
//public void setOldTaxes(BigDecimal[] old) {
//	// TODO Auto-generated method stub
//	oldTaxes = old;
//}


@Override
public void addCustomerToList() {
	// TODO Auto-generated method stub
	DialogFragment newFrag = AddCustomer.newInstance();
	newFrag.show(getSupportFragmentManager(), "add");
}


@Override
public void setCustomerID(int id) {
	// TODO Auto-generated method stub
	m_adapter.setCustId(id);
}


@Override
public void setName(String name) {
	// TODO Auto-generated method stub
	customerName = name;
	accounts.setText(m_adapter.getCustId()+ "-"+ customerName);
}

public void getManagerPermission(String Acess, final String option, final ManagerPrompt window, final View view){
	final ProgressDialog ringProgressDialog;
	ringProgressDialog = ProgressDialog.show(context,context.getResources().getString(R.string.pleasewait), context.getResources().getString(R.string.loading));
	final String methodName = "EmployeeMaintenence/EmployeeService.svc/getemployeebyid";
	final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	
	
	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
	storedetail.put("email_id", CornerStorePOS.email_id);
	storedetail.put("password", CornerStorePOS.password);
	storedetail.put("store_id",CornerStorePOS.StoreId);
	storedetail.put("station_id", CornerStorePOS.StationId);

	
	params.put("storedetail", storedetail);
	params.put("access_id",Acess);
	
	 JSONParser jParser = new JSONParser(new MyListener() {
		 
		 @Override
		 public void onResult(JSONObject json) {
			 ringProgressDialog.dismiss();
			 try {
				if(json.getJSONObject("employee").getString("employees_id").isEmpty()){
					 window.dismiss();
				}
				else{
					 Permission permission = Permission.convertjsontopermission(json);
					 window.dismiss();
					 if(option.equals("delAll")){
						 if(permission.delete_button  == 1){
							 if(mPTTApiContext != null)
									displayMange("Voided Invoice", "");
							 cleanWholeInvoice();
						 }
					 }else if(option.equals("discount")){
						 if(permission.discount  == 1)
							 discountAll(view);
					 }else if(option.equals("open")){
						 if(permission.open_cashdrawer == 1)
							 openCashDrawer("");
					 }else if(option.equals("negate")){
						 if(dialogFragment==null||!dialogFragment.isVisible())
		            		showFragmentDialog();
					 }else if(option.equals("recall")){
						 if(permission.void_invoice == 1)
							 recallInvoice();
					 }
				}
				
			 }catch(JSONException e) {
				 window.dismiss();
					e.printStackTrace();
			 }
		 }
	});
	jParser.execute(CornerStorePOS.Url, methodName, params);

}

private void discountAll(final View v){
	final DiscountDialog dialog = new DiscountDialog((BigDecimal)m_adapter.getTotalBalance(), 0 ,v.getContext());
	dialog.setTitle("Discount All");					
    dialog.show();
    
    dialog.getSubmitBtn().setOnClickListener(new View.OnClickListener() {
        /** After click OK, update the price TextView         *  
         */
            @Override
            public void onClick(View v) {
				int position = ((DiscountDialog) dialog).getPosition();
				
				Double discount = Double.parseDouble(((DiscountDialog) dialog).getPercentage().toString());
				Double amount = Double.parseDouble(((DiscountDialog) dialog).getAmount().toString());
				m_adapter.setWholeInvoiceDiscountPer(discount);		
				m_adapter.setWholeInvoiceDiscountAmt(amount);
				//
				m_adapter.UpdateAfterWholeInvoiceDiscount(discount);
				//
				
		    	if(mPTTApiContext != null){
		    		String price = m_adapter.getTotalGrandBalanceAfterWholeInvoiceDiscount().toString();
		    		String discountString = "Discount % " + discount;
		    		displayMange(discountString, price);
		    	}
		    	if(!SERVER_IP.isEmpty()){
		    		sendData("");
		    	}
            	dialog.dismiss();
            }
        });
	dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
		
		@Override
		public void onDismiss(DialogInterface dialog) {
			v.clearFocus();
		}
	});
}


class CheckWriteSocketConnectionState extends Thread {

	public void run() {
		while (interface_wifi != null) {
			try {
				long currentTimeMillis = System.currentTimeMillis();
				if (currentTimeMillis - lastWriteTime > 60000){
					interface_wifi.CloseDevice();
					interface_wifi.OpenDevice(POSIP, POSPORT);
					updateLastWriteTime();
				}
			} catch (Exception e) {
				Toast.makeText(context,
						"The connection to the printer has shut down, please click 'Connect Wi-Fi' button, try to connect to the printer.",
						Toast.LENGTH_LONG).show();
			}
		}
	}
}
private void updateLastWriteTime()
{
	lastWriteTime = System.currentTimeMillis();
}

@Override
public void open() {
	// TODO Auto-generated method stub
	DialogFragment dialog = Accounts.newInstance(m_adapter.getCustId());
	dialog.show(getSupportFragmentManager(), "openaccount");
}


@Override
public void onMyModButtonClick(ArrayList<Integer> mylist,
		ArrayList<Integer> nolist, ArrayList<Integer> remove, int row, int selected[],int no[],boolean edit) {
	// TODO Auto-generated method stub
	for (Map.Entry<String, ArrayList<HashMap<String, Object>>> entry : CornerStorePOS.PMap.entrySet())
	{				   
	   ArrayList<HashMap<String, Object>> sListValue = entry.getValue();
	   for (int a =0; a<sListValue.size();a++)
        {
		    HashMap<String, Object> tmpData = new HashMap((HashMap<String, Object>) sListValue.get(a));
		    for(int i = 0; i < mylist.size(); i++){
			    if (tmpData.get("product_id").toString().equals(Integer.toString(mylist.get(i))))
				{
			    	if(tmpData.containsKey("No")){
			    		tmpData.remove("No");
			    	}
			    	tmpData.put("row", row);
			    	AddtoGrid(tmpData);									      
				}
		    }
		    for(int i = 0; i < nolist.size(); i++){
			    if (tmpData.get("product_id").toString().equals(Integer.toString(nolist.get(i))))
				{
			    	tmpData.put("No", row);
			    	tmpData.put("row", row);
			    	AddtoGrid(tmpData);									      
				}
		    }
        }       				   					   
	}
	
	if(!groupIds.isEmpty()){
		if(groupIds.get(0).isAutoRingUp() && !edit){
			if(m_adapter.getCount() == 0){
			HashMap<String, Object> map = (HashMap<String, Object>) m_adapter.getData().get(row-1);
			autoRingup(map);}
		}else{
			String[] item_name = null;
			int[] item_id = null;
			String dialogTitle="";
			String pic[] = null;
			String prices[] = null;
			for(int i = 0; i < app.GroupProducts.size(); i++){
				if(app.GroupProducts.get(i).getKey() == groupIds.get(0).getModifierGroupId()){
					item_name = new String[app.GroupProducts.get(i).getModProducts().size()];
					item_id = new int[app.GroupProducts.get(i).getModProducts().size()];
					pic = new String[app.GroupProducts.get(i).getModProducts().size()];
					prices = new String[app.GroupProducts.get(i).getModProducts().size()];
					for(int a = 0; a < app.GroupProducts.get(i).getModProducts().size(); a++){
						item_name[a] = app.GroupProducts.get(i).getModProducts().get(a).getItemName();
						prices[a] = app.GroupProducts.get(i).getModProducts().get(a).getPrice();
						item_id[a] = app.GroupProducts.get(i).getModProducts().get(a).getProductId();
						pic[a] = app.GroupProducts.get(i).getModProducts().get(a).getPic();
					}
					
					for(ModifierGroups mg: app.ModifierGroup){
						if(mg.getGroupId() == groupIds.get(0).getModifierGroupId()){
							dialogTitle = mg.getPrompt();
							break;
						}
					}
					
					groupIds.remove(0);
					if(dialogTitle.isEmpty())
						dialogTitle = "Mods";
					FragmentManager fm = getSupportFragmentManager();
					PromptDialogFragment alert = PromptDialogFragment.newInstance(item_name,item_id,dialogTitle,pic,true,row,selected,no,prices,edit);
					alert.show(fm, "Alert_Dialog");
					break;
				}
			}
			
		}
	}
	
	if(!remove.isEmpty()){
		m_adapter.removeItems(row, remove);
	}
	
	sendData("");
}

public void saveOrders(String orderid){
	Calendar cal = Calendar.getInstance();
	String today_datetime = CornerStorePOS.dateFormat.format(cal.getTime());
	ArrayList<Map<String, Object>> data = m_adapter.getData();
	Order order = new Order();
	orders = order.converttoOrder(data);
	SaveInvoice si = new SaveInvoice(orderid,today_datetime,orders,m_adapter.getWholeInvoiceDiscountPer());
	app.heldInvoices.add(si);
	cleanWholeInvoice();
	Toast.makeText(getApplicationContext(), getResources().getString(R.string.invoiceSaved), Toast.LENGTH_SHORT).show();
}

public void getOrderId(){
	AlertDialog.Builder alert = new AlertDialog.Builder(this);

	alert.setTitle(getResources().getString(R.string.EnterOrderName));
	alert.setMessage(getResources().getString(R.string.PleaseNote));
	// Set an EditText view to get user input 
	final EditText input = new EditText(this);
	
	alert.setView(input);

	alert.setPositiveButton("Save", null);

	alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	  public void onClick(DialogInterface dialog, int whichButton) {
	    // Canceled.
		  dialog.dismiss();
	  }
	});
	
	final AlertDialog alertDialog = alert.create();
	
	alertDialog.show();
	brandAlertDialog(alertDialog);
	Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
    b.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        	if(input.getText().toString().isEmpty()){
    			Toast.makeText(getApplicationContext(), getResources().getString(R.string.OrderEmpty), Toast.LENGTH_LONG).show();
    		}else{
    			saveOrders(input.getText().toString());
    			alertDialog.dismiss();
    		}
        	
    	  }
        	
    		
        
    });
}

public static int[] convertIntegers(ArrayList<Integer> integers)
{	
	if(integers != null){
	    int[] ret = new int[integers.size()];
	    for (int i=0; i < ret.length; i++)
	    {
	        ret[i] = integers.get(i);
	    }
	    return ret;
	}else
		return null;
}


@Override
public void setGroupIds(ArrayList<ProductModifierValues> groups) {
	// TODO Auto-generated method stub
	groupIds = groups;
}


@Override
public void addNote() {
	// TODO Auto-generated method stub
	DialogFragment add = AddNote.newInstance();
	add.show(getSupportFragmentManager(), "add");
}


@Override
public void noteToGrid(String note) {
	// TODO Auto-generated method stub
	for(Map<String,Object>map:m_adapter.getData()){
		int key =(int) map.get("size");
		if(key == rowID){
			map.put("note", "\n" +note);;
			m_adapter.notifyDataSetChanged();
			break;
		}
	}
	
}


@Override
public void nickel(BigDecimal amount) {
	// TODO Auto-generated method stub
//	onSearchEvent("000000000000");
	if(amount.compareTo(BigDecimal.ZERO) != 0){
		HashMap<String, Object> LocalPMap = new HashMap(CornerStorePOS.PMap.get("000000000000").get(0));	
		LocalPMap.put("price", amount);
		LocalPMap.put("current_price",amount);
		AddtoGrid(LocalPMap);
	}
}


private void recallInvoice(){
	Calendar currentDate = Calendar.getInstance(); //Get the current date
	SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
	String dateNow = formatter.format(currentDate.getTime());
	Log.d("DATE", dateNow);
	ArrayList<InvoiceParcelable> invoicelist = InvoicesDialogFragment.getInvoicesList(dateNow);
	invoicesDialogFragment = InvoicesDialogFragment.newInstance(invoicelist,dateNow);
	FragmentManager fm = getSupportFragmentManager();
	if(invoicesDialogFragment != null){
		invoicesDialogFragment.show(
				fm, "Alert_Dialog");
	}
}


@Override
public void getOrder(ArrayList<Order> orders, int pos) {
	for(Order o: orders){
		m_adapter.addHeldInvoice((HashMap<String, Object>) o.getMap());
	}
	Log.d("discount",Double.toString(app.heldInvoices.get(pos).getDiscount()));
	m_adapter.setWholeInvoiceDiscountPer(app.heldInvoices.get(pos).getDiscount());
	m_adapter.UpdateAfterWholeInvoiceDiscount(app.heldInvoices.get(pos).getDiscount());
	app.heldInvoices.remove(pos);
	holdInvoice.setText("Hold Invoice");
}

public static void brandAlertDialog(AlertDialog dialog) {
    try {
        Resources resources = dialog.getContext().getResources();
        String color = "#da6637"; // your color here

        int alertTitleId = resources.getIdentifier("alertTitle", "id", "android");
        TextView alertTitle = (TextView) dialog.getWindow().getDecorView().findViewById(alertTitleId);
        alertTitle.setTextColor(Color.BLACK); // change title text color

        int titleDividerId = resources.getIdentifier("titleDivider", "id", "android");
        View titleDivider = dialog.getWindow().getDecorView().findViewById(titleDividerId);
        titleDivider.setBackgroundColor(Color.parseColor(color)); // change divider color
    } catch (Exception ex) {
        ex.printStackTrace();
    }
}

public static void dialogFragmentChangeTitleColor(Dialog dialog, int titleDividerId){
	TextView title = (TextView)dialog.findViewById( android.R.id.title );
	title.setTextColor( Color.parseColor("#000000") );
	View titleDivider = dialog.findViewById(titleDividerId);
	if (titleDivider != null)
	    titleDivider.setBackgroundColor(Color.parseColor("#da6637"));
}


@Override
public void setOldTax(BigDecimal[] old) {
	// TODO Auto-generated method stub
	oldTaxes = old;
}


@Override
public void setTaxExempt(boolean[] tax) {
	// TODO Auto-generated method stub
	isTaxed = tax;
}


@Override
public void displayMange(String text, String Price) {
	// TODO Auto-generated method stub
	if(mPTTApiContext != null){
		mPTTApiContext.clearText();
		mPTTApiContext.lineDisplay(1, text);
		String space = "";
		if(Price.length() == 4){
			space = "     ";
		}
		else if(Price.length() == 5){
			space = "    ";
		}else if(Price.length() == 6){
			space = "   ";
		}else if(Price.length() == 7){
			space = "  ";
		}
		mPTTApiContext.lineDisplay(2, "Grand Total"+ space+Price);
	}
	
}

StarIoExtManagerListener mStarIoExtManagerListener = new StarIoExtManagerListener() {
    @Override
    public void didBarcodeDataReceive(byte[] data) {
        String barcodeDataArray = new String(data);

        	Log.d("bar", barcodeDataArray);
        	barcodeDataArray= barcodeDataArray.replaceAll("\\s","");
        	try{
        		HashMap<String, Object> LocalPMap = new HashMap(CornerStorePOS.PMap.get(barcodeDataArray).get(0));
        		AddtoGrid(LocalPMap);
        	}catch(Exception E){
        		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);
				
					final View view = new View(context);
					alertDialogBuilder.setView(view);
//					// set title
//					alertDialogBuilder.setTitle("Setting up your System");

					// set dialog message
					alertDialogBuilder
						.setTitle(getString(R.string.ItemNotFound))
						.setCancelable(false)
						.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, closes
								dialog.dismiss();
							}
						  });
					
						// create alert dialog
						final AlertDialog alertDialog = alertDialogBuilder.create();
						alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
						    @Override
						    public void onShow(DialogInterface dialog) {
						        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
						        btnPositive.setHeight(100);
						        btnPositive.setTextSize(20);
						        btnPositive.setText("OK");
						        view.setFocusable(true);
						        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
						        params.height = 0;
						        params.width = 0;
								view.setLayoutParams(params);
								view.requestFocus();
						    }
						});
						alermThread = new Thread(new Runnable() {
							@Override
							public void run() {
								while(alertDialog.isShowing()){
									toneG.startTone(ToneGenerator.TONE_SUP_ERROR, 300); 
									try {
										Thread.sleep(500);
									} catch (InterruptedException e) {
										
										e.printStackTrace();
									}
								}
							}
						});

						// show it
						alertDialog.show();
						alermThread.start();
        	}
    }

    @Override
    public void didBarcodeReaderImpossible() {
    	Log.d("bar", "didBarcodeReaderImpossible");
    }

    @Override
    public void didBarcodeReaderConnect() {
    	Log.d("bar", "c0nnect");
    }

    @Override
    public void didBarcodeReaderDisconnect() {
    }

    @Override
    public void didAccessoryConnectSuccess() {
    	Log.d("bar", "c0nnect");
    }

    @Override
    public void didAccessoryConnectFailure() {
    	Log.d("bar", "didBarcodeReaderImpossible21");
    }

    @Override
    public void didAccessoryDisconnect() {
    }
    
    @Override
    public void didPrinterImpossible() {
        Log.d("print","Printer Impossible.");

    }

    @Override
    public void didPrinterOnline() {
        Log.d("print","Printer Online.");
    }

};

private void starIoExtManagerConnect() {
    AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask<Void, Void, Boolean>() {
        @Override
        protected void onPreExecute() {
          
        }

        @Override
        protected Boolean doInBackground(Void... params) {
        	
            mStarIoExtManager.disconnect();
            return mStarIoExtManager.connect();
        }

        @Override
        protected void onPostExecute(Boolean result) {
          

            if (!result) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

                dialogBuilder.setTitle("Communication Result");
                dialogBuilder.setMessage("failure.\nPrinter is offline.");
                dialogBuilder.setPositiveButton("OK", null);

                dialogBuilder.show();
            }
        }
    };

    asyncTask.execute();
}

public static void sendCommands(byte[] commands, StarIOPort port, Context context) {
    

    try {
        if (port == null) {
            
        }
//        StarPrinterStatus status;
//
//        status = port.beginCheckedBlock();
//
//        if (status.offline) {
//            throw new StarIOPortException("A printer is offline");
//        }

        port.writePort(commands, 0, commands.length);

//
//        if (status.coverOpen) {
//            throw new StarIOPortException("Printer cover is open");
//        }
//        else if (status.receiptPaperEmpty) {
//            throw new StarIOPortException("Receipt paper is empty");
//        }
//        else if (status.offline) {
//            throw new StarIOPortException("Printer is offline");
//        }

    }
    catch (StarIOPortException e) {
    	
    }

}

private void sendCommands(final byte[] data) {
    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
      
        @Override
        protected Void doInBackground(Void... params) {
            
                sendCommands(data, mStarIoExtManager.getPort(),context);     // 10000mS!!!
            
			return null;
        }

        
    };

    task.execute();
}

private void printKitchen() throws StarIOPortException{
	String printerpref = PreferenceManager.getDefaultSharedPreferences(context).getString("kitchentprint", "");
	if(printerpref.equals("SNBC/Partner Tech")){
		if(interface_wifi != null){
			ReceiptBuilder rb = new ReceiptBuilder(orders,"\n",accounts.getText().toString(), context,token);
			final ByteArrayOutputStream output = new ByteArrayOutputStream();	
			try {
		      	for (ArrayList<String> arr: rb.printout)
						output.write(arr.get(0).getBytes());
		      	output.write(PrinterCommands.FEED_PAPER_AND_CUT);
	//				pos_wifi = new POSSDK(interface_wifi);
					byte[] arr = output.toByteArray();
					output.close();
					pos_wifi.textSelectFontMagnifyTimes(5, 5);
					pos_wifi.textSelectFont(0, 0x08);
					pos_wifi.textStandardModeAlignment(1);
					pos_wifi.textPrint(Integer.toString(token).getBytes(),Integer.toString(token).getBytes().length);
					pos_wifi.textStandardModeAlignment(2);
					pos_wifi.textSelectFontMagnifyTimes(1, 1);
					pos_wifi.textPrint(arr, arr.length);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
			}
			token++;
			CSSharedPreferences.setToken(token);
		}
	}else{
		if(ports != null){
				ReceiptBuilder rb = new ReceiptBuilder(orders,"\n",accounts.getText().toString(), context,token);
				for (ArrayList<String> arr: rb.printout){
						ports.writePort(arr.get(0).getBytes(), 0, arr.get(0).getBytes().length);
				}
				ports.writePort(PrinterCommands.STARFEED_PAPER_AND_CUT, 0, PrinterCommands.STARFEED_PAPER_AND_CUT.length);
				token++;
				CSSharedPreferences.setToken(token);
			}
	}
	
	
}
class ClientThread implements Runnable {
	 ArrayList<Map<String, Object>> data;
	public ClientThread( ArrayList<Map<String, Object>> data){
		this.data = data;
	}
	@Override
	public void run() {
		Socket sockets = null;
		try {
			InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

			sockets = new Socket(serverAddr, SERVERPORT);
			ObjectOutputStream oos = new ObjectOutputStream(sockets.getOutputStream());
			oos.writeObject(data);
			oos.flush(); 
			
			BufferedReader in = new BufferedReader(new InputStreamReader(sockets.getInputStream()));
			while (!Thread.currentThread().isInterrupted()) {
                String serverMessage = in.readLine();

                if (serverMessage != null) {
                    //call the method messageReceived from MyActivity class
                    Log.d("MESSAGE", serverMessage);
                    ServerEmail = serverMessage;
                }
                serverMessage = null;

            }
			oos.close();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.
            try {
				sockets.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }

	}
	
	
}
public void sendData(String change){
	if(!SERVER_IP.isEmpty()){
		if(m_adapter.getCount() != 0){
			ArrayList<Map<String, Object>> temp = new ArrayList<Map<String, Object>>(m_adapter.getData());
			temp.get(0).put("subtotalString", m_adapter.getTotalBalanceAfterWholeInvoiceDiscount());
			temp.get(0).put("taxString", m_adapter.getTotalTaxAdded());
			temp.get(0).put("grandString", m_adapter.getTotalGrandBalanceAfterWholeInvoiceDiscount());
			
			new Thread(new ClientThread(temp)).start();
		}else if(change.isEmpty()){
			ArrayList<Map<String, Object>> temp = new ArrayList<Map<String, Object>>(m_adapter.getData());
			new Thread(new ClientThread(temp)).start();
		}else if(!change.isEmpty()){
			HashMap<String, Object> LocalPMap = new HashMap(CornerStorePOS.PMap.get("000000000000").get(0));	
			LocalPMap.put("change", change);
			ArrayList<Map<String, Object>> temp = new ArrayList<Map<String, Object>>();
			temp.add(LocalPMap);
			new Thread(new ClientThread(temp)).start();
		}
		
		
		
	}
}


@Override
public void sendDataToCFD() {
	// TODO Auto-generated method stub
	sendData("");
}

protected class IdleTimer
{
    private Boolean isTimerRunning;
    private IIdleCallback idleCallback;
    private int maxIdleTime;
    private Timer timer;

    public IdleTimer(int maxInactivityTime, IIdleCallback callback)
    {
        maxIdleTime = maxInactivityTime;
        idleCallback = callback;
    }

    /*
     * creates new timer with idleTimer params and schedules a task
     */
    public void startIdleTimer()
    {
        timer = new Timer();            
        timer.schedule(new TimerTask() {

            @Override
            public void run() {             
                idleCallback.inactivityDetected();
            }
        }, maxIdleTime);
        isTimerRunning = true;
    }

    /*
     * schedules new idle timer, call this to reset timer
     */
    public void restartIdleTimer()
    {
        stopIdleTimer();
        startIdleTimer();
    }

    /*
     * stops idle timer, canceling all scheduled tasks in it
     */
    public void stopIdleTimer()
    {
        timer.cancel();
        isTimerRunning = false;
    }

    /*
     * check current state of timer
     * @return boolean isTimerRunning
     */
    public boolean checkIsTimerRunning()
    {
        return isTimerRunning;
    }
}

protected interface IIdleCallback
{
    public void inactivityDetected();
}

}
