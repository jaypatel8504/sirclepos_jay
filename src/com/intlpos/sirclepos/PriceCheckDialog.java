package com.intlpos.sirclepos;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Tax;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
/**
 * Customized Dialog for Price Check
 * @author Long
 *
 */
public class PriceCheckDialog extends Dialog{
	private Context context;
	private TextView itemNo, priceText, itemDesc, tax1Name, tax1Value, tax2Name, tax2Value, tax3Name, tax3Value,
					 total;
	private EditText barcodeEdiText;
	private Button submit;
	private String barcode = null;
	private HashMap<String,ArrayList<HashMap<String,Object>>> pMap;
	public PriceCheckDialog(Context context, HashMap<String,ArrayList<HashMap<String,Object>>> pMap) {
		super(context);
		this.context = context;
		this.pMap = pMap;
		// TODO Auto-generated constructor stub
	}
	
	public PriceCheckDialog(Context context, HashMap<String,ArrayList<HashMap<String,Object>>> pMap, String barcode) {
		super(context);
		this.context = context;
		this.pMap = pMap;
		this.barcode = barcode;
	}
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.price_check_dialog);
        getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_white));
        itemNo = (TextView) findViewById(R.id.item_no);
        submit = (Button) findViewById(R.id.submit);
        priceText = (TextView) findViewById(R.id.price);
        itemDesc = (TextView) findViewById(R.id.item_desc);
        tax1Name = (TextView) findViewById(R.id.tax1_name);
        tax1Value = (TextView) findViewById(R.id.tax1_value);
        tax2Name = (TextView) findViewById(R.id.tax2_name);
        tax2Value = (TextView) findViewById(R.id.tax2_value);
        tax3Name = (TextView) findViewById(R.id.tax3_name);
        tax3Value = (TextView) findViewById(R.id.tax3_value);
        total = (TextView) findViewById(R.id.total);
        barcodeEdiText = (EditText) findViewById(R.id.barcode);
		tax1Name.setText(Tax.tax_desc[0]+":");
		tax2Name.setText(Tax.tax_desc[1]+":");
		tax3Name.setText(Tax.tax_desc[2]+":");
		if(barcode != null){
			SearchEvent(barcode);
		}
		barcodeEdiText.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
		        if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN) {
//		        	Toast.makeText(context, "scan", Toast.LENGTH_SHORT).show();
		        	SearchEvent(barcodeEdiText.getText().toString());
		        }
		        return false;
			}
		});
		submit.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				SearchEvent(barcodeEdiText.getText().toString());
			}
		});

	}
	
	private void SearchEvent(String barcode){
		HashMap<String,Object> item;
		if(pMap.get(barcode) == null){
			Toast.makeText(context, R.string.NoItem, Toast.LENGTH_SHORT).show();
		}
		else if(pMap.get(barcode).size() > 1){
			Toast.makeText(context, R.string.MoreThanOne, Toast.LENGTH_SHORT).show();
		}else{
			item = pMap.get(barcode).get(0);
			itemNo.setText(item.get("item_no").toString());
			BigDecimal price = (BigDecimal) item.get("price");
			priceText.setText(price.toString());
			itemDesc.setText(item.get("item_name").toString());
			BigDecimal tax1 = BigDecimal.ZERO,tax2 = BigDecimal.ZERO,tax3 = BigDecimal.ZERO;
			if((boolean) item.get("tax1")){
				tax1 = price.multiply(Tax.tax_rate1).setScale(2,BigDecimal.ROUND_HALF_EVEN);
				tax1Value.setText(tax1.toString());
			}else{
				tax1Value.setText("0.00");
			}
			if((boolean) item.get("tax2")){
				tax2 = price.multiply(Tax.tax_rate2).setScale(2,BigDecimal.ROUND_HALF_EVEN);
				tax2Value.setText(tax2.toString());
			}else{
				tax2Value.setText("0.00");
			}			
			if((boolean) item.get("tax3")){
				tax3 = price.multiply(Tax.tax_rate3).setScale(2,BigDecimal.ROUND_HALF_EVEN);
				tax3Value.setText(tax3.toString());
			}else{
				tax3Value.setText("0.00");
			}
			total.setText(price.add(tax1).add(tax2).add(tax3).setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
			
		}
	}
}
