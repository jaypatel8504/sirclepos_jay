package com.intlpos.sirclepos;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import com.intlpos.sirclepos.R;
import com.intlpos.database.PaymentAdapter;
import com.intlpos.database.PaymentType;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class PaymentFragment extends Fragment {

	private Button btn_close;
	private EditText balance_edit;
	private BigDecimal balance;
	private BigDecimal change;
	private KeyPadFragment keypadFrag;
	private boolean hasKeypad = false;
	private Button cash,credit,debit,giftcard,ebt,toAccount;
	private Button balanceDue;
//	private Button hide;
	private Button five,ten,twenty,fifty;
	public Button roundup;
	public TextView amount;
	public TextView balanceRemaine;
	private OnPaymentFinishedListener mListener;
	private ListView listview;
	private ArrayList<PaymentType> payments;
	private String name;
	private View header;
	private PaymentAdapter adapter;
	private int custId;
	private int width, height;
	private String ebtTotal;
	public TextView ebtMax, taxEbt;
	private BigDecimal ebtAmount, taxEx[];
	private boolean hasEbt;
	public ArrayList<PaymentType> getPayments() {
		return payments;
	}

	public void setPayments(ArrayList<PaymentType> payments) {
		this.payments = payments;
	}
	
	private String tax[];
	private BigDecimal taxEbtRemove;
	
    static PaymentFragment newInstance(BigDecimal balance, int custId, String name, Boolean hasEbt, String EbtTotal,String tax[]) {
    	PaymentFragment newFragment = new PaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("balance", balance);
        bundle.putInt("id", custId);
        bundle.putString("name", name);
        bundle.putBoolean("ebt", hasEbt);
        bundle.putString("ebtTotal",EbtTotal);
        bundle.putStringArray("ebtTax", tax);
        newFragment.setArguments(bundle);
        return newFragment;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey("balance")){
        	balance = (BigDecimal) args.getSerializable("balance");
        	custId = args.getInt("id");
        	name = args.getString("name");
        	hasEbt = args.getBoolean("ebt");
        	ebtTotal = args.getString("ebtTotal");
        	tax = args.getStringArray("ebtTax");
        }
        else{
        	balance = BigDecimal.ZERO;
        	custId = 1;
        	name = "";
        }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.payment_fragment, container, false);
		ebtAmount = new BigDecimal(ebtTotal);
		taxEbtRemove = BigDecimal.ZERO;
		taxEx = new BigDecimal[3];
		int i = 0;
		for(String x: tax){
			taxEx[i] = new BigDecimal(x);
			BigDecimal temp = new BigDecimal(x);
			taxEbtRemove = taxEbtRemove.add(temp);
			i++;
		}
		Log.d("remove", taxEbtRemove.toString());
		final FragmentManager fm = getChildFragmentManager();
		keypadFrag = KeyPadFragment.newInstance(balance);
		fm.beginTransaction().add(R.id.keypad_fragment,keypadFrag, "keypad").show(keypadFrag).commit();
		header = inflater.inflate(R.layout.payment_listviewheader, null);
		initView(v);
//		if(hasEbt)
			getScreenResolution(v);
		String[] types = getResources().getStringArray(R.array.paymenttype_array);
		payments = new ArrayList<PaymentType>();
//		for (String str: types){
//			PaymentType payment = new PaymentType(str,BigDecimal.ZERO,"",0,"");
//			payments.add(payment);
//		}

		listview = (ListView) v.findViewById(R.id.payment_listview);
		adapter = new PaymentAdapter(this.getActivity().getBaseContext(),listview ,payments);
		listview.addHeaderView(header);
		listview.setAdapter(adapter);
		return v;
	}
	
	private void getScreenResolution(View v)
	{
//		TableRow row = (TableRow) v.findViewById(R.id.remaingEBT);
//		row.setVisibility(View.VISIBLE);
//		taxEbt = (TextView)v.findViewById(R.id.afterTaxFS);
//		taxEbt.setText(taxEbt.getText().toString() + ": " + taxEbtRemove.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
//		ebtMax = (TextView)v.findViewById(R.id.fsamount);
//		String temp = ebtMax.getText().toString();
//		ebtMax.setText(temp + ": " + ebtTotal);
	    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    DisplayMetrics metrics = new DisplayMetrics();
	    display.getMetrics(metrics);
	    width = metrics.widthPixels;
	    height = metrics.heightPixels;
	    if(width == 1024 && height == 768){
	    	LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams) cash.getLayoutParams();
	    	params.height=175;
	    	cash.setLayoutParams(params);
	    	params = ( LinearLayout.LayoutParams) credit.getLayoutParams();
	    	params.height=175;
			credit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) debit.getLayoutParams();
	    	params.height=175;
			debit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) giftcard.getLayoutParams();
	    	params.height=175;
			giftcard.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) ebt.getLayoutParams();
	    	params.height=175;
			ebt.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) toAccount.getLayoutParams();
	    	params.height=175;
			toAccount.setLayoutParams(params);

	    }else if(width == 1280 && height == 800){
	    	Log.d("pay","1280x800");
	    	LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams) cash.getLayoutParams();
	    	params.height=200;
	    	cash.setLayoutParams(params);
	    	params = ( LinearLayout.LayoutParams) credit.getLayoutParams();
	    	params.height=200;
			credit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) debit.getLayoutParams();
	    	params.height=200;
			debit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) giftcard.getLayoutParams();
	    	params.height=200;
			giftcard.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) ebt.getLayoutParams();
	    	params.height=200;
			ebt.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) toAccount.getLayoutParams();
	    	params.height=200;
			toAccount.setLayoutParams(params);
	    }else if(width == 1280 && height == 752){
	    	Log.d("pay","1280x800");
	    	LinearLayout.LayoutParams params = ( LinearLayout.LayoutParams) cash.getLayoutParams();
	    	params.height=150;
	    	cash.setLayoutParams(params);
	    	params = ( LinearLayout.LayoutParams) credit.getLayoutParams();
	    	params.height=150;
			credit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) debit.getLayoutParams();
	    	params.height=150;
			debit.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) giftcard.getLayoutParams();
	    	params.height=150;
			giftcard.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) ebt.getLayoutParams();
	    	params.height=150;
			ebt.setLayoutParams(params);
			params = ( LinearLayout.LayoutParams) toAccount.getLayoutParams();
	    	params.height=150;
			toAccount.setLayoutParams(params);
	    }
	    
	}
	
	private void initView(View view) {
		// TODO Auto-generated method stub
		cash = (Button) view.findViewById(R.id.cash);
		credit = (Button) view.findViewById(R.id.credit);
		debit = (Button) view.findViewById(R.id.debit);
		giftcard = (Button) view.findViewById(R.id.giftcard);
		ebt = (Button) view.findViewById(R.id.ebt);
		toAccount = (Button) view.findViewById(R.id.toaccount);
		
		toAccount.setEnabled(true);
		
		five = (Button) view.findViewById(R.id.fivedollar);
		ten = (Button) view.findViewById(R.id.tendollar);
		twenty = (Button) view.findViewById(R.id.twentydollar);
		fifty = (Button) view.findViewById(R.id.fiftydollar);
		roundup = (Button) view.findViewById(R.id.roundup);
//		hide = (Button) view.findViewById(R.id.hide_paymentfragment);
		amount = (TextView) view.findViewById(R.id.balance_remain_text);
		SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String nickel = preference.getString("nickel", "");
		String bd = balance.toString();
		balanceRemaine = (TextView) view.findViewById(R.id.balance_remain);
		if(nickel.equals("1")){
			if(!bd.substring(bd.length() - 1).equals("0") || !bd.substring(bd.length() - 1).equals("5"))
			{
//				 BigDecimal result =  new BigDecimal(Math.ceil(balance.doubleValue() * 20) / 20);
//				 amount.setText("Amount Remaining/Cash Remaining:");
//				 balanceRemaine.setText("      "+balance+"/"+result.setScale(2, RoundingMode.HALF_UP).toString());
				PaymentFragmentListener payListener = new PaymentFragmentListener(this.getActivity(),balance
						,keypadFrag,this,mListener,payments,adapter,custId, name);
				 amount.setText("Amount Remaining/Cash Remaining:");
				 balanceRemaine.setText("      "+PaymentFragmentListener.getBalanceText(balance)+"/"+PaymentFragmentListener.getBalanceText(payListener.nearestnickel(balance)));
			}else
				balanceRemaine.setText(PaymentFragmentListener.getBalanceText(balance));
		}else
			balanceRemaine.setText(PaymentFragmentListener.getBalanceText(balance));
		roundup.setText(PaymentFragmentListener.getBalanceText(balance.setScale(0, BigDecimal.ROUND_UP)));
	}

	private void setListener(OnClickListener payListener) {
		// TODO Auto-generated method stub
		cash.setOnClickListener(payListener);
		credit.setOnClickListener(payListener);
		debit.setOnClickListener(payListener);
		giftcard.setOnClickListener(payListener);
		ebt.setOnClickListener(payListener);
		toAccount.setOnClickListener(payListener);
		five.setOnClickListener(payListener);
		ten.setOnClickListener(payListener);
		twenty.setOnClickListener(payListener);
		fifty.setOnClickListener(payListener);
		roundup.setOnClickListener(payListener);
//		hide.setOnClickListener(payListener);
	}

	@Override
	public void onResume() {
        super.onResume();
		Log.d("edittext", (balance_edit == null) +"");
		
		balance_edit = keypadFrag.getEdittext();
		balance_edit.setText(PaymentFragmentListener.getBalanceText(balance));
		PaymentFragmentListener payListener = new PaymentFragmentListener(this.getActivity(),balance
					,keypadFrag,this,mListener,payments,adapter,custId,name);
		setListener(payListener);
    } 
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
			mListener = (OnPaymentFinishedListener) activity;
		}catch(ClassCastException e){
			throw new ClassCastException(activity.toString()
					+ " must implement OnPaymentFinishedListener");
		}
	}
	@Override
	public void onDetach(){
		super.onDetach();
		mListener = null;
	}
	public interface OnPaymentFinishedListener{
		public void onPaymentFinished(boolean finished,BigDecimal change, boolean hasCash);
	}
	
}