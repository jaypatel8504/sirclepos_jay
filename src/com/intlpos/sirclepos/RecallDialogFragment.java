package com.intlpos.sirclepos;


import java.util.ArrayList;
import java.util.List;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ButtonParam;

import android.R.color;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class RecallDialogFragment extends DialogFragment {
	private DisplayMetrics dm;
//	private Context context = this;
	private final static int VIEW_MARGIN=2;
	private final static int BUTTON_MARGIN=10;
	private static final String TAG = "123";
	public final static int btnWidth = 100;
	public final static int btnHeight = 50;
	// var for viewPager
	private ArrayList<Fragment> fragmentsList;
	private ArrayList<ArrayList<ButtonParam>> btn_list;
	private int viewTag;
	private MyFragmentPagerAdapter adapter= null;
	private RadioGroup dotGroupButton = null;
	private ArrayList<ArrayList<Button>> btns_list;
	private ViewPager viewPager;
	private List<View> viewList;
	private View view;
	private String[] item_name;
	private int[] item_id;
    private Button submit,cancel;
	//private OnViewUpdateListener mCallback;
	private boolean hasMeasured;
	int screenWidth;
	int screenHeight;
	private String dialogTitle;
    private OnMySubmitButtonClickListener mListener;
    
    static RecallDialogFragment newInstance(String[] item_name,int[] item_id,String dialogTitle) {
    	RecallDialogFragment newFragment = new RecallDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("item_name", item_name);
        bundle.putIntArray("item_id", item_id);
        bundle.putString("dialogTitle", dialogTitle);
        newFragment.setArguments(bundle);
        return newFragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey("item_name")){
        	item_name = args.getStringArray("item_name");
        	item_id = args.getIntArray("item_id");
        	dialogTitle = args.getString("dialogTitle");
        }
        else 
        	item_name = new String[3];
        Log.d("Popup", "Button # = "+item_name.length);
        
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.mydialogfragment, container, false);
        screenWidth  = convertToPx(500);
        screenHeight  = convertToPx(500-80);
		Log.d("DIM", "Wid = "+screenWidth+" Hei = "+ screenHeight);
			// Wrote initView as a function!
			initFragViewParams(view,item_name,item_id);
        	initFragView(view);
//			viewPager.setOffscreenPageLimit(3);
       return view;
	}
	
//	@Override
//	public Dialog onCreateDialog(Bundle savedInstanceState) {
////		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.DialogStyle);
//		Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);  
//		  
//	    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
//		View view = inflater.inflate(R.layout.mydialogfragment, null, false);
//		dm = new DisplayMetrics();
//		this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
//		int densityDpi = dm.densityDpi;
////		int screenWidth = dm.widthPixels;
////		int screenHeight = dm.heightPixels;
//        int screenWidth = convertToPx(300);
//        int screenHeight = convertToPx(300);
////		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(screenWidth, screenHeight);
////		Log.d("DIM", "Wid = "+lp.width+" Hei = "+ lp.height);
//
////		lp.width = screenWidth;
////		lp.height = screenHeight;
//		view.setLayoutParams(lp);
//
//		
//		int buttonsPerPage = (screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)) *
//				(screenHeight/(btnHeight+VIEW_MARGIN));
//				
////		int buttonsPerLine = screenWidth/(buttonWidth+VIEW_MARGIN +BUTTON_MARGIN);
////		int linesPerPage = screenHeight/(buttonWidth+VIEW_MARGIN);
//		viewList = new ArrayList<View>();
//
//        final String sTitle[] = new String[count];
//		int total_buttons = sTitle.length;
//		int pages = (int) Math.ceil((double) total_buttons/buttonsPerPage);
//	
//	     // create buttons dynamically
//		Log.d(TAG, "Btn#= "+ total_buttons + " Btn/Page= "+buttonsPerPage+ " Page#= "+pages);
//		
//		btn_list = new ArrayList<ArrayList<ButtonParam>>();
//		ArrayList<ButtonParam> btn_params = new ArrayList<ButtonParam>();
//        int count = 0;
//        for (int i = 0; i<sTitle.length; i++){
//        	if (i == 0){
//        		btn_params.add(new ButtonParam("KeyBoard",R.drawable.keyboard, color.background_light));
//        	}
//        		 
//        	else{
//        		if (i % 2 == 0)
//        			btn_params.add(new ButtonParam("Button" + i, i, R.drawable.ic_launcher, Color.MAGENTA));
//        		else
//        			btn_params.add(new ButtonParam(i + "Button", i, R.drawable.bg_1, Color.LTGRAY));
//        		
//        	}
//			count++;
//			if (count == buttonsPerPage){
//				btn_list.add(btn_params);
//				btn_params = new ArrayList<ButtonParam>();
//				count = 0;
//			}
//		}
//        
//        //add the last several strings
//        if (btn_params.size() != 0)
//        	btn_list.add(btn_params);
//
//			
//			// Wrote initView as a function!
//        	initFragView(view);
//      
//		
////			viewPager.setOffscreenPageLimit(3);
//        	dialog.setContentView(view);
////        	builder.setView(view);
//       return dialog;
//	}
	public void initFragViewParams(View v,String[] item_name, int[] item_id){
		getDialog().getWindow().setTitle(dialogTitle);
		getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.shape));
		getDialog().setCanceledOnTouchOutside(false);
		//getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//		dm = new DisplayMetrics();
//		this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		int buttonsPerPage = (screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)) *
				(screenHeight/(btnHeight+VIEW_MARGIN));
		

		
//		int buttonsPerLine = screenWidth/(btnWidth+VIEW_MARGIN +BUTTON_MARGIN);
//		int linesPerPage = screenHeight/(btnWidth+VIEW_MARGIN);
		viewList = new ArrayList<View>();

        final String sTitle[] = item_name;
		int total_buttons = sTitle.length;
		int pages = (int) Math.ceil((double) total_buttons/buttonsPerPage);
	
	     // create buttons dynamically
		Log.d(TAG, "Btn#= "+ total_buttons + " Btn/Page= "+buttonsPerPage+ " Page#= "+pages);
		
		btn_list = new ArrayList<ArrayList<ButtonParam>>();
		ArrayList<ButtonParam> btn_params = new ArrayList<ButtonParam>();
        int count = 0;
        for (int i = 0; i<sTitle.length; i++){
        	btn_params.add(new ButtonParam(sTitle[i], i,item_id[i], 0, Color.MAGENTA));
			count++;
			if (count == buttonsPerPage){
				btn_list.add(btn_params);
				btn_params = new ArrayList<ButtonParam>();
				count = 0;
			}
		}
        
        //add the last several strings
        if (btn_params.size() != 0)
        	btn_list.add(btn_params);

	}
	public void initFragView(View v){
		viewPager = (ViewPager) v.findViewById(R.id.testViewPager);//Layout of viewPager
		Log.d(TAG, "ID of ViewPager = "+ viewPager.getId());
		dotGroupButton = (RadioGroup) v.findViewById(R.id.dotGroupButton);
		fragmentsList = new ArrayList<Fragment>();
		for (int i = 0; i < btn_list.size();i++){
			// Create a MyViewGroup
			ArrayList<ButtonParam> list = btn_list.get(i);
			int size = list.size();
			
			String[] str_arr = new String[size];
			int[] img_arr = new int[size];
			int[] color_arr = new int[size];
			float[] prices = new float[size];
			int[] id_arr = new int[size];
			for (int j = 0 ; j < size; j++){
				str_arr[j] = list.get(j).getText();
				id_arr[j] = list.get(j).getItem_id();
				img_arr[j] = list.get(j).getDrawable();
				color_arr[j] = list.get(j).getBgColor();
				prices[j] = list.get(j).getPrice();
			}
//			Log.d(TAG, "Len of String arr = "+ array.length);
			Fragment mFragment = TestDialogFragment.newInstance(btnWidth,btnHeight,str_arr,id_arr,prices,img_arr,color_arr);
			//set button for each page
			final RadioButton dotButton = new RadioButton(v.getContext());
			dotButton.setId(i);
			dotButton.setLayoutParams(new RadioGroup.LayoutParams(
					RadioGroup.LayoutParams.WRAP_CONTENT,
					RadioGroup.LayoutParams.WRAP_CONTENT));
			dotButton.setPadding(20, 20, 20, 20);
//			dotButton.setBackgroundResource(R.drawable.dot_bg);//background of each page
			dotButton.setButtonDrawable(R.drawable.dot_bg);
			
			dotButton.setTag(i);//set tag for each page's listener
			
			dotButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						viewPager.setCurrentItem((Integer)dotButton.getTag(),true);
					}
				}
			});
			Log.d(TAG, "dotGroupButton = "+ dotGroupButton);
			dotGroupButton.addView(dotButton);
			dotGroupButton.check(0);
			fragmentsList.add(mFragment);
		}
		
//	    adapter = new MyFragmentPagerAdapter(fm,fragmentsList);
//		viewPager.setAdapter(adapter);
//		adapter.notifyDataSetChanged();
		// set Page Change Listener (notice data changed to the adapter!)
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {				

			@Override
			public void onPageSelected(int position) {
				((RadioButton)dotGroupButton.getChildAt(position)).setChecked(true);
//				viewPager.findViewWithTag(position);
				//adapter.notifyDataSetChanged();// load new page while page changing
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		
		// submit and cancel button 

        submit = (Button) v.findViewById(R.id.submit_select);
        cancel = (Button) v.findViewById(R.id.cancel_select);
        submit.setOnClickListener(new Button.OnClickListener() {
    	    @Override
    	        public void onClick(View v) {
    	    	ArrayList<Integer> itemIdList = new ArrayList<Integer>();
    	    	for (Fragment frag:fragmentsList){
    	    		MyImageButton[] btn_list = ((TestDialogFragment) frag).button;
    	    		for (MyImageButton btn:btn_list){
    	    			Log.d("All_Id", btn.getItem_id()+"");
    	    			if (btn.selected)
    	    				itemIdList.add(btn.getItem_id());
    	    		}    	    		
    	    	}

    	    	mListener.onMySubmitButtonClick(itemIdList);
    	    	dismiss();
    	       }

    	     });
        cancel.setOnClickListener(new Button.OnClickListener() {
    	    @Override
    	        public void onClick(View v) {
    	    	dismiss();
    	       }

    	     });
	}
	

	    @Override
	    public void onViewCreated(View view, Bundle savedInstanceState) {
	        super.onViewCreated(view, savedInstanceState);

	        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.testViewPager);
	        adapter = new MyFragmentPagerAdapter(getChildFragmentManager(),fragmentsList);
	        mViewPager.setAdapter(adapter);
	    }
	    
	    public void updateFragView(String[] item_name){
	    	this.item_name = item_name;
	    }
	    
	    public int convertToPx(int input) {
            // Get the screen's density scale
            final float scale = getResources().getDisplayMetrics().density;
            // Convert the dps to pixels, based on density scale
            return (int) (input * scale + 0.5f);
    }
	@Override
    public void onResume()
	    {
	        super.onResume();
	        screenWidth  = convertToPx(500);
	        screenHeight  = convertToPx(500);
	        Window window = getDialog().getWindow();
	        window.setLayout(screenWidth, screenHeight);
	        window.setGravity(Gravity.CENTER);
	        //TODO:
	    } 
	
	public interface OnMySubmitButtonClickListener{
		public void onMySubmitButtonClick(ArrayList<Integer> mylist);		
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
		mListener = (OnMySubmitButtonClickListener) activity;
		} catch(ClassCastException e){
			throw new ClassCastException(activity.toString() 
					+ "must implement OnMyButtonClickListener();");
		}
	}
}


