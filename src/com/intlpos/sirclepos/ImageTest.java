package com.intlpos.sirclepos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



import java.util.concurrent.ExecutionException;

import com.intlpos.sirclepos.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

public class ImageTest extends Activity{
	ImageView image;
	Bitmap bm;
	@Override
	protected void onCreate(Bundle saved){
		super.onCreate(saved);
		setContentView(R.layout.imagetest);
		image = (ImageView)findViewById(R.id.img1);
		bm = getSavedImage();
		image.setImageBitmap(bm);
	}
	
	private File getDataFolder(Context context) {
		File dataDir = null;
	        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
	        	dataDir = new File(Environment.getExternalStorageDirectory(), "ImageFolder");
	            if(!dataDir.isDirectory()) {
	            	dataDir.mkdirs();
	            }
	        }
	        
	        if(!dataDir.isDirectory()) {
	        	dataDir = context.getFilesDir();
	        }
	        
		return dataDir;
	}
	
	private Bitmap getSavedImage(){
    	File cacheDir = getDataFolder(getApplicationContext());
    
    	File cacheFile = new File(cacheDir, "imagefile");
    	
    	InputStream fileInputStream = null;
		try {
			Log.d("ITS WORKING", "YAY");
			fileInputStream = new FileInputStream(cacheFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.d("ITS NOt WORKING", "NAY");
			e.printStackTrace();
		}
    	BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
    
    	bitmapOptions.inJustDecodeBounds = false;
    	Bitmap wallpaperBitmap = BitmapFactory.decodeStream(fileInputStream, null, bitmapOptions);
    	
    	return wallpaperBitmap;
    }

}
