package com.intlpos.sirclepos;

import org.json.JSONObject;

import com.intlpos.WCFAccess.StarListener;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;

import android.os.AsyncTask;
import android.widget.Toast;

public class StarEthernet extends AsyncTask<String, Void, Object> {
	private StarListener star;
	public StarEthernet(StarListener star){
		this.star = star;
	}
	
	@Override
	protected Object doInBackground(String... params) {
		// TODO Auto-generated method stub
		 StarIOPort port = null;
		 try {
			 	int i = 0;
			 	
			 	String portName = "TCP:"+params[0];
			 	String portNumb = "9100";
			 	while(port == null){
			 		
			 		port = StarIOPort.getPort(portName, portNumb, 10000);
			 		i++;
			 		portNumb = "910"+i;
			 	}
			} catch (StarIOPortException e) {
				// TODO Auto-generated catch block
				
				e.printStackTrace();
			}
		  
		    
		return port;
	}
	
	@Override
	protected void onPostExecute(Object result) 
	{		
		 super.onPostExecute(result);	
		 star.getStar((StarIOPort) result);
	}

}
