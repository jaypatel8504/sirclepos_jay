package com.intlpos.sirclepos;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.intlpos.sirclepos.R;
import com.intlpos.dialogs.Demo;
import com.intlpos.initsetup.StartScreen;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

public class DemoOrRegister extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.demo_or_register);
		Button demo = (Button) findViewById(R.id.demo);
		demo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment frag = Demo.newInstance();
				frag.show(getSupportFragmentManager(), "demo");
			}
		});
		Button register = (Button) findViewById(R.id.register);
		register.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(DemoOrRegister.this, RegisterTablet.class);
                startActivity(i);
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		
	}
	
}
