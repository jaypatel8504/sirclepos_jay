package com.intlpos.sirclepos;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManager;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManagerListener;

public class Star {
	private StarIoExtManager mStarIoExtManager;
	private Context context;
	
	public Star(StarIoExtManager mStarIoExtManager, Context context) {
		super();
		this.mStarIoExtManager = mStarIoExtManager;
		this.context = context;
	}

	public void starIoExtManagerConnect() {
        AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected void onPreExecute() {
              
            }

            @Override
            protected Boolean doInBackground(Void... params) {
            	
                mStarIoExtManager.disconnect();
                return mStarIoExtManager.connect();
            }

            @Override
            protected void onPostExecute(Boolean result) {
              

                if (!result) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

                    dialogBuilder.setTitle("Communication Result");
                    dialogBuilder.setMessage("failure.\nPrinter is offline.");
                    dialogBuilder.setPositiveButton("OK", null);

                    dialogBuilder.show();
                }
            }
        };

        asyncTask.execute();
    }

	public void sendCommands(byte[] commands, StarIOPort port, Context context) {
        

        try {
            if (port == null) {
                
            }

            port.writePort(commands, 0, commands.length);

        }
        catch (StarIOPortException e) {
        	
        }

    }

	public	 void sendCommands(final byte[] data) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
          
            @Override
            protected Void doInBackground(Void... params) {
                
                    sendCommands(data, mStarIoExtManager.getPort(),context);     // 10000mS!!!
                
    			return null;
            }

            
        };

        task.execute();
    }
	

    public StarIoExtManagerListener mStarIoExtManagerListener = new StarIoExtManagerListener() {
        @Override
        public void didBarcodeDataReceive(byte[] data) {
            String barcodeDataArray = new String(data);

        }

        @Override
        public void didBarcodeReaderImpossible() {
        	Log.d("bar", "didBarcodeReaderImpossible");
        }

        @Override
        public void didBarcodeReaderConnect() {
        	Log.d("bar", "c0nnect");
        }

        @Override
        public void didBarcodeReaderDisconnect() {
        }

        @Override
        public void didAccessoryConnectSuccess() {
        	Log.d("bar", "c0nnect");
        }

        @Override
        public void didAccessoryConnectFailure() {
        	Log.d("bar", "didBarcodeReaderImpossible21");
        }

        @Override
        public void didAccessoryDisconnect() {
        }
        
        @Override
        public void didPrinterImpossible() {
            Log.d("print","Printer Impossible.");

        }

        @Override
        public void didPrinterOnline() {
            Log.d("print","Printer Online.");
        }

    };
}
