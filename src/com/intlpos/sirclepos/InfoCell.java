package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InfoCell extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.backoffice);
		WebView myWebView = (WebView) findViewById(R.id.back);
		myWebView.setWebViewClient(new WebViewClient());
		
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.loadUrl("http://smart360pos.com/sirclereg/Home.aspx");
	}
}
