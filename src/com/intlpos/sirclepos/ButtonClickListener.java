package com.intlpos.sirclepos;

public class ButtonClickListener 
{
	public interface OnButtonClickedListener {
	    public void onButtonClicked(); 
	}
}
