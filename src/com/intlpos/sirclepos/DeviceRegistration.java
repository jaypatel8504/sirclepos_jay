package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;


import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Product;
import com.intlpos.global.AboutDialog;
import com.intlpos.global.Utils;
import com.intlpos.initsetup.FragmentContainerActivity;
import com.intlpos.login.EulaDialog;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

public class DeviceRegistration extends Activity {

	private static final int ABOUT = 0;
	private Button mRegisterButton;
	private EditText mEmail;
	private EditText mPassword;
	private EditText mStoreId;

	// while doing stationid popup window
	private EditText mStationId;
	private Button mCreateStationButton;
	private PopupWindow StationPopUp;
	
	String StoreId;
	String StationId;
	String EmailId;
	String Password;
	String MACAddress;
	//for recording the updates
	int VersionCode ;
	String VersionName;
	private TextView mContactUs;
	private CornerStorePOS app;

	/** called when the activity is first created */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_registration);
		app = (CornerStorePOS) getApplication();
		setWidgetReferences();
		addWidgetListeners();
		new EulaDialog(this).show();
	}

	private void addWidgetListeners() {
		mRegisterButton.setOnClickListener(new RegisterButtonListener());

	}

	private void setWidgetReferences() {
		mEmail = (EditText) this.findViewById(R.id.email);
		mPassword = (EditText) this.findViewById(R.id.password);
		mStoreId = (EditText) this.findViewById(R.id.storeid);
		// mStationId = (EditText)this.findViewById(R.id.stationid);
		mRegisterButton = (Button) this.findViewById(R.id.btnregister);
		mContactUs = (TextView) this.findViewById(R.id.contactus);
		//set Hyperlink
		String str = getString(R.string.contactus);
//		String email = "tech@internationalpointofsale.com";
//		SpannableString sp = new SpannableString(str);
//		sp.setSpan(new URLSpan(email), str.indexOf(email), str.indexOf(email)+email.length(),  
//                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);  
//		mContactUs.setText(sp); 
//		mContactUs.setMovementMethod(LinkMovementMethod.getInstance());  
		mContactUs.setText(str); 
		mContactUs.setAutoLinkMask(Linkify.ALL); 
		mContactUs.setMovementMethod(LinkMovementMethod.getInstance()); 
	}

	private class RegisterButtonListener implements OnClickListener {

		@Override
		public void onClick(View aV) {

			// Call webservice here , if webservice returns success then we can
			// store this detail into SharedPreperences

			// String url =
			// "http://10.0.0.31/accounts/check_credentials.svc/account";

			// GlobalSetup.ReadVariables();
			EmailId = mEmail.getText().toString();
			Password = mPassword.getText().toString();
			StoreId = mStoreId.getText().toString();
			// String MACAddress = Utils.getMACAddress("wlan0");
			
			MACAddress = Utils.getMACAddress("eth0");
			try {
				VersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
				VersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Log.d("MACAddress", "" + MACAddress);

			// need to define URL publicly as It is going to change for actual
			// hosting

			String methodName = "check_credentials.svc/registration";

			LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
			params.put("email_id", EmailId);
			params.put("password", Password);
			params.put("store_id", StoreId);
			params.put("mac_address", MACAddress);
			params.put("version_code", String.valueOf(VersionCode));
			params.put("version_name",VersionName);

			JSONParser jParser = new JSONParser(new MyListener() {

				@Override
				public void onResult(JSONObject json) {

					try {
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if (result == true) {
							//setPrefrences();
							// open up one pop-up to set the station id
							initiatepopupwindow();
							
							
						//	DeviceRegistration.this.finish();//try activityname.finish instead of this
							
						
							// setPrefrences();
							// connect_to_storeDB // may be I would want to put
							// this at Employee login Screen 's Page Load
						} else {
							Toast.makeText(
									DeviceRegistration.this,
									"Device Registration Failed \n"
											+ resultString, Toast.LENGTH_SHORT)
									.show();
						}

					} catch (JSONException e) {
						Log.e("JSONException", "" + e.toString());
					} catch (Exception e) {
						Log.e("DeviceReg", "" + e);
					}

				}
			});

			jParser.execute(app.Url, methodName, params);

		}

	}

	private void initiatepopupwindow() {
		LayoutInflater inflater = (LayoutInflater) DeviceRegistration.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.station_create,
				(ViewGroup) findViewById(R.id.stationpopup));

		StationPopUp = new PopupWindow(layout, 400, 400, true);
		StationPopUp.showAtLocation(layout, Gravity.CENTER, 0, 0);

		mStationId = (EditText) layout.findViewById(R.id.StationId);
		mCreateStationButton = (Button) layout
				.findViewById(R.id.CreateStaionButton);
		;
		mCreateStationButton.setOnClickListener(new CreateButtonListener());

	}

	private class CreateButtonListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			// call webservice's createstation function

			String methodName = "check_credentials.svc/createstation";

			StoreId = mStoreId.getText().toString();
			StationId = mStationId.getText().toString();
			EmailId = mEmail.getText().toString();
			Password = mPassword.getText().toString();
			
			LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
			params.put("store_id", StoreId);
			params.put("station_id", StationId);

			JSONParser jParser = new JSONParser(new MyListener() {

				@Override
				public void onResult(JSONObject json) {

					try {
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if (result == true) {
							
							Toast.makeText(DeviceRegistration.this,
									R.string.stationidset, Toast.LENGTH_LONG)
									.show();
							//StationPopUp.dismiss();
							setPrefrences();
							app.ReadVariables();

							Intent intent = new Intent();
							intent=new Intent(getBaseContext(),FragmentContainerActivity.class);						
					        startActivity(intent);
//							Intent i = new Intent(DeviceRegistration.this,EmployeeLogin.class);
//							i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
//					        startActivity(i);	
					        
//							Intent intent = new Intent(Intent.ACTION_MAIN);
//							//intent.addCategory(Intent.CATEGORY_LAUNCHER);
//							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//							startActivity(intent);
//							
//							Intent i = new Intent(DeviceRegistration.this, MainLogin.class);
//							startActivity(i);
						} else {
							Toast.makeText(DeviceRegistration.this,
									resultString, Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {
						Log.e("JSONException", "" + e.toString());
					} catch (Exception e) {
						Log.e("DeviceReg", "" + e);
					}

				}
			});

			jParser.execute(app.Url, methodName, params);

		}
	}

	private void setPrefrences() throws NameNotFoundException {
		StoreDetail storedetail = new StoreDetail();		
		storedetail.setnotfirsttime(true);
		storedetail.setemail_id(EmailId);
		storedetail.setpassword(Password);
		storedetail.setstoreid(StoreId);
		storedetail.setstationid(StationId);		
		CSSharedPreferences.savePreferences(storedetail);
		
	//	CSSharedPreferences.saveSettingsPreferences(storedetail);
		
		storedetail.setVersion_code(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
		storedetail.setVersion_name(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
		
		CSSharedPreferences.saveUpdatePreferences(storedetail);
		
		Log.d("StoreDetail", "" + storedetail.isCombine_lines());
		// else - I need to stay at the same registration form giving
		// message that its not successful

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,ABOUT,0,"About");
		return true;
		}


		public boolean onOptionsItemSelected (MenuItem item){
		switch (item.getItemId()){
		case ABOUT:
		AboutDialog about = new AboutDialog(this);
		about.setTitle("Do you need Help?");
		about.show();

		break;
		}
		return true;
		}
	
}
