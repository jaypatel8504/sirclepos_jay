package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;
import com.intlpos.initsetup.ReceiptDetails;
import com.intlpos.initsetup.USSalesTax;
import com.intlpos.storemaintenance.Deparment;
import com.intlpos.storemaintenance.Employee;

import android.app.Activity;
import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Build;

public class BackOfficeActivity extends FragmentActivity implements ReceiptDetails.OnFragmentSwitchListener {
	final String[] backOfficeName = {"employee","taxrate","Email Setup"};
	private Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_back_office);
		this.context = this;
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			final View rootView = inflater.inflate(R.layout.fragment_back_office,
					container, false);
			final ListView listview = (ListView) rootView.findViewById(R.id.backoffice_listview);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(),
		              android.R.layout.simple_list_item_1, android.R.id.text1, backOfficeName);
			listview.setAdapter(adapter);
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {    

					 FragmentManager fm = ((FragmentActivity) context).getSupportFragmentManager();
	                  switch (position){
	                  case 0:
	                	  break;
	                  case 1:
	                	  Fragment taxrate = new USSalesTax();
	                	  onFragmentSwitch(taxrate);
	                	  break;
	                  }
				}

   
            }); 
			
			return rootView;
		}
	}
	
	@Override
	public void onFragmentSwitch(Fragment fragment) {
		FragmentManager fm = getSupportFragmentManager();
		Fragment mContent = fm.findFragmentById(R.id.frag_container);
		if(mContent != fragment) {
			fm.popBackStack();
            fm.beginTransaction()
                .replace(R.id.frag_container, fragment) 
                .addToBackStack(null)
                .commit();
        }		
	}

	@Override
	public void onPressed(PreviewFragment frag) {
		// TODO Auto-generated method stub
		final android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();	
		ft.replace(android.R.id.content,frag, "NewFragmentTag");
		ft.commit();
		
	}
}
