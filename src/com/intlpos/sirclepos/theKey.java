package com.intlpos.sirclepos;

import android.os.Parcel;
import android.os.Parcelable;


public  class theKey {
	private String arrayKey[];
	private String arrayValues[];
	
	public theKey(String ak[], String av[]){
		arrayKey = ak;
		arrayValues = av;
	}
	
	public String[] getArrayKey() {
		return arrayKey;
	}
	public void setArrayKey(String[] arrayKey) {
		this.arrayKey = arrayKey;
	}
	public String[] getArrayValues() {
		return arrayValues;
	}
	public void setArrayValues(String[] arrayValues) {
		this.arrayValues = arrayValues;
	}
	

}
