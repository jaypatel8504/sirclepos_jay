package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.intlpos.WCFAccess.GetSampleCSV;
import com.intlpos.sirclepos.R;
import com.intlpos.initsetup.AddInventory;
import com.intlpos.initsetup.PostRequestTask;
import com.intlpos.initsetup.PostSaveSample;
import com.intlpos.initsetup.TaskFinished;
import com.john.beans.CSVProduct;
import com.john.beans.StoreDetail;
import com.john.file.DirChooserDialog;
import com.john.file.FileAdapter;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class PreviewFragment extends Fragment implements OnClickListener{
	private ListView list;
	private Context context;
	protected DirChooserDialog dlg;
	private int productCount;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	protected static final String ADD_URL = "InventoryImport/ImportService.svc/importinventory";
	protected static final String GET_URL = "InventoryImport/ImportService.svc/getproductscount";
	protected static final String[] HEADERS = {"item_no","item_name","cost","price","item_stock","tax_rate1","tax_rate2","tax_rate3","department_name","re_order_level"};
	private ArrayList<CSVProduct> csvlist;
	private FileAdapter m_adapter ;
	private String url;
	private Button exit, save;
	private String webResult;
	private ArrayList<ArrayList<String>> result;
	private boolean setup;
	boolean full = false;
	public PreviewFragment(String url, boolean setup){
		this.url = url;
		this.setup = setup;
	}
	
	class CSVRequest{
		StoreDetail storedetail;
		ArrayList<CSVProduct> inventorylist;
		public CSVRequest(StoreDetail storedetail, ArrayList<CSVProduct> inventorylist){
			this.storedetail = storedetail;
			this.inventorylist = inventorylist;
		}
	}
	
	private class GetProductCountRequest{
		StoreDetail storedetail;
		public GetProductCountRequest(StoreDetail storedetail){
			this.storedetail = storedetail;
		}
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_csv_import, container, false);
		Button open = (Button)view.findViewById(R.id.opencsv);
		open.setVisibility(View.GONE);
		exit = (Button) view.findViewById(R.id.finish);
		save = (Button) view.findViewById(R.id.submit);
		exit.setOnClickListener(this);
		save.setOnClickListener(this);
		list = (ListView) view.findViewById(R.id.csvlistview);
		View v = (View)inflater.inflate(R.layout.csvfilegrid_header, null);
		list.addHeaderView(v);
		context =getActivity();
		try {
			result = new GetSampleCSV(url,getActivity()).execute().get();
			

	    	ArrayList<String> headers = result.get(0);
			if(headers.size() != HEADERS.length){
				Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//				return;
			}
			for(int i = 0, length = headers.size(); i < length; i++){
				if (!headers.get(i).equals(HEADERS[i])){
					Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//					return;
				}
			}
			result.remove(0);
			try{
				for(ArrayList<String> list:result){
					if(list.get(0).isEmpty() || list.get(1).isEmpty()){
//						showAlertDialog("Item_Name and Item_No cannot be Null");
					}
					if(list.get(2).isEmpty())
						list.set(2, "0.00");
					if(list.get(3).isEmpty())
						list.set(3, "0.00");
					if(list.get(4).isEmpty())
						list.set(4, "0.00");
//					if(!isNumeric(list.get(2)) || !isNumeric(list.get(3)) || !isNumeric(list.get(4))){
//						showAlertDialog("Price, Cost and Stock should be a Number");
//					}
					if(list.get(5).isEmpty())
						list.set(5, "true");
					if(list.get(6).isEmpty())
						list.set(6, "false");
					if(list.get(7).isEmpty())
						list.set(7, "false");
					if(list.get(8).isEmpty())
						list.set(8, "none");
					if(list.size() == 9)
						list.add("10");
					list.add("/Date(" + System.currentTimeMillis() + ")/");
				}
				
				csvlist = new ArrayList<CSVProduct>();
				for(ArrayList<String> list: result){
					try{
					CSVProduct product = new CSVProduct(list);
					csvlist.add(product);
					}catch(Exception e){
						Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//						return;
					}
				}
				m_adapter = new FileAdapter(context, result);
				list.setAdapter(m_adapter);
				
				if(csvlist.size() + productCount > 6000){
					Toast.makeText(context, "Product list cannot more than 6000", Toast.LENGTH_SHORT).show();
//					return;
				}
				
			}catch(Exception e){
				
			}
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return view;
	}
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch(arg0.getId()){
		case R.id.finish:
			if(!setup){
				getActivity().finish();
				Intent i = new Intent(getActivity(), Settings.class);
				startActivity(i);
			}else{
				goBack back = (goBack) getActivity();
				back.goBack();
			}
			break;
		case R.id.submit:
			
			PostSaveSample postTask = new PostSaveSample(new TaskFinished(){
				@Override
				public void onTaskEnded(String result) {
					// TODO Auto-generated method stub
					String resultInt = null;
			    	resultInt =  result;
			    	try{
			    		productCount = Integer.parseInt(resultInt);
			    		Log.d("Product",Integer.toString(productCount));
			    	}catch(Exception e){
			    		Toast.makeText(context, resultInt+"Cannot get products number.", Toast.LENGTH_SHORT).show();
			    	}
			    	if(csvlist.size() + productCount > 6000){
			    		full = true;
						Toast.makeText(context, "Product list cannot more than 6000", Toast.LENGTH_SHORT).show();
						
						return;
					}else{
						CornerStorePOS app = (CornerStorePOS) ((Activity) context).getApplication();
						Gson gsons = new Gson();
						PostSaveSample postTasks = new PostSaveSample(new TaskFinished(){
							@Override
							public void onTaskEnded(String result) {
								// TODO Auto-generated method stub
								String result1 = result;
			//					
			//		    	} catch (InterruptedException | ExecutionException e) {
			//					e.printStackTrace();
			//				}
					    	JSONObject response = null;
					    	try {
					    		response = new JSONObject(result1);
					        	if(response.getBoolean("result"))
					        		Toast.makeText(context, "CSV File Import Successful!", Toast.LENGTH_SHORT).show();
					        	else{
					        		Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
					        	}
							} catch (JSONException e) {
								e.printStackTrace();
								Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
							}
							}
						}, getActivity());
						Log.d("APP", csvlist.get(1).getItem_name());
						app = (CornerStorePOS) ((Activity) context).getApplication();
						CSVRequest csvrequest = new CSVRequest(app.storedetail,csvlist);
			
				    	postTasks.execute(CornerStorePOS.Url+ADD_URL,gsons.toJson(csvrequest));
					}
				}
			},getActivity());
			
			Gson gson = new Gson();
			CornerStorePOS app = (CornerStorePOS) ((Activity) context).getApplication();
			GetProductCountRequest getcountrequest = new GetProductCountRequest(app.storedetail);
			Log.d("getCount", gson.toJson(getcountrequest));
	    	postTask.execute(CornerStorePOS.Url+GET_URL,gson.toJson(getcountrequest));
	    	
	    	break;
			
		}
	}
	
	public interface goBack{
		public void goBack();
	}
	
}
