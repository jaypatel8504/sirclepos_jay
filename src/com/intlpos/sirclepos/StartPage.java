package com.intlpos.sirclepos;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.usb.UsbDevice;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import com.intlpos.WCFAccess.GetDailyReports;
import com.intlpos.sirclepos.R;
import com.intlpos.database.ReceiptDetail;
import com.intlpos.database.Tax;
import com.intlpos.global.AboutDialog;
import com.intlpos.global.AppRater;
import com.intlpos.global.HelpDialog;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.PaymentProcessing;
import com.intlpos.mysharedpreferences.PreviousStartTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.sqldatabase.DepartmentDataSource;
import com.intlpos.sqldatabase.EmployeeDataSource;
import com.intlpos.sqldatabase.PermissionDataSource;
import com.intlpos.storemaintenance.Zout;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.zj.usbsdk.UsbController;

public class StartPage extends Activity {

	private static final int ABOUT = 0;
	private static final int HELP = 1;
	Button btn[] = new Button[5];
//	TextView mStoreName;
	TextView mEmployeeName;
	SharedPreferences pref;
	private PrintDataService printDataService = null; 
	private ScreenOffReceiver mybroadcast;
	private UsbController usbCtrl;
	private UsbDevice dev;
	private CornerStorePOS app;
//	private ImageView help;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	String sales[];
	String dept[];
	int payKey[];
	String payment[];
	String full[];
	String lekey[];
	String leValue[];
	String lekey1[];
	String leValue1[];
	String KEY_key[];
	String KEY_val[];
	theValues theVal[];
	int sizesOfDepart[];
	int totalSize;
	int sizeOfValArray;
	ProgressDialog ringProgressDialog;
	private static String TAG = "StartPage";
	private TableLayout tb;
	private BroadcastReceiver wifi;
	
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.newstartpage);	
//		AppRater.app_launched(this);
//		AppRater.showRateDialog(this, null);
		app = (CornerStorePOS) getApplication();
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		ReceiptDetail.setreceiptdetail();
		setWidgetReferences();
		GetAndSaveImage get = new GetAndSaveImage();
		get.execute(CSSharedPreferences.getPic());
		Log.d("PICTURE", CSSharedPreferences.getPic());
		Tax.settaxrates();
		
  		addWidgetListeners();
  		StoreDetail sq = CSSharedPreferences.getIpandPort();
  		Log.d("ingencio", sq.getIp() + "  "+ sq.getPort());
  		
		float size = new Button(this).getTextSize();
		Log.d("font size", size+" ");
//  		mStoreName.setText(ReceiptDetail.store_name);
	}

	private void getSharedReference() {
		// TODO Auto-generated method stub
		if (pref.getString("list_printer_type", "").equals("1")){
			int[] usb_port = new int[2];
			usb_port[0] = pref.getInt("usbport1", 0);
			usb_port[1] = pref.getInt("usbport2", 0);
			usbCtrl = new UsbController(this,mHandler);
			dev = usbCtrl.getDev(usb_port[0],usb_port[1]);
			if(dev != null){
				if( !(usbCtrl.isHasPermission(dev))){
					//Log.d("usb debug","request USB device permission.");
					usbCtrl.getPermission(dev);
				}else{
					Toast.makeText(getApplicationContext(), "Connecte to Usb Printer successfully!",
	                        Toast.LENGTH_SHORT).show();
				}
			}else{
				Toast.makeText(getApplicationContext(), "Cannot find Usb Printer",
                        Toast.LENGTH_SHORT).show();
			}
		}else if (pref.getString("list_printer_type", "").equals("0")){
			String address = pref.getString("list_bluetooth_printer", "");
			if (address == ""){
				Toast.makeText(getApplicationContext(), "Cannot find Bluetooth Printer", Toast.LENGTH_SHORT).show();
				return;
			}
			printDataService = new PrintDataService(getApplicationContext(),address);
			printDataService.connect();
		}
	}

	private final  Handler mHandler = new Handler() {
		@Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case UsbController.USB_CONNECTED:
            	Toast.makeText(getApplicationContext(), getString(R.string.msg_getpermission),Toast.LENGTH_SHORT).show();
            	break;
            default:
            	break;
            }
        }
    };
	private TextView stationId;
	private void setWidgetReferences() {
//		mStoreName = (TextView)findViewById(R.id.store_name);
		mEmployeeName = (TextView)findViewById(R.id.welcome);
		btn[0] = (Button)findViewById(R.id.pos_button);
		btn[1]=(Button)findViewById(R.id.backoffice_button);
		btn[2] = (Button)findViewById(R.id.settings_button);
		btn[3] = (Button)findViewById(R.id.logout_button);
		btn[4] = (Button)findViewById(R.id.suggestbox);
		stationId = (TextView) findViewById(R.id.station_id);
		stationId.setText(getString(R.string.station_id)+" "+CornerStorePOS.StationId);
//		help = (ImageView)findViewById(R.id.imageView2);
		
	}
	private void addWidgetListeners()
	{
		for(int i =0;i<5;i++){
			btn[i].setOnClickListener(new ButtonsListener());
		}	
		
//		help.setOnClickListener(new ButtonsListener());
	}
	private class ButtonsListener implements OnClickListener {
		
		@Override
		public void onClick(View v) {
			connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			Intent intent = null;
			try{
				unregisterReceiver(mybroadcast);
			}catch(IllegalArgumentException e){
				Log.d("InvoiceHome", "onStop");
			}
		switch(v.getId()){
		case R.id.pos_button:
			intent=new Intent(getBaseContext(),InvoiceHome.class);						
	        startActivity(intent); 
			break;
		case R.id.backoffice_button:
			Uri uri = Uri.parse("http://backoffice.sirclepos.com/");   
			intent = new Intent(Intent.ACTION_VIEW,uri);    
			startActivity(intent);    
			break;
		case R.id.suggestbox:
			DialogFragment newdialog = Suggestions.newInstance();
        	newdialog.show(getFragmentManager(), "suggest");
			break;
		case R.id.settings_button:
			intent = new Intent();
        	intent.setClass(StartPage.this, Settings.class);
    		startActivity(intent);
			break;
		case R.id.logout_button:
			intent = new Intent();
	        intent.setClass(StartPage.this, EmployeeLogin.class);
	    	startActivity(intent);
	    	finish();
			break;
		}
		}
	}
	
	
	private void setPreferenceEndTime(){
		StartTime startTime = CSSharedPreferences.loadStartTime();
		
		PreviousStartTime prevTime = new PreviousStartTime();
		prevTime.setPrevTime(startTime.getTime());
		CSSharedPreferences.savePrevTimePreferences(prevTime);
		
		
		CSSharedPreferences.setIsLoggedOut(true);
		
		EndTime end = new EndTime();
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mma");
		String currentDateandTime = sdf.format(new Date());
		
		end.setendTime(currentDateandTime);
		Log.d("STARTT TIME: ", "" + startTime.getTime());
		Log.d("END TIME: ", "" + end.getEndTime());
		CSSharedPreferences.saveEndTimePreferences(end);
		
		Log.d("THE TIME LOGGED IN WAS: ", "" + prevTime.getPrevTime());
		
	}
	
	public void ZoutReportShow() {	
		
	   String Array[] = new String[sales.length + full.length];
	   System.arraycopy(sales, 0, Array, 0, sales.length);
	   System.arraycopy(full, 0, Array, sales.length, full.length);
	  
	   String VALUE_key[] = new String[sizeOfValArray];
	   String VALUE_val[] = new String[sizeOfValArray];
	   
	   int count2 = 0;
	   sizesOfDepart = new int[theVal.length];
	   for(int i = 0; i< theVal.length; i++){
		   String temp[] = theVal[i].getKey();
		   String tempForVal[] = theVal[i].getValue();
		   
		   for(int j = 0; j < temp.length; j++){
			   VALUE_key[count2] = temp[j];
			   VALUE_val[count2] = tempForVal[j];
			   count2++;
		   }
		   if(i == 0){
			   sizesOfDepart[i] = count2;
		   }
		   else{
			   int size = 0;
			   for(int p = 0; p <sizesOfDepart.length; p++)
				   size += sizesOfDepart[i];
			   sizesOfDepart[i] = (count2 - size);
		   }
	   }
	   
	   
	   totalSize = count2;
	   Log.d("totalSize", ""+totalSize);
	   for(int i = 0; i< sizesOfDepart.length; i++){
		   Log.d("sizesOfDepart", ""+sizesOfDepart[i]);
	   }
	   Log.d("KEY_key:", ""+KEY_key.length);
	   DialogFragment newFragment = ZoutDialog.newInstance(Array, KEY_key, KEY_val, VALUE_key, VALUE_val, sizesOfDepart, totalSize, true, true);
	   newFragment.show(getFragmentManager(), "hello");
	   
	}
	
	public void parseResult(String result){
	
		//Daily Report
		sales = new String[9];
		try {
			
			JSONArray values = new JSONArray(result);
			for(int i =0; i < values.length();i++)
			{
				JSONObject total = values.getJSONObject(i);
				if(total.getString("value").equals("")){
					 sales[i] = "0.00";
				}
				else{
					sales[i] = total.getString("value");	
				}
					Log.d("The value", sales[i]);
			}
			
		} 
		catch (JSONException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(NullPointerException e){
			Log.d(TAG, "NULL");
		}
			
		
		
		
	}
	
	public void parseResult2(String result)
	{
		try {
			payKey = new int[7];
			payment = new String[7];
			Log.d("The pay", result);
			
			full = new String[7];
			
			for (int i = 0; i< full.length;i++){
				full[i] = "0.00";
			}
			if(result.length() > 2){
				JSONArray values = new JSONArray(result);
				
				for(int i =0; i < values.length();i++)
				{
					JSONObject total = values.getJSONObject(i);
					String temp = total.getString("Key");
					payKey[i] = Integer.parseInt(temp);
					payment[i] = total.getString("Value");
						
						Log.d("The payTotals", payment[i]);
				}
				
				
				for(int i = 0; i< payKey.length;i++){
					int temp = payKey[i]-1 ;
						
					if(temp > 0){
						full[temp] = payment[i];}
					
					else{
						full[payKey[i]] = payment[i];
					}
						
					Log.d("FULL", full[i]);
				}
			}
			
			
		} catch (JSONException e) {
				e.printStackTrace();
			}catch(NullPointerException e){
				Log.d(TAG, "NULL");
			}
	}
	
	public void parseResult3(String result)
	{
		
		try {
			
			JSONArray values = new JSONArray(result);
			JSONObject total[] = new JSONObject[values.length()];
			for(int i = 0; i < values.length();i++){
				total[i] = values.getJSONObject(i);
			}
			
			JSONArray[] Keyvalues = new JSONArray[values.length()];
			JSONArray[] Valuesval = new JSONArray[values.length()];
			
			for(int i = 0; i < values.length();i++){
				Keyvalues[i] = new JSONArray(total[i].getString("Key"));
				Valuesval[i] = new JSONArray(total[i].getString("Value"));
			}
			
			lekey =  new String[4];
			leValue = new String[4];
			 KEY_key = new String[Keyvalues.length*4];
			 KEY_val = new String[Keyvalues.length*4];
			
			int count = 0;
			for(int j = 0; j < Keyvalues.length; j++)
			{
				
				for(int i = 0; i <4; i++){
					JSONObject totalKey = Keyvalues[j].getJSONObject(i);
					KEY_key[count] =  totalKey.getString("key");
					KEY_val[count] = totalKey.getString("value");	
					count++;
				}
				
				
				
			}			
			
			theVal = new theValues[Valuesval.length];
			sizeOfValArray = 0;
			for(int j = 0; j < Valuesval.length; j++)
			{
				lekey1 =  new String[Valuesval[j].length()];
				leValue1 = new String[Valuesval[j].length()];
				sizeOfValArray+=Valuesval[j].length();
				
				for(int i = 0; i <Valuesval[j].length(); i++){
					
					JSONObject totalKey = Valuesval[j].getJSONObject(i);
					lekey1[i] =  totalKey.getString("key");
					leValue1[i] = totalKey.getString("value");
				}
				theVal[j] = new theValues(lekey1, leValue1);
			}
			Log.d("theVal", ""+theVal.length);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(NullPointerException e){
			Log.d(TAG, "NULL");
		}
	}
	
	public void showProgressBar() {
		ringProgressDialog = ProgressDialog.show(StartPage.this, "Please wait", "Loading...");	
	}
	
    @Override
    public void onStart() {
        super.onStart();
		mybroadcast = new ScreenOffReceiver();
		registerReceiver(mybroadcast, new IntentFilter(Intent.ACTION_SCREEN_OFF));
    }
    @Override
    public void onResume() {
        super.onResume();
		mEmployeeName.setText(getResources().getString(R.string.welcome) + " " + app.employee.first_name+" "+app.employee.last_name+" !");
		
//		getSharedReference();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
//    	Log.d("InvoiceHome", "onStop");
    	try {
    		   if (mybroadcast != null) {
    		     unregisterReceiver(mybroadcast);
    		   }
    		} catch (IllegalArgumentException e) {
    		    Log.i("StartPage","epicReciver is already unregistered");
    		    mybroadcast = null;
    		} // unregister broadcast receiver, avoid memory leak.
    }
    @Override
    public void onPause(){
    	super.onPause();
    	if (printDataService != null && printDataService.connect())
    		PrintDataService.disconnect();
    }
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,ABOUT,ABOUT,"About");
		menu.add(0,HELP,HELP,"Help");
		return true;
		}

    @Override
		public boolean onOptionsItemSelected (MenuItem item){
		switch (item.getItemId()){
			case ABOUT:
			AboutDialog about = new AboutDialog(this);
			about.setTitle("About us");
			about.show();	
			break;
			case HELP:
				HelpDialog help = new HelpDialog(this);
				help.setTitle("Do you need Help?");
				help.show();		
			break;
		}
		return true;
		}
    
    private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getFragmentManager(), "hello");
	
	}
    
    
    private  class GetAndSaveImage extends AsyncTask<String, Integer, Bitmap> {
	 	String url;
	 	
	 	
		@Override
		protected Bitmap doInBackground(String... params) {
		
			String newUrl = params[0];
			String newest = null;
			String bestURl = null;
			if(newUrl.contains("\\\\")){
				newest = "http://www."+ newUrl.replace("\\", "/");
				bestURl = newest.replace(" ", "%20");
				Log.d("NEWEST", newest);
			}
			URL url;
			try {
				url = new URL(bestURl);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			    connection.setDoInput(true);
			    connection.connect();
			    InputStream input = connection.getInputStream();
			    Bitmap myBitmap = BitmapFactory.decodeStream(input);
			    return myBitmap;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	
		
		@SuppressLint("NewApi")
		@Override
		protected void onPostExecute(Bitmap result){
			if(result != null){
				setImage(result);
				for(int i = 0; i < 5; i++){
					if(i !=3){
						int sdk = android.os.Build.VERSION.SDK_INT;
						if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
							btn[i].setBackgroundDrawable(getResources().getDrawable(R.drawable.startpagebutton));
						} else {
							btn[i].setBackground(getResources().getDrawable(R.drawable.startpagebutton));
						}
						
						btn[i].setTextColor(Color.BLACK);
					}
				}
			}
		}
		
		
		
	}
	
	@SuppressLint("NewApi")
	private void setImage(Bitmap result){
		LinearLayout layout = (LinearLayout) findViewById(R.id.background);
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
			layout.setBackgroundDrawable(new BitmapDrawable(getResources(),result));
		}else{
			layout.setBackground(new BitmapDrawable(getResources(),result));
		}
    	//iv.setScaleType(ScaleType.FIT_CENTER);
	}
   
}