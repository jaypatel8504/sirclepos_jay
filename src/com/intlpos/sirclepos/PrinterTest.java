package com.intlpos.sirclepos;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.intlpos.sirclepos.R;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.john.bluetoothprinter.helper.PrinterCommands;
import com.john.bluetoothprinter.helper.ReceiptBuilder;
import com.partner.pt100.printer.PrinterApiContext;
import com.partner.pt100.printer.PrinterManage;
import com.starmicronics.stario.StarIOPort;
import com.starmicronics.stario.StarIOPortException;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManager;
import com.starmicronics.starioextension.starioextmanager.StarIoExtManagerListener;

public class PrinterTest extends Activity {
	  PrintDataService printDataService;
	  private PrinterApiContext printApiContext;
	  private StarIoExtManager mStarIoExtManager;
	  private Star star;
	  private SharedPreferences preference;
	  private ByteArrayOutputStream output;	
	  private ReceiptBuilder rb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.printtest);
        preference = PreferenceManager.getDefaultSharedPreferences(this);
        output = new ByteArrayOutputStream();
        rb = new ReceiptBuilder(PrinterTest.this);
        getSharedReference();
        Button button = (Button) findViewById(R.id.print);
        button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String print= preference.getString("printerType", "");
		    	if(print.equals("Star mPop")){
		    		if(mStarIoExtManager != null){
			    		for (ArrayList<String> arr: rb.printout){
							star.sendCommands(arr.get(0).getBytes());
			        	}
			    		star.sendCommands(PrinterCommands.STARFEED_PAPER_AND_CUT);
		    		}
		    	}else if(print.equals("Partner Tech")){
		    		if(printApiContext != null){
		    			for (ArrayList<String> arr: rb.printout){
		    				printApiContext.printData(arr.get(0).getBytes(), arr.get(0).getBytes().length);
			        	}
		    		}
		    	}else{
				  if(printDataService.isConnected){
			        	for (ArrayList<String> arr: rb.printout){
			        		try {
								output.write(printDataService.sendwithCommand(arr.get(0), Integer.parseInt(arr.get(1))));
							} catch (NumberFormatException | IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			        	}
			        	printDataService.sendByteArray(output.toByteArray());
				    
			        }
			}
		    }
		});
      
       
    }
    
    
    
    private void getSharedReference() {
    	String address = preference.getString("list_bluetooth_printer", "");
    	String print= preference.getString("printerType", "");
    	if(print.equals("Star mPop")){
    		mStarIoExtManager = new StarIoExtManager(StarIoExtManager.Type.Standard, "BT:"+preference.getString("list_bluetooth_printer", ""), "", 10000, this);
	        star = new Star(mStarIoExtManager,getApplicationContext());
	        mStarIoExtManager.setListener(star.mStarIoExtManagerListener);
    		star.starIoExtManagerConnect();
    	}else if(print.equals("Partner Tech")){
    		printApiContext = new PrinterManage();
    		printApiContext.open();
    		printApiContext.initPrinter();
    	}else{
    		if (address == ""){
        		Toast.makeText(getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
        		printDataService = null;
       			return;
       		}else{
    	   		printDataService = new PrintDataService(getApplicationContext(),address);
    	   		printDataService.connect();
    	   		
       		}
    	}
    	
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    }
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	if (printDataService != null && printDataService.isConnected)
    		PrintDataService.disconnect();
    	
    }
}