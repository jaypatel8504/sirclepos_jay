package com.intlpos.sirclepos;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import com.intlpos.sirclepos.R;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListPopupWindow;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class SubTotalFragment extends Fragment{
	public ListPopupWindow getListPopupWindow() {
		return listPopupWindow;
	}

	private ListPopupWindow listPopupWindow;
	private Button moreDetails;
	public Button getMoreDetails() {
		Log.d("HELLO MOREDETAILS", "moredetails");
		return moreDetails;
	}

//	private Button balanceDue;
	private String[] details = null;

//	public SubtotalFragment() {
//		// Required empty public constructor
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		details = getResources().getStringArray(R.array.subtotal_details_array);
		View view = inflater.inflate(R.layout.subtotal_fragment, container, false);
		moreDetails = (Button) view.findViewById(R.id.more);
		
//		balanceDue = (Button) view.findViewById(R.id.balance);
		listPopupWindow = new ListPopupWindow(this.getActivity());
	        listPopupWindow.setAdapter(new ArrayAdapter<String>(
	        		this.getActivity(),
	            R.layout.moredetail_listitem, details));
	        listPopupWindow.setAnchorView(moreDetails);
//	        listPopupWindow.setHorizontalOffset(200);
//	        listPopupWindow.setVerticalOffset(-30);
	        float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
	        listPopupWindow.setWidth((int) pixels);
	        listPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

	        listPopupWindow.setModal(true);
		return view;
	}

	public String[] getDetails() {
		return details;
	}

	public void setDetails(String[] details) {
		this.details = details;
	}


}
