package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.HashMap;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ImageLoader;
import com.intlpos.database.InventoryParcelable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class TestFragment extends Fragment {
	private int btnWidth;
	private int btnHeight;
    private static final String TAG = "TestFragment";
    private OnMyButtonClickListener mListener;
    //private String[] sTitle = null;// = "hello android";
    
    private String[] dept_ids = null;
    private String[] dept_names = null;
    private int[] bg_res = null;
   
    private int[] bg_color = null;
    private float[] prices = null;
	private CornerStorePOS app;
	private boolean flag;
    private String pict[];

    // Rinkal - added while Department Lookup
    static TestFragment newInstance(int width, int height, String[] deptIds,String[] deptNames,/* int[] bg_drawable,*/ int[] bg_color,String pic[]) {
        TestFragment newFragment = new TestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("btnWidth", width);
        bundle.putInt("btnHeight", height);
        bundle.putStringArray("dept_id", deptIds);
        bundle.putStringArray("dept_name",deptNames);
      //  bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        bundle.putStringArray("pic",pic);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    
    
    static TestFragment newInstance(int width, int height, String[] s,float[] prices, int[] bg_drawable, int[] bg_color) {
        TestFragment newFragment = new TestFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("btnWidth", width);
        bundle.putInt("btnHeight", height);
        bundle.putStringArray("sTitle", s);
        bundle.putFloatArray("prices",prices);
        bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    static TestFragment newInstance(String[] s, int[] bg_drawable, int[] bg_color) {
        TestFragment newFragment = new TestFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("sTitle", s);
        bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Log.d(TAG, "TestFragment-----onCreate");
        Bundle args = getArguments();
        
        bg_res = (int[]) args.getIntArray("bg_res");
        bg_color = (int[]) args.getIntArray("bg_color");
        dept_ids = (String[]) args.getStringArray("dept_id");
        dept_names = (String[]) args.getStringArray("dept_name");
      //  prices = (float[]) args.getFloatArray("prices");
        btnWidth = (int) args.getInt("btnWidth");
        btnHeight = (int) args.getInt("btnHeight");
        pict = (String[]) args.getStringArray("pic");
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
//        Log.d(TAG, "TestFragment-----onCreateView");
        MyViewGroup view = (MyViewGroup) inflater.inflate(R.layout.pager_item, container, false);
        app = (CornerStorePOS) this.getActivity().getApplication();
        int len = dept_ids == null ? 0 : dept_ids.length;
        MyImageButton[] button = new MyImageButton[len];
        for(int i = 0 ; i<len;i++){
        	boolean pic = false;
        	button[i] = new MyImageButton(this.getActivity(),true);
        	MyViewGroup.LayoutParams param = new MyViewGroup.LayoutParams(btnWidth,LayoutParams.WRAP_CONTENT);
        	button[i].setMinimumHeight(btnHeight);
        	button[i].setMinimumWidth(btnWidth);
        	button[i].setLayoutParams(param);
        	
        	if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
	        	if(!pict[i].isEmpty()){
	        		try{
	        			pic = true;
	        			//Muhammad- This loads the image button. 
	        			ImageLoader imageLoader;
	        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight, getActivity());
	        			Log.d("pict", pict[i]);
	        			imageLoader.DisplayImage(pict[i], button[i]);
	        		}catch(NullPointerException e){
	        			button[i].setBackgroundResource(R.drawable.bg_deptbutton_states);
	        		}	
	        	}
        	}
        	
	        button[i].setBackgroundResource(R.drawable.bg_deptbutton_states);
        
        	button[i].setText(dept_names[i]);
        	button[i].setTextColor(Color.BLACK);
//       
        	button[i].setId(i);
        	button[i].setTag(dept_ids[i]);
        
        	view.addView(button[i]);
        	view.setBackgroundResource(R.drawable.shape);
        	if (dept_names[i] == "KeyBoard"){
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    		Toast.makeText(getActivity(), "Popup Keyboard", Toast.LENGTH_SHORT).show();            	    	
            	       }
            	     });
        	}
        	else{
            	button[i].setOnClickListener(new Button.OnClickListener() {
					private long lastClickTime;

					@Override
            	        public void onClick(View v) {
						// preventing double, using threshold of 1000 ms
			            if (SystemClock.elapsedRealtime() - lastClickTime > 1000){
							ringDialogTask(v);
			            }
			            lastClickTime = SystemClock.elapsedRealtime();
            	       }
            	     });
        	}
        }

        return view;
    }
	
	public void ringDialogTask(final View v) {
		final ProgressDialog ringProgressDialog = ProgressDialog.show(getActivity(), "Please wait...",	"Loading...");
		ringProgressDialog.setCancelable(true);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Here you should write your time consuming task...
        	    	String index = (String) v.getTag();
        	    	Log.d("item size", "department_id = "+ index);
        	    	//displayitemlookup();
        	    	ArrayList<InventoryParcelable> mylist = new ArrayList<InventoryParcelable>();	
        	    	
        			for (int i= 0 ; i< app.PList.size(); i++)
        			{			
        				HashMap<String,Object> h = app.PList.get(i);
        				if( h.get("tsdisplay").equals(true))
        				{				
        					if(h.get("department_id").equals(index))
        					{
        						InventoryParcelable ip = new InventoryParcelable();
        						//HashMap<String,Object> myHash = new HashMap<String, Object>();
        						//myHash.put("product_id", h.get("product_id"));
        						//myHash.put("item_name", h.get("item_name"));
        						//ip.map = myHash;
        						ip.map = h;
        						mylist.add(ip);         			
        					}
        				}
        			}
        	    	
//        	    	Log.d("CSPOSDeptItemsList",""+mylist.toString());
        	    	//Toast.makeText(getActivity(), ""+mylist.size() , Toast.LENGTH_SHORT).show();
        	    	
        	    	mListener.onMyButtonClick(mylist);
					
				} catch (Exception e) {
					Log.d("InvoiceHome", "Exception = "+e.toString());
					ringProgressDialog.dismiss();
					flag = false;
				}
				ringProgressDialog.dismiss();
				flag = false;
			}
		}).start();
	}

	/*private void displayitemlookup()
	{
		
		//get fragment manager
		FragmentManager fm = getActivity().getSupportFragmentManager();
		ItemLookupFragment itemlookup = (ItemLookupFragment) fm.findFragmentById(R.id.fragment_item_lookup);
		// we might need IF in the future
		if (true){
			itemlookup = ItemLookupFragment.newInstance(7);
			FragmentTransaction ft  = fm.beginTransaction();
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.attach(itemlookup).replace(R.id.fragment_item_lookup, itemlookup);
			ft.commit();
			//Toast.makeText(getActivity(), "Item_frag is hidden "+ itemlookup.isHidden(), Toast.LENGTH_SHORT).show();
		}		
		
	}*/

	public interface OnMyButtonClickListener{
		public void onMyButtonClick(ArrayList<InventoryParcelable> mylist);		
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		try{
		mListener = (OnMyButtonClickListener) activity;
		} catch(ClassCastException e){
			throw new ClassCastException(activity.toString() 
					+ "must implement OnMyButtonClickListener();");
		}
	}
	
    @Override
    public void onDestroy() {
        super.onDestroy();
//        Log.d(TAG, "TestFragment-----onDestroy");
    }

}
