package com.intlpos.sirclepos;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Tax;
import com.intlpos.database.TaxExempt;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class TaxDialog extends Dialog implements OnCheckedChangeListener{
	
	private TextView tv[];
	private CheckBox cb[];
	private Boolean taxExempt[];
	private Button ok;
	private Context context;
	private BigDecimal tax_value[], tax_rates[], old_taxes[];
	private Boolean changed[];
	private TaxExempt isTaxedExempt;
	private String[] tax_desc = Tax.tax_desc;
	
	public TaxDialog(Context context, TaxExempt isTaxedExempt) {
		super(context);
		this.isTaxedExempt = isTaxedExempt;
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.popup_tax);
        setValues();
        getWidgets();
        setWidgets();
	}
	
	private void setValues(){
		tax_rates = new BigDecimal[3];
		tax_value = isTaxedExempt.getTax(); // Get tax values
       
		for(int i  = 0; i < tax_value.length; i++){
        	tax_rates[i] = tax_value[i];
        }
		
        if (isTaxedExempt.isChanged() == false){ //tax exempt values need to be initialize 
			taxExempt = new Boolean[3];
			for (int i = 0; i < 3; i++) {
				if (tax_value[i].compareTo(BigDecimal.ZERO) == 0)// Set it to null if  there is no tax i.e the tax is zero
					taxExempt[i] = null;
				else
					taxExempt[i] = false;// Set it to false if they're taxed
			}
		}else{
			old_taxes = isTaxedExempt.getPrevious_tax();
			taxExempt = isTaxedExempt.getIsTaxExempt();
		}
	}
	
	private void getWidgets() {
		tv = new TextView[3];
		cb = new CheckBox[3];
		ok = (Button) findViewById(R.id.oktax);
		cb[0] = (CheckBox) findViewById(R.id.tax1Check);
		cb[1] = (CheckBox) findViewById(R.id.tax2Check);
		cb[2] = (CheckBox) findViewById(R.id.tax3Check);
	}
	
	
	public void setWidgets() {
		final ArrayList<TextView> tv_array = new ArrayList<TextView>();
		tv_array.add((TextView) findViewById(R.id.tax1));
		tv_array.add((TextView) findViewById(R.id.tax2));
		tv_array.add((TextView) findViewById(R.id.tax3));
		for (int i = 0; i < 3; i++) {
			if(tax_value[i] == null)//Safety precaution in case a tax value is null
				tax_value[i] = BigDecimal.ZERO;
			
			tv[i] = tv_array.get(i);
			if(tax_value[i].compareTo(BigDecimal.ZERO) == 0)
				tv[i].setText(tax_desc[i]+ " : " + "0.00");
			else
				tv[i].setText(tax_desc[i]+ " : " + tax_value[i]);
			
			if (tax_value[i].compareTo(BigDecimal.ZERO) == 0
					&& isTaxedExempt.isChanged() == false) { // if the tax value is zero we hide the checkbox because it cannot be taxed or exempt
				cb[i].setVisibility(View.GONE);
			}else if (isTaxedExempt.isChanged() == true) { //Tax exempt values changed, need to adjust checkbox values accordingly 
				if (taxExempt[i] == null) {
					cb[i].setVisibility(View.GONE);
					
				} else if (taxExempt[i] == true) {
//						if (tax_value[i].compareTo(BigDecimal.ZERO) == 0)
							tv[i].setText(tax_desc[i] + " : 0.00");//Item is taxExempt so we show 0 for total
						if(isTaxedExempt.getPosition() == 1000)
							tv[i].setText(tax_desc[i] + " : " + tax_value[i]);//Item is taxExempt however it is parital amount of total tax. Used for tax All
					
					cb[i].setChecked(false);
					cb[i].setOnCheckedChangeListener(this);
				} else if (taxExempt[i] == false) {// Because taxExempt equals false that item is taxed
					if(tax_value[i].compareTo(BigDecimal.ZERO) == 0 && old_taxes[i].compareTo(BigDecimal.ZERO)!= 0){
						tv[i].setText(tax_desc[i]+ " : " + old_taxes[i]);
//						tax_value[i] = old_taxes[i];leave commented
						tax_rates[i] = tax_value[i];
					}

					cb[i].setChecked(true);
					cb[i].setOnCheckedChangeListener(this);
				}
			} else {// Else the user has the option to tax or not to tax an item
				cb[i].setOnCheckedChangeListener(this);
			}
		}
		
	}
	
	public Button getOK(){// returns the button so we can retrieve values
		return ok;
	}
	
	public Boolean[] getTaxes() {
		return taxExempt;
	}

	public BigDecimal[] getTaxRates() {
		return tax_rates;
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		switch (buttonView.getId()) {
		case R.id.tax1Check:
			if (!isChecked) {// An item is tax exempt if it is true (box is unchecked)
				tv[0].setText(tax_desc[0] + " : 0.00");
				taxExempt[0] = true;
			} else {
				if (isTaxedExempt.isChanged() == false) {
					tv[0].setText(tax_desc[0] + " : " + tax_value[0]);
				} else{
					if (old_taxes == null) {
						tv[0].setText(tax_desc[0] + " : " + tax_value[0]);
					} else {
						tv[0].setText(tax_desc[0] + " : " + old_taxes[0]);
						tax_rates[0] = old_taxes[0];
					}
				}
				taxExempt[0] = false;
			}
			break;
		case R.id.tax2Check:
			if (!isChecked) {
				tv[1].setText(tax_desc[1] + " : 0.00");
				taxExempt[1] = true;
			} else {
				if (isTaxedExempt.isChanged() == false) {
					tv[1].setText(tax_desc[1] + " : " + tax_value[1]);
				} else {
					if (old_taxes == null) {
						tv[1].setText(tax_desc[1] + " : " + tax_value[1]);
					} else {
						tv[1].setText(tax_desc[1] + " : " + old_taxes[1]);
						tax_rates[1] = old_taxes[1];
					}
				}
				taxExempt[1] = false;
			}
			break;
		case R.id.tax3Check:
			if (!isChecked) {
				tv[2].setText(tax_desc[2] + " : 0.00");
				taxExempt[2] = true;
			} else {
				if (isTaxedExempt.isChanged() == false) {
					tv[2].setText(tax_desc[2] + " : " + tax_value[2]);
				} else {
					if (old_taxes == null) {
						tv[2].setText(tax_desc[2] + " : " + tax_value[2]);
					} else {
						Log.d("HELLO", "OLDTAXES");
						tv[2].setText(tax_desc[2] + " : " + old_taxes[2]);
						tax_rates[2] = old_taxes[2];
					}
				}
				taxExempt[2] = false;
			}
			break;
		}
	}
}
