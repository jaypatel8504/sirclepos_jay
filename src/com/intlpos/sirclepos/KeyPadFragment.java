package com.intlpos.sirclepos;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

import com.intlpos.sirclepos.R;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

public class KeyPadFragment extends Fragment {
	private static String TAG = "KeyPadFragment";
	private EditText edittext;
	private BigDecimal totalBalance;
	private BigDecimal change;
	private boolean decimalPad;
	private boolean hasDot;
	public boolean isDecimalPad() {
		return decimalPad;
	}


	public void setDecimalPad(boolean decimalPad) {
		this.decimalPad = decimalPad;
	}

	public String str = "";
	private Button button_1;
	private Button button_2;
	private Button button_3;
	private Button button_4;
	private Button button_5;
	private Button button_6;
	private Button button_7;
	private Button button_8;
	private Button button_9;
	private Button button_0;
	public Button priceQ;
	public Button button_signal;
	public Button button_clean;
	private DecimalFormat form = new DecimalFormat("0.00");

	  public static KeyPadFragment newInstance(BigDecimal balance)
	  {
	    KeyPadFragment keypadFrag = new KeyPadFragment();
	    Bundle Bundle = new Bundle();
	    Bundle.putSerializable("balance", balance);
	    keypadFrag.setArguments(Bundle);
	    return keypadFrag;
	  }
	
	  
	  public void onCreate(Bundle paramBundle)
	  {
	    super.onCreate(paramBundle);
	    if(!Locale.getDefault().toString().equals("en_US")){
	    	Locale.setDefault(Locale.US);
	    	Log.d(TAG, ""+Locale.getDefault().toString());
	    }
	    decimalPad = true;
	    Bundle bundle = getArguments();
	    if ((bundle != null) && (bundle.containsKey("balance")))
	    	totalBalance = (BigDecimal)bundle.getSerializable("balance");
	    else{
	    	totalBalance = BigDecimal.ZERO;
	    }
	  }
	  
	public EditText getEdittext() {
		return edittext;
	}

	public void setEdittext(String str) {
		this.edittext.setText(str);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View pView = inflater.inflate(R.layout.keypad_fragment, container, false);
		
		edittext = (EditText) pView.findViewById(R.id.totalbalance);
		Log.d("edittext", edittext.getText().toString());
		clean();
		edittext.setText(totalBalance.toString());
		edittext.setInputType(0);
		button_1 = (Button) pView.findViewById(R.id.keypad_1);
		button_2 = (Button) pView.findViewById(R.id.keypad_2);
		button_3 = (Button) pView.findViewById(R.id.keypad_3);
		button_4 = (Button) pView.findViewById(R.id.keypad_4);
		button_5 = (Button) pView.findViewById(R.id.keypad_5);
		button_6 = (Button) pView.findViewById(R.id.keypad_6);
		button_7 = (Button) pView.findViewById(R.id.keypad_7);
		button_8 = (Button) pView.findViewById(R.id.keypad_8);
		button_9 = (Button) pView.findViewById(R.id.keypad_9);
		button_0 = (Button) pView.findViewById(R.id.keypad_0);
		priceQ = (Button) pView.findViewById(R.id.priceQ);
		button_signal = (Button) pView.findViewById(R.id.keypad_signal);
		button_clean = (Button) pView.findViewById(R.id.keypad_clean);
		
		if(!PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("priceQ", false)){
			priceQ.setText("P");
		}else{
			priceQ.setText("Q");
		}
		
		button_1.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {	
            	str = decimalPad ? updateStr(str,"1"):str+"1";
            	edittext.setText(str);
            }
    });
		button_2.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"2"):str+"2";
            	edittext.setText(str);
            }
    });
		button_3.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"3"):str+"3";
            	edittext.setText(str);
            }
    });
		button_4.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"4"):str+"4";
            	edittext.setText(str);
            }
    });
		button_5.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"5"):str+"5";
            	edittext.setText(str);
            }
    });
		button_6.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"6"):str+"6";
            	edittext.setText(str);
            }
    });
		button_7.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"7"):str+"7";
            	edittext.setText(str);
            }
    });
		button_8.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"8"):str+"8";
            	edittext.setText(str);
            }
    });
		button_9.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"9"):str+"9";
            	edittext.setText(str);
            }
    });
		button_0.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"0"):str+"0";
            	edittext.setText(str);
            }
    });
		priceQ.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	if(priceQ.getText().toString().equals("P")){
            		priceQ.setText("Q");
	        	}else{
	        		priceQ.setText("P");
	        	}
            }
    });
		button_signal.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = changeSignal(str);
            	edittext.setText(str);
            }
    });
		button_clean.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	clean();
            }
    });
		
		
		return pView;
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        
        
    }
	
	public BigDecimal getTotalBanlance() {
		return totalBalance;
	}


	public void setTotalBanlance(BigDecimal totalBanlance) {
		this.totalBalance = totalBanlance;
	}


	private void addListenerOnButton(final EditText edittext) {
		// TODO Auto-generated method stub
		
	}


	public void clean(){
    	str = decimalPad ? "0.00":"";
    	if (!decimalPad){
    		hasDot = false;
    	}
    	edittext.setText(str);
	}
	protected String updateStr(String str1, String str2) {
		// TODO Auto-generated method stub
		BigDecimal price = new BigDecimal(str1+str2);
		price = price.multiply(BigDecimal.TEN);
		Log.d(TAG, ""+price);
		return form.format(price.doubleValue());
	}
	protected String changeSignal(String str1) {
		// TODO Auto-generated method stub
		BigDecimal price = new BigDecimal(str1);
		price = price.negate();
		return form.format(price.doubleValue());
	}
	protected String findCeil(String str1) {
		// TODO Auto-generated method stub
		BigDecimal price = new BigDecimal(str1);
		price.setScale(2,RoundingMode.CEILING);
		return form.format(price.doubleValue());
	}
    
    
}