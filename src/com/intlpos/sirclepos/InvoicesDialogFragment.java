package com.intlpos.sirclepos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.database.InvoiceParcelable;
import com.intlpos.database.InvoicesListViewAdapter;
import com.intlpos.initsetup.PostRequestTask;
import com.john.beans.FetchInvoice;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class InvoicesDialogFragment extends DialogFragment{
	private static final SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat formatter2= new SimpleDateFormat("MM-dd-yyyy");
	private ArrayList<InvoiceParcelable> data;
	private OnInvoiceSelectedListener mListener;
	private ListView listview;
	private InvoicesListViewAdapter adapter;
	private EditText invoiceNoSearch;
	private TextView curDate;
	private Calendar date;
//	private Button next,last;
	private Button datepicker;
    
	static InvoicesDialogFragment newInstance(ArrayList<InvoiceParcelable> data,String date) {
        InvoicesDialogFragment newFragment = new InvoicesDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("invoices", data);
        bundle.putString("date", date);
		newFragment.setArguments(bundle);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.data = args.getParcelableArrayList("invoices");
        String dateNow = args.getString("date");
        this.date = GregorianCalendar.getInstance();
        Date date = null;
        try {
			date = formatter.parse(dateNow);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.date.setTime(date);
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().getWindow().setTitle(getActivity().getString(
				R.string.please_select_the_invoice_you_would_like_to_recall));
		getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_white));
		
		int titleDividerId = getResources().getIdentifier("titleDivider", "id", "android");
		InvoiceHome.dialogFragmentChangeTitleColor(getDialog(),titleDividerId);
		
		final View view = inflater.inflate(R.layout.invoicelist_dialogfragment, container, false);
		View header = inflater.inflate(R.layout.invoicelist_item_header, null);
		listview = (ListView) view.findViewById(R.id.invoice_listview);
		invoiceNoSearch = (EditText) view.findViewById(R.id.invoice_num_edit);
		curDate = (TextView) view.findViewById(R.id.current_date);
		curDate.setText(formatter2.format(date.getTime()));
		datepicker = (Button) view.findViewById(R.id.datepicker);
//		last = (Button) view.findViewById(R.id.last_day);
//		next = (Button) view.findViewById(R.id.next_day);
//		last.setOnClickListener(new OnClickListener(){
//			@Override
//			public void onClick(View v) {
//				date.add(Calendar.DATE, -1);
//				data = getInvoicesList(formatter.format(date.getTime()));
//				adapter.setData(data);
//				adapter.notifyDataSetChanged();
//				curDate.setText(formatter2.format(date.getTime()));
//			}
//		});
//		next.setOnClickListener(new OnClickListener(){
//			@Override
//			public void onClick(View v) {
//				date.add(Calendar.DATE, 1);
//				data = getInvoicesList(formatter.format(date.getTime()));
//				adapter.setData(data);
//				adapter.notifyDataSetChanged();
//				curDate.setText(formatter2.format(date.getTime()));
//			}
//		});
		datepicker.setOnClickListener(new OnClickListener(){
		@Override
		public void onClick(View v) {
		    int mYear = date.get(Calendar.YEAR);
		    int mMonth = date.get(Calendar.MONTH);
		    int mDay = date.get(Calendar.DAY_OF_MONTH);
//		    System.out.println("the selected " + mDay);
		    DatePickerDialog dialog = new DatePickerDialog(view.getContext(),
		            new DateSetListener(), mYear, mMonth, mDay);
		    dialog.getDatePicker().setCalendarViewShown(false);
		    dialog.show();
		}
		});
		
		
		adapter = new InvoicesListViewAdapter(getActivity(), data);
		listview.addHeaderView(header, null, false);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				mListener.onInvoiceSelected((InvoiceParcelable) parent.getAdapter().getItem(position));
			
			            }
			        });
		

		invoiceNoSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Listview name of the class
            	adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
		
        return view;
    }
	


    @Override
    public void onPause() {
        super.onPause();
//        invoiceNoSearch.getText().clear();
    }
    
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnInvoiceSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}


	public interface OnInvoiceSelectedListener {
		// TODO: Update argument type and name
		public void onInvoiceSelected(InvoiceParcelable invoice);
	}
	

    public int convertToPx(int input) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (input * scale + 0.5f);
}
    public static ArrayList<InvoiceParcelable> getInvoicesList(String dateNow){
    	final String GET_INVOICE_URL = "InvoiceMaintenence/InvoiceService.svc/fetchdetailedinvoices";
		ArrayList<InvoiceParcelable> invoicelist = new ArrayList<InvoiceParcelable>();
    	PostRequestTask postRequest;
		JSONObject storedetail = new JSONObject();
    	JSONObject getInvoice = new JSONObject();
    	int flag = 0;
    	while(flag != 2){
	    	try {
				storedetail.put("email_id", CornerStorePOS.email_id);
	        	storedetail.put("password", CornerStorePOS.password);
	        	storedetail.put("store_id", CornerStorePOS.StoreId);
	        	storedetail.put("station_id", CornerStorePOS.StationId);
	        	getInvoice.put("storedetail", storedetail);
	
	        	getInvoice.put("invoice_date", dateNow);
//	        	Log.d("flag", flag+"");
	        	String uri = flag == 0? CornerStorePOS.Url+GET_INVOICE_URL:CornerStorePOS.BackupUrl+GET_INVOICE_URL;
	        	postRequest = new PostRequestTask();
				postRequest.execute(uri,getInvoice.toString());
	        	String response = "";
	        	try {
	    			response = postRequest.get();
	//    			Type type = new TypeToken<ArrayList<FetchInvoice>>(){}.getType();
	//    			Gson gson = new Gson();
	//    			ArrayList<FetchInvoice> list = gson.fromJson(response, type);
	    			JSONArray jsonarray = new JSONArray(response);
	//    			Log.d("response", jsonarray.get(0).toString());
	    			for(int i = 0; i < jsonarray.length();i++){ 
	    				invoicelist.add(new InvoiceParcelable(new FetchInvoice(jsonarray.getJSONObject(i))));
	    			}
		        	break;
	//    			Log.d("response", list.size()+"");
	    		} catch (InterruptedException e) {
	    			e.printStackTrace();
	    		} catch (ExecutionException e) {
	    			e.printStackTrace();
	    		}
			} catch (Exception e) {
				e.printStackTrace();
				flag++;
			}
    	}
    	return invoicelist;
    }
    
    class DateSetListener implements DatePickerDialog.OnDateSetListener {
    	@Override
    	public void onDateSet(DatePicker view, int year, int monthOfYear,
    			int dayOfMonth) {
            int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            String date= new StringBuilder()
            // Month is 0 based so add 1
            .append(mYear).append("-").append(mMonth + 1).append("-").append(mDay)
            .toString();
            curDate.setText(new StringBuilder()
            // Month is 0 based so add 1
            .append(mMonth + 1).append("-").append(mDay).append("-")
            .append(mYear).toString());
//            System.out.println(v.getText().toString());
            data = getInvoicesList(date);
            adapter.setData(data);
            adapter.notifyDataSetChanged();
    		
    	}
    }
}
