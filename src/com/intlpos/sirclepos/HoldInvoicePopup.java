package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



public class HoldInvoicePopup extends Dialog implements OnClickListener{
	private Context context;
	private CornerStorePOS app;
	private EditText name;
	private Button save, cancel;
	
	public HoldInvoicePopup(Context context) {
		super(context);
		this.context = context;
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.holdinvoice);
		app = (CornerStorePOS) context.getApplicationContext();
		getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_white));
        WindowManager.LayoutParams lp=this.getWindow().getAttributes();
        lp.width = 550;
        lp.height = 200;
        this.getWindow().setAttributes(lp);
        
        name = (EditText)findViewById(R.id.enterInvoice);
        save = (Button)findViewById(R.id.saveHoldInvoice);
        cancel = (Button)findViewById(R.id.cancelHoldInvoice);
        save.setOnClickListener(this);
        cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.saveHoldInvoice:
				String customer = name.getText().toString();
				if(!customer.isEmpty()){
					
				}
				else{
					Toast.makeText(context, R.string.invoiceempty, Toast.LENGTH_SHORT).show();
				}
				dismiss();
				break;
			case R.id.cancelHoldInvoice:
				dismiss();
				break;
		}
		
	}
	
	
	
}
