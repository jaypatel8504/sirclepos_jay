package com.intlpos.sirclepos;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.intlpos.WCFAccess.GetDailyReports;
import com.intlpos.WCFAccess.GetSampleCSV;
import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.JSONParserNew;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.WCFAccess.RegisterParser;
import com.intlpos.sirclepos.R;
import com.intlpos.global.AboutDialog;
import com.intlpos.initsetup.CsvFileReadFragment;
import com.intlpos.initsetup.StartScreen;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.storemaintenance.Zout;
import com.john.bluetoothprinter.helper.PrintDataService;

public class FragmentSettings extends PreferenceFragment implements OnSharedPreferenceChangeListener{
	
	public ListPreference bluetoothPrinter;
	public ListPreference bluetoothMagTek, kitchen;
	public ListPreference nickels, pole;
	public Preference bluetoothIngen;
	public Preference bluetoothTest, deleteNotes, restore, token;
	public SwitchPreference onOff;
	private BluetoothAdapter adapter;
	private PrintDataService printDataService = null; 
	private CornerStorePOS app;
	private Context context;
	ProgressDialog ringProgressDialog;
	String sales[];
	String dept[];
	int payKey[];
	String payment[];
	String full[];
	String lekey[];
	String leValue[];
	String lekey1[];
	String leValue1[];
	String KEY_key[];
	String KEY_val[];
	theValues theVal[];
	int sizeOfValArray;
	private Preference wifiPrinter;
	private SwitchPreference wifiPrinterSwitch;
	
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (CornerStorePOS)  getActivity().getApplication();
        addPreferencesFromResource(R.xml.general);
        context =  getActivity().getBaseContext();
        
        onOff = (SwitchPreference)findPreference("switch_printer");
        wifiPrinterSwitch = (SwitchPreference)findPreference("switch_wifi_printer");
        bluetoothPrinter = (ListPreference)findPreference("list_bluetooth_printer");
        bluetoothMagTek = (ListPreference)findPreference("list_bluetooth_card_reader");
        nickels = (ListPreference)findPreference("nickel");
        pole = (ListPreference)findPreference("pole_types");
        bluetoothIngen = (Preference)findPreference("ingenico");
        wifiPrinter = (Preference)findPreference("button_wifi_printer");
        deleteNotes = (Preference)findPreference("delete");
        restore = (Preference)findPreference("demo");
        token = (Preference)findPreference("resetToken");
        kitchen = (ListPreference)findPreference("kitchentprint");
        final Preference test = (Preference)findPreference("loginbuttontooth_printer_test");
        adapter = BluetoothAdapter.getDefaultAdapter();
       
        PreferenceScreen preferenceScreen = getPreferenceScreen();
        if(!CSSharedPreferences.loadDemo())
        	preferenceScreen.removePreference(restore);
        else{
        	restore.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
				
				@Override
				public boolean onPreferenceClick(Preference preference) {
					CSSharedPreferences.removePreferences();
					
					Intent i = new Intent(getActivity(), DemoOrRegister.class);
	                startActivity(i);
					return true;
				}
			});
        }
        
        token.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				CSSharedPreferences.setToken(1);
				Toast.makeText(getActivity(), "Reset Complete", Toast.LENGTH_SHORT).show();
				return true;
			}
		});
        
        kitchen.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
            	preference.setSummary(newValue.toString());
                return true;
            }
        });
        
        if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true){
        	bluetoothPrinter.setEnabled(true);
        	test.setEnabled(true);
        }else{
        	bluetoothPrinter.setEnabled(false);
        	test.setEnabled(false);
        }
        ListPreference listPreference = (ListPreference) findPreference("nickel");
        listPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
            	if(newValue.toString().equals("1")){
            		preference.setSummary("Rounded Transactions will not be recorded in reports");
            	} 
            	else{
            		preference.setSummary("Disabled");
            	}
                return true;
            }
        });
        
        pole.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
            	if(newValue.toString().equals("Sircle POS CFD")){
            		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

	        		alert.setTitle("IP Address");
	        		alert.setMessage("Please enter the ip address of the second tablet");

	        		// Set an EditText view to get user input 
	        		final EditText input = new EditText(getActivity());
	        		alert.setView(input);

	        		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        		public void onClick(DialogInterface dialog, int whichButton) {
	        		  String value = input.getText().toString().replaceAll("\\s","");
	        		  Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
	        		  editor.putString("cfd_ip", value);
	        		  editor.commit();
		        	}
	        		});

	        		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        		  public void onClick(DialogInterface dialog, int whichButton) {
	        		    // Canceled.
	        		  }
	        		});
	        		alert.show();
            	}
                return true;
            }
        });

        if(wifiPrinter != null){
			wifiPrinter.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(final Preference pref) {
	            	Log.d("click", "wifi");
	            	AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

	        		alert.setTitle("IP Address");
	        		alert.setMessage("Please type the ip adress of wifi printer");

	        		// Set an EditText view to get user input 
	        		final EditText input = new EditText(getActivity());
	        		alert.setView(input);

	        		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	        		public void onClick(DialogInterface dialog, int whichButton) {
	        		  String value = input.getText().toString();
	        		  Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
	        		  editor.putString("wifi_printer_ip", value);
	        		  editor.commit();
		        	}
	        		});

	        		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        		  public void onClick(DialogInterface dialog, int whichButton) {
	        		    // Canceled.
	        		  }
	        		});
	        		alert.show();
	                return true;
	            }
	        });     
		}
        onOff.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference,
                    Object newValue) {
                boolean switched = ((SwitchPreference) preference).isChecked();
                       
                if(switched == false){
                	bluetoothPrinter.setEnabled(true);
                	test.setEnabled(true);
                }
                else{
                	bluetoothPrinter.setEnabled(false);
                	test.setEnabled(false);
                }

                return true;
            }

        });
        
        wifiPrinter.setEnabled(true);
              
        
        if(adapter == null){
        	Toast.makeText(context, context.getString(R.string.device_not_support_bluetooth), Toast.LENGTH_SHORT).show();
        }
        else{
        	if (!adapter.isEnabled())     
			{   
			    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);    
			    intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);     
			    startActivity(intent);     
			}
        	
        	Set<BluetoothDevice> devices = adapter.getBondedDevices(); 
        	CharSequence[] bluetoothNames = {this.getResources().getString(R.string.no_avaliable_device)};
			CharSequence[] bluetoothMacAddress = {this.getResources().getString(R.string.no_avaliable_device)};
			if (!devices.isEmpty()){
				bluetoothNames = new CharSequence[devices.size()];
				bluetoothMacAddress = new CharSequence[devices.size()];
			}
			int count = 0;
			for(BluetoothDevice dev:devices)     
			{     
				bluetoothNames[count] = dev.getName();
				bluetoothMacAddress[count] = dev.getAddress();
				count++;
			}
			
			bluetoothPrinter.setEntries(bluetoothNames);
			bluetoothPrinter.setEntryValues(bluetoothMacAddress);
			bluetoothMagTek.setEntries(bluetoothNames);
			bluetoothMagTek.setEntryValues(bluetoothMacAddress);
			
			
		    if (test != null) {
		    	test.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
		            @Override
		            public boolean onPreferenceClick(Preference arg0) {
		            	
		            	Intent i = new Intent(getActivity(),PrinterTest.class);
		            	startActivity(i);
		            	return true;
		            	
		            }
		    	});
		    }
			if(!CSSharedPreferences.getPayment().getGateway().equals("chasepaymentech")){
				bluetoothIngen.setEnabled(false);
				bluetoothIngen.setSummary(R.string.gotochase);
			}else
				bluetoothIngen.setEnabled(true);
			if(bluetoothIngen != null){
				bluetoothIngen.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
		            @Override
		            public boolean onPreferenceClick(Preference arg0) {
						// TODO Auto-generated method stub
						if(!arg0.toString().isEmpty()){
							DialogFragment newFragment = Ingenico.newInstance();
							newFragment.show(getFragmentManager(), "Ingenico");
						}
						return false;
					}
				});
			}
        }
        
        deleteNotes.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				Set<String> mess = new HashSet<String>();
				CSSharedPreferences.setMessages(mess);
				
				Toast.makeText(getActivity(), getResources().getString(R.string.deleted), Toast.LENGTH_SHORT).show();
				return false;
			}
		});
        
        if(app.employee.admin_access == true){
	    	admin();
	    }
        else{
        	PreferenceScreen payent =(PreferenceScreen)findPreference("payment"); 
	    	payent.setEnabled(false);
	    	PreferenceScreen data =(PreferenceScreen)findPreference("data"); 
	    	data.setEnabled(false);
	    	PreferenceScreen zout =(PreferenceScreen)findPreference("zout"); 
	    	zout.setEnabled(false);
	    	 Preference xout = (Preference)findPreference("xreport");
	    	 xout.setEnabled(false);
	    	Preference appinfor =(Preference)findPreference("appinfor"); 
	    	appinfor.setEnabled(false);
	    	Preference importData =(Preference)findPreference("import");
	    	importData.setEnabled(false);
//	    	Preference contact =(Preference)findPreference("contact");
//	    	contact.setEnabled(false);
        }
	}
	
	private void admin(){
		if(CSSharedPreferences.loadDemo()){
			Preference importData =(Preference)findPreference("import");
	    	importData.setEnabled(false);
		}else{
			Preference importData =(Preference)findPreference("import");
	    	importData.setEnabled(true);
		}
		Preference zout = (Preference)findPreference("list_print_zout_report");
	    if (zout != null) {
	    	zout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	ConnectivityManager connManager  = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	            	NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	            	if(mWifi.isConnected()){
	            		DialogFragment zout = Zout.newInstance(false,true);
						zout.show(getFragmentManager(), "zoutNew");
	            	}
		        	else
		        	{
		        		Toast.makeText(getActivity(), R.string.wifi, Toast.LENGTH_LONG).show();
		        	}
	                return true;
	            }
	        });     
	    }
	    
	    Preference xout = (Preference)findPreference("xreport");
	    if (xout != null) {
	    	xout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	ConnectivityManager connManager  = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	            	NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	            	if(mWifi.isConnected()){
	            		DialogFragment zout = Zout.newInstance(false,false);
						zout.show(getFragmentManager(), "zoutNew");
	            	}
		        	else
		        	{
		        		Toast.makeText(getActivity(), R.string.wifi, Toast.LENGTH_LONG).show();
		        	}
	                return true;
	            }
	        });     
	    }
	    
	    Preference updateDatabase = (Preference)findPreference("button_update_database");
	    if (updateDatabase != null) {
	    	updateDatabase.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {

	        		final String methodName = "VersionMaintenence/VersionService.svc/updateversion";

	        		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
	        			
	        		
	        		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
	        		storedetail.put("email_id", CornerStorePOS.email_id);
	        		storedetail.put("password", CornerStorePOS.password);
	        		storedetail.put("store_id",CornerStorePOS.StoreId);
	        		storedetail.put("station_id", CornerStorePOS.StationId);
//	        		
	        		final StoreDetail sd = CSSharedPreferences.loadVersionPreferences();
	        		params.put("storedetail", storedetail);
	        		params.put("versioncode", sd.getVersion_code());
	        		params.put("versionname", sd.getVersion_name());
	        		
	        		JSONParser jParser = new JSONParser(new MyListener() {

	        			@Override
	        			public void onResult(JSONObject json) {

	        				try {
	        					Boolean result = json.getBoolean("result");
	        					String resultString = json.getString("resultString");
	        					if (result == true) 
	        					{							
	        						Toast.makeText(getActivity(), getString(R.string.welcometoversion)+" "+sd.getVersion_name()+sd.getVersion_code(), Toast.LENGTH_LONG).show();
	        					}
	        					else
	        					{
	        						Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();	
	        					}
	        				}
	        				catch(Exception ex)
	        				{
	        					JSONParser jParser = new JSONParser(new MyListener() {

	        						@Override
	        						public void onResult(JSONObject json) {

	        							try {
	        								Boolean result = json.getBoolean("result");
	        								String resultString = json.getString("resultString");
	        								if (result == true) 
	        								{							
	        									Toast.makeText(getActivity(), getString(R.string.welcometoversion)+" "+sd.getVersion_name()+sd.getVersion_code(), Toast.LENGTH_LONG).show();
	        								}
	        								else
	        								{
	        									Toast.makeText(getActivity(), resultString, Toast.LENGTH_LONG).show();	
	        								}
	        							}
	        							catch(Exception ex)
	        							{
	        								Log.d("CSPOS",""+ex);
	        							}
	        						}
	        					});
	        					
	        					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
	        				}
	        			}
	        		});
	        		
	        		jParser.execute(CornerStorePOS.Url, methodName, params);
	            	
//	            	String methodName = "DatabaseMaintenence/UpgradeDatabaSeservice.svc/upgradedb";
//	            	LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
//	        		
////	            	StoreDetail storedetail = new StoreDetail();
//	            	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
//	            	storedetail.put("email_id", CornerStorePOS.email_id);
//	            	storedetail.put("password", CornerStorePOS.password);
//	            	storedetail.put("store_id",CornerStorePOS.StoreId);
//	            	storedetail.put("station_id", CornerStorePOS.StationId);
//	            	params.put("storedetail", storedetail);
//	            	
//	            	params.put("versioncode", app.version_code);
//	            	params.put("versionname", app.version_name);
//	                
//	            	JSONParserNew jParser = new JSONParserNew(new MyListener() {
//
//	            		@Override
//	            		public void onResult(JSONObject json) {
//
//	            			try {
//	            			if(json.getBoolean("result")== true)	 
//	            			{								
//	            					// lets store it in a log invoice_id and grandtotal
//	            				Toast.makeText(context, R.string.dataSucc, Toast.LENGTH_SHORT).show();
//	            			}
//	            			else
//	            			{
//	            				//lets store it in a log Invoice,Order objects  /write some message saying failed etc...
//	            				Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
//	            			}
//	            			}
//	            			catch(Exception ex)
//	            			{
//	            				Log.d("SettingPref",""+ex);
//	            			}
//	            		}
//	            	});
//	            	
//	            	jParser.execute(CornerStorePOS.Url, methodName, params);
	                return true;
	            }
	        });     
	    }
	    
	    Preference clearinvoice = (Preference)findPreference("button_clearInvoices");
	    if (clearinvoice != null) {
	    	clearinvoice.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
	            	alertDialogBuilder.setTitle(R.string.Are);
	            	alertDialogBuilder
					.setMessage(R.string.inDele)
					.setCancelable(false)
					.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							String methodName = "DatabaseMaintenence/UpgradeDatabaSeservice.svc/runscript";
			            	LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			        		
//			            	StoreDetail storedetail = new StoreDetail();
			            	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
			            	storedetail.put("email_id", CornerStorePOS.email_id);
			            	storedetail.put("password", CornerStorePOS.password);
			            	storedetail.put("store_id",CornerStorePOS.StoreId);
			            	storedetail.put("station_id", CornerStorePOS.StationId);
//			            	storedetail.setemail_id(GlobalSetup.email_id);
//			            	storedetail.setpassword(GlobalSetup.password);
//			            	storedetail.setstoreid(GlobalSetup.StoreId);
//			            	storedetail.setstationid(GlobalSetup.StationId);
//			            	Gson gson = new Gson();
//			            	String StoreObj = gson.toJson(storedetail);
			            	params.put("storedetail", storedetail);
			            	
			            	params.put("scriptname", "clear_invoices");
			                
			            	JSONParserNew jParser = new JSONParserNew(new MyListener() {

			            		@Override
			            		public void onResult(JSONObject json) {

			            			try {
			            			if(json.getBoolean("result")== true)	 
			            			{								
			            					// lets store it in a log invoice_id and grandtotal
			            				Toast.makeText(context, R.string.deleIN, Toast.LENGTH_SHORT).show();
			            			}
			            			else
			            			{
			            				//lets store it in a log Invoice,Order objects  /write some message saying failed etc...
			            				Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
			            			}
			            			}
			            			catch(Exception ex)
			            			{
			            				Log.d("SettingPref",""+ex);
			            			}
			            		}
			            	});
			            	
			            	jParser.execute(CornerStorePOS.Url, methodName, params);
						}
					  })
					.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
					return true;
	            }});
	    }
	    
	    Preference clearproducts = (Preference)findPreference("button_clearProducts");
	    if (clearproducts != null) {
	    	clearproducts.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
	            	alertDialogBuilder.setTitle(R.string.Are);
	            	alertDialogBuilder
					.setMessage(R.string.proDele)
					.setCancelable(false)
					.setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							String methodName = "DatabaseMaintenence/UpgradeDatabaSeservice.svc/runscript";
			            	LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			        		
//			            	StoreDetail storedetail = new StoreDetail();
			            	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
			            	storedetail.put("email_id", CornerStorePOS.email_id);
			            	storedetail.put("password", CornerStorePOS.password);
			            	storedetail.put("store_id",CornerStorePOS.StoreId);
			            	storedetail.put("station_id", CornerStorePOS.StationId);
//			            	storedetail.setemail_id(GlobalSetup.email_id);
//			            	storedetail.setpassword(GlobalSetup.password);
//			            	storedetail.setstoreid(GlobalSetup.StoreId);
//			            	storedetail.setstationid(GlobalSetup.StationId);
//			            	Gson gson = new Gson();
//			            	String StoreObj = gson.toJson(storedetail);
			            	params.put("storedetail", storedetail);
			            	
			            	params.put("scriptname", "clear_products");
			                
			            	JSONParserNew jParser = new JSONParserNew(new MyListener() {

			            		@Override
			            		public void onResult(JSONObject json) {

			            			try {
			            			if(json.getBoolean("result")== true)	 
			            			{								
			            					// lets store it in a log invoice_id and grandtotal
			            				Toast.makeText(context, R.string.delePr, Toast.LENGTH_SHORT).show();
			            			}
			            			else
			            			{
			            				//lets store it in a log Invoice,Order objects  /write some message saying failed etc...
			            				Toast.makeText(context, json.getString("resultString"), Toast.LENGTH_SHORT).show();
			            			}
			            			}
			            			catch(Exception ex)
			            			{
			            				Log.d("SettingPref",""+ex);
			            			}
			            		}
			            	});
			            	
			            	jParser.execute(CornerStorePOS.Url, methodName, params);
						}
					  })
					.setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
	 
					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();
	 
					// show it
					alertDialog.show();
					return true;
	            }});
	    }
	    
	    Preference other = (Preference)findPreference("otherCheck");
	    CheckBoxPreference express = (CheckBoxPreference)findPreference("expressCheck");
	    
	    final StoreDetail sd = CSSharedPreferences.loadPayment();
	    StoreDetail storedet = new StoreDetail();
	   
	    if(sd.getPayMentModule() == false){
	    	other.setEnabled(false);
	    	express.setChecked(true);
	    	express.setSelectable(false);
	    	storedet.setExpress(true);
	    	CSSharedPreferences.setExpressCheckout(storedet);
	    }
	    
	    else{
	    	StoreDetail storedetails;
	    	express.setSelectable(true);
	    	
	    	storedetails = CSSharedPreferences.getExpressCheckout();
	    	Log.d("storedet: ", "" + storedetails.getExpress());
	    	
	    	if(storedetails.getExpress() == true){
	    		express.setChecked(true);
	    		other.setEnabled(false);
	    	}
	    	else if(storedetails.getExpress() == false){
	    		other.setEnabled(true);
	    		express.setChecked(false);
	    	}
	    }
	    
	    PreferenceScreen payentProcessing =(PreferenceScreen)findPreference("payment"); 
	    payentProcessing.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				if(sd.getPayMentModule() == false){
					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
	            	alertDialogBuilder.setTitle(R.string.payProcess);
	            	alertDialogBuilder
					.setMessage(R.string.payMessage)
					.setCancelable(false)
					.setPositiveButton("OK",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							dialog.cancel();
						}
					});
					
	            	AlertDialog alertDialog = alertDialogBuilder.create();
	           	 
					alertDialog.show();
				}
				return true;
			}
		});
	    
	    if(express != null){
	    	express.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
				
				@Override
				public boolean onPreferenceChange(Preference preference, Object newValue) {
					Preference other = (Preference)findPreference("otherCheck");
					StoreDetail storedetail = new StoreDetail();
					if(newValue.toString().equals("true"))
					{
						storedetail.setExpress(true);
						other.setEnabled(false);
						CSSharedPreferences.setExpressCheckout(storedetail);
					}
					else if(newValue.toString().equals("false")){
						if(CSSharedPreferences.loadPayment().getPayMentModule() == true){
							storedetail.setExpress(false);
							other.setEnabled(true);
							CSSharedPreferences.setExpressCheckout(storedetail);
						}
						else{
							
							Toast.makeText(getActivity(), R.string.Denied, Toast.LENGTH_LONG).show();
						}
					}
					return true;
				}
			});
	    }
	    
	   if(other!=null){
	    	other.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	show();
					return true;
				}
	        });
	    }
	   
	   Preference updatePay = (Preference)findPreference("updatepay");
	   if(updatePay != null){
		   updatePay.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	String methodName = "check_credentials.svc/updatepaymentmodule";
	            	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
	            	LinkedHashMap<String, Object> store = new LinkedHashMap<String, Object>();
	            	storedetail.put("store_id",CornerStorePOS.StoreId);
	        		storedetail.put("password", CornerStorePOS.password);
	        		storedetail.put("email_id", CornerStorePOS.email_id);
	        		storedetail.put("station_id", CornerStorePOS.StationId);
	        		store.put("storedetail", storedetail);
	        		JSONParser jParser = new JSONParser(new MyListener(){
						@Override
						public void onResult(JSONObject json) {
							// TODO Auto-generated method stub
							try{
								Preference otherPayment = (Preference)findPreference("otherCheck");
								CheckBoxPreference expressCheck = (CheckBoxPreference)findPreference("expressCheck");
								
								if(json.getBoolean("result") == true){
									Toast.makeText(getActivity(), R.string.updateSuccess, Toast.LENGTH_LONG).show();
									expressCheck.setSelectable(true);
									StoreDetail storedetail = new StoreDetail();		
									storedetail.setnotfirsttime(true);
									storedetail.setemail_id(CornerStorePOS.email_id);
									storedetail.setpassword(CornerStorePOS.password);
									storedetail.setstoreid(CornerStorePOS.StoreId);
									storedetail.setstationid(CornerStorePOS.StationId);
									storedetail.setPaymentModule(true);
									CSSharedPreferences.savePreferences(storedetail);

									
								}else{
									Toast.makeText(getActivity(), R.string.failedUpdate, Toast.LENGTH_LONG).show();
									otherPayment.setEnabled(false);
									expressCheck.setChecked(true);
							    	expressCheck.setSelectable(false);
								}
							}catch(Exception e){
								e.printStackTrace();
							}
						}
	        			
	        		});
	        		
	        		jParser.execute(CornerStorePOS.Url, methodName, store);
	        		
					return true;
				}
	        });
	   }
	    Preference appinfo = (Preference)findPreference("appinfor");
	    if (appinfo != null) {
	    	appinfo.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
	            	AboutDialog about = new AboutDialog(getActivity());
	    			about.setTitle(R.string.appinfor);
	    			about.show();	
					return true;
				}
	        });
	    }
	    

	    
	    Preference store1 = (Preference)findPreference("samplestore1");
	    if (store1 != null) {
	    	store1.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceNJDatabase.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceNJDatabase.csv");
	            	return true;
				}
	        });
	    }
	    
	    Preference store2 = (Preference)findPreference("samplestore2");
	    if (store2 != null) {
	    	store2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceStoreDatabase.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceStoreDatabase.csv");
	            	return true;
				}
	        });
	    }
	    
	    Preference store3 = (Preference)findPreference("samplestore3");
	    if (store3 != null) {
	    	store3.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LIConvenientDatabase.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LIConvenientDatabase.csv");
	            	return true;
				}
	        });
	    }
	    
	    Preference store4 = (Preference)findPreference("samplestore4");
	    if (store4 != null) {
	    	store4.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/MAConvenienceDatabase.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/MAConvenienceDatabase.csv");
	            	return true;
				}
	        });
	    }
	    
	    Preference liquor = (Preference)findPreference("liquor");
	    if (liquor != null) {
	    	liquor.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LiquorStoreDatabase.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LiquorStoreDatabase.csv");
	            	return true;
				}
	        });
	    }
	    
	    Preference tobacco = (Preference)findPreference("tobacco");
	    if (tobacco != null) {
	    	tobacco.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
	            @Override
	            public boolean onPreferenceClick(Preference arg0) {
//	            	showProgressBar();
//	            	new GetSampleCSV("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/tobaccostore.csv",getActivity()).execute();
//	            	ringProgressDialog.dismiss();
	            	switchToCSVFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/tobaccostore.csv");
	            	return true;
				}
	        });
	    }
//	    Preference employee = (Preference)findPreference("employees");
//	    if (employee != null) {
//	    	employee.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//	            @Override
//	            public boolean onPreferenceClick(Preference arg0) {
//	            	switchToCSVFragment("");
//	            	return true;
//				}
//	        });
//	    }
//	    
//	    Preference depart = (Preference)findPreference("depart");
//	    if (depart != null) {
//	    	depart.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//	            @Override
//	            public boolean onPreferenceClick(Preference arg0) {
//	            	switchToCSVFragment("depart");
//	            	return true;
//				}
//	        });
//	    }
	}
	
	private void show(){
		DialogFragment newFragment = Other.newInstance();
		newFragment.show(getFragmentManager(), "other");
		
	}
	
	public void ZoutReportShow(boolean zout) {	
		
		   String Array[] = new String[sales.length + full.length];
		   System.arraycopy(sales, 0, Array, 0, sales.length);
		   System.arraycopy(full, 0, Array, sales.length, full.length);
		  
		   String VALUE_key[] = new String[sizeOfValArray];
		   String VALUE_val[] = new String[sizeOfValArray];
		   int[] sizesOfDepart = new int[theVal.length];
		   int count2 = 0;
		   for(int i = 0; i< theVal.length; i++){
			   String temp[] = theVal[i].getKey();
			   String tempForVal[] = theVal[i].getValue();
			   
			   for(int j = 0; j < temp.length; j++){
				   VALUE_key[count2] = temp[j];
				   VALUE_val[count2] = tempForVal[j];
				   count2++;
			   }
			   
			if(i == 0){
				   sizesOfDepart[i] = count2;
			   }
			   else{
				   int size = 0;
				   for(int p = 0; p <sizesOfDepart.length; p++)
					   size += sizesOfDepart[p];
				   sizesOfDepart[i] = (count2 - size);
			   }
		   }
		   int totalSize = count2;
		
		   DialogFragment newFragment = ZoutDialog.newInstance(Array, KEY_key, KEY_val, VALUE_key, VALUE_val, sizesOfDepart, totalSize, false, zout);
		   newFragment.show(getFragmentManager(), "hello");
		   
		   
		}
		
		public void parseResult(String result){
		
			//Daily Report
			sales = new String[9];
			try {
				
				JSONArray values = new JSONArray(result);
				for(int i =0; i < values.length();i++)
				{
					JSONObject total = values.getJSONObject(i);
					if(total.getString("value").equals("")){
						 sales[i] = "0.00";
					}
					else{
						sales[i] = total.getString("value");	
					}
						Log.d("The value", sales[i]);
				}
				
			} 
			catch (JSONException e) {
					// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			
			
			
		}
		
		public void parseResult2(String result)
		{
			try {
				payKey = new int[7];
				payment = new String[7];
				Log.d("The pay", result);
				
				full = new String[7];
				
				for (int i = 0; i< full.length;i++){
					full[i] = "0.00";
				}
				if(result.length() > 2){
					JSONArray values = new JSONArray(result);
					
					for(int i =0; i < values.length();i++)
					{
						JSONObject total = values.getJSONObject(i);
						String temp = total.getString("Key");
						payKey[i] = Integer.parseInt(temp);
						payment[i] = total.getString("Value");
							
							Log.d("The payTotals", payment[i]);
					}
					
					
					for(int i = 0; i< payKey.length;i++){
						int temp = payKey[i]-1 ;
							
						if(temp > 0){
							full[temp] = payment[i];}
						
						else{
							full[payKey[i]] = payment[i];
						}
							
						Log.d("FULL", full[i]);
					}
				}
				
				
			} catch (JSONException e) {
					e.printStackTrace();
				}
		}
		
		public void parseResult3(String result)
		{
			try {
				
				JSONArray values = new JSONArray(result);
				JSONObject total[] = new JSONObject[values.length()];
				for(int i = 0; i < values.length();i++){
					total[i] = values.getJSONObject(i);
				}
				
				JSONArray[] Keyvalues = new JSONArray[values.length()];
				JSONArray[] Valuesval = new JSONArray[values.length()];
				
				for(int i = 0; i < values.length();i++){
					Keyvalues[i] = new JSONArray(total[i].getString("Key"));
					Valuesval[i] = new JSONArray(total[i].getString("Value"));
				}
				
				lekey =  new String[4];
				leValue = new String[4];
				 KEY_key = new String[Keyvalues.length*4];
				 KEY_val = new String[Keyvalues.length*4];
				
				int count = 0;
				for(int j = 0; j < Keyvalues.length; j++)
				{
					
					for(int i = 0; i <4; i++){
						JSONObject totalKey = Keyvalues[j].getJSONObject(i);
						KEY_key[count] =  totalKey.getString("key");
						KEY_val[count] = totalKey.getString("value");	
						count++;
					}
					
					
					
				}			
				
				theVal = new theValues[Valuesval.length];
				sizeOfValArray = 0;
				for(int j = 0; j < Valuesval.length; j++)
				{
					lekey1 =  new String[Valuesval[j].length()];
					leValue1 = new String[Valuesval[j].length()];
					sizeOfValArray+=Valuesval[j].length();
					Log.d("SIZE", "sitze " + Valuesval[j].length());
					for(int i = 0; i <Valuesval[j].length(); i++){
						
						JSONObject totalKey = Valuesval[j].getJSONObject(i);
						lekey1[i] =  totalKey.getString("key");
						leValue1[i] = totalKey.getString("value");
					}
					theVal[j] = new theValues(lekey1, leValue1);
				}
				Log.d("theVal", ""+theVal.length);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	
	@Override
	public void onSharedPreferenceChanged(
			SharedPreferences sharedPreferences, String key) {
		Preference preferences = findPreference(key);
	if (key.equals("list_bluetooth_printer")) {
        	
	        ListPreference preference = (ListPreference) findPreference(key);
	        if(preference instanceof ListPreference){
	        	ListPreference listPref = (ListPreference) preference;
	        	preference.setSummary(listPref.getEntry());
	        	
	        	//preference.setSummary(preference.getEntry());
	        	String address = listPref.getValue();
	        	Log.d("Printer", address);
	        	printDataService = new PrintDataService(context, address);
        	}
        }
        
        if (key.equals("list_bluetooth_card_reader")) {
        	
	        ListPreference listpreference = (ListPreference) findPreference(key);
	        if(listpreference instanceof ListPreference){
	        	ListPreference listPref = (ListPreference) listpreference;
	        	listpreference.setSummary(listPref.getEntry());

	        	String address = listPref.getValue();
	        	
	        	Log.d("CARD", address);
        	}
        }
        
        
        if (key.equals("list_print_receipt")) {
        	preferences.setSummary(((ListPreference) preferences).getEntry());
        }
        
        
	}
	
	public void showProgressBar() {
		ringProgressDialog = ProgressDialog.show(getActivity(), "Please wait", "Loading...");	
	}
	
	
	
	 @Override
	    public void onResume() {
	        super.onResume();
	        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	       
	    }

	    @Override
	    public void onPause() {
	        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	        super.onPause();	
	       
	    }
	    
	    @Override
	    public void onStart(){
	    		super.onStart();
	    		Log.d("CSS", CSSharedPreferences.getPayment().getGateway());
	    		if(CSSharedPreferences.getPayment().getGateway().equals("chasepaymentech") || CSSharedPreferences.getPayment().getGateway().equals("velocityemv")){
					bluetoothIngen.setEnabled(true);
				}else{
		    		bluetoothIngen.setEnabled(false);
					bluetoothIngen.setSummary(R.string.gotochase);
				}
	    }

		public void switchToCSVFragment(String url){
			((Settings) getActivity()).replaceFragments(url);
		}
	   
}
