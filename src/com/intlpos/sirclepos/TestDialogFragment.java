package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ImageLoader;

import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class TestDialogFragment extends Fragment {
	private int btnWidth;
	private int btnHeight;
    private static final String TAG = "TestFragment";
    private String[] sTitle = null;// = "hello android";
    private int[] bg_res = null;
    private String[] tags = null;
    private int[] bg_color = null;
    private float[] prices = null;
    private int[] item_id = null;
    private String[] pic = null;
    public MyImageButton[] button;
    private int pos;
    static TestDialogFragment newInstance(int width, int height, String[] s,int[] id,float[] prices, int[] bg_drawable, int[] bg_color, String pic[]) {
        TestDialogFragment newFragment = new TestDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("btnWidth", width);
        bundle.putInt("btnHeight", height);
        bundle.putStringArray("sTitle", s);
        bundle.putFloatArray("prices",prices);
        bundle.putIntArray("item_id", id);
        bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        bundle.putStringArray("pic", pic);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    static TestDialogFragment newInstance(int width, int height, String[] s,int[] id,float[] prices, int[] bg_drawable, int[] bg_color) {
        TestDialogFragment newFragment = new TestDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("sTitle", s);
        bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "TestFragment-----onCreate");
        Bundle args = getArguments();
        sTitle = args.getStringArray("sTitle");
        bg_res = (int[]) args.getIntArray("bg_res");
        bg_color = (int[]) args.getIntArray("bg_color");
        prices = (float[]) args.getFloatArray("prices");
        btnWidth = (int) args.getInt("btnWidth")+25;
        btnHeight = (int) args.getInt("btnHeight")+25;
        pic = args.getStringArray("pic");
        item_id = args.getIntArray("item_id");
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Log.d(TAG, "TestFragment-----onCreateView");
        MyViewGroup view = (MyViewGroup) inflater.inflate(R.layout.pager_item, container, false);
        Log.d(TAG, "ViewGroup = "+ view.toString());
//        MyViewGroup view = new MyViewGroup(inflater.inflate(R.layout.pager_item, container, false).getContext());
//        view.setId(0);
//        view.setWillNotDraw(false);
        int len = sTitle == null ? 0 : sTitle.length;
        Log.d(TAG, "len of string = " + len);
        button = new MyImageButton[len];
//        Button[] button = new Button[len];
        
        for(int i = 0 ; i<len;i++){
        	
        	button[i] = new MyImageButton(this.getActivity(),true);
//        	button[i] = new Button(this.getActivity());
        	MyViewGroup.LayoutParams param = new MyViewGroup.LayoutParams(btnWidth,LayoutParams.WRAP_CONTENT);
        	button[i].setMinimumHeight(btnHeight);
        	button[i].setMinimumWidth(btnWidth);
        	button[i].setItem_id(item_id[i]);
//        	button[i].setHeight(btnHeight);
//        	button[i].setWidth(btnWidth);
        	button[i].setLayoutParams(param);
//        	button[i].setTextSize(7);
//        	button[i].setPrice(prices[i]);
//        	button[i].getBackground().setColorFilter(bg_color[i], Mode.SRC_ATOP);
        	//Set Image
//        	int bg = bg_res[i] != 0 ? bg_res[i] : R.drawable.ic_launcher;
//        	Drawable drawable = getResources().getDrawable(bg_res[i]);
//        	Bitmap bitmap = ImageUtil.drawableToBitmap(drawable);
//    		Bitmap zoomBitmap = ImageUtil.zoomBitmap(bitmap, btnWidth, btnHeight); 
//    		Bitmap roundBitmap = ImageUtil.getRoundedCornerBitmap(zoomBitmap, 10.0f);
//    		Drawable d = new BitmapDrawable(getResources(),roundBitmap);
//    		Log.d(TAG, "bg_color = "+ bg_color[i] + " bg_res = "+bg);
//
//        	button[i].setImageDrawable(d);
        	button[i].setScaleType(ImageView.ScaleType.FIT_START);
        	if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
	        	if(!pic[i].isEmpty()){
	        		ImageLoader imageLoader;
        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight+15, getActivity(),true);
        			imageLoader.DisplayImage(pic[i], button[i]);
	        	}
        	}
        	
        	button[i].setBackgroundResource(R.drawable.bg_deptbutton_states);
//        	button[i].getBackground().setColorFilter(bg_color[i], Mode.SRC_ATOP);
        	button[i].setText(sTitle[i]);
        	button[i].setTextColor(Color.BLACK);
//        	button[i].setGravity(80);
        	//set id and Tag for Listener
//        	button[i].setId(i);
        	button[i].setTag(i);
        	button[i].selected = false;
//        	view.addOnLayoutChangeListener(new OnLayoutChangeListener(){
//        	@Override
//        	        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight,
//        	                int oldBottom) {
//        	    		Log.d(TAG, "Layout Changed!");
//        	        }
//        	    });
        	view.addView(button[i]);
        	view.setBackgroundResource(R.drawable.shape);
        	if (sTitle[i] == "KeyBoard"){
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    		Toast.makeText(getActivity(), "Popup Keyboard", Toast.LENGTH_SHORT).show();            	    	
            	       }
            	     });
        	}
        	else{
        		final int position = i;
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    	
            	    	//String index = (String) v.getTag();
            	    	MyImageButton mButton = (MyImageButton) v;
            	    	if (mButton.selected == false){
            	    		mButton.selected = true;
            	    		mButton.setBackgroundResource(R.drawable.bg_deptbutton_selected);
            	    	}else{
            	    		mButton.selected = false;
//            	    		mButton.setBackgroundResource(R.drawable.bg_deptbutton_states);
            	    		
                			if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
                	        	if(!pic[position].isEmpty()){
                	        		
                	        		ImageLoader imageLoader;
                        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight+15, getActivity());
                	        		imageLoader.DisplayImage(pic[position],mButton);
                	        	}else{
                	        		mButton.setBackgroundResource(R.drawable.bg_deptbutton_states);
                	        	}
                			}
                				mButton.setBackgroundResource(R.drawable.bg_deptbutton_states);
                			
            	    	}
            	       }
            	     });
        	}
        }
//    	MyViewGroup.LayoutParams vgParam = new MyViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
//				LayoutParams.WRAP_CONTENT);
//    	view.setLayoutParams(vgParam);
//		view.requestLayout();
//		view.invalidate();
//        view.setBackgroundResource(R.drawable.bg_1);

        return view;
    }
	


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "TestFragment-----onDestroy");
    }

}
