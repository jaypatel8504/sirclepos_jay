package com.intlpos.sirclepos;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.google.android.youtube.player.internal.v;
import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.john.bluetoothprinter.helper.ZoutReport;

import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ZoutDialog extends DialogFragment{
	String daily[];
	String key[];
	String val[];
	String val_key[];
	String val_value[];
	int sizesOfDepart[];
	int totalSize;
	TextView num[];
	TextView start, end;
	boolean ischeck;
	boolean logout, zout;
	Button print;
	Button cancel;
	private PrintDataService printDataService = null; 
	private CornerStorePOS app;
	ZoutReport zprint;
	private Context context;
	BluetoothAdapter mBluetoothAdapter;
	CheckBox checkPrint; 
	
	static ZoutDialog newInstance(String values[], String key[], String val[],
								  String val_key[], String val_value[], int sizeOfdept[], int totalSizeOfDept, boolean log, boolean zout){
		ZoutDialog zg = new ZoutDialog();
		Bundle args = new Bundle();
		args.putStringArray("val", values);
		args.putStringArray("key", key);
		args.putStringArray("val[]", val);
		args.putStringArray("val_key", val_key);
		args.putStringArray("val_value", val_value);
		args.putIntArray("sizesOfDepart", sizeOfdept);
		args.putInt("total", totalSizeOfDept);
		args.putBoolean("log", log);
		args.putBoolean("zout", zout);
		zg.setArguments(args);
		
		return zg;
	}
	

	  @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        daily = getArguments().getStringArray("val");
	        key = getArguments().getStringArray("key");
	        val =  getArguments().getStringArray("val[]");
	    	val_key = getArguments().getStringArray("val_key");
	    	val_value =  getArguments().getStringArray("val_value");
	    	logout = getArguments().getBoolean("log");
	    	sizesOfDepart = getArguments().getIntArray("sizesOfDepart");
	    	totalSize = getArguments().getInt("total");
	    	context = getActivity().getApplicationContext();
	    	zout = getArguments().getBoolean("zout");
	    	ischeck = false;
	    	
	  }
	
	  @Override
	  public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
		  
		  num = new TextView[daily.length];
		  if(zout)
			  getDialog().setTitle("Z-Out Report");
		  else
			  getDialog().setTitle("X-Out Report");
		  if(!daily[8].equals("0.00")){
			  daily[8] = daily[8].substring(0, daily[8].length() - 2);
		  }
		  
		 
		String split[] = daily[7].split("\\.");
		  
		 if(split[1].length() > 2){
			  daily[7] = daily[7].substring(0, daily[7].length() - 2);
		 }
		 
		  View v = inflater.inflate(R.layout.zout_dialog, container, false);
		  setCancelable(false);
		  app = (CornerStorePOS) getActivity().getApplication();
		  
		  addWidets(v);
		  CreateTableRows(v);
		  
		  setWidets();
		  
		 
		  Log.d("SPLIT THE STRING", "SPLIT OCCURS NEXT");
		  
		  return v;
	  }
	  
	  public void addWidets(View v){
		  print = (Button) v.findViewById(R.id.PrintZout);
		  cancel = (Button) v.findViewById(R.id.Cancel);
		  checkPrint = (CheckBox) v.findViewById(R.id.PrintR);
		  
		  num[0] = (TextView) v.findViewById(R.id.UnDicSales);
		  num[1] = (TextView) v.findViewById(R.id.Invoice);
		  num[2] = (TextView) v.findViewById(R.id.NetSales);
		  num[3] = (TextView) v.findViewById(R.id.Tax1);
		  num[4] = (TextView) v.findViewById(R.id.Tax2);
		  num[5] = (TextView) v.findViewById(R.id.Tax3);
		  num[6] = (TextView) v.findViewById(R.id.Grand);
		  num[7] = (TextView) v.findViewById(R.id.NetTaxed);
		  num[8] = (TextView) v.findViewById(R.id.NetNonTaxed);
		  num[9] = (TextView) v.findViewById(R.id.split);
		  num[10] = (TextView) v.findViewById(R.id.cash);
		  num[11] = (TextView) v.findViewById(R.id.credit);
		  num[12] = (TextView) v.findViewById(R.id.debit);
		  num[13] = (TextView) v.findViewById(R.id.gift);
		  num[14] = (TextView) v.findViewById(R.id.foodstamp);
		  num[15] = (TextView) v.findViewById(R.id.toAccount);
		  start = (TextView)v.findViewById(R.id.startTimeZ);
		  end = (TextView)v.findViewById(R.id.endTimeZ);
	  }
	  
	  public void setWidets()
	  {
		  for(int x = 10; x < daily.length; x++)
		  {
			  if(!daily[x].equals("0.00")){
				  if(!daily[x].contains(".")){
					  daily[x]+= ".00";
				  }
				  else{
					  String addZeros[] = daily[x].split("\\.");
					  if(addZeros[1].length() == 1){
						  daily[x]+= "0";
					  }
				  }
				  
			  }
		  }
		  
		  StartTime sTime = CSSharedPreferences.loadStartTime();
		  EndTime eTime = CSSharedPreferences.loadEndTime();
		  DateFormat f1 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date d1 = null;
			Date d2 = null;
			try {
				if(logout == true)
					d1 = f1.parse(sTime.getTime());
				else{
					d1 = f1.parse(CSSharedPreferences.loadPrevTime().getPrevTime());
				}
				d2 = f1.parse(eTime.getEndTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			DateFormat f2 = new SimpleDateFormat("MM/dd/yyyy h:mma");
		  start.setText(f2.format(d1));
		  if(zout)
			  end.setText(f2.format(d2));
		
			  
		  for(int i = 0; i < daily.length; i++){
			  num[i].setText(daily[i]);
		  }
		  
		  
		  checkPrint.setOnClickListener(new OnClickListener() {

	            @Override
	            public void onClick(View v) {
	                // TODO Auto-generated method stub
	                if(checkPrint.isChecked()){
	                	ischeck = true;
	                	
	                }else{
	                    ischeck = false;
	                }
	            }
	        });
		  
//		  print.setOnClickListener(new OnClickListener() {           
//
//			  @Override
//			  public void onClick(View v) 
//			  {
//				  
//				  if(printDataService == null){
//					  	Toast.makeText(getActivity().getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
//						return;
//					}
//				  
//				  ByteArrayOutputStream output = new ByteArrayOutputStream();
//				  try {	
//					  
////					  	if(logout == true)
////					  		zprint = new ZoutReport(context, daily, key, val, val_key, val_value, ischeck, sizesOfDepart,true);
////					  	else
////					  		zprint = new ZoutReport(context, daily, key, val, val_key, val_value, ischeck, sizesOfDepart,false);
////			        	for (ArrayList<String> arr: zprint.printout){
////			        		output.write(printDataService.sendwithCommand(arr.get(0), Integer.parseInt(arr.get(1))));
////			        	}
//			        	
//			    		if (!printDataService.isConnected){
//			    			Toast.makeText(context, getString(R.string.printer_not_connected), Toast.LENGTH_SHORT).show();
//			    			return;
//			    		}
//			    		//printDataService.sendFeedCutCommand();
//			    		printDataService.sendByteArray(output.toByteArray());
//			    		printDataService.sendFeedCutCommand();
//			    		PrintDataService.disconnect();
//			    		dismiss();
//			    		if(logout == true){
//			    		Intent zoutIntent = new Intent(getActivity(), MainLogin.class);
//			    		startActivity(zoutIntent);}
//				  }	
//				  catch (IOException e) {
//					
//					e.printStackTrace();
//				}
//		  }});
		  
		  cancel.setOnClickListener(new OnClickListener() {           

			  @Override
			  public void onClick(View v) 
			  {	
				  	if(printDataService != null){
				  		PrintDataService.disconnect();
				  	}
				  	
				  	dismiss();
				  	
				  	if(logout == true){ 
			    		Intent zoutIntent = new Intent(getActivity(), MainLogin.class);
			    		startActivity(zoutIntent);
				  	}
			  }
			  
		  });
	  }
	  
	  private void CreateTableRows(View view){
		  int count = 0;
		  int pos = 0;
		  int items = 0;
		  TableLayout dept = (TableLayout)view.findViewById(R.id.DeptTotals);
		  TableLayout deptItems = (TableLayout)view.findViewById(R.id.item);
		  Log.d("val_key.length", ""+val_key.length);
		  Log.d("sizesOfDepart.length", ""+sizesOfDepart.length);
		  
		  

		  for(int i = 0; i < key.length/4; i++){
			  for(int x = 0; x < 4; x++)
			  {
				  TableRow tr = new TableRow(view.getContext());
				  tr.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
				  
				  TextView keyname = new TextView(view.getContext());
				  TextView valname = new TextView(view.getContext());
				
				  
				  keyname.setText(key[count]);
				  keyname.setPadding(3, 3, 3, 3);
				  keyname.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1 ));
				  
				  if(key[count].equals("Sales Amount")){
					  if(val[count].contains(".")){
						  String addZeros[] = val[count].split("\\.");
						  if(addZeros[1].length() == 1){
							  val[count]+= "0";
						  }
						  if(addZeros[1].length() > 2){
							  String temp = addZeros[1].substring(0, 2);
							  
							  val[count] = addZeros[0]+"."+temp;
						  }
					  }
					  else{
						  val[count]+= ".00";
					  }
					  
				  }
				  
				  if(key[count].equals("Total Qty"))
				  {
					  val[count]+= ".00";
				  }
				  
				  valname.setText(val[count]);
				  valname.setPadding(3, 3, 3, 3);
				  valname.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1 ));
				  valname.setGravity(Gravity.END);
				  tr.addView(keyname);
				  tr.addView(valname);
				  dept.addView(tr);
				  count++;
				  if(x == 3){
					  if(pos < sizesOfDepart.length){
						  for(int q = 0; q < sizesOfDepart[pos]; q++){
							 // tr.removeAllViews();
							  TableRow table = new TableRow(view.getContext());
							  table.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
							  
							  TextView itemname = new TextView(view.getContext());
							  TextView itemcount = new TextView(view.getContext());
							  Log.d("VAlKEY", val_key[q]);
							  itemname.setText(val_key[q]);
							  itemname.setPadding(3, 3, 3, 3);
							  itemname.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1 ));
							  
							  Log.d("val_valuebefore", val_value[q]);
							  Log.d("val_sub", val_value[q].substring(0, val_value[q].length() - 2));
							 
							  
							  if(val_value[q].contains(".")){
								  String addZeros[] = val_value[q].split("\\.");
								  if(addZeros[1].length() > 2){
									  val_value[q] =  val_value[q].substring(0, val_value[q].length() - 2);
								  }
							  
							  }
							  itemcount.setText(val_value[q]);
							  itemcount.setPadding(3, 3, 3, 3);
							  itemcount.setLayoutParams(new TableRow.LayoutParams(0, android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 1 ));
							  itemcount.setGravity(Gravity.END);
							  
							  table.addView(itemname);
							  table.addView(itemcount);
							  dept.addView(table);
						  }
						pos++;
					  }
				  }
			  }
		  }
		 
	  }
	  
	  private void getSharedReference() {
		  SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
		  String address = preference.getString("list_bluetooth_printer", "");
			if (address == ""){
				printDataService = null;
				Toast.makeText(getActivity().getApplicationContext(), getString(R.string.cannot_find_bluetooth_printer), Toast.LENGTH_SHORT).show();
				return;
			}
			else{
				printDataService = new PrintDataService(getActivity().getApplicationContext(),address);
				printDataService.connect();
				if(!printDataService.isConnected)
					printDataService = null;
			}
	  }
	  
	  @Override
	  public void onStart() {
	      super.onStart();
	      if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_printer", false) == true)
	    	  getSharedReference();
	  } 
	  
	
}