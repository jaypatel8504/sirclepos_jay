package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.JSONObject;

import android.R.color;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.ButtonParam;
import com.intlpos.database.Department;

public class DepartmentLookupFragment extends Fragment 
{
	private View viewMain = null;
	private CornerStorePOS app;
//	private Context context = this;
	private final static int VIEW_MARGIN=2;
	private final static int BUTTON_MARGIN=10;
	private static final String TAG = "123";
	public int btnWidth = 105;
	public int btnHeight = 68;
	// var for viewPager
	private ArrayList<Fragment> fragmentsList;
	public ArrayList<Fragment> getFragmentsList() {
		return fragmentsList;
	}



	public void setFragmentsList(ArrayList<Fragment> fragmentsList) {
		this.fragmentsList = fragmentsList;
	}

	private ArrayList<ArrayList<ButtonParam>> btn_list;
	private int viewTag;
	private MyFragmentPagerAdapter adapter= null;
	private RadioGroup dotGroupButton = null;
	private ArrayList<ArrayList<Button>> btns_list;
	private ViewPager viewPager;
	private List<View> viewList;
	private View view;
//	private List<Department> departmentList;
	private int departmentSize = 0;
//	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	
//	Muhammad - got rid off webservice so it is no longer dynamic in POS Screen, only when logging in or syncing
	private void calltodeptwebservice() {
		// call to department display webservice
//		String methodName = "DepartmentMaintenence/DepartmentService.svc/getdepartments";
//
//		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
//
//		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
//		storedetail.put("email_id", CornerStorePOS.email_id);
//		storedetail.put("password", CornerStorePOS.password);
//		storedetail.put("store_id", CornerStorePOS.StoreId);
//		storedetail.put("station_id", CornerStorePOS.StationId);
//
//		params.put("storedetail", storedetail);

//		JSONParser jParser = new JSONParser(new MyListener() {

//			@Override
//			public void onResult(JSONObject json) {

//				try {

					if (app.DepartmentList == null) {
						Log.d(TAG, "is null");
					} else {
//						departmentList = new ArrayList<Department>();
//						departmentList = Department
//								.convertJSONtodepartment(json);
						departmentSize = app.DepartmentList.size();
						//
						Log.d("CSPOSDepartmentSize",""+departmentSize);
						
					 		int screenWidth = viewMain.getWidth();
					 		int screenHeight = viewMain.getHeight();
							DisplayMetrics dm = new DisplayMetrics();
							dm = viewMain.getContext().getResources().getDisplayMetrics();
							int deviceWidth = dm.widthPixels;
					 		if(deviceWidth > 2000){
					 			btnWidth = deviceWidth/13;
					 			btnHeight = deviceWidth/32;
					 		}
					 		 WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							 Display display = wm.getDefaultDisplay();
							 DisplayMetrics metrics = new DisplayMetrics();
							 display.getMetrics(metrics);
							 final int displaywidth = metrics.widthPixels;
							 final int displayheight = metrics.heightPixels;	
							 if(displaywidth > 1920 && displayheight > 1200){
								 btnWidth = btnWidth*2;
						 		 btnHeight = btnHeight*2;
							 }
					 		int buttonsPerPage = (screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)) *
					 				(screenHeight/(btnHeight+VIEW_MARGIN));
					 		
					 		
					 		viewList = new ArrayList<View>();
					 		

					         //final String sTitle[] = new String[101];
					 		//int total_buttons = sTitle.length;
					 		int total_buttons = departmentSize;
					 		int pages = (int) Math.ceil((double) total_buttons/buttonsPerPage);
					 	
					 	     // create buttons dynamically
					 		Log.d(TAG, "Btn#= "+ total_buttons + " Btn/Page= "+buttonsPerPage+ " Page#= "+pages);
					 		
					 		btn_list = new ArrayList<ArrayList<ButtonParam>>();
					 		ArrayList<ButtonParam> btn_params = new ArrayList<ButtonParam>();
					 		
					         int count = 0;
					         for (int i = 0; i<total_buttons; i++)
					         {
					         	btn_params.add(new ButtonParam(app.DepartmentList.get(i).getDept_Id(),app.DepartmentList.get(i).getDept_Name(), R.color.button_color,app.DepartmentList.get(i).getUrl()));
					         	
					         	
					 			count++;
					 			if (count == buttonsPerPage){
//					 				Log.d(TAG, "Page is FULL!");
					 				btn_list.add(btn_params);
					 				btn_params = new ArrayList<ButtonParam>();
					 				count = 0;
					 			}
					 		}
					         
					         //add the last several strings
					         if (btn_params.size() != 0)
					         	btn_list.add(btn_params);

					 			
					        viewPager = (ViewPager) getActivity().findViewById(R.id.testViewPager);//Layout of viewPager
					 		dotGroupButton = (RadioGroup) getActivity().findViewById(R.id.dotGroupButton);
					 		fragmentsList = new ArrayList<Fragment>();
					 		for (int i = 0; i < btn_list.size();i++)
					 		{
					 			// Create a MyViewGroup
					 			ArrayList<ButtonParam> list = btn_list.get(i);
					 			int size = list.size();
					 			
					 		//	int[] img_arr = new int[size];
					 			int[] color_arr = new int[size];
					 			//Rinkal - While Department LookUp
					 			String[] dept_id = new String[size];
					 			String[] dept_name = new String[size];
					 			String [] pic = new String[size];
					 			for (int j = 0 ; j < size; j++){
					 				dept_id[j] = list.get(j).getBtn_deptId();
					 				dept_name[j] = list.get(j).getBtn_deptName();
					 			//	img_arr[j] = list.get(j).getDrawable();
					 				color_arr[j] = list.get(j).getBgColor();
					 				pic[j] = list.get(j).getPic();
					 				
					 			}
					 				
//					 			Log.d(TAG, "Len of String arr = "+ array.length);
					 			Fragment mFragment = TestFragment.newInstance(btnWidth,btnHeight,dept_id,dept_name,color_arr,pic);
					 			//set button for each page
					 			final RadioButton dotButton = new RadioButton(getActivity().getBaseContext());
					 			dotButton.setId(i);
					 			dotButton.setLayoutParams(new RadioGroup.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
					 			dotButton.setPadding(20, 0, 20, 0);
//					 			dotButton.setBackgroundResource(R.drawable.dot_bg);//background of each page
					 			dotButton.setButtonDrawable(R.drawable.dot_bg);
					 			
					 			dotButton.setTag(i);//set tag for each page's listener
					 			
					 			dotButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					 				@Override
					 				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					 					if(isChecked){
					 						viewPager.setCurrentItem((Integer)dotButton.getTag(),true);
					 					}
					 				}
					 			});
					 			Log.d(TAG, "dotGroupButton = "+ dotGroupButton);
					 			dotGroupButton.addView(dotButton);
					 			dotGroupButton.check(0);
					 			fragmentsList.add(mFragment);
					 		}
					 		
					 	    adapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), fragmentsList);
					 		viewPager.setAdapter(adapter);
					 		adapter.notifyDataSetChanged();
					 		
					 		// set Page Change Listener (notice data changed to the adapter!)
					 		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() 
					 		{				

					 			@Override
					 			public void onPageSelected(int position) {
					 				((RadioButton)dotGroupButton.getChildAt(position)).setChecked(true);
//					 				viewPager.findViewWithTag(position);
//					 				adapter.notifyDataSetChanged();// load new page while page changing
					 			}
					 			
					 			@Override
					 			public void onPageScrolled(int arg0, float arg1, int arg2) {
					 			}
					 			
					 			@Override
					 			public void onPageScrollStateChanged(int arg0) {
					 			}
					 		});
					       
					 		
//					 			viewPager.setOffscreenPageLimit(3);
						//
						Log.d("Dept",""+departmentSize);
					}
//				} catch (Exception e) {
//					Log.d("GetDept", e.toString());
//				}
//			}
//		});

//		jParser.execute(CornerStorePOS.Url, methodName, params);
	}

	
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {		
		app = (CornerStorePOS) this.getActivity().getApplication();
		viewMain = inflater.inflate(R.layout.department_lookup_fragment, container, false);
		// I had to add this ViewTreeObserver because - createdepartments() needs to use this dept view to 
		// get screenwidth and screenheight so the function was getting null value so using this treeviewObserver function 
		// I am waiting for view to get created so my departmentlist creation function does not get the null value
		ViewTreeObserver observer = viewMain .getViewTreeObserver();   

	    observer.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void onGlobalLayout() {
				if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
					viewMain.getViewTreeObserver().removeGlobalOnLayoutListener(this);
					calltodeptwebservice();
				} else {
					viewMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
					calltodeptwebservice();
				}
				
				//viewMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//				createdepartments();
				
				
			}
		});
		
//		calltodeptwebservice();
       return viewMain;
	}
	
	
	
	
	public static DepartmentLookupFragment newInstance()
	{
		DepartmentLookupFragment deptlookupfragment = new DepartmentLookupFragment();
		return deptlookupfragment;
	}
	
}

