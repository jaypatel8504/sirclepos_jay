package com.intlpos.sirclepos;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.WCFAccess.RegisterParser;
import com.intlpos.sirclepos.R;
import com.intlpos.global.Utils;
import com.intlpos.initsetup.FragmentContainerActivity;
import com.intlpos.login.EulaDialog;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.mysharedpreferences.StoreDetail;

public class RegisterTablet extends Activity implements OnItemSelectedListener, OnClickListener{

	private Spinner country;
	private EditText registerInfo[] = new EditText[3];
	int VersionCode;
	String VersionName;
	private EditText mStationId;
	private Button mCreateStationButton;
	private PopupWindow StationPopUp;
	String getEditText[] = new String[3];
	String custInfo[]  = new String [10];
	String getStationId;
	String USAorCANADA;
	Button register[] = new Button[2];
	private CornerStorePOS app;
	private String ids[] = new String[10];
	EditText editInfo[] = new EditText[11];
	EditText editApp[] = new EditText[3]; 
	String getEditApp[] = new String[3];
	private Spinner stores;
	private EditText purchase;
	private String getPurchased;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	private boolean pay; 
	private final BroadcastReceiver wifiBroad = new MyWiFiStateListener();
	private TextView pos, info[];
	private String special_chars[] = {"\\t", "\\b", "\\n", "\\r", "\\f","+", "-", "&&", "||",  "(", ")", "{", "}", "[", "]", "^",
            "~", "*", "?", "\\", "\""};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		new EulaDialog(this).show();
		IntentFilter filter = new IntentFilter();
	    filter.addAction("android.net.wifi.STATE_CHANGE");
	    registerReceiver(wifiBroad, filter);  
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.registertablet);
		
		app = (CornerStorePOS) getApplication();
		connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
		addWidgets();
		setWidgets();
	}

	private void setWidgets() {
		
		register[0].setOnClickListener(this);
		register[1].setOnClickListener(this);
		
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.store, android.R.layout.simple_spinner_item);
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		stores.setAdapter(adapter);
		stores.setOnItemSelectedListener(this);
	}

	private void addWidgets() {
		
		registerInfo[0] = (EditText)findViewById(R.id.setEmailStep1);
		registerInfo[1] = (EditText)findViewById(R.id.password);
		registerInfo[2] = (EditText)findViewById(R.id.storeid);
		purchase = (EditText)findViewById(R.id.purchase);
		
		register[0] = (Button)findViewById(R.id.newStore);
		register[1] = (Button)findViewById(R.id.NewStation);
		
		stores = (Spinner) findViewById(R.id.storepurchase);
		
		
	}
	
	private void initiatepopupwindow() {
		LayoutInflater inflater = (LayoutInflater) RegisterTablet.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.station_create,
				(ViewGroup) findViewById(R.id.stationpopup));

		 WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 int width = metrics.widthPixels;
		 int height = metrics.heightPixels;
		if(width < 1920 && height < 1200)
			StationPopUp = new PopupWindow(layout, 250, 250, true);
		else
			StationPopUp = new PopupWindow(layout, 450, 450, true);
		StationPopUp.showAtLocation(layout, Gravity.CENTER, 0, 0);

		mStationId = (EditText) layout.findViewById(R.id.StationId);
		
		mCreateStationButton = (Button) layout
				.findViewById(R.id.CreateStaionButton);
		;
		mCreateStationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getStationId = mStationId.getText().toString();
            	int check = 0;
            	if(!getStationId.isEmpty()){
            		check = Integer.parseInt(getStationId);
            	}
            	else{
    				Toast.makeText(getApplicationContext(), R.string.EmptyID, Toast.LENGTH_LONG).show();
    				return;
            	}
            	if(check > 0 && check < 100){
            		if(CSSharedPreferences.getWifi()==true)
            			createStationId();
            		else
            			show();
            	}
            	else
            		Toast.makeText(getApplicationContext(), R.string.pick, Toast.LENGTH_LONG).show();
            	/*Bundle bundle = new Bundle();
				bundle.putStringArray("info", getEditText);
				bundle.putString("usORcan", USAorCANADA);
				Intent receipt = new Intent(getApplicationContext(), FragmentContainerActivity.class);
				receipt.putExtras(bundle);
				startActivity(receipt);*/
            }
        });

	}

	private void initiatepopupwindow2() {
		LayoutInflater inflater = (LayoutInflater) RegisterTablet.this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.createnewstation,
				(ViewGroup) findViewById(R.id.stationpopup2));

		 WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 int width = metrics.widthPixels;
		 int height = metrics.heightPixels;
		 
		if(width < 1920 && height < 1200)
			StationPopUp = new PopupWindow(layout, 250, 250, true);
		else
			StationPopUp = new PopupWindow(layout, 450, 450, true);
		StationPopUp.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		mStationId = (EditText) layout.findViewById(R.id.StationId2);
		mCreateStationButton = (Button) layout
				.findViewById(R.id.CreateStaionButton2);
		;
		mCreateStationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	getStationId = mStationId.getText().toString();
            	int check =0;
            	if(!getStationId.isEmpty()){
            		check = Integer.parseInt(getStationId);
            	}
            	else{
            		Toast.makeText(getApplicationContext(), R.string.EmptyID, Toast.LENGTH_LONG).show();
            		return;
            	}
            	if(check > 0 && check < 100){
            		if(CSSharedPreferences.getWifi() == true)
            				addStationId();
            		else
            			show();
            	}	
            	else
            		Toast.makeText(getApplicationContext(), R.string.pick, Toast.LENGTH_LONG).show();
            }
        });

	}
	
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
				if(arg0.getId() == R.id.storepurchase){
					String get = arg0.getItemAtPosition(arg2).toString();
					if(get.equals("Other")){
						purchase.setVisibility(View.VISIBLE);
						purchase.setText("");
					}else{
						purchase.setText(get);
						purchase.setVisibility(View.GONE);
					}
				}else{
					USAorCANADA =  arg0.getItemAtPosition(arg2).toString();
					if(USAorCANADA.equals("Other")){
						USAorCANADA = "";
						editInfo[10].setVisibility(View.VISIBLE);
					}
				}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			if(arg0.getId() == R.id.storepurchase){
				getPurchased = arg0.getItemAtPosition(0).toString();
				purchase.setText(getPurchased);
			}else
				USAorCANADA =  arg0.getItemAtPosition(0).toString();
		
		}

		@Override
		public void onClick(View v) {
			
			switch(v.getId()){
				case R.id.infoText:
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://smart360pos.com/sirclereg/Home.aspx"));
					startActivity(browserIntent);
					break;
				case R.id.newStore:
					boolean reg = true;
					for(int i = 0; i < registerInfo.length; i++)
					{
						getEditText[i] = registerInfo[i].getText().toString().replaceAll("\\s+","");
						if(getEditText[i].isEmpty()){
							Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
							reg = false;
							return;
						}
					}
					if(getEditText[0].contains(" ")){
						Toast.makeText(getApplicationContext(), R.string.emailspace, Toast.LENGTH_LONG).show();
						return;
					}
					if(purchase.getText().toString().isEmpty()){
						Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
						reg = false;
						return;
					}
					try {
						VersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
						VersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
					} catch (NameNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(reg == true && checkLength() == true)
						customerInfo();
					break;
					
				case R.id.NewStation:
					boolean reg1 = true;
					for(int i = 0; i < registerInfo.length; i++)
					{
						getEditText[i] = registerInfo[i].getText().toString().replaceAll("\\s+","");
						if(getEditText[i].isEmpty()){
							Toast.makeText(getApplicationContext(), "YOU MUST FILL OUT EVERY TEXTFIELD!", Toast.LENGTH_LONG).show();
							reg1 = false;
							return;
						}
					}
					if(purchase.getText().toString().isEmpty()){
						Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
						reg1 = false;
						return;
					}
					try {
						VersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
						VersionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
					} catch (NameNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					if(reg1 == true && checkLength() == true){
						if(CSSharedPreferences.getWifi() == false){
							show();
						}
						else{
							newStationTab();
						}
					}
					break;
			}
		}
		
		private void customerInfo(){
			LinearLayout dynam = (LinearLayout)findViewById(R.id.dynamic);
			dynam.removeAllViews();
			
			LinearLayout linear = new LinearLayout(this);
			linear.setOrientation(LinearLayout.VERTICAL);
			linear.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
			                                             LayoutParams.WRAP_CONTENT));
			
			TextView tv = new TextView(this);
			tv.setText(R.string.deviceRegistration);
			tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
			                                            LayoutParams.WRAP_CONTENT));
			
			tv.setTextSize(getResources().getDimension(R.dimen.textsize));
			linear.addView(tv);
			
			String getArray[] = getResources().getStringArray(R.array.infoArray);
			String getEditText[] = getResources().getStringArray(R.array.editText);
			ids = getResources().getStringArray(R.array.ids);
			info= new TextView[11];
			for(int i = 0; i < 11; i++){
				info[i] = new TextView(this);
				info[i].setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
				info[i].setText(getArray[i]);
				//linear.addView(info[i]);
			}
			
			
			for(int i = 0; i < 10; i++){
				editInfo[i] = new EditText(this);
				editInfo[i].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                        LayoutParams.WRAP_CONTENT));
				if(i != 10){
					editInfo[i].setHint(getEditText[i]);
				}
				editInfo[i].setSingleLine(true);   
				linear.addView(info[i]);
				linear.addView(editInfo[i]);
			}
			editInfo[0].requestFocus();
			editInfo[9].setRawInputType(InputType.TYPE_CLASS_NUMBER);
//			editInfo[10].setVisibility(View.GONE);
			
			linear.addView(info[10]);
			editInfo[10] = new EditText(this);
			editInfo[10].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT));
			editInfo[10].setSingleLine(true);   
			linear.addView(editInfo[10]);
			editInfo[10].setVisibility(View.GONE);
			country = new Spinner(this);
			ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(this, R.array.country_arrays, android.R.layout.simple_spinner_item);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			country.setAdapter(dataAdapter);
			country.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
			country.setOnItemSelectedListener(this);
			linear.addView(country);
			Button continued = new Button(this);
			continued.setText("Continue");
			
			continued.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
			continued.setWidth(165);
			
			continued.setBackgroundResource(R.drawable.loginbutton);
			
			linear.addView(continued);
			dynam.addView(linear);
			
			continued.setOnClickListener(new View.OnClickListener() {
				@Override
		        public void onClick(View v) {
					boolean reg = true;
					boolean isreplaced = replace();
		            for(int i = 0; i < custInfo.length; i++){
		            	custInfo[i] = editInfo[i].getText().toString();
		            	
		            	if(custInfo[i].isEmpty()){
		            		if(i != 5){
		            		Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
		            		reg = false;
		            		return;}
		            	}
		            }
		            if(USAorCANADA.isEmpty()){
		            	USAorCANADA = editInfo[10].getText().toString();
		            	if(USAorCANADA.isEmpty()){
			            	Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
			            	reg = false;
		            	}
		            }
		            if(reg == true && isreplaced == true){
		            	if(CSSharedPreferences.getWifi()==false){
							show();
						}
		            	else{
		            		if(checkNewStore() == true)
		            			registerTab();
		            	}
		            }
		        }
		    });
		}
		
		private void newStation(){
			LinearLayout dynam = (LinearLayout)findViewById(R.id.dynamic);
			dynam.removeAllViews();
			
			
			LinearLayout linear = new LinearLayout(this);
			linear.setOrientation(LinearLayout.VERTICAL);
			linear.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
			                                             LayoutParams.WRAP_CONTENT));
			
			TextView title = new TextView(this);
			title.setText(R.string.add);
			title.setTextSize(getResources().getDimension(R.dimen.textsize));
			title.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT));
			linear.addView(title);
			
			TextView subtitle = new TextView(this);
			subtitle.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT));
			subtitle.setText(R.string.pleasegotosettings);
			linear.addView(subtitle);
			
			
			String appinfo[] = getResources().getStringArray(R.array.AppInfo);
			TextView appText[] = new TextView[3];
			
			
			for(int i = 0; i < appText.length; i++){
				appText[i] = new TextView(this);
				appText[i].setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                    LayoutParams.WRAP_CONTENT));
				editApp[i] = new EditText(this);
				editApp[i].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
	                    LayoutParams.WRAP_CONTENT));
				editApp[i].setSingleLine(true);
				appText[i].setText(appinfo[i]);
				linear.addView(appText[i]);
				linear.addView(editApp[i]);
			}
			editApp[0].requestFocus();
			editApp[1].setInputType(InputType.TYPE_CLASS_TEXT | 
				    InputType.TYPE_TEXT_VARIATION_PASSWORD);
//			editApp[2].setRawInputType(InputType.TYPE_CLASS_NUMBER);
			Button continued = new Button(this);
			continued.setText("Continue");
			continued.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT));
			continued.setWidth(165);
			continued.setBackgroundResource(R.drawable.loginbutton);
			
			continued.setOnClickListener(new View.OnClickListener() {
				@Override
		        public void onClick(View v) {
					boolean reg = true;
		            for(int i = 0; i < getEditApp.length; i++){
		            	
		            	getEditApp[i] = editApp[i].getText().toString().replaceAll("\\s+","");
		            	
		            	if(getEditApp[i].isEmpty()){
		            		Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
		            		reg = false;
		            		return;
		            	}
		            }
		            if(!getPurchase()){
		            	reg = false;
		            	return;
		            }
		            if(reg == true){
		            	if(CSSharedPreferences.getWifi() == true)
		            		createNewStationTab();
		            	else
		            		show();
		            }
				}
		    });
			linear.addView(continued);
			continued.requestFocus();
			dynam.addView(linear);
		}
		
		private void registerTab(){
			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo wInfo = wifiManager.getConnectionInfo();
			String mac = wInfo.getMacAddress();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String dateReg = dateFormat.format(date).toString(); 
			
			String dat = "/Date("+System.currentTimeMillis()+")/";
			final String methodName = "check_credentials.svc/registration";
			
			LinkedHashMap<String, Object> cust = new LinkedHashMap<String, Object>();
			cust.put("device_address", mac);
			cust.put("registration_date", dat);
			cust.put("store_name", custInfo[0]);
			cust.put("email", custInfo[1]);
			cust.put("first_name", custInfo[2]);
			cust.put("last_name", custInfo[3]);
			cust.put("address1", custInfo[4]);
			cust.put("address2", custInfo[5]);
			cust.put("city", custInfo[6]);
			cust.put("state", custInfo[7]);
			cust.put("zipcode", custInfo[8]);
			cust.put("phone_no", custInfo[9]);
			cust.put("country", USAorCANADA);
			cust.put("fromwhere_place", getPurchased);
			
			final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("email_id", getEditText[0]);
			params.put("password", getEditText[1]);
			params.put("store_id", getEditText[2]);
			params.put("version_code", String.valueOf(VersionCode));
			params.put("version_name",VersionName);
			params.put("cust",cust);
			
			JSONParser jParser = new JSONParser(new MyListener() {

				@Override
				public void onResult(JSONObject json) {
					try {
			//			Log.d("RESULT", json.toString());
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if(resultString.equals("payment_module : False")){
							pay = false;
						}
						else if(resultString.equals("payment_module : True")){
							pay = true;
						}
						if(result == true)
						{
							Log.d("ISPAYTRUE",""+pay);
							initiatepopupwindow();
							
						}
						else{
							Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
						}
					}
					catch (Exception e) {
						JSONParser jParser = new JSONParser(new MyListener() {

							@Override
							public void onResult(JSONObject json) {
								try {
						//			Log.d("RESULT", json.toString());
									Boolean result = json.getBoolean("result");
									String resultString = json.getString("resultString");
									if(resultString.equals("payment_module : False")){
										pay = false;
									}
									else if(resultString.equals("payment_module : True")){
										pay = true;
									}
									if(result == true)
									{
										Log.d("ISPAYTRUE",""+pay);
										initiatepopupwindow();
										
									}
									else{
										Toast.makeText(getApplicationContext(), resultString, Toast.LENGTH_LONG).show();
									}
								}
								catch (JSONException e) {
									Log.e("JSONException", "" + e.toString());}
							}
						});
						jParser.execute(CornerStorePOS.BackupUrl, methodName, params);}
				}
			});
			jParser.execute(CornerStorePOS.Url, methodName, params);
		}	
		
		
		private void createNewStationTab(){
			WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			WifiInfo wInfo = wifiManager.getConnectionInfo();
			String mac = wInfo.getMacAddress();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			System.out.println(dateFormat.format(date));
			String dateReg = dateFormat.format(date).toString();
			final String methodName = "check_credentials.svc/registration";
			
			String dat = "/Date("+System.currentTimeMillis()+")/";
			
			
			LinkedHashMap<String, Object> cust = new LinkedHashMap<String, Object>();
			cust.put("device_address", mac);
			cust.put("registration_date", dat);
			cust.put("store name", "");
			cust.put("email", "");
			cust.put("first_name", "");
			cust.put("last_name", "");
			cust.put("address1", "");
			cust.put("address2", "");
			cust.put("city", "");
			cust.put("state", "");
			cust.put("zipcode", "");
			cust.put("phone_no", "");
			cust.put("country", "");
			cust.put("fromwhere_place", getPurchased);
			
			final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("email_id", getEditApp[0]);
			params.put("password", getEditApp[1]);
			params.put("store_id", getEditApp[2]);
			params.put("version_code", String.valueOf(VersionCode));
			params.put("version_name",VersionName);
			params.put("cust",cust);
			
			JSONParser jParser = new JSONParser(new MyListener() {

				@Override
				public void onResult(JSONObject json) {
					try {
						Log.d("RESULT", json.toString());
						Boolean result = json.getBoolean("result"); 
						String resultString = json.getString("resultString");
						if(resultString.equals("payment_module : False")){
							pay = false;
						}
						else if(resultString.equals("payment_module : True")){
							pay = true;
						}
						if(result == true)
						{
							Log.d("ISPAYTRUE",""+pay);
							initiatepopupwindow2();
						}
						else{
							Toast.makeText(RegisterTablet.this,
									resultString, Toast.LENGTH_LONG).show();
						}
					}
					catch (Exception e) {
						JSONParser jParser = new JSONParser(new MyListener() {

							@Override
							public void onResult(JSONObject json) {
								try {
									Log.d("RESULT", json.toString());
									Boolean result = json.getBoolean("result"); 
									String resultString = json.getString("resultString");
									if(resultString.equals("payment_module : False")){
										pay = false;
									}
									else if(resultString.equals("payment_module : True")){
										pay = true;
									}
									if(result == true)
									{
										Log.d("ISPAYTRUE",""+pay);
										initiatepopupwindow2();
									}
									else{
										Toast.makeText(RegisterTablet.this,
												resultString, Toast.LENGTH_LONG).show();
									}
								}
								catch (JSONException e) {
									Log.e("JSONException", "" + e.toString());}
							}
						});
						jParser.execute(CornerStorePOS.BackupUrl, methodName, params);}
				}
			});
			jParser.execute(CornerStorePOS.Url, methodName, params);
		}	
	
		public void createStationId(){
			final String methodName = "check_credentials.svc/createstation";
			final LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
			params.put("store_id", getEditText[2]);// changed station id to correct version!
			params.put("station_id", getStationId);
			
			JSONParser jParser = new JSONParser(new MyListener() {
				
				@Override
				public void onResult(JSONObject json) {

					try {
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if (result == true) {
							setPrefrences();
							app.ReadVariables();
							Bundle bundle = new Bundle();
							int size = getEditText.length + custInfo.length;
							String receiptDetail[] = new String[size]; 
							for(int i = 0; i < size; i++){
								if( i < 3){
									receiptDetail[i] = getEditText[i];
								}
								if(i >= 3){
									receiptDetail[i] = custInfo[i-3].replaceAll("\\\\", "");;
								}
							}
							bundle.putStringArray("info", receiptDetail);
							bundle.putString("usORcan", USAorCANADA);
							Intent receipt = new Intent(getApplicationContext(), FragmentContainerActivity.class);
							receipt.putExtras(bundle);
							startActivity(receipt);
						} else {
							Toast.makeText(RegisterTablet.this,
									resultString, Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {
						Log.e("JSONException", "" + e.toString());
					} catch (Exception e) {
						JSONParser jParser = new JSONParser(new MyListener() {
							
							@Override
							public void onResult(JSONObject json) {

								try {
									Boolean result = json.getBoolean("result");
									String resultString = json.getString("resultString");
									if (result == true) {
										setPrefrences();
										app.ReadVariables();
										Bundle bundle = new Bundle();
										int size = getEditText.length + custInfo.length;
										String receiptDetail[] = new String[size]; 
										for(int i = 0; i < size; i++){
											if( i < 3){
												receiptDetail[i] = getEditText[i];
											}
											if(i >= 3){
												receiptDetail[i] = custInfo[i-3].replaceAll("\\\\", "");;
											}
										}
										bundle.putStringArray("info", receiptDetail);
										bundle.putString("usORcan", USAorCANADA);
										Intent receipt = new Intent(getApplicationContext(), FragmentContainerActivity.class);
										receipt.putExtras(bundle);
										startActivity(receipt);
									} else {
										Toast.makeText(RegisterTablet.this,
												resultString, Toast.LENGTH_LONG).show();
									}

								} catch (JSONException e) {
									Log.e("JSONException", "" + e.toString());
								} catch (Exception e) {
									Log.e("DeviceReg", "" + e);
								}
								
								
							}
						});

						jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
					}
					
					
				}
			});

			jParser.execute(CornerStorePOS.Url, methodName, params);
		}
		
		
		private void newStationTab(){
			final String methodName = "check_credentials.svc/disablestore";
			
			final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("email_id", getEditText[0]);
			params.put("password", getEditText[1]);
			params.put("store_id", getEditText[2]);
			
			
			JSONParser jParser = new JSONParser(new MyListener() {

				@Override
				public void onResult(JSONObject json) {
					try {
						Log.d("RESULT", json.toString());
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if(result == true)
						{
							newStation();
						}
						else{
							Toast.makeText(RegisterTablet.this,
									resultString, Toast.LENGTH_LONG).show();
						}
					}
					catch (Exception e) {
						JSONParser jParser = new JSONParser(new MyListener() {

							@Override
							public void onResult(JSONObject json) {
								try {
									Log.d("RESULT", json.toString());
									Boolean result = json.getBoolean("result");
									String resultString = json.getString("resultString");
									if(result == true)
									{
										newStation();
									}
									else{
										Toast.makeText(RegisterTablet.this,
												resultString, Toast.LENGTH_LONG).show();
									}
								}
								catch (JSONException e) {
									Log.e("JSONException", "" + e.toString());}
							}
						});
						jParser.execute(CornerStorePOS.BackupUrl, methodName, params);}
				}
			});
			jParser.execute(CornerStorePOS.Url, methodName, params);
		}
		
		public void addStationId(){ //NEW STATION!!!!!!!
			final String methodName = "check_credentials.svc/createstation";
			final LinkedHashMap<String, String> params = new LinkedHashMap<String, String>();
			params.put("store_id", getEditApp[2]);
			params.put("station_id", getStationId);
			
			JSONParser jParser = new JSONParser(new MyListener() {
				
				@Override
				public void onResult(JSONObject json) {

					try {
						Boolean result = json.getBoolean("result");
						String resultString = json.getString("resultString");
						if (result == true) {
							setPrefrencesStation();
							app.ReadVariables();
							
							Intent i = new Intent(getApplicationContext(), EmployeeLogin.class);
							startActivity(i);
						} else {
							Toast.makeText(RegisterTablet.this,
									resultString, Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {
						Log.e("JSONException", "" + e.toString());
					} catch (Exception e) {
						JSONParser jParser = new JSONParser(new MyListener() {
							
							@Override
							public void onResult(JSONObject json) {

								try {
									Boolean result = json.getBoolean("result");
									String resultString = json.getString("resultString");
									if (result == true) {
										setPrefrencesStation();
										app.ReadVariables();
										
										Intent i = new Intent(getApplicationContext(), EmployeeLogin.class);
										startActivity(i);
									} else {
										Toast.makeText(RegisterTablet.this,
												resultString, Toast.LENGTH_LONG).show();
									}

								} catch (JSONException e) {
									Log.e("JSONException", "" + e.toString());
								} catch (Exception e) {
									Log.e("DeviceReg", "" + e);
								}
								
								
							}
						});

						jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
					}
					
					
				}
			});

			jParser.execute(CornerStorePOS.Url, methodName, params);
		}
		

		private void setPrefrences() throws NameNotFoundException {
			
			StoreDetail storedetail = new StoreDetail();		
			storedetail.setnotfirsttime(true);
			storedetail.setemail_id(getEditText[0]);
			storedetail.setpassword(getEditText[1]);
			storedetail.setstoreid(getEditText[2]);
			storedetail.setstationid(getStationId);	
			storedetail.setPaymentModule(pay);
			CSSharedPreferences.savePreferences(storedetail);
			
			
			storedetail.setExpress(true);
			CSSharedPreferences.setExpressCheckout(storedetail);
			
			storedetail.setVersion_code(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
			storedetail.setVersion_name(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
			
			CSSharedPreferences.saveUpdatePreferences(storedetail);
			
			setPreferenceStartTime();
		
		}
		
		
		private void setPrefrencesStation() throws NameNotFoundException {
			StoreDetail storedetail = new StoreDetail();		
			storedetail.setnotfirsttime(true);
			storedetail.setemail_id(getEditApp[0]);
			storedetail.setpassword(getEditApp[1]);
			storedetail.setstoreid(getEditApp[2]);
			storedetail.setstationid(getStationId);
			storedetail.setPaymentModule(pay);
			CSSharedPreferences.savePreferences(storedetail);
			
			storedetail.setExpress(true);
			CSSharedPreferences.setExpressCheckout(storedetail);
			
			storedetail.setVersion_code(getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);
			storedetail.setVersion_name(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
			
			CSSharedPreferences.saveUpdatePreferences(storedetail);
			
			setPreferenceStartTime();
		}
		
		private boolean replace(){
			for(int i = 0; i < registerInfo.length; i++)
			{
				getEditText[i] = registerInfo[i].getText().toString();
				if(getEditText[i].isEmpty()){
					Toast.makeText(getApplicationContext(), R.string.fillText, Toast.LENGTH_LONG).show();
					return false;
				}
			}
			return true;
		}
		
		public static void setPreferenceStartTime(){
			StartTime start = new StartTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			String currentDateandTime = sdf.format(new Date());
			start.setTime(currentDateandTime);
			
			CSSharedPreferences.saveStartTimePreferences(start);
			
		}
		
		private void show(){
			DialogFragment newFragment = WifiPopup.newInstance();
			newFragment.show(getFragmentManager(), "hello");
		}
		
		@Override
		public void onResume() {
		    super.onResume();  // Always call the superclass method first
		    connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    
		    IntentFilter filter = new IntentFilter();
		    filter.addAction("android.net.wifi.STATE_CHANGE");
		    registerReceiver(wifiBroad, filter);  
		}
		
		@Override
		public void onPause(){
			super.onPause();
			unregisterReceiver(wifiBroad);
		}
		
		private boolean checkLength(){
			if(getEditText[0].length() > 32){
				Toast.makeText(getApplicationContext(), R.string.emailEx, Toast.LENGTH_LONG).show();
				return false;
			}
			if(getEditText[1].length() > 45){
				Toast.makeText(getApplicationContext(), R.string.passwordEx, Toast.LENGTH_LONG).show();
				return false;
			}
			if(getEditText[2].length() > 7){
				Toast.makeText(getApplicationContext(), R.string.storeEx, Toast.LENGTH_LONG).show();
				return false;
			}
			return true;
		}
		
		private boolean checkNewStore(){
			String getCharacterCheck[] = getResources().getStringArray(R.array.characterCheck);
			for(int i = 0; i < custInfo.length; i++){
				if(custInfo[i].length() > 32){
					Toast.makeText(getApplicationContext(), getCharacterCheck[i], Toast.LENGTH_LONG).show();
					return false;
				}
				if(i == 1){
					if(!isEmailValid(custInfo[i])){
						Toast.makeText(getApplicationContext(), R.string.fixemailCust, Toast.LENGTH_LONG).show();
						return false;
					}
				}
			}
			
			
			String info[] = getResources().getStringArray(R.array.infoArray);
			for(int i = 0; i <custInfo.length; i++){
				for(int q = 0; q < special_chars.length; q++){
					if(custInfo[i].contains(special_chars[q])){
						Toast.makeText(getApplicationContext(), "NO " + special_chars[q] + " in " + info[i], Toast.LENGTH_SHORT).show();
						return false;
					}
				}
			}
			

			for(int i = 0; i <custInfo.length; i++){
					if(custInfo[i].contains("\'")){
						String temp = custInfo[i].replaceAll("'", "\\\\'");
						custInfo[i] = temp;
					}
			}
			
			if(!getPurchase())
				return false;
			
			
			return true;
		}
		
		  public static boolean isTablet(Context context) {
		        return (context.getResources().getConfiguration().screenLayout
		                & Configuration.SCREENLAYOUT_SIZE_MASK)
		                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
		    }
		  
		  public static boolean isEmailValid(String email) {
			  return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
			}
		  
		  private boolean getPurchase(){
			  getPurchased = purchase.getText().toString();
				
				for(int i = 0; i < special_chars.length; i++){
					if(getPurchased.contains(special_chars[i])){
						Toast.makeText(getApplicationContext(), "NO " + special_chars[i] + " in " + getResources().getString(R.string.purchase), Toast.LENGTH_SHORT).show();
						return false;
					}
				}
				
				if(getPurchased.contains("\'")){
					String temp = getPurchased.replaceAll("'", "\\\\'");
					getPurchased = temp;
				}
				return true;
		  }
}
