package com.intlpos.sirclepos;

import java.util.Locale;

import com.intlpos.sirclepos.R;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

public class ScreenOnReceiver extends BroadcastReceiver {
	EmployeeLoginPopupWindow windows;
	public ScreenOnReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
			Log.d("Intent", "Screen On");
			//Toast.makeText(context, "Screen On", Toast.LENGTH_SHORT).show();
			View layout = LayoutInflater.from(context).inflate(R.layout.employee_login, null);
			if (windows == null){
				if(!Locale.getDefault().toString().equals("en_US")){
					windows = new EmployeeLoginPopupWindow(context,layout ,410,440, false, false);
				}else{
					DisplayMetrics dm = new DisplayMetrics();
					dm = context.getApplicationContext().getResources().getDisplayMetrics();

					if(dm.widthPixels == 1280 && dm.heightPixels == 752){
						windows = new EmployeeLoginPopupWindow(context,layout ,430, 360, false,false);
					}else
						windows = new EmployeeLoginPopupWindow(context,layout ,400, 340, false,false);
				}
			}
			windows.getAccessIdEditText().getText().clear();
		    windows.showAtLocation(layout, Gravity.CENTER, 0, 0);
		}
		// an Intent broadcast.
		//throw new UnsupportedOperationException("Not yet implemented");
	}
}
