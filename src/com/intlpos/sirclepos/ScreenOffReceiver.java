package com.intlpos.sirclepos;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

public class ScreenOffReceiver extends BroadcastReceiver {
	public ScreenOffReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
			Log.d("Intent", "Screen Off");
			//Toast.makeText(context, "Screen On", Toast.LENGTH_SHORT).show();
			Intent i = new Intent(context,EmployeeLogin.class);
			context.startActivity(i);
			((Activity) context).finish();
		}
		// an Intent broadcast.
		//throw new UnsupportedOperationException("Not yet implemented");
	}
}
