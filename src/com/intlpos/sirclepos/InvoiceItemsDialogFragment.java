package com.intlpos.sirclepos;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.WCFAccess.JSONParserNew;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Invoice;
import com.intlpos.database.InvoiceItemListViewAdapter;
import com.intlpos.database.InvoiceItemParcelable;
import com.intlpos.database.InvoiceParcelable;
import com.intlpos.database.InvoicesListViewAdapter;
import com.intlpos.database.Order;
import com.intlpos.initsetup.PostRequestTask;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.paymentprocessing.PaymentListener;
import com.intlpos.paymentprocessing.chasepaymentech;
import com.intlpos.paymentprocessing.velocity;
import com.intlpos.sirclepos.InvoicesDialogFragment.OnInvoiceSelectedListener;
import com.john.beans.FetchInvoice;
import com.john.bluetoothprinter.helper.PrintDataService;
import com.john.bluetoothprinter.helper.ReceiptBuilder;
import com.usaepay.sdk.classes.Customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class InvoiceItemsDialogFragment extends DialogFragment{
	private static final SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
	private ArrayList<Order> data;
	private FetchInvoice invoice;
	private ListView listview;
	private Button printReceipt,voidInvoice;
	private TextView subtotal,tax,grandtotal, sid, vid, auth;
	private InvoiceItemListViewAdapter adapter;
	private OnPrintRecallInvoiceListener mListener;
    private CornerStorePOS app;
    private String paymentprocessor;
    private Context context;
	static InvoiceItemsDialogFragment newInstance(InvoiceParcelable invoice) {
        InvoiceItemsDialogFragment newFragment = new InvoiceItemsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("invoice", invoice);
		newFragment.setArguments(bundle);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        InvoiceParcelable invoiceParcelable = args.getParcelable("invoice");
        invoice = invoiceParcelable.getInvoice();
      	data = invoice.getValue();
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		getDialog().getWindow().setTitle(getActivity().getString(R.string.invoice_detail));
		getDialog().getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_white));
		final View view = inflater.inflate(R.layout.invoiceitemlist_dialogfragment, container, false);
		View header = inflater.inflate(R.layout.invoiceitemlist_item_header, null);
		final Invoice invoiceBody = invoice.getKey();
		app = (CornerStorePOS) getActivity().getApplicationContext();
		paymentprocessor = CSSharedPreferences.getPayment().getGateway();
		context = InvoiceItemsDialogFragment.this.getActivity();
		listview = (ListView) view.findViewById(R.id.invoice_listview);
		subtotal = (TextView) view.findViewById(R.id.subtotal_amt);
		tax = (TextView) view.findViewById(R.id.tax_amt);
		grandtotal = (TextView) view.findViewById(R.id.grandtotal_amt);
		subtotal.setText(invoice.getKey().getSub_total().setScale(2,BigDecimal.ROUND_HALF_EVEN).toString());
		sid = (TextView) view.findViewById(R.id.s_id);
		vid = (TextView) view.findViewById(R.id.v_id);
		sid.setText("Sold Cashier ID: "+ invoiceBody.getS_cashier_id());
		String dates[] = invoiceBody.getInvoice_date().split(" ");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		Date currentdate = new Date();
		try {
			date = dateFormat.parse(dates[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!invoiceBody.getV_cashier_id().isEmpty())
			vid.setText("Void Cashier ID: "+invoiceBody.getV_cashier_id());
		BigDecimal totalTax = invoice.getKey().getTotal_tax1().add(invoice.getKey().getTotal_tax2().add(invoice.getKey().getTotal_tax3()));
		tax.setText(totalTax.setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
		grandtotal.setText(invoice.getKey().getGrand_total().setScale(2, BigDecimal.ROUND_HALF_EVEN).toString());
		printReceipt = (Button) view.findViewById(R.id.print);
		printReceipt.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				
				Log.d("id", Integer.toString(invoiceBody.getCustomer_id()));
				String name = "";
				for(int i =0; i < app.CustomerList.size(); i++){
					if(app.CustomerList.get(i).getCustomer_id() == invoiceBody.getCustomer_id()){
						name = app.CustomerList.get(i).getFirst_name() + " " + app.CustomerList.get(i).getLast_name();
					}
				}
				ReceiptBuilder rb = new ReceiptBuilder(data,invoiceBody,invoiceBody.getInvoice_id()+"",true,view.getContext(),name,0);
				mListener.printRecallInvoice(rb);
			}
		});
		auth = (TextView) view.findViewById(R.id.auth);
		voidInvoice = (Button) view.findViewById(R.id.voidin);
		if(invoiceBody.getAuth_code() != null && !invoiceBody.getAuth_code().isEmpty()){
			auth.setText("Auth Code: " + invoiceBody.getAuth_code());
		}
		
		if(!dateFormat.format(date).equals(dateFormat.format(currentdate)))
			voidInvoice.setEnabled(false);
		if(!invoiceBody.getStatus().equals("S") || invoiceBody.getPayment_type() == 1){
			voidInvoice.setEnabled(false);
		}else{
			voidInvoice.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if(!CSSharedPreferences.getExpressCheckout().getExpress()){
						if(invoiceBody.getAuth_code() != null)
						{
							switch(paymentprocessor)
							{
							case "velocity": case "velocityemv":
								Log.d("velocity-void in ","velocity-void in");
								final ProgressDialog progressDialog = new ProgressDialog(context);
								velocity velocity = new velocity(context,progressDialog,new PaymentListener() {
	
									@Override
									public void onResult(String[] result) 
									{
										// TODO Auto-generated method stub
										progressDialog.dismiss();
										//Log.d("velocity result - ",result[0]+""+result[1]);
										if(result[0].equalsIgnoreCase("Successful")|| result[0].equalsIgnoreCase("AP")|| result[0].equalsIgnoreCase("APPROVAL"))
										{
											voidinvoice(invoiceBody);
											Toast.makeText(context, "Transaction voided", Toast.LENGTH_LONG).show();
											
										}
										else
										{
											Toast.makeText(context, result[0].toString(), Toast.LENGTH_LONG).show();
										}
	
									}
								});
								velocity.runTransaction("Undo",invoiceBody.getGrand_total(),invoiceBody.getAuth_code());
								progressDialog.dismiss();	
								break;
							case "chasepaymentech":
								chasepaymentech chase = new chasepaymentech(context,new PaymentListener() {	
									@Override
									public void onResult(String[] result) {
										// TODO Auto-generated method stub
										if(result[0].equals("Approved"))
										{
											
											AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
													context);
	
												alertDialogBuilder.setTitle("Please Confirm That Transaction Is Voided");
	
												alertDialogBuilder
													.setMessage("Click yes to confrim!")
													.setCancelable(false)
													.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
														public void onClick(DialogInterface dialog,int id) {
															voidinvoice(invoiceBody);
														}
													  })
													.setNegativeButton("No",new DialogInterface.OnClickListener() {
														public void onClick(DialogInterface dialog,int id) {
															dialog.cancel();
														}
													});
	
													AlertDialog alertDialog = alertDialogBuilder.create();
	
													alertDialog.show();
											
											
											//Toast.makeText(context, "voided", Toast.LENGTH_LONG).show();
										}
										else
										{
											Toast.makeText(context, result[0].toString(), Toast.LENGTH_LONG).show();
										}
	
									}
								},context.getResources().getStringArray(R.array.chaseResources));
								
								String payment = Invoice.PAYMENT_TYPE[invoiceBody.getPayment_type()];
								chase.execute("void",payment.toLowerCase(),invoiceBody.getGrand_total());
								break;
							
							default:
								voidinvoice(invoiceBody);
								break;
							}
						}
					}
					else
					{
						voidinvoice(invoiceBody);
					}
					
				}
			});
		}
		adapter = new InvoiceItemListViewAdapter(getActivity(), data);
		listview.addHeaderView(header, null, false);
		listview.setAdapter(adapter);
//		listview.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
//				dismiss();
//			            }
//			        });
		
		
        return view;
    }
	
	private void voidinvoice(Invoice invoiceBody){
		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		
//		StoreDetail storedetail = new StoreDetail();
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		
		params.put("storedetail", storedetail);
		params.put("invoice_id", invoiceBody.getInvoice_id());
		params.put("v_cashier_id", app.employee.employees_id);
		final String methodName = "InvoiceMaintenence/InvoiceService.svc/voidinvoice";
		
		JSONParserNew jParser = new JSONParserNew(new MyListener() {
			
			@Override
			public void onResult(JSONObject json) {
				try {
					boolean voided = json.getBoolean("result");
					if(voided){
						Toast.makeText(context, R.string.voided, Toast.LENGTH_LONG).show();
						voidInvoice.setEnabled(false);
					}else{
						
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnPrintRecallInvoiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}


	public interface OnPrintRecallInvoiceListener {
		// TODO: Update argument type and name
		public void printRecallInvoice(ReceiptBuilder rb);
	}
    @Override
    public void onPause() {
        super.onPause();
    }	

    public int convertToPx(int input) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (input * scale + 0.5f);
}
    public static ArrayList<InvoiceParcelable> getInvoicesList(String dateNow){
    	final String GET_INVOICE_URL = "InvoiceMaintenence/InvoiceService.svc/fetchdetailedinvoices";
		ArrayList<InvoiceParcelable> invoicelist = new ArrayList<InvoiceParcelable>();
    	PostRequestTask postRequest = new PostRequestTask();
		JSONObject storedetail = new JSONObject();
    	JSONObject getInvoice = new JSONObject();
    	try {
			storedetail.put("email_id", CornerStorePOS.email_id);
        	storedetail.put("password", CornerStorePOS.password);
        	storedetail.put("store_id", CornerStorePOS.StoreId);
        	storedetail.put("station_id", CornerStorePOS.StationId);
        	getInvoice.put("storedetail", storedetail);

        	getInvoice.put("invoice_date", dateNow);

//        	Log.d("response", "url:"+CornerStorePOS.Url+GET_INVOICE_URL+" json:"+getInvoice.toString());
			postRequest.execute(CornerStorePOS.Url+GET_INVOICE_URL,getInvoice.toString());
        	String response = "";
        	try {
    			response = postRequest.get();
//    			Type type = new TypeToken<ArrayList<FetchInvoice>>(){}.getType();
//    			Gson gson = new Gson();
//    			ArrayList<FetchInvoice> list = gson.fromJson(response, type);
    			JSONArray jsonarray = new JSONArray(response);
//    			Log.d("response", jsonarray.get(0).toString());
    			for(int i = 0; i < jsonarray.length();i++){
    				invoicelist.add(new InvoiceParcelable(new FetchInvoice(jsonarray.getJSONObject(i))));
    			}
//    			Log.d("response", list.size()+"");
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		} catch (ExecutionException e) {
    			e.printStackTrace();
    		}
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	return invoicelist;
    }
}
