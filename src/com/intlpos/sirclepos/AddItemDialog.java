package com.intlpos.sirclepos;


import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.intlpos.sirclepos.R;
import com.intlpos.global.KeyPad;
import com.intlpos.global.KeyPad2;
import com.intlpos.initsetup.AddInventory;
import com.intlpos.initsetup.AddStandardInventory;
import com.intlpos.initsetup.PostRequestTask;
import com.john.beans.Department;
import com.john.beans.Product;
import com.john.beans.StoreDetail;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupWindow.OnDismissListener;
/**
 * Customized Dialog for quick adding items
 * @author Long
 *
 */
public class AddItemDialog extends Dialog{
	private AddItemDialog dialog = this;
	private Context context;
	private String GET_URL = "DepartmentMaintenence/DepartmentService.svc/getdepartments";
	protected String GET_COUNT_URL = "InventoryImport/ImportService.svc/getproductscount";
	private String ADD_URL = "InventoryMaintenence/InventoryService.svc/saveproduct";
	private ArrayList<Department> department_list;
	private int addInventoryIndex;
	private EditText barcode,itemName,cost,price,itemStock;
	private TextView tv;
	private Spinner department_spinner;
	private CheckBox tax1,tax2,tax3;
	private PostRequestTask postTask;
	private boolean isAutofill;
	private Button submit;
	private CornerStorePOS app;
	private Button finish;
	private int productCount = -1;
	private ArrayList<BigDecimal> textArray;
	public AddItemDialog(Context context) {
		super(context);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);        
        setContentView(R.layout.fragment_addstandardinventory);
		app = (CornerStorePOS) context.getApplicationContext();
		ADD_URL = CornerStorePOS.Url + ADD_URL;
		GET_URL = CornerStorePOS.Url + GET_URL;
		getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.shape_white));
		
        WindowManager.LayoutParams lp=this.getWindow().getAttributes();
        
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 final int displaywidth = metrics.widthPixels;
		 final int displayheight = metrics.heightPixels;	
		 if(displaywidth < 1920 && displayheight < 1200){
	        lp.width = 800;
	        lp.height = 500;
		 }else{
			 lp.width = 1600;
		     lp.height = 1000;
		 }
        this.getWindow().setAttributes(lp);
        TextView tv = (TextView) findViewById(R.id.textview);
        tv.setVisibility(View.GONE);
        tv = (TextView) findViewById(R.id.cost_text);
        tv.setVisibility(View.GONE);
        tv = (TextView) findViewById(R.id.stock_text);
        tv.setVisibility(View.GONE);
        tv = (TextView) findViewById(R.id.department_text);
        tv.setVisibility(View.GONE);
     // get product counts
		PostRequestTask postTask = new PostRequestTask();
		Gson gson = new Gson();
		CornerStorePOS app = (CornerStorePOS) context.getApplicationContext();
		GetProductCountRequest getcountrequest = new GetProductCountRequest(app.storedetail);
		Log.d("getCount", gson.toJson(getcountrequest));
    	postTask.execute(CornerStorePOS.Url+GET_COUNT_URL,gson.toJson(getcountrequest));
    	String result = null;
    	try {
			result = postTask.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
    	try{
    		productCount = Integer.parseInt(result);
    	}catch(Exception e){
    		Toast.makeText(context, R.string.cannot_get_products_number, Toast.LENGTH_SHORT).show();
    	}
	String[] name = context.getResources().getStringArray(R.array.inventory_input);
	if(isAutofill){
		tv.setText(name[1]);
	}else{
		tv.setText(name[0]);
	}
	barcode = (EditText) findViewById(R.id.barcode_itemnumber);
	itemName = (EditText) findViewById(R.id.itemname);
	itemName.setText("Quick Add Item");
	cost = (EditText) findViewById(R.id.cost);
	cost.setVisibility(View.GONE);
	price = (EditText) findViewById(R.id.price);
	itemStock = (EditText) findViewById(R.id.itemstock);
	itemStock.setVisibility(View.GONE);
	department_spinner = (Spinner) findViewById(R.id.spinner_department);
	department_spinner.setVisibility(View.GONE);
	tax1 = (CheckBox) findViewById(R.id.tax1);
	tax1.setChecked(true);
	tax2 = (CheckBox) findViewById(R.id.tax2);
	tax3 = (CheckBox) findViewById(R.id.tax3);
		//saveStandardInventory();
		submit = (Button) findViewById(R.id.submitinventory);
		finish = (Button) findViewById(R.id.finishlogininventory);
		submit.setOnClickListener(new View.OnClickListener(){
		
			@Override
			public void onClick(View v) {
				if (barcode.getText().toString() == "" || barcode.getText().toString().isEmpty() || itemName.getText().toString() == ""|| itemName.getText().toString().isEmpty()){
					Toast.makeText(context, R.string.barcode_and_itemname_cannot_be_empty, Toast.LENGTH_SHORT).show();
					return;
				}
				String price_text = price.getText().toString().isEmpty() ? "0.00":price.getText().toString();
				textArray = new ArrayList<BigDecimal>(3);
				textArray.add(new BigDecimal(price_text).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				textArray.add(BigDecimal.ZERO);
				textArray.add(new BigDecimal(100));
				for(BigDecimal dec:textArray){
					if (dec.compareTo(new BigDecimal(100000)) == 1){
						Toast.makeText(context, "Price/Cost/Stock cannot larger than 100000", Toast.LENGTH_SHORT).show();
						return;
					}
				}
				boolean flag = saveStandardInventory();
				if (flag){
					Toast.makeText(context, R.string.product_saved , Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(context, R.string.save_product_failed , Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		if(productCount == -1 || productCount > 6000){
		//	Toast.makeText(getActivity(), productCount+"" , Toast.LENGTH_SHORT).show();
			submit.setEnabled(false);
			submit.setText(R.string.product_count_is_invalid);
		}
		finish.setOnClickListener(new View.OnClickListener(){
		
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
			
		});
	}
	
	private class GetProductCountRequest{
		StoreDetail storedetail;
		public GetProductCountRequest(StoreDetail storedetail){
			this.storedetail = storedetail;
		}
	}
	class SaveInventory{
		StoreDetail storedetail;
		Product product;
		public String modifiergroupslist;
		public SaveInventory(StoreDetail storedetail, Product product){
			this.storedetail = storedetail;
			this.product = product;
		}
		public SaveInventory() {
			// TODO Auto-generated constructor stub
		}
	}
	private boolean saveStandardInventory() {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		int pos = department_spinner.getSelectedItemPosition();
		department_list = getDepartmentList();
		Product product = new Product(barcode.getText().toString(),itemName.getText().toString(),textArray.get(0).toString(),textArray.get(1).toString()
    			,textArray.get(2).toString(),tax1.isChecked(),tax2.isChecked(),tax3.isChecked(),department_list.get(0).getDepartment_id(),department_list.get(0).getDepartment_name(), "", false, false, false, false);
    	SaveInventory saveInventory = new SaveInventory();
    	saveInventory.product = product;
    	saveInventory.storedetail = app.storedetail;
    	saveInventory.modifiergroupslist = "";
		String request = gson.toJson(saveInventory);
		Log.d("123", request);
		postTask = new PostRequestTask();
		postTask.execute(ADD_URL,request);
		String response = "";
		try {
			response = postTask.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Log.d("123", response);
		JSONObject obj;
		try {
			obj = new JSONObject(response);
	    	if (obj.get("result").equals(true)){
	    		return true;
	    	}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private ArrayList<Department> getDepartmentList() {
		// TODO Auto-generated method stub
		JSONObject storedetail = new JSONObject();
		JSONObject obj = new JSONObject();
		ArrayList<Department> depts = null;
		try {
        	storedetail.put("email_id", CornerStorePOS.email_id);
        	storedetail.put("password", CornerStorePOS.password);
        	storedetail.put("store_id", CornerStorePOS.StoreId);
        	storedetail.put("station_id", CornerStorePOS.StationId);
        	obj.put("storedetail", storedetail);
    		postTask = new PostRequestTask();
    		postTask.execute("http://smart360pos.com/sirclepos/v1_320/DepartmentMaintenence/DepartmentService.svc/getdepartments",obj.toString());
    		
    		String response = "";
    		try {
    			response = postTask.get();
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ExecutionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		obj = new JSONObject(response);
    		Gson gson = new Gson();
    		Type listType = new TypeToken<ArrayList<Department>>() {}.getType();
    			
    		depts = gson.fromJson(obj.getString("department_list"), listType);
//        	Log.d("123", depts.size()+"");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(context, R.string.request_failed , Toast.LENGTH_SHORT).show();
		}
		
		return depts;
	}

}
