package com.intlpos.sirclepos;



import javax.swing.text.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MyImageButton extends ImageButton {
    private String text = null;  //Text of the button
    private String price;
    private Context context;
    private boolean yes;
    private boolean edit;
    private boolean depart;
	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public boolean isYes() {
		return yes;
	}

	public void setYes(boolean yes) {
		this.yes = yes;
	}

	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}

	private int item_id;
    public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public boolean selected;

	private int color;               //Color of the text
    public MyImageButton(Context context, AttributeSet attrs) {
        super(context,attrs);
        this.context = context;
    }
    
    public MyImageButton(Context context) {
        super(context);
        this.context = context;
    }
    
    public MyImageButton(Context context,String price) {
        super(context);
        this.context = context;
        this.price = price;
    }
    
    public MyImageButton(Context context,boolean depart) {
        super(context);
        this.context = context;
        
        this.depart = depart;
    }
     
    public void setText(String text){
        this.text = text;       //set text
    }
     
    public void setTextColor(int color){
        this.color = color;    //set Text color
    }
     
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        paint.setTextAlign(Paint.Align.CENTER);
		DisplayMetrics dm = new DisplayMetrics();
		dm = context.getResources().getDisplayMetrics();
		int deviceWidth = dm.widthPixels;
        paint.setTypeface(Typeface.SANS_SERIF);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		 Display display = wm.getDefaultDisplay();
		 DisplayMetrics metrics = new DisplayMetrics();
		 display.getMetrics(metrics);
		 final int displaywidth = metrics.widthPixels;
		 final int displayheight = metrics.heightPixels;	
		 if(displaywidth < 1920 && displayheight < 1200){
			 paint.setTextSize(15);
		 }else{
			 paint.setTextSize(30);
		 }
//        paint.setTextSize(deviceWidth/95);
        paint.setColor(color);  
        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("switch_pictures", false)){
	        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("ui", false)){
	        	if(depart == true){
	        		if(text.length() > 10){
		    			text = text.substring(0, 11);
		    			text += "...";
		    		}
		        	canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()-5, paint);
	        	}else{
	        		 if(displaywidth < 1920 && displayheight < 1200){
	        			 paint.setTextSize(18);
	        		 
			        	if(text.length() >= 18){
			    			text = text.substring(0, 14);
			    			text += "...";
			    		}
			        	
			        	canvas.drawText(text, canvas.getWidth()/2, deviceWidth/10, paint);
	        		 }else{
	        			 if(text.length() >= 14){
				    			text = text.substring(0, 11);
				    			text += "...";
				    		}
	        			 paint.setTextSize(36);
	        			 canvas.drawText(text, canvas.getWidth()/2, (canvas.getHeight()/2)+75, paint);
	        		 }
	        	}
	        }else{
		        if(price == null || price == ""){
		        	if(text.length() > 10){
		    			text = text.substring(0, 11);
		    			text += "...";
		    		}
		        	canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()-5, paint);
		        }else{
		        	if(text.length() > 10){
		    			text = text.substring(0, 7);
		    			text += "...";
		    		}
		        	if(displaywidth < 1920 && displayheight < 1200)
		        		canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()-5, paint);
		        	else
		        		canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()-5, paint);
	//		        if(text.length() < 10)
	//		        	canvas.drawText(text, deviceWidth/22, deviceWidth/50, paint);  //onDraw text
	//		        else{
	//		        	if(text.length() > 20)
	//		        		text = text.substring(0, 20);
	//		        	canvas.drawText(text.substring(0, 10), deviceWidth/22, deviceWidth/75, paint);  //onDraw text
	//		        	canvas.drawText(text.substring(10), deviceWidth/22, deviceWidth/40, paint);  //onDraw text
	//		        }
		        }
		        if(price != null && price != "0.00"){
		            paint.setTextAlign(Paint.Align.CENTER);
		            paint.setTypeface(Typeface.SANS_SERIF);
		            if(displaywidth < 1920 && displayheight < 1200){
		   			 	paint.setTextSize(15);
		   			 	canvas.drawText(price, canvas.getWidth()/2, canvas.getHeight()-18, paint);
			   		 }else{
			   			 paint.setTextSize(30);
			   			canvas.drawText(price, canvas.getWidth()/2, canvas.getHeight()-28, paint);
			   		 }
		        	  //onDraw text
		        }
	        }
        }else{
        	if(PreferenceManager.getDefaultSharedPreferences(context).getBoolean("ui", false)){
        		 if(displaywidth < 1920 && displayheight < 1200){
        			 paint.setTextSize(18);
        		 }else{
        			 paint.setTextSize(36);
        		 }
	        	if(text.length() > 20){
	    			text = text.substring(0, 16);
	    			text += "...";
	    		}
	        	canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()/2, paint);
	        }else{
	        	if(price == null || price == ""){
		        	if(text.length() > 10){
		    			text = text.substring(0, 11);
		    			text += "...";
		    		}
		        	canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()/2, paint);
		        }else{
		        	if(text.length() > 10){
		    			text = text.substring(0, 11);
		    			text += "...";
		    		}
		        	canvas.drawText(text, canvas.getWidth()/2, canvas.getHeight()/2, paint);
	//		        if(text.length() < 10)
	//		        	canvas.drawText(text, deviceWidth/22, deviceWidth/50, paint);  //onDraw text
	//		        else{
	//		        	if(text.length() > 20)
	//		        		text = text.substring(0, 20);
	//		        	canvas.drawText(text.substring(0, 10), deviceWidth/22, deviceWidth/75, paint);  //onDraw text
	//		        	canvas.drawText(text.substring(10), deviceWidth/22, deviceWidth/40, paint);  //onDraw text
	//		        }
		        }
		        if(price != null && price != "0.00"){
		            paint.setTextAlign(Paint.Align.CENTER);
		            paint.setTypeface(Typeface.SANS_SERIF);
		            if(displaywidth < 1920 && displayheight < 1200){
	        			 paint.setTextSize(15);
	        		 }else{
	        			 paint.setTextSize(30);
	        		 }
		        	canvas.drawText(price, canvas.getWidth()/2, (canvas.getHeight()/2)-18, paint);  //onDraw text
		        }
	        }
        }
    }

 
}