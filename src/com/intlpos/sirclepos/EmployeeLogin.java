package com.intlpos.sirclepos;

import java.util.Locale;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.intlpos.sirclepos.R;
import com.intlpos.global.AboutDialog;
import com.intlpos.login.EulaDialog;
import com.intlpos.login.WhatsNewDialog;

public class EmployeeLogin extends Activity {
	final public int ABOUT = 0;
	View layout;
	Button btn[] = new Button[14];
	EditText AccessIdEditText;
	EmployeeLoginPopupWindow windows;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);
		Bundle extras = getIntent().getExtras();
		if(extras != null && extras.getBoolean("has_update")){
			new WhatsNewDialog(this).show();
		}
		
		
		DisplayMetrics dm = new DisplayMetrics();
		dm = this.getApplicationContext().getResources().getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		Log.d("Size", ""+dm.widthPixels + "x" + dm.heightPixels);
		int width = 400;
		if (screenWidth > 2000) {
			width = 800;
		}
		layout = LayoutInflater.from(this).inflate(R.layout.employee_login, null);
		if(!Locale.getDefault().toString().equals("en_US")){
			windows = new EmployeeLoginPopupWindow(this,layout ,410, 440, true,false);
	    }
		else{
			if(dm.widthPixels == 1280 && dm.heightPixels == 752){
				windows = new EmployeeLoginPopupWindow(this,layout ,430, 360, true,false);
			}else if(dm.widthPixels < 1920 && dm.heightPixels < 1200)
				windows = new EmployeeLoginPopupWindow(this,layout ,400, 340, true,false);
			else
				windows = new EmployeeLoginPopupWindow(this,layout ,800, 720, true,false);
		}
		
		windows.show();
			//windows.show(findViewById(R.id.homepage));

	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,ABOUT,0,"About");
		return true;
		}


		public boolean onOptionsItemSelected (MenuItem item){
		switch (item.getItemId()){
		case ABOUT:
		AboutDialog about = new AboutDialog(this);
		about.setTitle("Do you need Help?");
		about.show();

		break;
		}
		return true;
		}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		if(windows.isShowing())
			windows.dismiss();
	}

}
