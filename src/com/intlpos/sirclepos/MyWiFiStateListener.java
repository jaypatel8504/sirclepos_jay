package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;

public class MyWiFiStateListener extends BroadcastReceiver{
	
	ConnectivityManager connManager;
	NetworkInfo mWifi;
	
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        String action = intent.getAction();
        connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        Log.d("TEMP", action);
        
        
        if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)){
        	
            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if(info.getType() == ConnectivityManager.TYPE_WIFI){
            	if(mWifi.isConnectedOrConnecting()){
            		CSSharedPreferences.setWifi(true);
            		Toast.makeText(context, R.string.WIFION, Toast.LENGTH_LONG).show();
            	}
            	else{
            		CSSharedPreferences.setWifi(false);
            		Toast.makeText(context, R.string.WIFIOFF, Toast.LENGTH_LONG).show();
            	}
            }
        }
        
        
    }

}
