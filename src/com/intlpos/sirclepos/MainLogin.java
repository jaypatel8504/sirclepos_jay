package com.intlpos.sirclepos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONObject;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.database.Product;
import com.intlpos.initsetup.YoutubeActivity;
import com.intlpos.login.EulaDialog;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class MainLogin extends Activity {

	private CornerStorePOS app;
	/** called when the activity is first created */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page);
		app = (CornerStorePOS) getApplication();
		//CSSharedPreferences.removePreferences();
		
		app.ReadVariables();

		if (CornerStorePOS.NOTFirstTime == true) {
			//CSSharedPreferences.removePreferences();
			//if this is not the first time they are installing the software than while software update i need to call the webservice to store the new version parameters

			boolean hasUpdate = false;
			try {
				int version_code = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
				String version_name = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				if(app.version_code != version_code && app.version_name != version_name)
				{
					//call webservice to update the version and we can do other activities like showing what's new popup etc...
					hasUpdate = true;
					storeversion_preferences(version_code,version_name);
					storeversion_webservice(version_code,version_name);
					
				}
				
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			
			Intent i = new Intent(this, EmployeeLogin.class);
			i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
			i.putExtra("has_update", hasUpdate);
			startActivity(i);
			// setContentView(R.layout.main_login);
			// setWidgetReferences();
			// addWidgetListeners();

		} else {

//			new EulaDialog(this).show();
			Intent i = new Intent(getBaseContext(), YoutubeActivity.class);
			i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
			startActivity(i);
			// Log.d("FirstTime", "True");
			// Intent i = new Intent(MainLogin.this, DeviceRegistration.class);
			// startActivity(i);
		}
		finish();
	}
	
	private void storeversion_preferences(int version_code,String version_name)
	{
		StoreDetail storedetail = new StoreDetail();
		storedetail.setVersion_code(version_code);
		storedetail.setVersion_name(version_name);
		CSSharedPreferences.saveUpdatePreferences(storedetail);
	}
	private void storeversion_webservice(final int version_code,final String version_name)
	{
		
		String methodName = "VersionMaintenence/VersionService.svc/updateversion";

		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
//		
//		
		
		params.put("storedetail", storedetail);
		params.put("versioncode", version_code);
		params.put("versionname", version_name);
		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {
					Boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if (result == true) 
					{							
						Toast.makeText(MainLogin.this, getString(R.string.welcometoversion)+" "+version_name+version_code, Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(MainLogin.this, resultString, Toast.LENGTH_LONG).show();	
					}
				}
				catch(Exception ex)
				{
					Log.d("CSPOS",""+ex);
				}
			}
		});
		
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}

}
