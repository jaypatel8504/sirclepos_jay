package com.intlpos.sirclepos;

import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.intlpos.sirclepos.R;
import com.intlpos.database.ImageLoader;

public class SelectModsFragment extends Fragment {
	private int btnWidth;
	private int btnHeight;
    private static final String TAG = "TestFragment";
    private String[] sTitle = null;// = "hello android";
    private int[] bg_res = null;
    private String[] tags = null;
    private int[] bg_color = null;
    private float[] prices = null;
    private int[] item_id = null;
    private String[] pic = null;
    private Button yes, no, addNote;
    private boolean yesSelected;
    public MyImageButton[] button;
    private int selected[];
    private int nos[];
    private int pos;
    static SelectModsFragment newInstance(int width, int height, String[] s,int[] id,float[] prices, int[] bg_drawable, int[] bg_color, String pic[],int selected[],int no[]) {
    	SelectModsFragment newFragment = new SelectModsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("btnWidth", width);
        bundle.putInt("btnHeight", height);
        bundle.putStringArray("sTitle", s);
        bundle.putFloatArray("prices",prices);
        bundle.putIntArray("item_id", id);
        bundle.putIntArray("bg_res",bg_drawable);
        bundle.putIntArray("bg_color", bg_color);
        bundle.putStringArray("pic", pic);
        bundle.putIntArray("selected", selected);
        bundle.putIntArray("no", no);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "TestFragment-----onCreate");
        Bundle args = getArguments();
        sTitle = args.getStringArray("sTitle");
        bg_res = (int[]) args.getIntArray("bg_res");
        bg_color = (int[]) args.getIntArray("bg_color");
        prices = (float[]) args.getFloatArray("prices");
        btnWidth = (int) args.getInt("btnWidth");
        btnHeight = (int) args.getInt("btnHeight");
        pic = args.getStringArray("pic");
        item_id = args.getIntArray("item_id");
        selected = args.getIntArray("selected");
        nos =  args.getIntArray("no");
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Log.d(TAG, "TestFragment-----onCreateView");
        View v = inflater.inflate(R.layout.mods, container, false);
        yesSelected = true;
        yes = (Button) v.findViewById(R.id.yes);
        no = (Button) v.findViewById(R.id.no);
        addNote = (Button)v.findViewById(R.id.add);
        
        addNote.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AddNotetoItem add = (AddNotetoItem) getActivity();
				add.addNote();
			}
		});
        
        yes.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 yesSelected = true;
			}
		});
        no.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 yesSelected = false;
			}
		});
//        LinearLayout ll = (LinearLayout)v.findViewById(R.id.mods);
        MyViewGroup viewGroup = (MyViewGroup)v.findViewById(R.id.mods);
        LayoutParams lp = new LayoutParams(btnWidth, btnHeight+15);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        		btnWidth, btnHeight+15);
        
        params.setMargins(5, 5, 0, 5);
        
        int len = sTitle == null ? 0 : sTitle.length;
        button = new MyImageButton[len];
        
        for(int i = 0 ; i<len;i++){
        	
        	button[i] = new MyImageButton(this.getActivity(),true);
        	button[i].setMinimumHeight(btnHeight+15);
        	button[i].setMinimumWidth(btnWidth);
        	button[i].setItem_id(item_id[i]);
        	
        	button[i].setLayoutParams(params);
        	button[i].setScaleType(ImageView.ScaleType.FIT_START);
        	if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
	        	if(!pic[i].isEmpty()){
	        		ImageLoader imageLoader;
        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight+15, getActivity(),true);
        			imageLoader.DisplayImage(pic[i], button[i]);
	        	}
        	}
        	
        		button[i].setBackgroundResource(R.drawable.bg_deptbutton_states);
        	button[i].setText(sTitle[i]);
        	button[i].setTextColor(Color.BLACK);

        	button[i].setTag(i);
        	button[i].selected = false;
        	button[i].setEdit(false);
        	if(selected != null){
        		for(int index:selected){
        			if(index == item_id[i]){
        				button[i].selected = true;
        				button[i].setYes(true);
        				button[i].setBackgroundResource(R.drawable.bg_deptbutton_selected);
        				button[i].setEdit(true);
        			}
        		}
        	}
        	if(nos!= null){
        		for(int index:nos){
        			if(index == item_id[i]){
        				button[i].selected = true;
        				button[i].setYes(false);
        				button[i].setBackgroundResource(R.drawable.button_yellow);
        				button[i].setEdit(true);
        			}
        		}
        	}
        	

        	viewGroup.addView(button[i]);
        	v.setBackgroundResource(R.drawable.shape);
        	if (sTitle[i] == "KeyBoard"){
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    		Toast.makeText(getActivity(), "Popup Keyboard", Toast.LENGTH_SHORT).show();            	    	
            	       }
            	     });
        	}
        	else{
        		final int position = i;
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    	
            	    	MyImageButton mButton = (MyImageButton) v;
            	    	if (mButton.selected == false){
            	    		mButton.selected = true;
            	    		if(yesSelected){
            	    			mButton.setYes(true);
            	    			mButton.setBackgroundResource(R.drawable.bg_deptbutton_selected);
            	    		}
            	    		else{
            	    			mButton.setYes(false);
            	    			mButton.setBackgroundResource(R.drawable.button_yellow);
            	    		}
            	    	}else{
            	    		mButton.selected = false;
//                			if(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean("switch_pictures", false)){
//                	        	if(!pic[position].isEmpty()){
//                	        		
//                	        		ImageLoader imageLoader;
//                        			imageLoader = new ImageLoader(getActivity().getApplicationContext(),btnWidth, btnHeight+15, getActivity());
//                	        		imageLoader.DisplayImage(pic[position],mButton);
//                	        	}
//                			}
                				mButton.setBackgroundResource(R.drawable.bg_deptbutton_states);
                			
            	    	}
            	       }
            	     });
        	}
        }
        return v;
    }
	
	
	public interface AddNotetoItem{
		public void addNote();
	}

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "TestFragment-----onDestroy");
    }

}
