package com.intlpos.sirclepos;

import com.intlpos.sirclepos.R;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

public class ItemHighlighterListener implements OnItemClickListener{

    private View lastSelectedView = null;
    private Context context;

    public ItemHighlighterListener(Context context){
    	this.context = context;
    }

	public void clearSelection()
    {
        if(lastSelectedView != null) lastSelectedView.setBackgroundColor(context.getResources().getColor(R.color.WHITE));
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
    	Log.d("test","clicked");
        clearSelection();
        lastSelectedView = view;
        view.setBackgroundColor(context.getResources().getColor(R.color.grid_background_color));
    }
}
