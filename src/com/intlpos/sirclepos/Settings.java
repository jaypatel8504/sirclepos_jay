package com.intlpos.sirclepos;

import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;
import android.widget.Button;
import android.widget.Toast;

import com.intlpos.database.Department;
import com.intlpos.initsetup.StartScreen;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.storemaintenance.AddEditDepartment;
import com.intlpos.storemaintenance.AddEmployee;
import com.intlpos.storemaintenance.Deparment;
import com.intlpos.storemaintenance.Employee;
import com.intlpos.storemaintenance.EmployeeInformation;
import com.john.bluetoothprinter.helper.PrintDataService;

public class Settings extends Activity {
	 @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        // Display the fragment as the main content.
	        getFragmentManager().beginTransaction()
	                .replace(android.R.id.content, new FragmentSettings())
	                .commit();
	    }
	 	
	 
	 public void replaceFragments(String url){
		 final android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();	
		 if(url.isEmpty()){	 
			ft.replace(android.R.id.content, new Employee(), "NewFragmentTag");
			ft.commit();
		 }else if(url.equals("depart")){
			ft.replace(android.R.id.content, new Deparment(), "Depart");
			ft.commit();	
		 }
		 else{
			ft.replace(android.R.id.content, new PreviewFragment(url,false), "NewFragmentTag");
			ft.commit();
		 }
	 }
	 
	 public void addFrag(EmployeeInformation emp){
		 final android.app.FragmentTransaction ft = getFragmentManager().beginTransaction(); 
		 if(emp == null){
			ft.replace(android.R.id.content, new AddEmployee(), "New");
			ft.addToBackStack(null);
			ft.commit();
		 }else{
			AddEmployee employee = new AddEmployee(emp);
			ft.replace(android.R.id.content, employee, "Edit");
			ft.addToBackStack(null);
			ft.commit();
		 }
	 }
	 
	 public void addDepartment(Department depart){
		 final android.app.FragmentTransaction ft = getFragmentManager().beginTransaction(); 
		 if(depart == null){
			ft.replace(android.R.id.content, new AddEditDepartment(null), "NewDepart");
			ft.addToBackStack(null);
			ft.commit();
		 }else{
			 ft.replace(android.R.id.content, new AddEditDepartment(depart), "NewDepart2");
			 ft.addToBackStack(null);
			 ft.commit();
		 }
	 }
	
	 
}