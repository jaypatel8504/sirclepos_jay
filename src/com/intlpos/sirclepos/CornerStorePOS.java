package com.intlpos.sirclepos;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.database.Department;
import com.intlpos.database.DeptProduct;
import com.intlpos.database.Employee;
import com.intlpos.database.Invoice;
import com.intlpos.database.ModifierGroupProducts;
import com.intlpos.database.ModifierGroups;
import com.intlpos.database.Order;
import com.intlpos.database.Permission;
import com.intlpos.database.Product;
import com.intlpos.database.ProductModifierGroups;
import com.intlpos.database.ProductModifierValues;
import com.intlpos.database.SaveInvoice;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.PreviousStartTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.storemaintenance.EmployeeInformation;
import com.john.beans.Customer;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class CornerStorePOS extends Application {
	 private static Context context;
//		public static String Url = "http://10.0.0.15/accounts/";
		public static String Url = "http://smart360pos.com/sirclepos/v1_320/";
		public static String BackupUrl = "http://cornerstorereg.info/sirclepos/v1_320/";
		public static DecimalFormat dec_format = new DecimalFormat("0.00");
		public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a");
		public DefaultHttpClient httpClient;
		public static boolean NOTFirstTime;
		public static String email_id;
		public static String password;
		public static String StoreId;
		public static String StationId;
//		for setting preferences
		public com.john.beans.StoreDetail storedetail ;
		public boolean combine_lines ;
		public String print_receipt ;
		public boolean receipt_type ;
		public boolean stock_prompt ;
		public String print_zout ;
		public String printer_macaddress;
		public String printer_type;
		public boolean printer_paper_type;
		public int[] printer_usb_port;
		public boolean qty_keypad;
		public Employee employee;
		public Permission permission;
		
			
		// for update (versioning)
		public int version_code;
		public String version_name;
		
		public String startTim;
		public String endTim;
		public String prevTim;
		
		public ArrayList<HashMap<String, Object>> PList ;
		public ArrayList<DeptProduct> DList;
		public ArrayList<Department>DepartmentList;
		public ArrayList<Customer> CustomerList;
		public ArrayList<ModifierGroups> ModifierGroup;
		public ArrayList<ProductModifierGroups> ProductModifier;
		public ArrayList<ModifierGroupProducts> GroupProducts;
		public static HashMap<String,ArrayList<HashMap<String,Object>>> PMap ;
		public  ArrayList<EmployeeInformation> employeeInfo;
		public ArrayList<SaveInvoice> heldInvoices;
		public void ReadVariables() {		
			
			httpClient = new DefaultHttpClient();
			
			StoreDetail storedetail = CSSharedPreferences.loadPreferences();		
			NOTFirstTime = storedetail.getnotfirsttime();
			email_id = storedetail.getemail_id();
			password = storedetail.getpassword();
			StoreId = storedetail.getstoreid();
			StationId = storedetail.getstationid();
			//for setting preferences
			this.storedetail = new com.john.beans.StoreDetail(email_id,password,StoreId,StationId);
			combine_lines = storedetail.isCombine_lines();
			print_receipt = storedetail.getPrint_receipt();
			receipt_type = storedetail.isReceipt_type();
			stock_prompt = storedetail.isStock_prompt();
			print_zout = storedetail.getPrint_zout();
			printer_type = storedetail.getPrinter_type();
			printer_macaddress = storedetail.getPrinter_macaddress();
			printer_usb_port = storedetail.getPrinter_usb_port();
			printer_paper_type = storedetail.isPaper_type();
			qty_keypad = storedetail.isQty_keypad();
			//for update (versioning)
			version_code =storedetail.getVersion_code();
			version_name = storedetail.getVersion_name();
			
		//	definelocalstore();
			StartTime startTime = CSSharedPreferences.loadStartTime();
			startTim = startTime.getTime();
			
			EndTime endTime = CSSharedPreferences.loadEndTime();
			endTim = endTime.getEndTime();
			
			PreviousStartTime prevTime = CSSharedPreferences.loadPrevTime();
			prevTim = prevTime.getPrevTime();
		}
	/*	private static void definelocalstore()
		{
			store = new Store();
			store.email_id = email_id;
			store.password = password;
			store.store_id = StoreId;
			store.station_id = StationId;
		}*/
		@Override
	    public void onCreate(){
	        super.onCreate();
	        CornerStorePOS.context = getApplicationContext();
	        ReadVariables();	        
	    }

	    public static Context getAppContext() {
	        return CornerStorePOS.context;
	    }
	    @Override
	    public void onTerminate(){
	    	super.onTerminate();
			PList = null;
			DList = null;
			PMap = null;
			context = null;
			ModifierGroup = null;
			DepartmentList = null;
			CustomerList = null;
			GroupProducts = null;
			ProductModifier = null;
	    }
	    
	    
}
