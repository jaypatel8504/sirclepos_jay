package com.intlpos.sirclepos;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.intlpos.sirclepos.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PdfViewer extends Activity {
	
	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pdfviewer);
		Intent intent = getIntent();
		String help = intent.getExtras().getString("result");
		
		WebView webView = (WebView) findViewById(R.id.webview);
		webView.setWebViewClient(new WebViewClient());
		
		webView.getSettings().setJavaScriptEnabled(true);
		//webView.loadUrl("http://docs.google.com/gview?embedded=true&url=http://smart360pos.com/sirclereg/Smart360POSmanuals/Why_POS.pdf");
		
		
			webView.loadUrl("http://docs.google.com/gview?embedded=true&url="+help);
		
	}
	
	
}
