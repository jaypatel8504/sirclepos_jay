package com.intlpos.global;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import com.intlpos.sirclepos.R;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.InputType;
import android.text.method.NumberKeyListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Toast;

public class KeyPad2 extends PopupWindow {
	private View pView;
	private KeyPad2 keypad;
	private EditText edittext;
	private int result;
	private boolean decimalPad;
	private boolean hasDot;
	private boolean finished_flag;
	public boolean isDecimalPad() {
		return decimalPad;
	}


	public void setDecimalPad(boolean decimalPad) {
		this.decimalPad = decimalPad;
	}

	private String str = "";
	private Button button_1;
	private Button button_2;
	private Button button_3;
	private Button button_4;
	private Button button_5;
	private Button button_6;
	private Button button_7;
	private Button button_8;
	private Button button_9;
	private Button button_0;
	private Button button_00;
	private Button button_signal;
	private Button button_clean;
	private Button button_ok;	
	public Button getButton_ok() {
		return button_ok;
	}

	private static DecimalFormat form = new DecimalFormat("0.00");
	

    public KeyPad2(EditText edittext, View contentView, int width, int height, boolean decimalPad) {
        super(contentView, width, height);
        keypad = this;
        this.decimalPad = decimalPad;
        this.edittext = edittext;        
        this.edittext.setKeyListener(new NumberKeyListener(){     

			@Override
			public char[] getAcceptedChars() {
				// TODO Auto-generated method stub
				char[] mychar = {'0','1','2','3','4','5','6','7','8','9','.'};
				return mychar;
			}

			@Override
			public int getInputType() {
				// TODO Auto-generated method stub
				return InputType.TYPE_NULL;
			}
    	});
        this.str = edittext.getText().toString();
        pView = contentView;
        setFocusable(false);// get Focus
        setTouchable(true);
        setOutsideTouchable(true);
        setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        addListenerOnButton(edittext);
        this.setFinished(false);
        this.hasDot = true;
    }


	public EditText getEdittext() {
		return edittext;
	}

	public void setEdittext(EditText edittext) {
		this.edittext = edittext;
		this.str = edittext.getText().toString();
	}

	private void addListenerOnButton(final EditText edittext) {
		// TODO Auto-generated method stub
		button_1 = (Button) pView.findViewById(R.id.keypad_1);
		button_2 = (Button) pView.findViewById(R.id.keypad_2);
		button_3 = (Button) pView.findViewById(R.id.keypad_3);
		button_4 = (Button) pView.findViewById(R.id.keypad_4);
		button_5 = (Button) pView.findViewById(R.id.keypad_5);
		button_6 = (Button) pView.findViewById(R.id.keypad_6);
		button_7 = (Button) pView.findViewById(R.id.keypad_7);
		button_8 = (Button) pView.findViewById(R.id.keypad_8);
		button_9 = (Button) pView.findViewById(R.id.keypad_9);
		button_0 = (Button) pView.findViewById(R.id.keypad_0);
		button_00 = (Button) pView.findViewById(R.id.keypad_00);
		button_signal = (Button) pView.findViewById(R.id.keypad_signal);
		button_clean = (Button) pView.findViewById(R.id.keypad_clean);
		button_ok = (Button) pView.findViewById(R.id.keypad_ok);
		
		button_1.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {	
            	str = decimalPad ? updateStr(str,"1"):str+"1";
            	edittext.setText(str);
            }
    });
		button_2.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"2"):str+"2";
            	edittext.setText(str);
            }
    });
		button_3.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"3"):str+"3";
            	edittext.setText(str);
            }
    });
		button_4.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"4"):str+"4";
            	edittext.setText(str);
            }
    });
		button_5.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"5"):str+"5";
            	edittext.setText(str);
            }
    });
		button_6.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"6"):str+"6";
            	edittext.setText(str);
            }
    });
		button_7.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"7"):str+"7";
            	edittext.setText(str);
            }
    });
		button_8.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"8"):str+"8";
            	edittext.setText(str);
            }
    });
		button_9.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"9"):str+"9";
            	edittext.setText(str);
            }
    });
		button_0.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? updateStr(str,"0"):str+"0";
            	edittext.setText(str);
            }
    });
		button_00.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
//            	str = hasDot ? str:str+".";
//            	hasDot = true;
            	str = decimalPad ? updateStr(str,"0"):str+"0";
            	str = decimalPad ? updateStr(str,"0"):str+"0";
            	edittext.setText(str);
            }
    });
		button_signal.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = changeSignal(str);
            	edittext.setText(str);
            }
    });
		button_clean.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	str = decimalPad ? "0.00":"";
            	if (!decimalPad){
            		hasDot = false;
            	}
            	edittext.setText(str);
            }
    });
		button_ok.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {  

                if(str.equals("")){
	               Toast.makeText(pView.getContext(), pView.getContext().getString(R.string.please_put_the_qty), Toast.LENGTH_SHORT).show();
                }else{
	               edittext.setText(findCeil(str));
	               setFinished(true);
	               keypad.dismiss();
                }
             }
    });
		
	}


	protected String updateStr(String str1, String str2) {
		// TODO Auto-generated method stub
		BigDecimal price = new BigDecimal(str1+str2);
		price = price.multiply(BigDecimal.TEN);
		return form.format(price.doubleValue());
	}
	protected String changeSignal(String str1) {
		try{
			BigDecimal price = new BigDecimal(str1);
			price = price.negate();
			return form.format(price.doubleValue());
		}catch (Exception e){
			return str1;
		}
	}
	public static String findCeil(String str1) {
		// TODO Auto-generated method stub
		BigDecimal price = new BigDecimal(str1);
		price.setScale(2,RoundingMode.CEILING);
		return form.format(price.doubleValue());
	}
	public int getResult() {
		result = Integer.parseInt(str);		
		return result;
	}


	public boolean isFinished() {
		return finished_flag;
	}


	public void setFinished(boolean finished_flag) {
		this.finished_flag = finished_flag;
	}


    
}