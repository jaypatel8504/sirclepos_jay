package com.intlpos.global;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.graphics.Color;
import android.widget.TextView;
public class AboutDialog extends Dialog{
	private static Context mContext = null;
	public AboutDialog(Context context) {
	super(context);
	mContext = context;
	}

@Override


public void onCreate(Bundle savedInstanceState) {
CornerStorePOS app; 

setContentView(R.layout.about_dialog);
WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
WifiInfo wInfo = wifiManager.getConnectionInfo();
String mac = wInfo.getMacAddress();
TextView tv = (TextView)findViewById(R.id.info_text);

PackageInfo versionInfo = getPackageInfo();

String about = "Version: "+ versionInfo.versionName + versionInfo.versionCode+ "\n" + "E-mail: " + CornerStorePOS.email_id+ "\n" 
			   + "Password: " + CornerStorePOS.password + "\n"+ "Store ID: "+ CornerStorePOS.StoreId + "\n" + "MAC: " + mac;

tv.setText(about);
}

private PackageInfo getPackageInfo() {
    PackageInfo pi = null;
    try {
         pi = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), PackageManager.GET_ACTIVITIES);
    } catch (PackageManager.NameNotFoundException e) {
        e.printStackTrace();
    }
    return pi;
}

}
