package com.intlpos.global;

import java.util.Date;

import com.intlpos.sirclepos.R;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {
    private final static String APP_TITLE = "SirclePOS";
    private final static String APP_PNAME = "com.intlpos.cornerstorepos";
    
    private final static int DAYS_UNTIL_PROMPT = 7;
    private final static int LAUNCHES_UNTIL_PROMPT = 10;
    
    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) { return ; }
        
        SharedPreferences.Editor editor = prefs.edit();
        


        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }
        Date firstLanchDate = new Date(date_firstLaunch);
        Date currentDate = new Date(System.currentTimeMillis());
        // Wait at least n days before opening
         if ( daysBetween(firstLanchDate,currentDate) > DAYS_UNTIL_PROMPT ) {
            // Increment launch counter
            long launch_count = prefs.getLong("launch_count", 0) + 1;
            editor.putLong("launch_count", launch_count);
            Log.d("apprater", "launch_count "+launch_count);
            if (launch_count < LAUNCHES_UNTIL_PROMPT) {
                showRateDialog(mContext, editor);
            }
        }
        Log.d("apprater", "Days between "+daysBetween(firstLanchDate,currentDate));
        editor.commit();
    }   
    
    
	public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle("Rate " + APP_TITLE);

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        TextView tv = new TextView(mContext);
        tv.setTextSize(25);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setAutoLinkMask(Linkify.WEB_URLS);
        tv.setLinksClickable(true);
        tv.setText(mContext.getString(R.string.rate_us));

        SpannableString str = SpannableString.valueOf("\tGoogle\n"); 
        str.setSpan(new URLSpan("market://details?id=" + APP_PNAME), 0, 7, 
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 
        tv.append(str);
        str = SpannableString.valueOf("\tAmazon\n"); 
        str.setSpan(new URLSpan("http://www.amazon.com/International-Point-of-Sale-LLC/product-reviews/B00MYAOPF0/"), 0, 7, 
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 
        tv.append(str);
        str = SpannableString.valueOf("\tSlideme"); 
        str.setSpan(new URLSpan("http://slideme.org/node/9929968/reviews"), 0, 8, 
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE); 
        tv.append(str);
        tv.setWidth(240);
        tv.setPadding(4, 0, 4, 10);
        ll.addView(tv);
        
//        Button b1 = new Button(mContext);
//        b1.setText("Rate " + APP_TITLE);
//        b1.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + APP_PNAME)));
//                dialog.dismiss();
//            }
//        });        
//        ll.addView(b1);

        Button b2 = new Button(mContext);
        b2.setText("Remind me later");
//        b2.setBackground(mContext.getResources().getDrawable(R.drawable.button_blue));
        b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ll.addView(b2);

//        Button b3 = new Button(mContext);
//        b3.setText("No, thanks");
//        b3.setOnClickListener(new OnClickListener() {
//            public void onClick(View v) {
//                if (editor != null) {
//                    editor.putBoolean("dontshowagain", true);
//                    editor.commit();
//                }
//                dialog.dismiss();
//            }
//        });
//        ll.addView(b3);

        dialog.setContentView(ll);        
        dialog.show();        
    }
    
    public static int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
}

}
// see http://androidsnippets.com/prompt-engaged-users-to-rate-your-app-in-the-android-market-appirater
