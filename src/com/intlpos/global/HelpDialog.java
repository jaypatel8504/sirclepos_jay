package com.intlpos.global;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.graphics.Color;
import android.widget.TextView;
public class HelpDialog extends Dialog{
	private static Context mContext = null;
	public HelpDialog(Context context) {
	super(context);
		mContext = context;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		CornerStorePOS app; setContentView(R.layout.about_dialog);
		TextView tv = (TextView)findViewById(R.id.info_text);
		String about = "Need Help?\n" + "Call Royal Sovereign Customer Service at 1-866-961-2673\n";
		String link = "Or Visit\n" + "www.smart360pos.com";
		tv.setText(about + link);
		Linkify.addLinks(tv, Linkify.ALL);
	}

}
