package com.intlpos.WCFAccess;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.intlpos.initsetup.PostRequestTask;
import com.intlpos.sirclepos.CornerStorePOS;
import com.john.beans.CSVProduct;
import com.john.beans.StoreDetail;
import com.john.file.FileAdapter;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class GetSampleCSV extends AsyncTask<Object, Void, ArrayList<ArrayList<String>>>{
	private String url;
	private Context context;
	InputStream is = null;
	private CornerStorePOS app;
	protected static final String[] HEADERS = {"item_no","item_name","cost","price","item_stock","tax_rate1","tax_rate2","tax_rate3","department_name","re_order_level"};
	private ArrayList<CSVProduct> csvlist;
	protected static final String ADD_URL = "InventoryImport/ImportService.svc/importinventory";
	private int productCount;
	
	public GetSampleCSV(String url, Context context){
		this.url = url;
		this.context = context;
	}
	
	private class GetProductCountRequest{
		StoreDetail storedetail;
		public GetProductCountRequest(StoreDetail storedetail){
			this.storedetail = storedetail;
		}
	}
	
	class CSVRequest{
		StoreDetail storedetail;
		ArrayList<CSVProduct> inventorylist;
		public CSVRequest(StoreDetail storedetail, ArrayList<CSVProduct> inventorylist){
			this.storedetail = storedetail;
			this.inventorylist = inventorylist;
		}
	}
	
	@Override
	protected ArrayList<ArrayList<String>> doInBackground(Object... params) {
		ArrayList<ArrayList<String>> dataAL = new ArrayList<ArrayList<String>>();
		URL u = null;
		try {
			u = new URL(url);
			InputStream in = u.openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
		    while ((line = reader.readLine()) != null) {
		    	ArrayList<String> ticketStr = new ArrayList<String>();
		    	 String item[] = line.split(",");//split by comma
	             ticketStr.clear();
	             for(int i=0; i<item.length; i++){
//	            	 Log.d("TSTR",item[i]);
	                 ticketStr.add(i, item[i]);
	             }
	             dataAL.add(ticketStr);
//		        out.append(line);
		    }
		    reader.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
		return dataAL;
	}
	
	@Override
	protected void onPostExecute(ArrayList<ArrayList<String>> result) 
	{	
//		PostRequestTask postTask = new PostRequestTask();
//		Gson gson = new Gson();
//		CornerStorePOS app = (CornerStorePOS) ((Activity) context).getApplication();
//		GetProductCountRequest getcountrequest = new GetProductCountRequest(app.storedetail);
//		Log.d("getCount", gson.toJson(getcountrequest));
//    	postTask.execute(CornerStorePOS.Url+"InventoryImport/ImportService.svc/getproductscount",gson.toJson(getcountrequest));
//		String resultInt = null;
//		try{
//			resultInt = postTask.get();
//		}catch (InterruptedException | ExecutionException e) {
//			e.printStackTrace();
//		}
//    	try{
//    		productCount = Integer.parseInt(resultInt);
//    	}catch(Exception e){
//    		Toast.makeText(context, resultInt+"Cannot get products number.", Toast.LENGTH_SHORT).show();
//    	}
//		
//		Log.d("Result", result.get(0).get(0));
//		ArrayList<String> headers = result.get(0);
//		if(headers.size() != HEADERS.length){
//			Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//			return;
//		}
//		for(int i = 0, length = headers.size(); i < length; i++){
//			if (!headers.get(i).equals(HEADERS[i])){
//				Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//				return;
//			}
//		}
//		result.remove(0);
//		try{
//		for(ArrayList<String> list:result){
//			if(list.get(0).isEmpty() || list.get(1).isEmpty()){
////				showAlertDialog("Item_Name and Item_No cannot be Null");
//			}
//			if(list.get(2).isEmpty())
//				list.set(2, "0.00");
//			if(list.get(3).isEmpty())
//				list.set(3, "0.00");
//			if(list.get(4).isEmpty())
//				list.set(4, "0.00");
////			if(!isNumeric(list.get(2)) || !isNumeric(list.get(3)) || !isNumeric(list.get(4))){
////				showAlertDialog("Price, Cost and Stock should be a Number");
////			}
//			if(list.get(5).isEmpty())
//				list.set(5, "true");
//			if(list.get(6).isEmpty())
//				list.set(6, "false");
//			if(list.get(7).isEmpty())
//				list.set(7, "false");
//			if(list.get(8).isEmpty())
//				list.set(8, "none");
//			if(list.size() == 9)
//				list.add("10");
//			list.add("/Date(" + System.currentTimeMillis() + ")/");
//		}
////		byte[] data = ReadCSVtoArrayList.readCSVToByteArray(path);
//		csvlist = new ArrayList<CSVProduct>();
//		for(ArrayList<String> list: result){
//			try{
//			CSVProduct product = new CSVProduct(list);
//			csvlist.add(product);
//			}catch(Exception e){
//				Toast.makeText(context, "Data is not in correct format.", Toast.LENGTH_SHORT).show();
//				return;
//			}
//		}
//		
//		if(csvlist.size() + productCount > 6000){
//			Toast.makeText(context, "Product list cannot more than 6000", Toast.LENGTH_SHORT).show();
//			return;
//		}
//
//		gson = new Gson();
//		postTask = new PostRequestTask();
//		Log.d("APP", csvlist.get(1).getItem_name());
//		app = (CornerStorePOS) ((Activity) context).getApplication();
//		CSVRequest csvrequest = new CSVRequest(app.storedetail,csvlist);
////    	Log.d("Json", gson.toJson(csvrequest));
//    	postTask.execute(CornerStorePOS.Url+ADD_URL,gson.toJson(csvrequest));
//    	String result1 = null;
//    	try {
//			result1 = postTask.get();
//		} catch (InterruptedException | ExecutionException e) {
//			e.printStackTrace();
//		}
//    	JSONObject response = null;
//    	try {
//    		response = new JSONObject(result1);
//        	if(response.getBoolean("result"))
//        		Toast.makeText(context, "CSV File Import Successful!", Toast.LENGTH_SHORT).show();
//        	else{
//        		Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
//        	}
//		} catch (JSONException e) {
//			e.printStackTrace();
//			Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
//		}
//    	
//		}catch(Exception e){
//			Toast.makeText(context, "FAILED", Toast.LENGTH_LONG).show();
//		}
	}
	
	

}
