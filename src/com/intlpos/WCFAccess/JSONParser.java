package com.intlpos.WCFAccess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.net.UrlQuerySanitizer.ParameterValuePair;
import android.os.AsyncTask;
import android.util.Log;
import com.intlpos.sirclepos.R;

public class JSONParser extends AsyncTask<Object, Void, JSONObject> 
{
	InputStream is = null;
	JSONObject jObj = null;
	String jason = "";
	private MyListener listener;
	public JSONParser(MyListener listener)
	{		
		this.listener = listener;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject doInBackground(Object... params) 
	{
		WebService webservice = new WebService((String)params[0]);
		jObj = webservice.webInvoke((String)params[1], (LinkedHashMap<String, Object>)params[2]);
		return jObj;		
	}

	@Override
	protected void onPostExecute(JSONObject result) 
	{		
		 super.onPostExecute(result);		
		 if (listener != null) {			
		      listener.onResult(result);		      
		    }	
		 
		
	}
}
