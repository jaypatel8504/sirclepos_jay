package com.intlpos.WCFAccess;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.util.Log;

import com.google.gson.Gson;
import com.google.zxing.client.android.HttpHelper.ContentType;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
public class WebService {

	//DefaultHttpClient httpClient;
	private String ret;
	private byte rest[];
	InputStream is = null;
	HttpResponse response = null;
	HttpPost httpPost = null;
	HttpGet httpGet = null;
	String webServiceUrl;
	JSONObject jObj;
	private DefaultHttpClient httpClient;
//	static private Map ERROR = new HashMap();

	// The serviceName would be the name of the Service you are going to be
	// using.

	public WebService(String serviceName) {
		httpClient = new DefaultHttpClient();
		webServiceUrl = serviceName;
	}

	// Use this method to do HttpPost\WebInvoke on a web Service
	public JSONObject webInvoke(String methodName,
			LinkedHashMap<String, Object> params) {
		JSONStringer obj = new JSONStringer();
		try {
			obj.object();

			for (Map.Entry<String, Object> param : params.entrySet()) 
			{
				if ( param.getValue() instanceof LinkedHashMap)
				{
					Map<String,Object> innerparam = (Map<String, Object>) param.getValue();
					JSONObject innerobj = new JSONObject();    								

					
					for (Map.Entry<String, Object> para : innerparam.entrySet())
					{
						innerobj.put(para.getKey(),para.getValue());
					}			
					
				
					obj.key(param.getKey()).value(innerobj);	
					
				}
				else
				{
					obj.key(param.getKey()).value(param.getValue());
				}
			}
			obj.endObject();
		} catch (JSONException e) {
			Log.e("CornerStorePOS", "JSONException :" + e);
		}

		return webInvoke(methodName, obj);
	}

	private JSONObject webInvoke(String methodName, JSONStringer data) {
		ret = null;
		httpPost = new HttpPost(webServiceUrl + methodName);
		response = null;

		StringEntity se = null;
		 ByteArrayEntity entity = null;
		try {
			entity = new ByteArrayEntity(data.toString().getBytes("UTF-8"));
			entity.setContentType("application/json");
//			se = new StringEntity(data.toString());
//			se.setContentEncoding("UTF-8");
//			se.setContentType("application/json");
		} catch (UnsupportedEncodingException e) {
			Log.e("CornerStorePOS",
					"HttpUtils : UnsupportedEncodingException : " + e);
		}
		httpPost.setEntity(entity);
		Log.d("CornerStorePOS", webServiceUrl + "?" + data);

		try {
				response = httpClient.execute(httpPost);
				Log.d("CornerStorePOS", response.toString());
			if (response != null) {
				HttpEntity httpEntity = response.getEntity();
				Log.d("CornerStorePOS", response.toString());
				is = httpEntity.getContent();

			}
		} catch (Exception e) {
			Log.e("CornerStorePOS", "HttpUtils : " + e);
		}
		
		try {
			ret = new String(readStream(is));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		BufferedReader reader = null;
//		try {
//			reader = new BufferedReader(new InputStreamReader(
//					is, "iso-8859-1"));
//			StringBuilder sb = new StringBuilder();
//			String line = null;
//			while ((line = reader.readLine()) != null) {
//				sb.append(line + "\n");
//			}
//			is.close();
//			ret = sb.toString(); // here json type is string
//		} catch (Exception e) {
//			Log.e("Buffer Error", "Error converting result " + e.toString());
//		} finally {
//			try {
//		        reader.close();
//		    } catch (IOException e) {
//		        e.printStackTrace();
//		    }
//		}

		// try parse the string to a JASON object
		try {
				jObj = new JSONObject(ret);
		} catch (Exception e) {
			Log.e("JASON Parser", "Exception " + e.toString());
		}
		// return JASON String

		return jObj;

	}
	
	public String toGetString(String methodName,
			LinkedHashMap<String, Object> params) {
		JSONStringer obj = new JSONStringer();
		try {
			obj.object();

			for (Map.Entry<String, Object> param : params.entrySet()) 
			{
				if ( param.getValue() instanceof LinkedHashMap)
				{
					Map<String,Object> innerparam = (Map<String, Object>) param.getValue();
					JSONObject innerobj = new JSONObject();    								

					
					for (Map.Entry<String, Object> para : innerparam.entrySet())
					{
						innerobj.put(para.getKey(),para.getValue());
					}			
					
				
					obj.key(param.getKey()).value(innerobj);	
					
				}
				else
				{
					obj.key(param.getKey()).value(param.getValue());
				}
			}
			obj.endObject();
		} catch (JSONException e) {
			Log.e("CornerStorePOS", "JSONException :" + e);
		}

		return webStringInvoke(methodName, obj);
	}
	
	public String webStringInvoke(String methodName, JSONStringer data) {
		ret = null;
		httpPost = new HttpPost(webServiceUrl + methodName);
		response = null;

		StringEntity se = null;
		ByteArrayEntity entity = null;
		try {
			entity = new ByteArrayEntity(data.toString().getBytes("UTF-8"));
			entity.setContentType("application/json");
			se = new StringEntity(data.toString());
			se.setContentEncoding("UTF-8");
			se.setContentType("application/json");
		} catch (UnsupportedEncodingException e) {
			Log.e("CornerStorePOS",
					"HttpUtils : UnsupportedEncodingException : " + e);
		}
		httpPost.setEntity(entity);
		Log.d("CornerStorePOS", webServiceUrl + "?" + data);

		try {
				response = httpClient.execute(httpPost);

			if (response != null) {
				HttpEntity httpEntity = response.getEntity();

				is = httpEntity.getContent();

			}
		} catch (Exception e) {
			Log.e("CornerStorePOS", "HttpUtils : " + e);
		}
		
		try {
			ret = new String(readStream(is));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;

	}
	
	//For Reports
	public JSONArray webInvokeReports(String methodName,
			LinkedHashMap<String, Object> params) {
		JSONStringer obj = new JSONStringer();
		JSONObject jsonObject = new JSONObject();
		try {
			obj.object();
			
			for (Map.Entry<String, Object> param : params.entrySet()) 
			{
				if ( param.getValue() instanceof LinkedHashMap)
				{
					Map<String,Object> innerparam = (Map<String, Object>) param.getValue();
					JSONObject innerobj = new JSONObject();    								

					
					for (Map.Entry<String, Object> para : innerparam.entrySet())
					{
						innerobj.put(para.getKey(),para.getValue());
					}			
					
				
					obj.key(param.getKey()).value(innerobj);	
					jsonObject.put(param.getKey(), innerobj);
				}
				else
				{
					jsonObject.put(param.getKey(), param.getValue());
					obj.key(param.getKey()).value(param.getValue());
				}
			}
			obj.endObject();
			String temp = jsonObject.toString().replace("\"[", "[").replace("]\"", "]").replace("\\", "");
			jsonObject = new JSONObject(temp);
			Log.d("jsonObject", temp);
		} catch (JSONException e) {
			Log.e("CornerStorePOS1", "JSONException :" + e);
		}

		return webInvokeReports(methodName, jsonObject);
	}
	
	private JSONArray webInvokeReports(String methodName, JSONObject data) {
		JSONArray jsonArray = null;
		ret = null;
		httpPost = new HttpPost(webServiceUrl + methodName);
		response = null;

		StringEntity se = null;
		 ByteArrayEntity entity = null;
		try {
			entity = new ByteArrayEntity(data.toString().getBytes("UTF-8"));
			entity.setContentType("application/json");
			se = new StringEntity(data.toString());
			se.setContentEncoding("UTF-8");
			se.setContentType("application/json");
		} catch (UnsupportedEncodingException e) {
			Log.e("CornerStorePOS",
					"HttpUtils : UnsupportedEncodingException : " + e);
		}
		httpPost.setEntity(entity);
		Log.d("CornerStorePOS", webServiceUrl + "?" + data);

		try {
				response = httpClient.execute(httpPost);
				Log.d("CornerStorePOSresponse", response.toString());
			if (response != null) {
				HttpEntity httpEntity = response.getEntity();
				Log.d("CornerStorePOSresponseif", response.toString());
				is = httpEntity.getContent();

			}
		} catch (Exception e) {
			Log.e("CornerStorePOS", "HttpUtils : " + e);
		}
		
		try {
			ret = new String(readStream(is));
			Log.d("ret",ret);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// try parse the string to a JASON object
		try {
			jsonArray = new JSONArray(ret);
		} catch (Exception e) {
			Log.e("JASON Parser", "Exception " + e.toString());
		}
		// return JASON String

		return jsonArray;

	}
	
	/**
	 * Read InputStream
	 * 
	 * @param inStream
	 * @return byteArray
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outSteam.write(buffer, 0, len);
		}
		outSteam.close();
		inStream.close();
		return outSteam.toByteArray();
	}

}