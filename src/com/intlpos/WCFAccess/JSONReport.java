package com.intlpos.WCFAccess;

import java.io.InputStream;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;



public class JSONReport extends AsyncTask<Object, Void, JSONArray>{
	InputStream is = null;
	JSONArray jArray = null;
	String jason = "";
	private JSONArrayListener listener;
	public JSONReport(JSONArrayListener listener){
		this.listener = listener;
	}
	
	@Override
	protected JSONArray doInBackground(Object... params) 
	{
		WebService webservice = new WebService((String)params[0]);
		jArray = webservice.webInvokeReports((String)params[1], (LinkedHashMap<String, Object>)params[2]);
		return jArray;		
	}

	@Override
	protected void onPostExecute(JSONArray result) 
	{		
		 super.onPostExecute(result);		
		 if (listener != null) {			
		      listener.onResult(result);		      
		    }	
		 
		
	}
}

