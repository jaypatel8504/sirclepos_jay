package com.intlpos.WCFAccess;

import java.io.InputStream;
import java.util.LinkedHashMap;

import org.json.JSONObject;

import android.os.AsyncTask;

public class GetPermission extends AsyncTask<Object, Void, JSONObject> 
{
	InputStream is = null;
	JSONObject jObj = null;
	String jason = "";
	
	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject doInBackground(Object... params) 
	{
		WebService webservice = new WebService((String)params[0]);
		jObj = webservice.webInvoke((String)params[1], (LinkedHashMap<String, Object>)params[2]);
		return jObj;		
	}

}