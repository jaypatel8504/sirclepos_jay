package com.intlpos.WCFAccess;

import org.json.JSONArray;

public interface JSONArrayListener {
	public abstract void onResult(JSONArray json);
}
