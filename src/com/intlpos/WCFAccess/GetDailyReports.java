package com.intlpos.WCFAccess;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.EndTime;
import com.intlpos.mysharedpreferences.PreviousStartTime;
import com.intlpos.mysharedpreferences.StartTime;
import com.intlpos.sirclepos.CornerStorePOS;

public class GetDailyReports extends AsyncTask<Object, Void, String>{
	String result;
	String theUrl;
	public String getZoutReport(String url, boolean fromStartPage, boolean zout)
	{
		String returnResult = "";
		GetDailyReports task = new GetDailyReports();
		PreviousStartTime prevTime = CSSharedPreferences.loadPrevTime();
		StartTime startTime = CSSharedPreferences.loadStartTime();
		EndTime endTime = CSSharedPreferences.loadEndTime();
		String theEND ="";
		if(zout)
			theEND = endTime.getEndTime();
		else{
			Log.d("XOUT","theEND");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			String currentDateandTime = sdf.format(new Date());
			theEND = currentDateandTime;
			Log.d("XOUT",theEND);
		}
		LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		
		if(fromStartPage == true){
			Log.d("Start Page", "Start Page");
			Log.d("Start Page Start Time", "Start"+startTime.getTime());
			params.put("storedetail", storedetail);
			params.put("startdate", startTime.getTime());
			params.put("enddate", endTime.getEndTime());
			
			task.execute(CornerStorePOS.Url, url, params);
			
		}
		else if(fromStartPage == false){
			Log.d("XOUT",theEND);
			params.put("storedetail", storedetail);
			if(zout)
				params.put("startdate", prevTime.getPrevTime());
			else
				params.put("startdate", startTime.getTime());
			params.put("enddate", theEND);

			task.execute(CornerStorePOS.Url, url, params);
			
		}
		
		try {
			returnResult = task.get();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return returnResult;
	}         
	
	
	@Override      
	protected String doInBackground(Object... params) {
		WebService webservice = new WebService((String)params[0]);
		result = webservice.toGetString((String)params[1], (LinkedHashMap<String, Object>)params[2]);
		return result;
	}
	
	@Override
	protected void onPostExecute(String result) 
	{	
		
			
	
		//Log.d("RESULT", result);
			
	}

}