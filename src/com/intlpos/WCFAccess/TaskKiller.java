package com.intlpos.WCFAccess;

import java.util.TimerTask;

import android.os.AsyncTask;
import com.intlpos.sirclepos.R;

class TaskKiller extends TimerTask {
	 private AsyncTask<?, ?, ?> mTask;
	 public TaskKiller(AsyncTask<?, ?, ?> task) {
	  this.mTask = task;
	 }

	  public void run() {
	  mTask.cancel(true);
	 }
	}
