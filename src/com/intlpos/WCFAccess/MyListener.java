package com.intlpos.WCFAccess;

import com.intlpos.sirclepos.R;
import org.json.JSONObject;

public interface MyListener {
	public abstract void onResult(JSONObject json);
}
