package com.intlpos.WCFAccess;

import com.starmicronics.stario.StarIOPort;

public interface StarListener {
	public void getStar(StarIOPort port);
}
