package com.intlpos.WCFAccess;

import java.util.LinkedHashMap;

import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class RegisterParser extends AsyncTask<Object, Void, String>{
	String result;
	String theUrl;
	private MyListener listener;
	
	public RegisterParser(MyListener listener){
		this.listener = listener;
	}
	
	@Override
	protected String doInBackground(Object... params) {
		WebService webservice = new WebService((String)params[0]);
		result = webservice.toGetString((String)params[1], (LinkedHashMap<String, Object>)params[2]);
		return result;
	}

	
	@Override
	protected void onPostExecute(String result) 
	{		
		 Log.d("RESULT!", ""+result);
		
	}
}
