package com.intlpos.WCFAccess;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import android.util.Log;

import com.google.gson.Gson;
import com.intlpos.sirclepos.R;
public class WebServiceNew {

	//DefaultHttpClient httpClient;
	private String ret;
	InputStream is = null;
	HttpResponse response = null;
	HttpPost httpPost = null;
	HttpGet httpGet = null;
	String webServiceUrl;
	JSONObject jObj;
	private DefaultHttpClient httpClient;
//	static private Map ERROR = new HashMap();

	// The serviceName would be the name of the Service you are going to be
	// using.

	public WebServiceNew(String serviceName) {
		httpClient = new DefaultHttpClient();
		webServiceUrl = serviceName;
	}



	public JSONObject webInvoke(String methodName, LinkedHashMap<String, Object> params) {
		ret = null;
		httpPost = new HttpPost(webServiceUrl + methodName);
		response = null;

		StringEntity se = null;
		ByteArrayEntity entity = null;
		try {
				
			Gson gson = new Gson();
			String paramsObj = gson.toJson(params);
			Log.d("jsonobj", paramsObj);
			entity = new ByteArrayEntity(paramsObj.toString().getBytes("UTF-8"));
			entity.setContentType("application/json");
			se = new StringEntity(paramsObj);
			se.setContentEncoding("UTF-8");
			se.setContentType("application/json");
		} catch (UnsupportedEncodingException e) {
			Log.e("CornerStorePOS",
					"HttpUtils : UnsupportedEncodingException : " + e);
		}
		httpPost.setEntity(entity);
		Log.d("CornerStorePOS", webServiceUrl + methodName + "?" + params);

		try {
				response = httpClient.execute(httpPost);

			if (response != null) {
				HttpEntity httpEntity = response.getEntity();

				is = httpEntity.getContent();

			}
		} catch (Exception e) {
			Log.e("CornerStorePOS", "HttpUtils : " + e);
		}

		try {
			ret = new String(readStream(is));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// try parse the string to a JASON object
		try {
				jObj = new JSONObject(ret);
		} catch (Exception e) {
			Log.e("JASON Parser", "Exception " + e.toString());
		}
		// return JASON String

		return jObj;

	}

	/**
	 * Read InputStream
	 * 
	 * @param inStream
	 * @return byteArray
	 * @throws Exception
	 */
	public static byte[] readStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = -1;
		while ((len = inStream.read(buffer)) != -1) {
			outSteam.write(buffer, 0, len);
		}
		outSteam.close();
		inStream.close();
		return outSteam.toByteArray();
	}
}
