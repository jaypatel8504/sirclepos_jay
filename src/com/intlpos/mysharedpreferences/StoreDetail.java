package com.intlpos.mysharedpreferences;

import com.intlpos.sirclepos.R;


public class StoreDetail {

	public static String PREFS_NAME = "CornerStorePrefsFile";
	public static String KEY_NOT_FIRST_TIME = "notfirsttime";
	public static String KEY_EMAIL_ID = "email_id";
	public static String KEY_PASSWORD = "password";
	public static String KEY_STOREID = "storeid";
	public static String KEY_STATIONID = "stationid";
	public static String PAYMENT_MODULE = "paymentmodule";
	public static String EXPRESS_CHECKOUT="expresscheckout";
	public static String KEY_LOGOUT = "logout";
	public static String KEY_IP = "ip";
	public static String KEY_Port = "port";

	private boolean notfirsttime;
	private String email_id;
	private String password;
	private String storeid;
	private String stationid;
	//
	//start - settings related preferences
	
	public static String KEY_COMBINE_LINES = "switch_combine_lines"; // false = no combine lines , true = combine lines , default 1
	public static String KEY_PRINT_RECEIPT = "list_print_receipt"; // 0 = prompt ,1 = yes ,2 = no  , default prompt
	public static String KEY_RECEIPT_TYPE = "switch_full_desc";   // 1 = full , 0 = short , default short
	public static String KEY_STOCK_PROMPT = "switch_stock_prompt"; // 1 = yes or 0 = no , default no
	public static String KEY_PRINT_ZOUT = "list_print_zout_report"; // 1 = yes or 0 = no , default no
	public static String KEY_PRINTER_TYPE = "list_printer_type";//NONE or USB or BLUETOOTH or WIFI
	public static String KEY_PRINTER_MACADDRESS = "printer_macaddress"; // if the printer type is bluetooth printer_macaddress stores the mac address of the printer
	public static String KEY_PRINTER_USB_PORT1 = "usbport1";
	public static String KEY_PRINTER_USB_PORT2 = "usbport2";
	public static String KEY_PRINTER_PAPER_TYPE = "switch_bluetooth_printer";
	public static String KEY_QTY_KEYPAD = "switch_qty_keypad";
	
	private boolean combine_lines = false;
	private String print_receipt = "2";
	private boolean receipt_type = false;
	private boolean stock_prompt = false;
	private String print_zout = "2";
	private boolean qty_keypad = false;
	
	
	private boolean paymentModule; 
	private String Pin;
	private String PrimaryUrl;
	private String paymentType;
	private String secureKey;
	private boolean express;
	private boolean logout;
	private int port;
	private String ip;
		
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setLogout(boolean logg){
		logg = logout;
	}
	
	public boolean getLogout(){
		return logout;
	}
	
	public void setExpress(boolean expressCheck){
		express = expressCheck;
	}
	
	public boolean getExpress(){
		return express;
	}
	
	public void setPaymentModule(boolean pay){
		paymentModule = pay;
	}
	
	public boolean getPayMentModule(){
		return paymentModule; 
	}
	
	public String getPrint_zout() {
		return print_zout;
	}


	public void setPrint_zout(String print_zout) {
		this.print_zout = print_zout;
	}


	private String printer_macaddress;
	private boolean paper_type;
	private int[] printer_usb_port;
	public int[] getPrinter_usb_port() {
		return printer_usb_port;
	}


	public void setPrinter_usb_port(int[] printer_usb_port) {
		this.printer_usb_port = printer_usb_port;
	}


	private String printer_type = "2" ; 
	
	//end - settings related preferences
	
	
	//start - for update
	public static String KEY_VERSION_CODE = "version_code";
	public static String KEY_VERSION_NAME = "version_name"; 
	private int version_code ;
	private String version_name ;
	//end - for update
	
	public StoreDetail()
	{}
	/*public StoreDetail(Boolean _firsttime,String _storeid,int _stationid)
	{
		this.firsttime = _firsttime;
		this.storeid = _storeid;
		this.stationid = _stationid;		
	}*/
	
	
	public boolean getnotfirsttime()
	{
		return notfirsttime;
	}
	
	public void setnotfirsttime(boolean _notfirsttime)
	{
		this.notfirsttime = _notfirsttime;
	}
	
	public String getstoreid()
	{
		return storeid;
	}
	
	public void setstoreid(String _storeid)
	{
		this.storeid = _storeid;
	}
	
	public String getstationid()
	{
		return stationid;
	}
	
	public void setstationid(String _stationid)
	{
		this.stationid = _stationid;
	}
	
	public String getemail_id()
	{
		return email_id;
	}
	
	public void setemail_id(String _email_id)
	{
		this.email_id = _email_id;
	}
	
	public String getpassword()
	{
		return password;
	}
	
	public void setpassword(String _password)
	{
		this.password = _password;
	}


	public boolean isCombine_lines() {
		return combine_lines;
	}


	public void setCombine_lines(boolean combine_lines) {
		this.combine_lines = combine_lines;
	}


	public String getPrint_receipt() {
		return print_receipt;
	}


	public void setPrint_receipt(String print_receipt) {
		this.print_receipt = print_receipt;
	}


	public boolean isReceipt_type() {
		return receipt_type;
	}


	public void setReceipt_type(boolean receipt_type) {
		this.receipt_type = receipt_type;
	}


	public boolean isStock_prompt() {
		return stock_prompt;
	}


	public void setStock_prompt(boolean stock_prompt) {
		this.stock_prompt = stock_prompt;
	}


	public String getPrinter_macaddress() {
		return printer_macaddress;
	}


	public void setPrinter_macaddress(String printer_macaddress) {
		this.printer_macaddress = printer_macaddress;
	}


	public String getPrinter_type() {
		return printer_type;
	}


	public void setPrinter_type(String printer_type) {
		this.printer_type = printer_type;
	}


	public int getVersion_code() {
		return version_code;
	}


	public void setVersion_code(int versionCode) {
		this.version_code = versionCode;
	}


	public String getVersion_name() {
		return version_name;
	}


	public void setVersion_name(String version_name) {
		this.version_name = version_name;
	}


	public boolean isPaper_type() {
		return paper_type;
	}


	public void setPaper_type(boolean paper_type) {
		this.paper_type = paper_type;
	}


	public boolean isQty_keypad() {
		return qty_keypad;
	}


	public void setQty_keypad(boolean qty_keypad) {
		this.qty_keypad = qty_keypad;
	}
	
}
