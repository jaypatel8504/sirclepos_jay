package com.intlpos.mysharedpreferences;

public class PaymentProcessing {
	public static String PREFS_NAME = "CornerStorePrefsFile";
	public static String KEY_MERCHANTID = "merchantid";
	public static String KEYURL = "url";
	public static String KEYPASSWORD = "merchantpassword";
	public static String KEYGATEWAY = "gateway";
	
	private String id;
	private String url;
	private String password;
	private String gateway;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	
	
}
