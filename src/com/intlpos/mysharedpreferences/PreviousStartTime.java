package com.intlpos.mysharedpreferences;

public class PreviousStartTime {
	
	public static String PREV_TIME = "CornerStorePreviousStartTimePrefsFile";
	public static String KEY_PREV_TIME="prev_start_Time";
	private String prevStartTime;

	public void setPrevTime(String Time){
		this.prevStartTime = Time;
	}
	
	public String getPrevTime(){
		return prevStartTime;
	}

}
