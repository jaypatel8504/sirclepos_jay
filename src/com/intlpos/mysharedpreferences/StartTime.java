package com.intlpos.mysharedpreferences;

public class StartTime {
	public static String PREFS_NAME = "CornerStoreStartTimePrefsFile";
	public static String KEY_START_TIME="startTime";
	private String startTime;

	public void setTime(String Time){
		this.startTime = Time;
	}
	
	public String getTime(){
		return startTime;
	}

}
