package com.intlpos.mysharedpreferences;

public class ImageHelper {
	public static String PREFSHELPER="imagehelper";
	
	private String imageId;
	private byte[] imageByteArray;
	private String url;
	public ImageHelper(String id, String url){
		imageId = id;
		this.url = url;
	}
	
	public ImageHelper() {
		// TODO Auto-generated constructor stub
	}

	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public byte[] getImageByteArray() {
		return imageByteArray;
	}
	
	public void setImageByteArray(byte[] imageByteArray) {
		this.imageByteArray = imageByteArray;
	}
	
	public String getUrl(){
		return url;
	}
}
