package com.intlpos.mysharedpreferences;

public class Demo {
	private boolean isDemo;
	public static String PREFS_DEMO = "CornerStoreDemoPrefsFile";
	public static String KEY_DEMO="demo";
	
	
	public boolean isDemo() {
		return isDemo;
	}

	public void setDemo(boolean isDemo) {
		this.isDemo = isDemo;
	}
}
