package com.intlpos.mysharedpreferences;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


public class CSSharedPreferences {
	
			
	public static void savePreferences(StoreDetail storedetail)
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(StoreDetail.KEY_NOT_FIRST_TIME, storedetail.getnotfirsttime());
		editor.putString(StoreDetail.KEY_EMAIL_ID, storedetail.getemail_id());
		editor.putString(StoreDetail.KEY_PASSWORD, storedetail.getpassword());
		editor.putString(StoreDetail.KEY_STOREID, storedetail.getstoreid());
		editor.putString(StoreDetail.KEY_STATIONID, storedetail.getstationid());
		editor.putBoolean(StoreDetail.PAYMENT_MODULE, storedetail.getPayMentModule());
		
		editor.commit(); 		
		
	}
	/*public static void saveSettingsPreferences(StoreDetail storedetail)
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		//settings preferences
				editor.putBoolean(StoreDetail.KEY_COMBINE_LINES, storedetail.isCombine_lines());
				editor.putString(StoreDetail.KEY_PRINT_RECEIPT, storedetail.getPrint_receipt());
				editor.putBoolean(StoreDetail.KEY_RECEIPT_TYPE, storedetail.isReceipt_type());
				editor.putBoolean(StoreDetail.KEY_STOCK_PROMPT, storedetail.isStock_prompt());
				editor.putString(StoreDetail.KEY_PRINTER_MACADDRESS, storedetail.getPrinter_macaddress());
				editor.putString(StoreDetail.KEY_PRINTER_TYPE, storedetail.getPrinter_type());
				editor.commit(); 	
	}*/
	public static void saveUpdatePreferences(StoreDetail storedetail)
	{		
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		//for updates
				editor.putInt(StoreDetail.KEY_VERSION_CODE, storedetail.getVersion_code());
				editor.putString(StoreDetail.KEY_VERSION_NAME, storedetail.getVersion_name());
				editor.commit(); 	
				
	}
	
	public  static StoreDetail loadPreferences()
	{	
		StoreDetail storedetail = new StoreDetail();
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		storedetail.setnotfirsttime(sharedPreferences.getBoolean(StoreDetail.KEY_NOT_FIRST_TIME, false));
		storedetail.setemail_id(sharedPreferences.getString(StoreDetail.KEY_EMAIL_ID, ""));
		storedetail.setpassword(sharedPreferences.getString(StoreDetail.KEY_PASSWORD, ""));
		storedetail.setstoreid(sharedPreferences.getString(StoreDetail.KEY_STOREID, ""));
		storedetail.setstationid(sharedPreferences.getString(StoreDetail.KEY_STATIONID, ""));
		//settings preferences
		SharedPreferences settingsPreferences = PreferenceManager.getDefaultSharedPreferences(CornerStorePOS.getAppContext());
		storedetail.setCombine_lines(settingsPreferences.getBoolean(StoreDetail.KEY_COMBINE_LINES,true));
		storedetail.setPrint_receipt(settingsPreferences.getString(StoreDetail.KEY_PRINT_RECEIPT,"2"));
		storedetail.setReceipt_type(settingsPreferences.getBoolean(StoreDetail.KEY_RECEIPT_TYPE,false));		
		storedetail.setStock_prompt(settingsPreferences.getBoolean(StoreDetail.KEY_STOCK_PROMPT,false));
		storedetail.setPrint_zout(settingsPreferences.getString(StoreDetail.KEY_PRINT_ZOUT,"2"));		
		storedetail.setPrinter_macaddress(settingsPreferences.getString(StoreDetail.KEY_PRINTER_MACADDRESS, ""));
		storedetail.setPrinter_type(settingsPreferences.getString(StoreDetail.KEY_PRINTER_TYPE, "2"));
		int[] usb_port = new int[2];
		usb_port[0] = settingsPreferences.getInt("usbport1", 0);
		usb_port[1] = settingsPreferences.getInt("usbport2", 0);
		storedetail.setPrinter_usb_port(usb_port);
		storedetail.setPaper_type(settingsPreferences.getBoolean(StoreDetail.KEY_PRINTER_PAPER_TYPE, true));
		storedetail.setQty_keypad(settingsPreferences.getBoolean(StoreDetail.KEY_QTY_KEYPAD, false));
		//update preferences
		storedetail.setVersion_code(sharedPreferences.getInt(StoreDetail.KEY_VERSION_CODE, 0));
		storedetail.setVersion_name(sharedPreferences.getString(StoreDetail.KEY_VERSION_NAME, ""));
		Log.d("Comments", "First time");
	    return storedetail;
	
	    //the app is being launched for first time, do something        
	    

	             // first time task

	    // record the fact that the app has been started at least once
	   // settings.edit().putBoolean("first_time", false).commit(); 
	}
	
	public  static StoreDetail loadVersionPreferences()
	{	
		StoreDetail storedetail = new StoreDetail();
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		//update preferences
		storedetail.setVersion_code(sharedPreferences.getInt(StoreDetail.KEY_VERSION_CODE, 0));
		storedetail.setVersion_name(sharedPreferences.getString(StoreDetail.KEY_VERSION_NAME, ""));
		Log.d("Comments", "First time");
	    return storedetail;
	}
	
/*	public static void updatePrefrences(Context context,StoreDetail storedetail)
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();				
		editor.putInt(StoreDetail.KEY_STATIONID, storedetail.getstationid());
		editor.commit(); 
	}*/
	
	public static void removePreferences()
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
	    editor.remove(StoreDetail.KEY_NOT_FIRST_TIME);
	    editor.remove(StoreDetail.KEY_EMAIL_ID);
	    editor.remove(StoreDetail.KEY_PASSWORD);
	    editor.remove(StoreDetail.KEY_STATIONID);
	    editor.remove(StoreDetail.KEY_STOREID);
	    editor.remove(StoreDetail.KEY_COMBINE_LINES);
	    editor.remove(StoreDetail.KEY_PRINT_RECEIPT);
	    editor.remove(StoreDetail.KEY_RECEIPT_TYPE);
	    editor.remove(StoreDetail.KEY_STOCK_PROMPT);
	    editor.remove(StoreDetail.KEY_STOCK_PROMPT);
	    editor.remove(StoreDetail.KEY_PRINTER_TYPE);
	    editor.remove(StoreDetail.KEY_VERSION_CODE);
	    editor.remove(StoreDetail.KEY_VERSION_NAME);
	    editor.remove(Demo.KEY_DEMO);
	    editor.commit();	    
	}
	
	
	public static void saveStartTimePreferences(StartTime startTime)
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StartTime.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(StartTime.KEY_START_TIME, startTime.getTime());
		
		editor.commit(); 		
	}
	
	public static void saveEndTimePreferences(EndTime endTime)
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(EndTime.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(EndTime.KEY_END_TIME, endTime.getEndTime());
		
		editor.commit(); 		
	}
	
	public static void savePrevTimePreferences(PreviousStartTime prevTime)
	{
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(PreviousStartTime.PREV_TIME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(PreviousStartTime.KEY_PREV_TIME, prevTime.getPrevTime());
		
		editor.commit(); 		
	}
	
	public static StartTime loadStartTime()
	{
		StartTime startTime = new StartTime();
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StartTime.PREFS_NAME, 0);
		startTime.setTime(sharedPreferences.getString(StartTime.KEY_START_TIME, ""));
		
		return startTime;
	}
	
	public static EndTime loadEndTime()
	{
		EndTime endTime = new EndTime();
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(EndTime.PREFS_NAME, 0);
		endTime.setendTime(sharedPreferences.getString(EndTime.KEY_END_TIME, ""));
		
		return endTime;
	}
	
	public static PreviousStartTime loadPrevTime()
	{
		PreviousStartTime prevTime = new PreviousStartTime();
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(PreviousStartTime.PREV_TIME, 0);
		prevTime.setPrevTime(sharedPreferences.getString(PreviousStartTime.KEY_PREV_TIME, ""));
		
		return prevTime;
	}
	
	public static void setDemoPref(boolean demo){
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(Demo.KEY_DEMO, demo);
		
		editor.commit();
	}
	
	public static boolean loadDemo(){
		boolean demo;
		
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		demo = sharedPreferences.getBoolean(Demo.KEY_DEMO, false);
		
		return demo;
	}
	 public static StoreDetail loadPayment(){
		 StoreDetail storedetail = new StoreDetail();
			SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
			storedetail.setPaymentModule(sharedPreferences.getBoolean(StoreDetail.PAYMENT_MODULE, false));
			
		return storedetail;
	 }

	 public static void  setExpressCheckout(StoreDetail storedetail){

			SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(StoreDetail.EXPRESS_CHECKOUT, storedetail.getExpress());
			editor.commit();
	 }
	 
	 public static StoreDetail getExpressCheckout(){
		 StoreDetail storedetail = new StoreDetail();
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 storedetail.setExpress(sharedPreferences.getBoolean(StoreDetail.EXPRESS_CHECKOUT, false));
			
		 return storedetail;
	 }
	 
	 public static void setPaymentProcessing(PaymentProcessing pp){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(PaymentProcessing.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(PaymentProcessing.KEY_MERCHANTID, pp.getId());
			editor.putString(PaymentProcessing.KEYURL, pp.getUrl());
			editor.putString(PaymentProcessing.KEYPASSWORD, pp.getPassword());
			editor.putString(PaymentProcessing.KEYGATEWAY, pp.getGateway());
			
			editor.commit(); 		
	 }
	 
	 public static PaymentProcessing getPayment(){
		PaymentProcessing pp = new PaymentProcessing();
		
		SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(PaymentProcessing.PREFS_NAME, 0);
		pp.setId(sharedPreferences.getString(PaymentProcessing.KEY_MERCHANTID, ""));
		pp.setPassword(sharedPreferences.getString(PaymentProcessing.KEYPASSWORD, ""));
		pp.setGateway(sharedPreferences.getString(PaymentProcessing.KEYGATEWAY, ""));
		pp.setUrl(sharedPreferences.getString(PaymentProcessing.KEYURL, ""));
		
		return pp;
	 }
	 
	 
	 public static void setIsLoggedOut(boolean log){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(StoreDetail.KEY_LOGOUT, log);
			editor.commit();
	 }
	 
	 public static boolean getLogOut(){
	
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 boolean logged = sharedPreferences.getBoolean(StoreDetail.KEY_LOGOUT, false);
		 
		 return logged;
	 }
	 
	 public static void setWifi(boolean wif){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean("Wifi", wif);
			editor.commit();
	 }
	 
	 public static boolean getWifi(){
	
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 boolean wif = sharedPreferences.getBoolean("Wifi", false);
		 
		 return wif;
	 }
	 
	 
	 
	 public static void saveIpAndPort(StoreDetail sd){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(StoreDetail.KEY_IP, sd.getIp());
			editor.putInt(StoreDetail.KEY_Port, sd.getPort());
			
			editor.commit(); 		
	 }
	 
	 public static StoreDetail getIpandPort(){
		 StoreDetail storedetail = new StoreDetail();
			SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
			storedetail.setIp(sharedPreferences.getString(StoreDetail.KEY_IP, "10.0.0.20"));
			storedetail.setPort(sharedPreferences.getInt(StoreDetail.KEY_Port, 4554));
			
			return storedetail;
	 }
	 
	 public static void setIntsall(boolean wif){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean("install", wif);
			editor.commit();
	 }
	 
	 public static boolean getIntsall(){
	
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 boolean wif = sharedPreferences.getBoolean("install", false);
		 
		 return wif;
	 }
	 
	 
	 public static void setPic(String pic){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString("PIC", pic);
			editor.commit();
	 }
	 
	 public static String getPic(){
			
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 String pic = sharedPreferences.getString("PIC", "");
		 
		 return pic;
	 }
	 
	 public static void setMessages(Set<String> mess){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putStringSet("mess", mess);
		    editor.commit();
	 }
	 
	 public static Set<String> getMess(){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 Set<String> mySet = new HashSet<String>();
		 Set<String> messages = sharedPreferences.getStringSet("mess", mySet);
		 return messages;
	 }
	 
	 public static int getToken(){
			
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		 int token = sharedPreferences.getInt("TOKEN", 1);
		 
		 return token;
	 }
	 
	 public static void setToken(int token){
		 SharedPreferences sharedPreferences = CornerStorePOS.getAppContext().getSharedPreferences(StoreDetail.PREFS_NAME, 0);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putInt("TOKEN", token);
		    editor.commit();
	 }

}