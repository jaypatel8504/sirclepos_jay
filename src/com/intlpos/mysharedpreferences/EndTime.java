package com.intlpos.mysharedpreferences;

public class EndTime {
	public static String PREFS_NAME = "CornerStoreStartTimePrefsFile";
	public static String KEY_END_TIME="end";
	private String endTime;
	
	public void setendTime(String Time){
		this.endTime = Time;
	}
	
	public String getEndTime(){
		return endTime;
	}
	
}
