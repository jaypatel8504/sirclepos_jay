package com.intlpos.initsetup;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.WifiPopup;
import com.john.beans.CSVProduct;
import com.john.beans.StoreDetail;
import com.john.file.DirChooserDialog;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class CsvFileReadFragment extends ReceiptDetails {
	protected static final String ADD_URL = "InventoryImport/ImportService.svc/importinventory";
	protected static final String GET_URL = "InventoryImport/ImportService.svc/getproductscount";
	private View view;
	private Button open,submit,finish;
	private ListView list;
	private Context context;
	protected DirChooserDialog dlg;
	private int productCount;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	
	public CsvFileReadFragment() {
		// Required empty public constructor
	}
	class CSVRequest{
		StoreDetail storedetail;
		ArrayList<CSVProduct> inventorylist;
		public CSVRequest(StoreDetail storedetail, ArrayList<CSVProduct> inventorylist){
			this.storedetail = storedetail;
			this.inventorylist = inventorylist;
		}
	}
	
	private class GetProductCountRequest{
		StoreDetail storedetail;
		public GetProductCountRequest(StoreDetail storedetail){
			this.storedetail = storedetail;
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.fragment_csv_import, container, false);
		context = getActivity();
		open = (Button) view.findViewById(R.id.opencsv);
		list = (ListView) view.findViewById(R.id.csvlistview);
		View v = (View)inflater.inflate(R.layout.csvfilegrid_header, null);
		list.addHeaderView(v);
		// get product counts
		PostRequestTask postTask = new PostRequestTask();
		Gson gson = new Gson();
		CornerStorePOS app = (CornerStorePOS) ((Activity) context).getApplication();
		GetProductCountRequest getcountrequest = new GetProductCountRequest(app.storedetail);
		Log.d("getCount", gson.toJson(getcountrequest));
    	postTask.execute(CornerStorePOS.Url+GET_URL,gson.toJson(getcountrequest));
    	String result = null;
    	try {
			result = postTask.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
    	try{
    		productCount = Integer.parseInt(result);
    	}catch(Exception e){
    		Toast.makeText(context, result+"Cannot get products number.", Toast.LENGTH_SHORT).show();
    	}
		
		open.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String path = null;
				String [] fileType = {"csv"};
				dlg = new DirChooserDialog(context,1,fileType,path,list);
				dlg.setTitle("Choose csv file dir");
				dlg.show();
				
			}
		});
		submit = (Button) view.findViewById(R.id.submit);
		finish = (Button) view.findViewById(R.id.finish);
		submit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			    mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			    if(mWifi.isConnected()){
				PostRequestTask postTask = new PostRequestTask();
				Gson gson = new Gson();
				CornerStorePOS app = (CornerStorePOS) ((Activity) context).getApplication();
				ArrayList<CSVProduct> list = dlg.getCsvlist();
				if(list == null){
					Toast.makeText(context, R.string.chooseacsv, Toast.LENGTH_SHORT).show();
					return;
				}
				if(list.size() + productCount > 6000){
					Toast.makeText(context, "Product list cannot more than 6000", Toast.LENGTH_SHORT).show();
					return;
				}
				CSVRequest csvrequest = new CSVRequest(app.storedetail,dlg.getCsvlist());
//            	Log.d("Json", gson.toJson(csvrequest));
            	postTask.execute(CornerStorePOS.Url+ADD_URL,gson.toJson(csvrequest));
            	String result = null;
            	try {
					result = postTask.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
            	JSONObject response = null;
            	try {
            		response = new JSONObject(result);
                	if(response.getBoolean("result"))
                		Toast.makeText(context, "CSV File Import Successful!", Toast.LENGTH_SHORT).show();
                	else{
                		Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
                	}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(context, "CSV File Import Failed!", Toast.LENGTH_SHORT).show();
				}
			   }
			    else{
			    	show();
			    }
			}
			   
			
		});
		finish.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				AddInventory frag = new AddInventory();
				onButtonPressed(frag);
			}			
		});
		return view;
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}

}
