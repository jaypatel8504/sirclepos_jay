package com.intlpos.initsetup;
import com.intlpos.sirclepos.DemoOrRegister;
import com.intlpos.sirclepos.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PartnerTech extends Activity implements View.OnClickListener{
	
	private Button partner, other, yes, no, next;
	private ImageView retail, qsr;
	private EditText ip;
	private boolean partnertech, isQsr;
	private TextView kitchTitle;
	private LinearLayout kitchen; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.partnertech);
		partnertech = false;
		isQsr = false;
		getWidgets();
		setWidgets();
	}
	
	private void getWidgets(){
		partner = (Button) findViewById(R.id.partner);
		other = (Button) findViewById(R.id.other);
		yes = (Button) findViewById(R.id.yes);
		no = (Button) findViewById(R.id.no);
		next = (Button) findViewById(R.id.next);
				
		retail = (ImageView) findViewById(R.id.retail);
		qsr = (ImageView) findViewById(R.id.qsr);
		
		ip = (EditText) findViewById(R.id.ip);
		
		kitchen= (LinearLayout) findViewById(R.id.kitchen);
		kitchTitle = (TextView) findViewById(R.id.kitchTitle);
	}
	
	private void setWidgets(){
		partner.setOnClickListener(this);
		other.setOnClickListener(this);
		yes.setOnClickListener(this);
		no.setOnClickListener(this);
		retail.setOnClickListener(this);
		qsr.setOnClickListener(this);
		next.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.partner:
				partnertech = true;
				partner.setBackgroundDrawable(getResources().getDrawable(R.drawable.selectedbutton));
				other.setBackgroundDrawable(getResources().getDrawable(R.drawable.startpagebutton));
				kitchen.setVisibility(View.VISIBLE);
				kitchTitle.setVisibility(View.VISIBLE);
				break;
			case R.id.other:
				partnertech = false;
				partner.setBackgroundDrawable(getResources().getDrawable(R.drawable.startpagebutton));
				other.setBackgroundDrawable(getResources().getDrawable(R.drawable.selectedbutton));
				kitchen.setVisibility(View.GONE);
				kitchTitle.setVisibility(View.GONE);
				break;
			case R.id.yes:
				ip.setVisibility(View.VISIBLE);
				break;
			case R.id.no:
				ip.setVisibility(View.GONE);
				break;
			case R.id.retail:
				retail.setBackgroundDrawable(getResources().getDrawable(R.drawable.bordersolid));
				qsr.setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
				isQsr = false;
				break;
			case R.id.qsr:
				retail.setBackgroundDrawable(getResources().getDrawable(R.drawable.border));
				qsr.setBackgroundDrawable(getResources().getDrawable(R.drawable.bordersolid));
				isQsr = true;
				break;
			case R.id.next:
				setSettings();
				Intent i = new Intent(getApplicationContext(), DemoOrRegister.class);
				startActivity(i);
				break;
		}
	}
	
	public void setSettings(){
		 SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		 SharedPreferences.Editor editor = settings.edit();
		 editor.putString("list_print_receipt", "2");
		 editor.putBoolean("switch_pictures",true);
		if(partnertech){
			 editor.putBoolean("switch_printer",true);
			 editor.putString("printerType", "Partner Tech");
			 editor.putString("cash_types", "Partner Tech");
			 editor.putString("pole_types","Partner Tech");
		}
		if(isQsr){
			 editor.putBoolean("ui",true);
		}
		if(!ip.getText().toString().isEmpty()){
			editor.putString("wifi_printer_ip", ip.getText().toString());
			editor.putString("kitchentprint", "SNBC/Partner Tech");
		}
		editor.commit();
	}

}
