package com.intlpos.initsetup;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Application;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.WifiPopup;
import com.john.beans.Department;
import com.john.beans.Product;
import com.john.beans.StoreDetail;

public class AddStandardInventory extends ReceiptDetails {
	
	private String GET_URL = "DepartmentMaintenence/DepartmentService.svc/getdepartments";
	protected String GET_COUNT_URL = "InventoryImport/ImportService.svc/getproductscount";
	private String ADD_URL = "InventoryMaintenence/InventoryService.svc/saveproduct";
	private ArrayList<Department> department_list;
	private int addInventoryIndex;
	private EditText barcode,itemName,cost,price,itemStock;
	private TextView tv;
	private Spinner department_spinner;
	private CheckBox tax1,tax2,tax3;
	private PostRequestTask postTask;
	private boolean isAutofill;
	private Button submit;
	private CornerStorePOS app;
	private Button finish;
	private int productCount = -1;
	private ArrayList<BigDecimal> textArray;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	
	public AddStandardInventory() {
		// Required empty public constructor
	}
	public AddStandardInventory(boolean isAutoFill) {
		this.isAutofill = isAutoFill;
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	private class GetProductCountRequest{
		StoreDetail storedetail;
		public GetProductCountRequest(StoreDetail storedetail){
			this.storedetail = storedetail;
		}
	}
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		app = (CornerStorePOS) this.getActivity().getApplication();
		ADD_URL = CornerStorePOS.Url + ADD_URL;
		GET_URL = CornerStorePOS.Url + GET_URL;
		final View view = inflater.inflate(R.layout.fragment_addstandardinventory, container, false);
		// get product counts
				PostRequestTask postTask = new PostRequestTask();
				Gson gson = new Gson();
				CornerStorePOS app = (CornerStorePOS) this.getActivity().getApplication();
				GetProductCountRequest getcountrequest = new GetProductCountRequest(app.storedetail);
				Log.d("getCount", gson.toJson(getcountrequest));
		    	postTask.execute(CornerStorePOS.Url+GET_COUNT_URL,gson.toJson(getcountrequest));
		    	String result = null;
		    	try {
					result = postTask.get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
		    	try{
		    		productCount = Integer.parseInt(result);
		    	}catch(Exception e){
		    		Toast.makeText(this.getActivity().getBaseContext(), R.string.cannot_get_products_number, Toast.LENGTH_SHORT).show();
		    	}
		tv = (TextView) view.findViewById(R.id.textview);
		String[] name = getResources().getStringArray(R.array.inventory_input);
		if(isAutofill){
			tv.setText(name[1]);
		}else{
			tv.setText(name[0]);
		}
		barcode = (EditText) view.findViewById(R.id.barcode_itemnumber);
		itemName = (EditText) view.findViewById(R.id.itemname);
		cost = (EditText) view.findViewById(R.id.cost);
		price = (EditText) view.findViewById(R.id.price);
		itemStock = (EditText) view.findViewById(R.id.itemstock);
		department_spinner = (Spinner) view.findViewById(R.id.spinner_department);
		tax1 = (CheckBox) view.findViewById(R.id.tax1);
		tax2 = (CheckBox) view.findViewById(R.id.tax2);
		tax3 = (CheckBox) view.findViewById(R.id.tax3);
		department_list = getDepartmentList();
		String[] dept_names = new String[department_list.size()];
		for(int i = 0, len = department_list.size(); i < len ; i++){
			dept_names[i] = department_list.get(i).getDepartment_name();
		}
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,
				dept_names);
		department_spinner.setAdapter(adapter);
//    	saveStandardInventory();
    	submit = (Button) view.findViewById(R.id.submitinventory);
    	finish = (Button) view.findViewById(R.id.finishlogininventory);
    	submit.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
			    mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if(!mWifi.isConnected()){
					show();
					return;
				}
				String getCheckIN[] =  getResources().getStringArray(R.array.checkIN);
				if (barcode.getText().toString().isEmpty() || itemName.getText().toString().isEmpty()){
					Toast.makeText(view.getContext(), R.string.barcode_and_itemname_cannot_be_empty, Toast.LENGTH_SHORT).show();
				}
				if(barcode.getText().toString().length() > 50){
					Toast.makeText(view.getContext(), getCheckIN[0], Toast.LENGTH_SHORT).show();
					return;
				}
				if(itemName.getText().toString().length() > 50){
					Toast.makeText(view.getContext(), getCheckIN[1], Toast.LENGTH_SHORT).show();
					return;
				}
				if(price.getText().toString().length() > 8){
					Toast.makeText(view.getContext(), getCheckIN[2], Toast.LENGTH_SHORT).show();
					return;
				}
				if(cost.getText().toString().length() > 8){
					Toast.makeText(view.getContext(), getCheckIN[3], Toast.LENGTH_SHORT).show();
					return;
				}
				if(itemStock.getText().toString().length() >8){
					Toast.makeText(view.getContext(), getCheckIN[4], Toast.LENGTH_SHORT).show();
					return;
				}
				
				
				String price_text = price.getText().toString().isEmpty() ? "0.00":price.getText().toString();
				String cost_text = cost.getText().toString().isEmpty() ? "0.00":cost.getText().toString();
				String stock_text = itemStock.getText().toString().isEmpty() ? "0.00":itemStock.getText().toString();
				textArray = new ArrayList<BigDecimal>(3);
				textArray.add(new BigDecimal(price_text).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				textArray.add(new BigDecimal(cost_text).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				textArray.add(new BigDecimal(stock_text).setScale(2, BigDecimal.ROUND_HALF_EVEN));
				for(BigDecimal dec:textArray){
					if (dec.compareTo(new BigDecimal(100000)) == 1){
						Toast.makeText(view.getContext(), "Price/Cost/Stock cannot larger than 100000", Toast.LENGTH_SHORT).show();
						return;
					}
				}
				boolean flag = saveStandardInventory();
				if (flag){
					AddStandardInventory frag = new AddStandardInventory(isAutofill);
					onButtonPressed(frag);
					Toast.makeText(getActivity(), R.string.product_saved , Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getActivity(), R.string.save_product_failed , Toast.LENGTH_SHORT).show();
				}
			}
    		
    	});
    	if(productCount == -1 || productCount > 6000){
//    		Toast.makeText(getActivity(), productCount+"" , Toast.LENGTH_SHORT).show();
    		submit.setEnabled(false);
    		submit.setText(R.string.product_count_is_invalid);
    	}
    	finish.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AddInventory frag = new AddInventory();
				onButtonPressed(frag);
			}
    		
    	});
		return view;
	}
	class SaveInventory{
		StoreDetail storedetail;
		Product product;
		public String modifiergroupslist;
		public SaveInventory(StoreDetail storedetail, Product product){
			this.storedetail = storedetail;
			this.product = product;
		}
		public SaveInventory() {
			// TODO Auto-generated constructor stub
		}
	}
	private boolean saveStandardInventory() {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		int pos = department_spinner.getSelectedItemPosition();
		Product product = new Product(barcode.getText().toString(),itemName.getText().toString(),textArray.get(0).toString(),textArray.get(1).toString()
    			,textArray.get(2).toString(),tax1.isChecked(),tax2.isChecked(),tax3.isChecked(),department_list.get(pos).getDepartment_id(),department_list.get(pos).getDepartment_name(),
    			"", false, false, false, false);
    	SaveInventory saveInventory = new SaveInventory();
    	saveInventory.product = product;
    	saveInventory.storedetail = app.storedetail;
    	saveInventory.modifiergroupslist = "";
		String request = gson.toJson(saveInventory);
//		Log.d("123", request);
		postTask = new PostRequestTask();
		postTask.execute(ADD_URL,request);
		String response = "";
		try {
			response = postTask.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Log.d("123", response);
		JSONObject obj;
		try {
			obj = new JSONObject(response);
	    	if (obj.get("result").equals(true)){
	    		return true;
	    	}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(NullPointerException e){
			Toast.makeText(getActivity(), R.string.SaveFailed, Toast.LENGTH_LONG).show();
		}
		return false;
	}

	private ArrayList<Department> getDepartmentList() {
		// TODO Auto-generated method stub
		JSONObject storedetail = new JSONObject();
		JSONObject obj = new JSONObject();
		ArrayList<Department> depts = null;
		try {
        	storedetail.put("email_id", CornerStorePOS.email_id);
        	storedetail.put("password", CornerStorePOS.password);
        	storedetail.put("store_id", CornerStorePOS.StoreId);
        	storedetail.put("station_id", CornerStorePOS.StationId);
        	obj.put("storedetail", storedetail);
    		postTask = new PostRequestTask();
    		postTask.execute("http://smart360pos.com/sirclepos/v1_320/DepartmentMaintenence/DepartmentService.svc/getdepartments",obj.toString());
    		
    		String response = "";
    		try {
    			response = postTask.get();
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ExecutionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		obj = new JSONObject(response);
    		Gson gson = new Gson();
    		Type listType = new TypeToken<ArrayList<Department>>() {}.getType();
    			
    		depts = gson.fromJson(obj.getString("department_list"), listType);
//        	Log.d("123", depts.size()+"");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getActivity(), R.string.request_failed , Toast.LENGTH_SHORT).show();
		}
		
		return depts;
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}
}
