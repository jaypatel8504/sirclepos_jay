package com.intlpos.initsetup;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import com.intlpos.sirclepos.R;

public class JSONWriteTool {

    private File mFile;

    /**
     * 构�?�函�?
     * @throws FileNotFoundException 
     */
    public JSONWriteTool(String fileName) throws FileNotFoundException {
    	File sdCard = Environment.getExternalStorageDirectory();
    	File dir = new File (sdCard.getAbsolutePath() + "/JSONObj/");
    	dir.mkdirs();
    	mFile = new File(dir, fileName);

//    	FileOutputStream f = new FileOutputStream(file);
//        mFile = new File(Environment.getExternalStorageDirectory().getPath() + fileName);
    }

    /**
     * 写入文件的方�?
     * 
     * @param key
     * @param value
     * @return
     */
    public boolean WriteToJSONFile(String key, String value) {
        boolean result = false;
        try
        {
            if (mFile.exists())
            {
                //直接写入进去
                // 创建成功
                JSONObject object = new JSONObject(this.GetJSONFileContent());
                try {
                    object.put(key, value);
                    writeFile(mFile.getAbsolutePath(), object.toString());
                    result = true;
                    Log.d("JSONFile", "JSONFile Wrote Success!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else
            {
                boolean createSuccess = mFile.createNewFile();
                if (createSuccess)
                {
                    // 创建成功
                    JSONObject object = new JSONObject();
                    try {
                        object.put(key, value);
                        writeFile(mFile.getAbsolutePath(), object.toString());
                        result = true;
                        Log.d("JSONFile", "JSONFile Wrote Success!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    return result;
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 新建Json文件时，写入json数据
     * 
     * @param filePath
     * @param sets
     * @throws IOException
     */
    protected void writeFile(String filePath, String sets) throws IOException {
        FileWriter fw = new FileWriter(filePath);
        PrintWriter out = new PrintWriter(fw);
        out.write(sets);
        out.println();
        fw.close();
        out.close();
    }
    /**
     * 将JSON文件中的数据读取为string
     * 
     * @return
     */
    protected String GetJSONFileContent() {
        String fileContentStr = "";
        try {
            FileInputStream inStream = new FileInputStream(mFile);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = inStream.read(buffer)) != -1) {
                stream.write(buffer, 0, length);
            }
            stream.close();
            inStream.close();
            fileContentStr = stream.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileContentStr;
    }

    /**
     * 删除�?条Json文件中的数据
     * 
     * @param key
     * @return
     */
    public boolean DeleteJsonData(String key) {
        boolean result = false;

        try {
            JSONObject json = new JSONObject(this.GetJSONFileContent());
            json.remove(key);
            result = true;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }
}
