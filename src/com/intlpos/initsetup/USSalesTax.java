package com.intlpos.initsetup;

import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.database.Tax;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.MyWiFiStateListener;
import com.intlpos.sirclepos.WifiPopup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class USSalesTax extends ReceiptDetails {
	private String ADD_URL = "companymaintenence/taxservice.svc/savetaxdetail";
	private OnFragmentSwitchListener mListener;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	public AsyncTask <String, Void, String> postTask;
	public USSalesTax() {
		// Required empty public constructor
	}
	private EditText tax1_name,tax2_name,tax3_name,tax1_value,tax2_value,tax3_value;
	private Button submit;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		ADD_URL = CornerStorePOS.Url+ADD_URL;
		final View view =inflater.inflate(R.layout.fragment_ussales_tax, container, false);
		
		tax1_name = (EditText) view.findViewById(R.id.tax1Name);
		tax1_name.setText("TAX 1");
		tax2_name = (EditText) view.findViewById(R.id.tax2Name);
		tax3_name = (EditText) view.findViewById(R.id.tax3Name);
		String[] taxrates = Tax.tax_desc;
		if(taxrates[0] == null || taxrates[0] == ""){
			tax1_name.setText("TAX1");
		}else{
			tax1_name.setText(taxrates[0]);
		}
		if(taxrates[1] == null || taxrates[1] == ""){
			tax2_name.setText("TAX2");
		}else{
			tax2_name.setText(taxrates[1]);
		}
		if(taxrates[2] == null || taxrates[2] == ""){
			tax3_name.setText("TAX3");
		}else{
			tax3_name.setText(taxrates[2]);
		}
		tax1_value = (EditText) view.findViewById(R.id.tax1Value);
		tax2_value = (EditText) view.findViewById(R.id.tax2Value);
		tax3_value = (EditText) view.findViewById(R.id.tax3Value);
		tax1_value.setText("0.00");
		tax2_value.setText("0.00");
		tax3_value.setText("0.00");
		submit = (Button) view.findViewById(R.id.ussubmit);
		submit.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {	connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
    			mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            	if(mWifi.isConnected()){	
	            	getActivity().setProgressBarIndeterminateVisibility(true);
	            	JSONObject tax = new JSONObject();
	            	JSONObject storedetail = new JSONObject();
	            	JSONObject savetaxdetail = new JSONObject();
	            	try {
	            		String tax_rate1 = tax1_value.getText().toString().isEmpty()? "0":tax1_value.getText().toString();
	            		String tax_rate2 = tax2_value.getText().toString().isEmpty()? "0":tax2_value.getText().toString();
	            		String tax_rate3 = tax3_value.getText().toString().isEmpty()? "0":tax3_value.getText().toString();
	            		
	            		String tax_desc1 = tax1_name.getText().toString() == null? "0":tax1_name.getText().toString();
	            		String tax_desc2 = tax2_name.getText().toString() == null? "0":tax2_name.getText().toString();
	            		String tax_desc3 = tax3_name.getText().toString() == null? "0":tax3_name.getText().toString();
	            		String taxesDes[] = {tax_desc1, tax_desc2, tax_desc3};
	            		String check[] = getResources().getStringArray(R.array.taxCheck);
	            		if(tax_rate1.length() > 7){
	            			Toast.makeText(getActivity(), check[0], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_rate2.length() > 7){
	            			Toast.makeText(getActivity(), check[1], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_rate3.length() > 7){
	            			Toast.makeText(getActivity(), check[2], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_desc1.length() > 10){
	            			Toast.makeText(getActivity(), "Tax Description 1 exceeds 10 characters. Please enter at least 10 characters for Tax Description 1", Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_desc2.length() > 10){
	            			Toast.makeText(getActivity(), "Tax Description 2 exceeds 10 characters. Please enter at least 10 characters for Tax Description 2", Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_desc3.length() > 10){
	            			Toast.makeText(getActivity(), "Tax Description 3 exceeds 10 characters. Please enter at least 10 characters for Tax Description 3", Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		String special_chars[] = {"\\t", "\\b", "\\n", "\\r", "\\f","+", "-", "&&", "||",  "(", ")", "{", "}", "[", "]", "^",
	                            "~", "*", "?", "\\", "\""};
	            		for(int i = 0; i <3; i++){
	            			for(int k = 0; k < special_chars.length;  k++){
	            				if(taxesDes[i].contains(special_chars[k])){
	            					Toast.makeText(getActivity().getApplicationContext(), special_chars[k] + " is not allowed in " + taxesDes[i], Toast.LENGTH_SHORT).show();
	            					return;
	            				}
	            			}
	            		}
	            		
						tax.put("tax_rate1", tax_rate1);
		            	tax.put("tax_rate2", tax_rate2);
		            	tax.put("tax_rate3", tax_rate3);
		            	tax.put("tax_desc1", tax_desc1);
		            	tax.put("tax_desc2", tax_desc2);
		            	tax.put("tax_desc3", tax_desc3);
		            	storedetail.put("email_id", CornerStorePOS.email_id);
		            	storedetail.put("password", CornerStorePOS.password);
		            	storedetail.put("store_id", CornerStorePOS.StoreId);
		            	storedetail.put("station_id", CornerStorePOS.StationId);
		            	savetaxdetail.put("storedetail", storedetail);
		            	savetaxdetail.put("tax", tax);
		        		postTask = new PostRequestTask();
		        		postTask.execute(ADD_URL,savetaxdetail.toString());
		        		
		        		String response = "";
		        		try {
		        			response = postTask.get();
		        		} catch (InterruptedException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        		} catch (ExecutionException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        		}
		        		
		        		JSONObject obj = new JSONObject(response);	        		
		        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
	
		            	getActivity().setProgressBarIndeterminateVisibility(false);
	//	            	if (obj.get("result").equals(true)){
	//	            		Bundle bundle = new Bundle();
	//	            		bundle.putString("resultString", obj.get("resultString").toString());
	//	            		Intent intent = new Intent();
	//	            		intent.putExtras(bundle);
	//	            		intent.setClass(getActivity(), Main.class);
	//	            		startActivity(intent);
	//	            		getActivity().finish();
	//	            	}
		            	setupDeptPopUP();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						postTask = new PostRequestTask();
		        		postTask.execute(CornerStorePOS.BackupUrl+"companymaintenence/taxservice.svc/savetaxdetail",savetaxdetail.toString());
		        		
		        		String response = "";
		        		try {
		        			response = postTask.get();
		        		} catch (InterruptedException e1) {
		        			// TODO Auto-generated catch block
		        			e1.printStackTrace();
		        		} catch (ExecutionException e1) {
		        			// TODO Auto-generated catch block
		        			e1.printStackTrace();
		        		}
		        		
		        		JSONObject obj = null;
						try {
							obj = new JSONObject(response);
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}	        		
		        		try {
							Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							Toast.makeText(getActivity(), "Request Failed!" , Toast.LENGTH_SHORT).show();
						}
	
		            	getActivity().setProgressBarIndeterminateVisibility(false);
	//	            	if (obj.get("result").equals(true)){
	//	            		Bundle bundle = new Bundle();
	//	            		bundle.putString("resultString", obj.get("resultString").toString());
	//	            		Intent intent = new Intent();
	//	            		intent.putExtras(bundle);
	//	            		intent.setClass(getActivity(), Main.class);
	//	            		startActivity(intent);
	//	            		getActivity().finish();
	//	            	}
		            	setupDeptPopUP();
						
					}
            	}
            	else{
            		show();
            	}
            }
    });
		
		return view;
	}
	
	private void setupDeptPopUP() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

//			// set title
//			alertDialogBuilder.setTitle("Setting up your System");

			// set dialog message
			alertDialogBuilder
				.setTitle(R.string.wouldin)
				.setCancelable(false)
				.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, closes
		        		AddInventory addInventory = new AddInventory();
		        		onButtonPressed(addInventory);
						dialog.dismiss();
					}
				  })
				.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
	            		Intent intent = new Intent();
	            		intent.setClass(getActivity(), EmployeeLogin.class);
	            		startActivity(intent);
	            		getActivity().finish();
						dialog.dismiss();
					}
				});

				// create alert dialog
				final AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
				    @Override
				    public void onShow(DialogInterface dialog) {
				        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
				        btnPositive.setHeight(100);
				        btnPositive.setTextSize(20);

				        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
				        btnNegative.setHeight(100);
				        btnNegative.setTextSize(20);
				    }
				});
				// show it
				alertDialog.show();
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}

}
