package com.intlpos.initsetup;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.intlpos.sirclepos.R;

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> fragmentsList;

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public MyFragmentPagerAdapter(FragmentManager fragmentManager, ArrayList<Fragment> fragmentsList2) {
        super(fragmentManager);
        this.fragmentsList = fragmentsList2;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public Fragment getItem(int arg0) {
        return fragmentsList.get(arg0);
    }

    public ArrayList<Fragment> getFragmentsList() {
		return fragmentsList;
	}

	public void setFragmentsList(ArrayList<Fragment> fragmentsList) {
		this.fragmentsList = fragmentsList;
	}

	@Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

}
