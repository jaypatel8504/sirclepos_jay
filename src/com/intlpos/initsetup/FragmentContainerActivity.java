package com.intlpos.initsetup;

import com.intlpos.sirclepos.R;
import com.intlpos.global.AboutDialog;
import com.intlpos.sirclepos.PreviewFragment;
import com.intlpos.sirclepos.PreviewFragment.goBack;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class FragmentContainerActivity extends FragmentActivity implements ReceiptDetails.OnFragmentSwitchListener, goBack{
	private static final int ABOUT = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_fragment_container);
		Bundle bundle = getIntent().getExtras();
		setProgressBarIndeterminateVisibility(false);
		FragmentManager fm = getSupportFragmentManager();
		Fragment receiptFrag = new ReceiptDetails();
		receiptFrag.setArguments(bundle);
		Fragment importInventory = new CsvFileReadFragment();
		fm.beginTransaction().add(R.id.frag_container, receiptFrag).show(receiptFrag).commit();
		
		
	}
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
            if(keyCode == KeyEvent.KEYCODE_BACK){
                    return true;
            }
            return false;
    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0,ABOUT,0,"About");
		return true;
		}

	@Override
		public boolean onOptionsItemSelected (MenuItem item){
		switch (item.getItemId()){
		case ABOUT:
		AboutDialog about = new AboutDialog(this);
		about.setTitle("Do you need Help?");
		about.show();

		break;
		}
		return true;
		}

	@Override
	public void onFragmentSwitch(Fragment fragment) {
		// TODO Auto-generated method stub
		FragmentManager fm = getSupportFragmentManager();
		Fragment mContent = fm.findFragmentById(R.id.frag_container);
		if(mContent != fragment) {
            mContent = fragment;
            fm.beginTransaction()
                .replace(R.id.frag_container, fragment) 
                .addToBackStack(null)
                .commit();
        }
		
	}
	@Override
	public void onPressed(PreviewFragment frag) {
		// TODO Auto-generated method stub
		final android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();	
		ft.replace(R.id.frag_container,frag, "NewFragmentTag");
		ft.commit();
	}
	@Override
	public void goBack() {
		// TODO Auto-generated method stub
		FragmentManager fm = getSupportFragmentManager();
		Fragment mContent = fm.findFragmentById(R.id.frag_container);
		Fragment fragment = new SampleDatabase();
		if(mContent != fragment) {
            mContent = fragment;
            fm.beginTransaction()
                .replace(R.id.frag_container, fragment) 
                .addToBackStack(null)
                .commit();
        }
	}
	
        

}
