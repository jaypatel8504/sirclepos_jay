package com.intlpos.initsetup;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.WifiPopup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class CanadaSalesTax extends ReceiptDetails {
	private String GET_URL = "/salestax/fetchtax.svc/gettaxrates";
	private String ADD_URL = "companymaintenence/taxservice.svc/savetaxdetail";
	private OnFragmentSwitchListener mListener;
	public Map<String,ArrayList<Float>> taxMap;
	public ArrayList<String> provinceList;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	class Province{
		String name;
		float gst,pst,hst;
		public Province(String name,float gst, float pst, float hst){
			this.name = name;
			this.gst = gst;
			this.pst = pst;
			this.hst = hst;
		}
	}
	public CanadaSalesTax() {
		// Required empty public constructor
	}
//	public TextView gst_text,pst_text,hst_text;
	public EditText gst_edit,pst_edit,hst_edit;

	public AsyncTask <String, Void, String> postTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		// Inflate the layout for this fragment
		final View view = inflater.inflate(R.layout.fragment_canada_sales_tax,
				container, false);	
//		gst_text = (TextView) view.findViewById(R.id.gstPercentage);
//		pst_text = (TextView) view.findViewById(R.id.pstPercentage);
//		hst_text = (TextView) view.findViewById(R.id.hstPercentage);
		gst_edit = (EditText) view.findViewById(R.id.gstValue);
		pst_edit = (EditText) view.findViewById(R.id.pstValue);
		hst_edit = (EditText) view.findViewById(R.id.hstValue);
//		GET_URL = GlobalSetup.Url.substring(0, 19) + GET_URL;
		GET_URL = "http://smart360pos.com/salestax/fetchtax.svc/gettaxrates";
		ADD_URL = CornerStorePOS.Url + ADD_URL;
		postTask = new PostRequestTask();
		postTask.execute(GET_URL,"");
		String response = "";
		try {
			response = postTask.get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			gst_edit = (EditText) view.findViewById(R.id.gstValue);
			pst_edit = (EditText) view.findViewById(R.id.pstValue);
			hst_edit = (EditText) view.findViewById(R.id.hstValue);
//			GET_URL = GlobalSetup.Url.substring(0, 19) + GET_URL;
			GET_URL = "https://www.cornerstorereg.info/salestax/fetchtax.svc/gettaxrates";
			ADD_URL = CornerStorePOS.Url + ADD_URL;
			postTask = new PostRequestTask();
			postTask.execute(GET_URL,"");
			try {
				response = postTask.get();
			} catch (InterruptedException | ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
        taxMap = new HashMap<String,ArrayList<Float>>();
   		provinceList = new ArrayList<String>();
   		JSONObject resp_obj;
		try {
			
			resp_obj = new JSONObject(response);
			JSONArray resp_arr = resp_obj.getJSONArray("canadatax_list"); 
	   		 String name;
	   		 float gst,pst,hst;
	   		 ArrayList<Float> taxRate_arr;
	   		 for (int i = 0, len = resp_arr.length(); i < len; i++){
	   			 JSONObject obj = resp_arr.getJSONObject(i);
	   			 name = obj.getString("province_name");
	   			 provinceList.add(name);
	   			 gst = (float) obj.getDouble("gst_value");
	   			 Log.d("GST: ", ""+obj.getDouble("gst_value"));
	   			 pst = (float) obj.getDouble("pst_value");
	   			 Log.d("PST: ", ""+obj.getDouble("pst_value"));
	   			 hst = (float) obj.getDouble("hst_value");
	   			 Log.d("HST: ", ""+obj.getDouble("hst_value"));
	   			 taxRate_arr = new ArrayList<Float>();
	   			 taxRate_arr.add(gst);
	   			 taxRate_arr.add(pst);
	   			 taxRate_arr.add(hst);
	   			 taxMap.put(name, taxRate_arr);
	   		 }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Spinner spinner = (Spinner) view.findViewById(R.id.province_spinner);
		final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,
	            provinceList);
		spinner.setAdapter(adapter1); 
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				//Toast.makeText(getActivity(), pos+"", Toast.LENGTH_SHORT).show();
				ArrayList<Float> tax = taxMap.get(provinceList.get(pos));
				gst_edit.setText(tax.get(0)+"");
				pst_edit.setText(tax.get(1)+"");
				hst_edit.setText(tax.get(2)+"");
		    }
		    public void onNothingSelected(AdapterView<?> parent) {
		    }
		});
//		Button confirm = (Button) view.findViewById(R.id.confirm);
//		confirm.setOnClickListener(new OnClickListener(){
//            public void onClick(View v)
//            {	
//            	if (!gst_edit.getText().toString().isEmpty())
//            		gst_text.setText(form.format(Double.parseDouble(gst_edit.getText().toString()))+"%"); 
//            	if (!pst_edit.getText().toString().isEmpty())
//            		pst_text.setText(form.format(Double.parseDouble(pst_edit.getText().toString()))+"%"); 
//            	if (!hst_edit.getText().toString().isEmpty())
//            		hst_text.setText(form.format(Double.parseDouble(hst_edit.getText().toString()))+"%"); 
//            }
//    });
		Button submit = (Button) view.findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener(){
            public void onClick(View v)
            {
            	connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
    			mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            	if(mWifi.isConnected()){
	            	getActivity().setProgressBarIndeterminateVisibility(true);
	            	JSONObject tax = new JSONObject();
	            	JSONObject storedetail = new JSONObject();
	            	JSONObject savetaxdetail = new JSONObject();
	            	try {
	            		String tax_rate1 = gst_edit.getText().toString() == null? "0":gst_edit.getText().toString();
	            		String tax_rate2 = pst_edit.getText().toString() == null? "0":pst_edit.getText().toString();
	            		String tax_rate3 = hst_edit.getText().toString() == null? "0":hst_edit.getText().toString();
	            		String check[] = getResources().getStringArray(R.array.taxCheck);
	            		if(tax_rate1.length() > 7){
	            			Toast.makeText(getActivity(), check[0], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_rate2.length() > 7){
	            			Toast.makeText(getActivity(), check[1], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		if(tax_rate3.length() > 7){
	            			Toast.makeText(getActivity(), check[2], Toast.LENGTH_LONG).show();
	            			return;
	            		}
	            		
	            		
						tax.put("tax_rate1", tax_rate1);
		            	tax.put("tax_rate2", tax_rate2);
		            	tax.put("tax_rate3", tax_rate3);
		            	tax.put("tax_desc1", "gst");
		            	tax.put("tax_desc2", "pst");
		            	tax.put("tax_desc3", "hst");
		            	storedetail.put("email_id", CornerStorePOS.email_id);
		            	storedetail.put("password", CornerStorePOS.password);
		            	storedetail.put("store_id", CornerStorePOS.StoreId);
		            	storedetail.put("station_id", CornerStorePOS.StationId);
		            	savetaxdetail.put("storedetail", storedetail);
		            	savetaxdetail.put("tax", tax);
		        		postTask = new PostRequestTask();
		        		postTask.execute(ADD_URL,savetaxdetail.toString());
		        		
		        		String response = "";
		        		try {
		        			response = postTask.get();
		        		} catch (InterruptedException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        		} catch (ExecutionException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        			
		        		}
		        		
		        		JSONObject obj = new JSONObject(response);	        		
		        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
		        		
		            	getActivity().setProgressBarIndeterminateVisibility(false);
	//	            	if (obj.get("result").equals(true)){
	//	            		Bundle bundle = new Bundle();
	//	            		bundle.putString("resultString", obj.get("resultString").toString());
	//	            		Intent intent = new Intent();
	//	            		intent.putExtras(bundle);
	//	            		intent.setClass(getActivity(), Main.class);
	//	            		startActivity(intent);
	//	            		getActivity().finish();
	//	            	}
		            	
	
		            	setupDeptPopUP();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						postTask = new PostRequestTask();
		        		postTask.execute(CornerStorePOS.BackupUrl+"companymaintenence/taxservice.svc/savetaxdetail",savetaxdetail.toString());
		        		String repsonse;
		        		try {
		        			repsonse = postTask.get();
		        			JSONObject obj = new JSONObject(repsonse);	        		
			        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
			        		
			            	getActivity().setProgressBarIndeterminateVisibility(false);
			            	setupDeptPopUP();
						} catch (InterruptedException | ExecutionException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							Toast.makeText(getActivity(), "Request Failed!" , Toast.LENGTH_SHORT).show();
						}
						
					}
            	}
            	else{
            		show();
            	}
            }
    });

		return view;
	}	
	
	private void setupDeptPopUP() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

//			// set title
//			alertDialogBuilder.setTitle("Setting up your System");

			// set dialog message
			alertDialogBuilder
				.setTitle(R.string.wouldin)
				.setCancelable(false)
				.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, closes
		        		AddInventory addInventory = new AddInventory();
		        		onButtonPressed(addInventory);
						dialog.dismiss();
					}
				  })
				.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
	            		Intent intent = new Intent();
	            		intent.setClass(getActivity(), EmployeeLogin.class);
	            		startActivity(intent);
	            		getActivity().finish();
						dialog.dismiss();
					}
				});

				// create alert dialog
				final AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
				    @Override
				    public void onShow(DialogInterface dialog) {
				        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
				        btnPositive.setHeight(100);
				        btnPositive.setTextSize(20);

				        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
				        btnNegative.setHeight(100);
				        btnNegative.setTextSize(20);
				    }
				});
				// show it
				alertDialog.show();
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}
	
	

}
                                                                                 