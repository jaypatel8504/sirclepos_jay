package com.intlpos.initsetup;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.EmployeeLogin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class AddDepartment extends ReceiptDetails {
	private OnFragmentSwitchListener mListener;
	protected String ADD_URL = "DepartmentMaintenence/DepartmentService.svc/savedepartmentlist";
	TextView tv = null;
	ListView lv = null;
	Button btn_selectAll = null;
	Button btn_inverseSelect = null;
	Button btn_calcel = null;
	Button btn_preview = null;
	Button btn_done = null;
	String[] name = null;
	FragmentManager fm;
	FragmentTransaction ft;
	ArrayList<String> listStr = null;
	private AsyncTask <String, Void, String> postTask;
	private List<HashMap<String, Object>> list = null;
	private CheckableListViewAdapter adapter;

	public AddDepartment() {
		// Required empty public constructor
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ADD_URL = CornerStorePOS.Url+ADD_URL;
        listStr = new ArrayList<String>();
        Fragment newFrag = MyDialogFragment.newInstance(listStr);
        fm = getActivity().getSupportFragmentManager();
		ft = fm.beginTransaction();
		ft.add(R.id.frame_frag_container, newFrag).show(newFrag).commit();
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_adddepartment, container, false);
		name = getResources().getStringArray(R.array.department_array);
		tv = (TextView) view.findViewById(R.id.tv);
		lv = (ListView) view.findViewById(R.id.lv);
		btn_selectAll = (Button) view.findViewById(R.id.selectall);
		btn_inverseSelect = (Button) view.findViewById(R.id.inverseselect);
		btn_calcel = (Button) view.findViewById(R.id.cancel);
		btn_preview = (Button) view.findViewById(R.id.preview);
		btn_done = (Button) view.findViewById(R.id.submit_departments);
		showCheckBoxListView();
		
		//
		btn_selectAll.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				listStr = new ArrayList<String>();
				for(int i=0;i<list.size();i++){
					CheckableListViewAdapter.isSelected.put(i,true);
					listStr.add(name[i]);
				}
				adapter.notifyDataSetChanged();
				tv.setText("You selected"+listStr.size()+"item(s)");
			}
		});
		
		//
		btn_inverseSelect.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				for(int i=0;i<list.size();i++){
					if(CheckableListViewAdapter.isSelected.get(i)==false){
						CheckableListViewAdapter.isSelected.put(i, true);
						listStr.add(name[i]);
					}
					else{
						CheckableListViewAdapter.isSelected.put(i, false);
						listStr.remove(name[i]);
					}
				}
				adapter.notifyDataSetChanged();
				tv.setText("You selected"+listStr.size()+"item(s)");
			}
			
		});
		
		//
		btn_calcel.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				for(int i=0;i<list.size();i++){
					if(CheckableListViewAdapter.isSelected.get(i)==true){
						CheckableListViewAdapter.isSelected.put(i, false);
						listStr.remove(name[i]);
					}
				}
				adapter.notifyDataSetChanged();
				tv.setText("You selected"+listStr.size()+"item(s)");
			}
			
		});
		
		
		btn_preview.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				JSONWriteTool jsonFile = null;
				try {
					jsonFile = new JSONWriteTool("department.txt");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				for(int i = 0, len = listStr.size(); i < len; i++){
					jsonFile.WriteToJSONFile("Index", i+"");
					jsonFile.WriteToJSONFile("Name", listStr.get(i));
				}				
				Fragment oldFrag = fm.findFragmentById(R.id.frame_frag_container);
				Fragment newFrag = MyDialogFragment.newInstance(listStr);
				if(oldFrag != newFrag) {
					oldFrag = newFrag;
		            fm.beginTransaction()
		                .replace(R.id.frame_frag_container, newFrag) 
		                .addToBackStack(null)
		                .commit();
		        }
			}
			
		});
		btn_done.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				listStr = new ArrayList<String>();
				for(int i=0;i<list.size();i++){
					CheckableListViewAdapter.isSelected.put(i,true);
					listStr.add(name[i]);
				}
				if (listStr.size()>4){
				JSONArray departments = new JSONArray(listStr.subList(4, listStr.size()));// remove the first 4 items.
            	JSONObject storedetail = new JSONObject();
            	JSONObject savedepartmentlist = new JSONObject();
            	try {
            		
	            	storedetail.put("email_id", CornerStorePOS.email_id);
	            	storedetail.put("password", CornerStorePOS.password);
	            	storedetail.put("store_id", CornerStorePOS.StoreId);
	            	storedetail.put("station_id", CornerStorePOS.StationId);
	            	savedepartmentlist.put("storedetail", storedetail);
	            	savedepartmentlist.put("departments", departments);
	        		postTask = new PostRequestTask();
	        		postTask.execute(ADD_URL,savedepartmentlist.toString());
	        		Log.d("123", savedepartmentlist.toString());
	        		String response = "";
	        		try {
	        			response = postTask.get();
	        		} catch (InterruptedException e) {
	        			// TODO Auto-generated catch block
	        			e.printStackTrace();
	        		} catch (ExecutionException e) {
	        			// TODO Auto-generated catch block
	        			e.printStackTrace();
	        		}
	        		//Toast.makeText(getActivity(), response , Toast.LENGTH_LONG).show();
	        		JSONObject obj = new JSONObject(response);	    
//	        		if (!obj.getBoolean("result")){
//	        			Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();    
//	        		}
	        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show(); 
					setupInventoryPopUP();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(getActivity(), "Request Failed!" , Toast.LENGTH_SHORT).show();
				}
				}else{
					setupInventoryPopUP();
				}
			}
		});
		return view;
	}
	
	private void setupInventoryPopUP() {
		// TODO Auto-generated method stub
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

//			// set title
//			alertDialogBuilder.setTitle("Setting up your System");

			// set dialog message
			alertDialogBuilder
				.setTitle("Would you like to setup Inventories now?")
				.setCancelable(false)
				.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, closes
		        		AddInventory addInventory = new AddInventory();
		        		onButtonPressed(addInventory);
						dialog.dismiss();
					}
				  })
				.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
	            		Intent intent = new Intent();
	            		intent.setClass(getActivity(), EmployeeLogin.class);
	            		startActivity(intent);
	            		getActivity().finish();
						dialog.dismiss();
					}
				});

				// create alert dialog
				final AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
				    @Override
				    public void onShow(DialogInterface dialog) {
				        Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
				        btnPositive.setHeight(100);
				        btnPositive.setTextSize(20);

				        Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
				        btnNegative.setHeight(100);
				        btnNegative.setTextSize(20);
				    }
				});
				// show it
				alertDialog.show();
	}
	
	public void showCheckBoxListView() {
		list = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < name.length; i++) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("item_tv", name[i]);
			map.put("item_cb", false);
			list.add(map);

			adapter = new CheckableListViewAdapter(this.getActivity(), list, R.layout.listviewitem,
					new String[] { "item_tv", "item_cb" }, new int[] {
							R.id.item_tv, R.id.item_cb });
			lv.setAdapter(adapter);
			listStr = new ArrayList<String>();
			lv.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View view,
						int position, long arg3) {
					ViewHolder holder = (ViewHolder) view.getTag();
					holder.cb.toggle();
					CheckableListViewAdapter.isSelected.put(position, holder.cb.isChecked()); 
					if (holder.cb.isChecked() == true) {
						listStr.add(name[position]);
					} else {
						listStr.remove(name[position]);
					}
					tv.setText("You selected"+listStr.size()+"item(s)");
				}

			});
		}
	}

}
