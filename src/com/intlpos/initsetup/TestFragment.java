package com.intlpos.initsetup;

import java.util.ArrayList;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.MyViewGroup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.shapes.Shape;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

public class TestFragment extends Fragment {
	private int btnWidth = 110;
	private int btnHeight = 48;
    private static final String TAG = "TestFragment";
    private String[] sTitle = null;// = "hello android";
    
    static Fragment newInstance(String[] s) {
        TestFragment newFragment = new TestFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("sTitle", s);
        newFragment.setArguments(bundle);
        newFragment.setRetainInstance(true);
        return newFragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "TestFragment-----onCreate");
        Bundle args = getArguments();
        sTitle = args.getStringArray("sTitle");
    }


	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        Log.d(TAG, "TestFragment-----onCreateView");
        MyViewGroup view = (MyViewGroup) inflater.inflate(R.layout.pager_item, container, false);
        Log.d(TAG, "ViewGroup = "+ view.toString());
//        MyViewGroup view = new MyViewGroup(inflater.inflate(R.layout.pager_item, container, false).getContext());
//        view.setId(0);
//        view.setWillNotDraw(false);
        int len = sTitle == null ? 0 : sTitle.length;
        Log.d(TAG, "len of string = " + len);
        Button[] button = new Button[len];
//        Button[] button = new Button[len];
        for(int i = 0 ; i<len;i++){
        	button[i] = new Button(this.getActivity());
//        	button[i] = new Button(this.getActivity());
        	MyViewGroup.LayoutParams param = new MyViewGroup.LayoutParams(btnWidth,LayoutParams.WRAP_CONTENT);
        	button[i].setMinimumHeight(btnHeight);
        	button[i].setMinimumWidth(btnWidth);
        	button[i].setBackgroundResource(R.drawable.bg_deptbutton_states);
        	button[i].setText(sTitle[i]);
        	button[i].setTextColor(Color.BLACK);
        	button[i].setHeight(btnHeight);
        	button[i].setWidth(btnWidth);
        	button[i].setLayoutParams(param);
//        	button[i].setGravity(80);
        	//set id and Tag for Listener
//        	button[i].setId(i);
        	button[i].setTag(i);
//        	view.addOnLayoutChangeListener(new OnLayoutChangeListener(){
//        	@Override
//        	        public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight,
//        	                int oldBottom) {
//        	    		Log.d(TAG, "Layout Changed!");
//        	        }
//        	    });
        	view.addView(button[i]);
            	button[i].setOnClickListener(new Button.OnClickListener() {
            	    @Override
            	        public void onClick(View v) {
            	    	Button btn = (Button) v;
            	    	Toast.makeText(getActivity(), btn.getText().toString() , Toast.LENGTH_SHORT).show();
            	       }
            	     });
        	
        }
        return view;
    }
	

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "TestFragment-----onDestroy");
    }

}
