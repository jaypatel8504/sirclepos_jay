package com.intlpos.initsetup;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.PreviewFragment;
import com.intlpos.sirclepos.WifiPopup;

public class SampleDatabase extends ReceiptDetails{
	private String[] name;
	private ListView lv;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_addinventory, container, false);
		
		name = new String[7];
		name[0] = getResources().getString(R.string.store1);
		name[1] = getResources().getString(R.string.store2);
		name[2] = getResources().getString(R.string.store3);
		name[3] = getResources().getString(R.string.store4);
		name[4] = getResources().getString(R.string.LiquorStore);
		name[5] = getResources().getString(R.string.TobaccoStore);
		name[6] = getResources().getString(R.string.goback);
		lv = (ListView) view.findViewById(R.id.listView1);
		ArrayAdapter<String> itemsAdapter = 
			    new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, name);
		lv.setAdapter(itemsAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
				mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				switch(position){
				case 0:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceNJDatabase.csv",true));}
					else{
						show();
					}
					break;
				case 1:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/ConvenienceStoreDatabase.csv",true));
					}
					else
						show();
					break;
				case 2:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LIConvenientDatabase.csv",true));
					}
					else{
						show();
					}
					break;
				case 3:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/MAConvenienceDatabase.csv",true));
					}
					else{
						show();
					}
					break;
				case 4:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/LiquorStoreDatabase.csv",true));
					}
					else{
						show();
					}
					break;
				case 5:
					if(mWifi.isConnected()){
						onListPressed(new PreviewFragment("http://www.smart360pos.com/sircleposbackoffice/SampleDatabases/tobaccostore.csv",true));
					}
					else{
						show();
					}
					break;
				case 6:
					onButtonPressed(new AddInventory());
					break;
				}
			}

		});
		return view;
	}
	
	
	@Override
	public void onResume() {
	    super.onResume();
	    connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}
}
