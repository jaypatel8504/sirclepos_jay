package com.intlpos.initsetup;

import com.intlpos.sirclepos.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * 
 */
public class TabFragment extends Fragment {
	private TabHost tabHost;
	private static int currentlayout = 0;
	private View view;
	public TabFragment() {
		// Required empty public constructor
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.fragment_tab, container, false);
//		setFragment();
//		changeLayout();
		return view;
	}
	
//	private void changeLayout() {
//        //根据用户的点击位置的下标显示相应的fragment
//        tabHost.setCurrentTab(currentlayout);
//    }
//
//    private void setFragment() {
//        //通过组件的id初始化tabHost的实例
//        tabHost = (TabHost)view.findViewById(R.id.tabhost);
//        tabHost.setup();
//        //tabWidget = tabHost.getTabWidget();
//        //往tabHost分别添加fragment
//        tabHost.addTab(tabHost.newTabSpec("Canada").setIndicator("Canada").setContent(R.id.frag_canada));
//        tabHost.addTab(tabHost.newTabSpec("United State").setIndicator("United State").setContent(R.id.frag_us));
//        //设置默认显示布局
//        tabHost.setCurrentTab(0);
//    }

}
