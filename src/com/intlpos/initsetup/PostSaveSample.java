package com.intlpos.initsetup;

import com.intlpos.sirclepos.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler.Callback;

public class PostSaveSample extends AsyncTask<String, Void, String> {
	
	public TaskFinished saving = null;
	private Context context;
	private ProgressDialog progressDialog;;
	
	
	public PostSaveSample(TaskFinished saving,Context context){
		this.saving = saving;
		this.context = context;
	}
	
    

	@Override
    protected void onPreExecute() {
        //disableUI();
    	 
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Saving");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setProgress(0);
			progressDialog.show();
    }

    @Override
    protected String doInBackground(String... formFieldValues) {
//		String request = xml;
		HTTPOST httpost = new HTTPOST();    		
//		httpost.getResponseByXML(URL, request);
//		httpost.getResponseByFile(URL);
		String URL = formFieldValues[0];
		String request = formFieldValues[1];
        return httpost.getResponseByJSON(URL, request);
    }

    @Override
    protected void onPostExecute(String result) {
		saving.onTaskEnded(result);
    	progressDialog.dismiss();
		 
    }
    
    
}
