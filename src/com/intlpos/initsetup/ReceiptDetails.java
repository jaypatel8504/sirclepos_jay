package com.intlpos.initsetup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.PreviewFragment;
import com.intlpos.sirclepos.WifiPopup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link ReceiptDetails.OnFragmentInteractionListener} interface to handle
 * interaction events.
 * 
 */
public class ReceiptDetails extends Fragment {
	private String ADD_URL = "CompanyMaintenence/ReceiptService.svc/savereceiptdetail";
	private AsyncTask <String, Void, String> postTask;
	private Fragment receiptFrag = this;
	private EditText storeId,storeName,address1,address2,contactNo,emailId,storeImage,
	  				 footerMsg0,footerMsg1,footerMsg2,footerMsg3,footerMsg4,footerMsg5,footerMsg6,footerMsg7,footerMsg8,footerMsg9;
	private ArrayList<EditText> editText_list;
	private String usOrcan;				 
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	private OnFragmentSwitchListener mListener;
	private String[] text_arr;
	public ReceiptDetails() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_receipt_details, container,
				false);
		
		
		String info[] =  getArguments().getStringArray("info"); 
//		for(int i = 0; i < info.length; i++){
//			if(info[i].contains("\\")){
//				info[i]=info[i].replaceAll("\\", "");
//			}
//		}
		usOrcan = getArguments().getString("usORcan");
		ADD_URL = CornerStorePOS.Url+ADD_URL;
		//Toast.makeText(getActivity(), ADD_URL, Toast.LENGTH_SHORT).show();
		storeId = (EditText) view.findViewById(R.id.storeid);
		storeId.setHint("Ex: 1234");
		storeId.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		
		storeName = (EditText) view.findViewById(R.id.storename);
		storeName.setText(info[3], TextView.BufferType.EDITABLE);
		
		address1 = (EditText) view.findViewById(R.id.addressline1);
		address1.setText(info[7], TextView.BufferType.EDITABLE);
		
		address2 = (EditText) view.findViewById(R.id.addressline2);
		address2.setText(info[8], TextView.BufferType.EDITABLE);
		
		contactNo = (EditText) view.findViewById(R.id.contactnumber);
		contactNo.setText(info[12], TextView.BufferType.EDITABLE);
		
		emailId = (EditText) view.findViewById(R.id.emailid);
		emailId.setText(info[4], TextView.BufferType.EDITABLE);
		
		storeImage = (EditText) view.findViewById(R.id.storeimage);
		TextView storeImageText = (TextView) view.findViewById(R.id.storeimage_text);
		storeImageText.setVisibility(View.GONE);
		storeImage.setVisibility(View.GONE);
		footerMsg0 = (EditText) view.findViewById(R.id.footermsg0);
		footerMsg1 = (EditText) view.findViewById(R.id.footermsg1);
		footerMsg2 = (EditText) view.findViewById(R.id.footermsg2);
		footerMsg3 = (EditText) view.findViewById(R.id.footermsg3);
		footerMsg4 = (EditText) view.findViewById(R.id.footermsg4);
		footerMsg5 = (EditText) view.findViewById(R.id.footermsg5);
		footerMsg6 = (EditText) view.findViewById(R.id.footermsg6);
		footerMsg7 = (EditText) view.findViewById(R.id.footermsg7);
		footerMsg8 = (EditText) view.findViewById(R.id.footermsg8);
		footerMsg9 = (EditText) view.findViewById(R.id.footermsg9);
		
		editText_list = new ArrayList<EditText>();
		
		editText_list.add(storeId);
		editText_list.add(storeName);
		editText_list.add(address1);
		editText_list.add(address2);
		editText_list.add(contactNo);
		editText_list.add(emailId);
		editText_list.add(storeImage);
		editText_list.add(footerMsg0);
		editText_list.add(footerMsg1);
		editText_list.add(footerMsg2);
		editText_list.add(footerMsg3);
		editText_list.add(footerMsg4);
		editText_list.add(footerMsg5);
		editText_list.add(footerMsg6);
		editText_list.add(footerMsg7);
		editText_list.add(footerMsg8);
		editText_list.add(footerMsg9);
		
		
		
		Button submit = (Button) view.findViewById(R.id.submitreceipt);
		submit.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
				connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
				mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				// TODO Auto-generated method stub
				if(!mWifi.isConnected()){
					show();
				}
				else{
					JSONObject receipt = new JSONObject();
	            	JSONObject storedetail = new JSONObject();
	            	JSONObject savereceiptdetail = new JSONObject();
	            	try {
	            		int len = editText_list.size();
	            		text_arr = new String[len];
	            		for (int i = 0; i<len; i++){
	            			text_arr[i] = editText_list.get(i).getText().toString();
	            		}
	            		if(!text_arr[0].isEmpty()){
	            			try { 
	            		        Integer.parseInt(text_arr[0]); 
	            		    } catch(NumberFormatException e) { 
	            		    	Toast.makeText(getActivity(), R.string.storeNumberMust, Toast.LENGTH_SHORT).show();
	            		        return; 
	            		    } 
	            		}
	            		
	        			if(text_arr[0] == null || text_arr[0].isEmpty()){
	        				Toast.makeText(getActivity(), R.string.storeNumCant, Toast.LENGTH_SHORT).show();
	        				return;
	        			}        			
	        			if(text_arr[1] == null || text_arr[1].isEmpty()){
	        				Toast.makeText(getActivity(), R.string.storeNameCant, Toast.LENGTH_SHORT).show();
	        				return;
	        			}
	        			if(checkNewStore() == false){
	        				return;
	        			}
	            		JSONArray footer = new JSONArray(Arrays.asList(Arrays.copyOfRange(text_arr, 7, text_arr.length)));
	            		if(text_arr[1].length() > 32)
	            		{
	            			text_arr[1] = text_arr[1].substring(0, Math.min(text_arr[1].length(), 32));
	            			Log.d("Truncate", text_arr[1]);
	            		}
	            		
	            		receipt.put("receiptdetail_id", 1);
	            		receipt.put("store_no", text_arr[0]);
	            		receipt.put("store_name", text_arr[1]);
	            		receipt.put("address1", text_arr[2]);
	            		receipt.put("address2", text_arr[3]);
	            		receipt.put("contact_no", text_arr[4]);
	            		receipt.put("email_id", text_arr[5]);
	            		receipt.put("picture", text_arr[6]);
	            		receipt.put("footer", footer);
	            		
		            	storedetail.put("email_id", CornerStorePOS.email_id);
		            	storedetail.put("password", CornerStorePOS.password);
		            	storedetail.put("store_id", CornerStorePOS.StoreId);
		            	storedetail.put("station_id", CornerStorePOS.StationId);
		            	savereceiptdetail.put("storedetail", storedetail);
		            	savereceiptdetail.put("receiptdetail", receipt);
		        		postTask = new PostRequestTask();
		        		postTask.execute(ADD_URL,savereceiptdetail.toString());
		        		Log.d("123", savereceiptdetail.toString());
		        		String response = "";
		        		try {
		        			response = postTask.get();
		        		} catch (InterruptedException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        		} catch (ExecutionException e) {
		        			// TODO Auto-generated catch block
		        			e.printStackTrace();
		        		}
		        		//Toast.makeText(getActivity(), response , Toast.LENGTH_LONG).show();
		        		JSONObject obj = new JSONObject(response);
		        		Log.d("PIC", obj.toString());
		        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
		        		
		            	getActivity().setProgressBarIndeterminateVisibility(false);
		            	
		            	if(!usOrcan.equals("Canada")){
		            		Fragment us = new USSalesTax();
							onButtonPressed(us);
		            	}
		            	else{
		            		Fragment canada = new CanadaSalesTax();
							onButtonPressed(canada);
		            	}
		            	
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						try {
		            		int len = editText_list.size();
		            		text_arr = new String[len];
		            		for (int i = 0; i<len; i++){
		            			text_arr[i] = editText_list.get(i).getText().toString();
		            		}
		            		if(!text_arr[0].isEmpty()){
		            			try { 
		            		        Integer.parseInt(text_arr[0]); 
		            		    } catch(NumberFormatException e1) { 
		            		    	Toast.makeText(getActivity(), R.string.storeNumberMust, Toast.LENGTH_SHORT).show();
		            		        return; 
		            		    } 
		            		}
		            		
		        			if(text_arr[0] == null || text_arr[0].isEmpty()){
		        				Toast.makeText(getActivity(), R.string.storeNumCant, Toast.LENGTH_SHORT).show();
		        				return;
		        			}        			
		        			if(text_arr[1] == null || text_arr[1].isEmpty()){
		        				Toast.makeText(getActivity(), R.string.storeNameCant, Toast.LENGTH_SHORT).show();
		        				return;
		        			}
		        			if(checkNewStore() == false){
		        				return;
		        			}
		            		JSONArray footer = new JSONArray(Arrays.asList(Arrays.copyOfRange(text_arr, 7, text_arr.length)));
		            		if(text_arr[1].length() > 32)
		            		{
		            			text_arr[1] = text_arr[1].substring(0, Math.min(text_arr[1].length(), 32));
		            			Log.d("Truncate", text_arr[1]);
		            		}
		            		
		            		receipt.put("receiptdetail_id", 1);
		            		receipt.put("store_no", text_arr[0]);
		            		receipt.put("store_name", text_arr[1]);
		            		receipt.put("address1", text_arr[2]);
		            		receipt.put("address2", text_arr[3]);
		            		receipt.put("contact_no", text_arr[4]);
		            		receipt.put("email_id", text_arr[5]);
		            		receipt.put("picture", text_arr[6]);
		            		receipt.put("footer", footer);
		            		
			            	storedetail.put("email_id", CornerStorePOS.email_id);
			            	storedetail.put("password", CornerStorePOS.password);
			            	storedetail.put("store_id", CornerStorePOS.StoreId);
			            	storedetail.put("station_id", CornerStorePOS.StationId);
			            	savereceiptdetail.put("storedetail", storedetail);
			            	savereceiptdetail.put("receiptdetail", receipt);
			        		postTask = new PostRequestTask();
			        		ADD_URL = CornerStorePOS.BackupUrl + "CompanyMaintenence/ReceiptService.svc/savereceiptdetail";
			        		postTask.execute(ADD_URL,savereceiptdetail.toString());
			        		Log.d("123", savereceiptdetail.toString());
			        		String response = "";
			        		try {
			        			response = postTask.get();
			        		} catch (InterruptedException e1) {
			        			// TODO Auto-generated catch block
			        			e1.printStackTrace();
			        		} catch (ExecutionException e1) {
			        			// TODO Auto-generated catch block
			        			e1.printStackTrace();
			        		}
			        		//Toast.makeText(getActivity(), response , Toast.LENGTH_LONG).show();
			        		JSONObject obj = new JSONObject(response);
			        		Log.d("PIC", obj.toString());
			        		Toast.makeText(getActivity(), obj.get("resultString").toString() , Toast.LENGTH_SHORT).show();
			        		
			            	getActivity().setProgressBarIndeterminateVisibility(false);
			            	
			            	if(usOrcan.equals("United States")){
			            		Fragment us = new USSalesTax();
								onButtonPressed(us);
			            	}
			            	else if(usOrcan.equals("Canada")){
			            		Fragment canada = new CanadaSalesTax();
								onButtonPressed(canada);
			            	}
			            	
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							Toast.makeText(getActivity(), "Request Failed!" , Toast.LENGTH_SHORT).show();
						}
					}
				}
			}
			
		});
		return view;
	}
	
	private boolean checkNewStore(){
		String getCharacterCheck[] = getResources().getStringArray(R.array.characterCheck);
		if(text_arr[0].length() > 4){
			Toast.makeText(getActivity().getApplicationContext(), R.string.storeNum, Toast.LENGTH_LONG).show();
			return false;
		}
		if(text_arr[1].length() > 32){
			Toast.makeText(getActivity().getApplicationContext(), getCharacterCheck[0], Toast.LENGTH_LONG).show();
			return false;
		}
		if(text_arr[2].length() > 32){
			Toast.makeText(getActivity().getApplicationContext(), getCharacterCheck[4], Toast.LENGTH_LONG).show();
			return false;
		}
		if(text_arr[3].length() > 32){
			Toast.makeText(getActivity().getApplicationContext(), getCharacterCheck[5], Toast.LENGTH_LONG).show();
			return false;
		}
		if(text_arr[4].length() > 32 ){
			Toast.makeText(getActivity().getApplicationContext(), getCharacterCheck[9], Toast.LENGTH_LONG).show();
			return false;
		}
		if(text_arr[5].length() > 32){
			Toast.makeText(getActivity().getApplicationContext(), getCharacterCheck[1], Toast.LENGTH_LONG).show();
			return false;
		}
		
		String special_chars[] = {"\\t", "\\b", "\\n", "\\r", "\\f","+", "-", "&&", "||",  "(", ")", "{", "}", "[", "]", "^",
                "~", "*", "?", "\\", "\""};
		for(int i = 0; i <text_arr.length; i++){
			for(int q = 0; q < special_chars.length; q++){
				if(text_arr[i].contains(special_chars[q])){
					Toast.makeText(getActivity().getApplicationContext(), "NO " + special_chars[q] + " in " + text_arr[i], Toast.LENGTH_SHORT).show();
					return false;
				}
			}
		}
		
		int count = 1;
		for(int i = 7; i < 17; i++){
			
			if(text_arr[i].length() > 32){
				String message =getActivity().getApplicationContext().getString(R.string.footerNum) + " " + count + " " + getActivity().getApplicationContext().getString(R.string.footer32);
				Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
				return false;
			}
			
			count++;
		}
		
		for(int i = 0; i <text_arr.length; i++){
			if(text_arr[i].contains("\'")){
				String temp = text_arr[i].replaceAll("'", "\\\\'");
				text_arr[i] = temp;}
		}
		
		return true;
	}
	
	// May not needed.
	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Fragment frag) {
		if (mListener != null) {
			mListener.onFragmentSwitch(frag);
		}
	}
	
	public void onListPressed(PreviewFragment frag){
		if (mListener != null) {
			mListener.onPressed(frag);
		}
	}
	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentSwitchListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentSwitchListener {
		// TODO: Update argument type and name
		public void onFragmentSwitch(Fragment frag);
		public void onPressed(PreviewFragment frag);
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}
}
