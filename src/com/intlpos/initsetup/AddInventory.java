package com.intlpos.initsetup;

import com.intlpos.sirclepos.R;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.WifiPopup;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class AddInventory extends ReceiptDetails {
	
	
	private String[] name;
	private ListView lv;
	private ConnectivityManager connManager;
	private NetworkInfo mWifi;
	
	public AddInventory() {
		// Required empty public constructor
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_addinventory, container, false);
		
		name = getResources().getStringArray(R.array.inventory_input);
		lv = (ListView) view.findViewById(R.id.listView1);
		ArrayAdapter<String> itemsAdapter = 
			    new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_list_item_1, name);
		lv.setAdapter(itemsAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long arg3) {
				connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
				mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				switch(position){
				case 0:
					if(mWifi.isConnected()){
						AddStandardInventory frag = new AddStandardInventory(false);
						onButtonPressed(frag);}
					else{
						show();
					}
					break;
				case 1:
					if(mWifi.isConnected()){
						AddStandardInventory frag2 = new AddStandardInventory(true);
						onButtonPressed(frag2);
					}
					else
						show();
					break;
				case 2:
					if(mWifi.isConnected()){
						SampleDatabase frag3 = new SampleDatabase();
						onButtonPressed(frag3);
					}
					else{
						show();
					}
					break;
				case 3:
            		Intent intent = new Intent();
            		intent.setClass(getActivity(), EmployeeLogin.class);
            		startActivity(intent);
            		getActivity().finish();
            		break;
				}
			}

		});
		return view;
	}
	
	
	@Override
	public void onResume() {
	    super.onResume();
	    connManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
		mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	}
	
	private void show(){
		DialogFragment newFragment = WifiPopup.newInstance();
		newFragment.show(getActivity().getFragmentManager(), "hello");
	
	}
}
