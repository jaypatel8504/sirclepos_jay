package com.intlpos.initsetup;

import android.os.AsyncTask;
import android.util.Log;

import com.intlpos.sirclepos.R;

public class PostRequestTask extends AsyncTask<String, Void, String> {
    @Override
    protected void onPreExecute() {
        //disableUI();
    }

    @Override
    protected String doInBackground(String... formFieldValues) {
//		String request = xml;
		HTTPOST httpost = new HTTPOST();    		
//		httpost.getResponseByXML(URL, request);
//		httpost.getResponseByFile(URL);
		String URL = formFieldValues[0];
		String request = formFieldValues[1];
        return httpost.getResponseByJSON(URL, request);
    }

    @Override
    protected void onPostExecute(String result) {

		 
    }
}