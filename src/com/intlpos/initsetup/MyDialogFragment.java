package com.intlpos.initsetup;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.intlpos.sirclepos.R;

import android.R.color;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MyDialogFragment extends Fragment {
	private DisplayMetrics dm;
//	private Context context = this;
	private final static int VIEW_MARGIN=2;
	private final static int BUTTON_MARGIN=10;
	private static final String TAG = "123";
	public final static int btnWidth = 110;
	public final static int btnHeight = 48;
	// var for viewPager
	private ArrayList<Fragment> fragmentsList;
	private ArrayList<ArrayList<String>> btn_list;
	private int viewTag;
	private MyFragmentPagerAdapter adapter= null;
	private RadioGroup dotGroupButton = null;
	private ArrayList<ArrayList<Button>> btns_list;
	private ViewPager viewPager;
	private List<View> viewList;
	private View view;
	private String[] button_names;
	//private OnViewUpdateListener mCallback;

	int screenWidth;
	int screenHeight;
	
    static MyDialogFragment newInstance(ArrayList<String> listStr) {
    	MyDialogFragment newFragment = new MyDialogFragment();
        Bundle bundle = new Bundle();
        int len = listStr == null? 0:listStr.size();
        String[] names = new String[len];
        for (int i=0; i<len; i++){
        	names[i] = listStr.get(i);
        	Log.d(TAG, "name = "+names[i]);
        }
        bundle.putStringArray("btn_name", names);
        newFragment.setArguments(bundle);
        return newFragment;
    }
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null && args.containsKey("btn_name"))
        	button_names = args.getStringArray("btn_name");
        else 
        	button_names = null;
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.initsetup_dialogfragment, container, false);

        screenWidth = 680;
        screenHeight = 280;
        
//        ViewTreeObserver vto = view.getViewTreeObserver();
//		vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
//
//		    @SuppressWarnings("deprecation")
//			@SuppressLint("NewApi")
//			@Override
//		    public void onGlobalLayout() {
//		        view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//		        screenWidth = view.getWidth();
//		        screenHeight = view.getHeight();
//		    }
//
//		});
		
//		screenWidth = view.getWidth();
//		screenHeight = view.getHeight();

			// Wrote initView as a function!
			initFragViewParams();
        	initFragView(view);
      
		
//			viewPager.setOffscreenPageLimit(3);
       return view;
	}
	
	public void initFragViewParams(){
		//getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//		dm = new DisplayMetrics();
//		this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		int buttonsPerPage = (screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)) *
				(screenHeight/(btnHeight+VIEW_MARGIN));
		
		Log.d("Buttons", "Col = "+screenWidth/(btnWidth+BUTTON_MARGIN+VIEW_MARGIN)+" Row = "+ screenHeight/(btnHeight+VIEW_MARGIN));
		
//		int buttonsPerLine = screenWidth/(buttonWidth+VIEW_MARGIN +BUTTON_MARGIN);
//		int linesPerPage = screenHeight/(buttonWidth+VIEW_MARGIN);
		viewList = new ArrayList<View>();

        ArrayList<String> sTitle = new ArrayList<String>(Arrays.asList(button_names));
        sTitle.add(0,"Tax 3");
        sTitle.add(0,"Tax 2");
        sTitle.add(0,"Tax 1");
        sTitle.add(0,"Non-Taxable Item");
		int total_buttons = sTitle.size();
		int pages = (int) Math.ceil((double) total_buttons/buttonsPerPage);
	
	     // create buttons dynamically
		Log.d(TAG, "Btn#= "+ total_buttons + " Btn/Page= "+buttonsPerPage+ " Page#= "+pages);
		
		btn_list = new ArrayList<ArrayList<String>>();
        
		int start = 0;
		int end = buttonsPerPage;
		while(end<total_buttons){
			ArrayList<String> btn_params = new ArrayList<String>(sTitle.subList(start, end));
			btn_list.add(btn_params);
			start += buttonsPerPage;
			end += buttonsPerPage;
		}
		
        //add the last several strings
        if (total_buttons % buttonsPerPage != 0){
        	ArrayList<String> btn_params = new ArrayList<String>(sTitle.subList(start, total_buttons));
        	btn_list.add(btn_params);
        }
        Log.d(TAG, "btn_list size = "+btn_list.size());
	}
	
	public void initFragView(View v){
		viewPager = (ViewPager) v.findViewById(R.id.testViewPager);//Layout of viewPager
		Log.d(TAG, "ID of ViewPager = "+ viewPager.getId());
		dotGroupButton = (RadioGroup) v.findViewById(R.id.dotGroupButton);
		fragmentsList = new ArrayList<Fragment>();
		for (int i = 0; i < btn_list.size();i++){
			// Create a MyViewGroup
			ArrayList<String> list = btn_list.get(i);
			int size = list.size();			
			String[] str_arr = new String[size];
			for (int j = 0 ; j < size; j++){
				str_arr[j] = list.get(j);
			}
//			Log.d(TAG, "Len of String arr = "+ array.length);
			Fragment mFragment = TestFragment.newInstance(str_arr);
			//set button for each page
			final RadioButton dotButton = new RadioButton(v.getContext());
			dotButton.setId(i);
			dotButton.setLayoutParams(new RadioGroup.LayoutParams(
					RadioGroup.LayoutParams.WRAP_CONTENT,
					RadioGroup.LayoutParams.WRAP_CONTENT));
			dotButton.setPadding(20, 20, 20, 20);
//			dotButton.setBackgroundResource(R.drawable.dot_bg);//background of each page
			dotButton.setButtonDrawable(R.drawable.dot_bg);
			
			dotButton.setTag(i);//set tag for each page's listener
			
			dotButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked){
						viewPager.setCurrentItem((Integer)dotButton.getTag(),true);
					}
				}
			});
			Log.d(TAG, "dotGroupButton = "+ dotGroupButton);
			dotGroupButton.addView(dotButton);
			dotGroupButton.check(0);
			fragmentsList.add(mFragment);
		}
		
//	    adapter = new MyFragmentPagerAdapter(fm,fragmentsList);
//		viewPager.setAdapter(adapter);
//		adapter.notifyDataSetChanged();
		// set Page Change Listener (notice data changed to the adapter!)
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {				

			@Override
			public void onPageSelected(int position) {
				((RadioButton)dotGroupButton.getChildAt(position)).setChecked(true);
//				viewPager.findViewWithTag(position);
				//adapter.notifyDataSetChanged();// load new page while page changing
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	

	    @Override
	    public void onViewCreated(View view, Bundle savedInstanceState) {
	        super.onViewCreated(view, savedInstanceState);

	        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.testViewPager);
	        adapter = new MyFragmentPagerAdapter(getChildFragmentManager(),fragmentsList);
	        mViewPager.setAdapter(adapter);
	    }
	    
	    public int convertToPx(int input) {
            // Get the screen's density scale
            final float scale = getResources().getDisplayMetrics().density;
            // Convert the dps to pixels, based on density scale
            return (int) (input * scale + 0.5f);
    }
	@Override
    public void onResume()
	    {
	        super.onResume();
			int screenWidth = convertToPx(500);
			int screenHeight = convertToPx(500);
//	        Window window = getDialog().getWindow();
//	        window.setLayout(screenWidth, screenHeight);
//	        window.setGravity(Gravity.CENTER);
	        //TODO:
	    } 
	
//	public interface OnViewUpdateListener {   
//        public void onFragmentListChanged(int num);   
//    }   
//	
//	@Override   
//    public void onAttach(Activity activity) {   
//        super.onAttach(activity);   
//           
//        // This makes sure that the container activity has implemented   
//        // the callback interface. If not, it throws an exception   
//        try {   
//            mCallback = (OnViewUpdateListener) activity;   
//        } catch (ClassCastException e) {   
//            throw new ClassCastException(activity.toString()   
//                    + " must implement OnViewUpdateListener");   
//        }   
//    } 
}


