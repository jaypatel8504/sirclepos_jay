package com.intlpos.initsetup;

import java.util.LinkedHashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.intlpos.WCFAccess.JSONParser;
import com.intlpos.WCFAccess.JSONParserNew;
import com.intlpos.WCFAccess.MyListener;
import com.intlpos.sirclepos.R;
import com.intlpos.mysharedpreferences.CSSharedPreferences;
import com.intlpos.mysharedpreferences.StoreDetail;
import com.intlpos.sirclepos.CornerStorePOS;
import com.intlpos.sirclepos.DemoOrRegister;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.RegisterTablet;

public class StartScreen extends Activity {
	private static int SPLASH_TIME_OUT = 1000;
	private CornerStorePOS app;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.startscreen);
		app = (CornerStorePOS) getApplication();
		app.ReadVariables();

		new Handler().postDelayed(new Runnable() {
			
			@Override
            public void run() {
                
				if (CornerStorePOS.NOTFirstTime == true) {
					
					boolean hasUpdate = false;
					try {
						int version_code = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
						String version_name = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
						if(app.version_code != version_code && app.version_name != version_name)
						{
							//call webservice to update the version and we can do other activities like showing what's new popup etc...
							hasUpdate = true;
							storeversion_preferences(version_code,version_name);
							storeversion_webservice(version_code,version_name);
							
						}
						
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}		
					
					Intent i = new Intent(StartScreen.this, EmployeeLogin.class);
					i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
					i.putExtra("has_update", hasUpdate);
					startActivity(i);
				}
				else{
					Intent i = new Intent(StartScreen.this, PartnerTech.class);
	                startActivity(i);
				}
                
                finish();
            }
        }, SPLASH_TIME_OUT);
	}
	
	
	private void updateDB(final int version_code,final String version_name){
		final String methodName = "DatabaseMaintenence/UpgradeDatabaSeservice.svc/upgradedb";
    	final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
		
//    	StoreDetail storedetail = new StoreDetail();
    	LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
    	storedetail.put("email_id", CornerStorePOS.email_id);
    	storedetail.put("password", CornerStorePOS.password);
    	storedetail.put("store_id",CornerStorePOS.StoreId);
    	storedetail.put("station_id", CornerStorePOS.StationId);
    	params.put("storedetail", storedetail);
    	Log.d("VC", String.valueOf(version_code));
    	Log.d("VN", version_name);
    	params.put("versioncode", version_code);
    	params.put("versionname", version_name);
        
    	JSONParserNew jParser = new JSONParserNew(new MyListener() {

    		@Override
    		public void onResult(JSONObject json) {

    			try {
    			if(json.getBoolean("result")== true)	 
    			{								
    				Toast.makeText(StartScreen.this, R.string.dataSucc, Toast.LENGTH_SHORT).show();
    				storeversion_webservice(version_code,version_name);
    			}
    			else
    			{
    				Toast.makeText(StartScreen.this, json.getString("resultString"), Toast.LENGTH_SHORT).show();
    			}
    			}
    			catch(Exception ex)
    			{
    		        
    		    	JSONParserNew jParser = new JSONParserNew(new MyListener() {

    		    		@Override
    		    		public void onResult(JSONObject json) {

    		    			try {
    		    			if(json.getBoolean("result")== true)	 
    		    			{								
    		    				Toast.makeText(StartScreen.this, R.string.dataSucc, Toast.LENGTH_SHORT).show();
    		    				storeversion_webservice(version_code,version_name);
    		    			}
    		    			else
    		    			{
    		    				Toast.makeText(StartScreen.this, json.getString("resultString"), Toast.LENGTH_SHORT).show();
    		    			}
    		    			}
    		    			catch(Exception ex)
    		    			{
    		    				
    		    			}
    		    		}
    		    	});
    		    	
    		    	jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
    			}
    		}
    	});
    	
    	jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
	private void storeversion_preferences(int version_code,String version_name)
	{
		StoreDetail storedetail = new StoreDetail();
		storedetail.setVersion_code(version_code);
		storedetail.setVersion_name(version_name);
		CSSharedPreferences.saveUpdatePreferences(storedetail);
	}
	
	private void storeversion_webservice(final int version_code,final String version_name)
	{
		
		final String methodName = "VersionMaintenence/VersionService.svc/updateversion";

		final LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
			
		
		LinkedHashMap<String, Object> storedetail = new LinkedHashMap<String, Object>();
		storedetail.put("email_id", CornerStorePOS.email_id);
		storedetail.put("password", CornerStorePOS.password);
		storedetail.put("store_id",CornerStorePOS.StoreId);
		storedetail.put("station_id", CornerStorePOS.StationId);
		
		params.put("storedetail", storedetail);
		params.put("versioncode", version_code);
		params.put("versionname", version_name);
		
		JSONParser jParser = new JSONParser(new MyListener() {

			@Override
			public void onResult(JSONObject json) {

				try {
					Boolean result = json.getBoolean("result");
					String resultString = json.getString("resultString");
					if (result == true) 
					{							
						Toast.makeText(StartScreen.this, getString(R.string.welcometoversion)+" "+version_name+version_code, Toast.LENGTH_LONG).show();
					}
					else
					{
						Toast.makeText(StartScreen.this, resultString, Toast.LENGTH_LONG).show();	
					}
				}
				catch(Exception ex)
				{
					JSONParser jParser = new JSONParser(new MyListener() {

						@Override
						public void onResult(JSONObject json) {

							try {
								Boolean result = json.getBoolean("result");
								String resultString = json.getString("resultString");
								if (result == true) 
								{							
									Toast.makeText(StartScreen.this, getString(R.string.welcometoversion)+" "+version_name+version_code, Toast.LENGTH_LONG).show();
								}
								else
								{
									Toast.makeText(StartScreen.this, resultString, Toast.LENGTH_LONG).show();	
								}
							}
							catch(Exception ex)
							{
								Log.d("CSPOS",""+ex);
							}
						}
					});
					
					jParser.execute(CornerStorePOS.BackupUrl, methodName, params);
				}
			}
		});
		
		jParser.execute(CornerStorePOS.Url, methodName, params);
	}
	
}
