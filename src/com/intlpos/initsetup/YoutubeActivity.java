package com.intlpos.initsetup;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.intlpos.sirclepos.R;
import com.intlpos.global.DeveloperKey;
import com.intlpos.sirclepos.EmployeeLogin;
import com.intlpos.sirclepos.FullscreenActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class YoutubeActivity extends Activity implements OnInitializedListener {
	Button finish;
	Context context = this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(!isTablet(this)) // UNCOMMENT LATER!
			setContentView(R.layout.activity_youtube_phone);
		else{
			setContentView(R.layout.activity_youtube);
	        
	        finish = (Button) findViewById(R.id.finish);
	        finish.setOnClickListener(new OnClickListener(){ 

				@Override
				public void onClick(View v) {
					Intent i = new Intent(context, FullscreenActivity.class);
					i.setFlags(i.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
					startActivity(i);					
					finish();
				}
	        	
	        });
		}
		
	    YouTubePlayerFragment youTubePlayerFragment =
	            (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
	        youTubePlayerFragment.initialize(DeveloperKey.DEVELOPER_KEY, this);

	}
	
	  @Override
	  public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
	      boolean wasRestored) {
	    if (!wasRestored) {
	      player.cueVideo("CmUr4UD6pew");
	    }
	  }

	  
	  /**
	    * Detect if it is tablet
	    * @param context
	    * @return
	    */
	    public static boolean isTablet(Context context) {
	        return (context.getResources().getConfiguration().screenLayout
	                & Configuration.SCREENLAYOUT_SIZE_MASK)
	                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	    }
	    
	    
	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult arg1) {
		// TODO Auto-generated method stub
		
	}
}
