package com.intlpos.initsetup;

public interface TaskFinished{
	public abstract void onTaskEnded(String result);
}
